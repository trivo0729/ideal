﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="flightList.aspx.cs" Inherits="CUTUK.flightList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Page Title -->
    <title>Travelo - Travel, Tour Booking HTML5 Template</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">

            <div class="main-header">

                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="#" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>


                </div>
            </div>

            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="#">MY ACCOUNT</a></li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                    </ul>
                </div>
            </div>

            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">
                                    Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox">
                                Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Flight Search Results</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">HOME</a></li>
                    <li class="active">Flight Search Results</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <div class="col-md-s12" id="Filter_type">
                                <ul class="nav nav-tabs tabs-left" id="tabs" style="width: 100%; text-align: center;">
                                    <li role="presentation" class="active"><a href="#TabOut" data-toggle="tab" style="cursor: pointer">OutBound Airline</a></li>
                                    <li role="presentation" class=""><a href="#TabIn" data-toggle="tab" style="cursor: pointer">Inbound Airline</a></li>
                                </ul>
                            </div>
                            <div class="tab-content" style="width: 100%; height: auto; padding-left: 10px; padding-right: 10px; background: transparent; padding: 0">
                                <div class="tab-pane active" id="TabOut">
                                    <!-- TOP TIP -->
                                    <div class="filtertip2">
                                        <div class="padding20">
                                            <p class="size13"><span class="size13 bold">6 </span>Flights found</p>
                                            <span class="size14">from</span>
                                            <p class="size13 mt10"><b>Mumbai</b> to <b>Delhi</b> </p>
                                        </div>
                                        <div class="tip-arrow2"></div>
                                    </div>
                                    <!-- END OF BOOK FILTERS -->
                                    <div class="padding20title" style="padding: 0px 20px 0px 20px;">
                                        <h3 class="opensans dark">Filter by</h3>
                                    </div>
                                    <div class="line2"></div>
                                    <!--Departure -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#collapse4_Out">Departure time <span class="collapsearrow"></span></button>
                                    <div id="collapse4_Out" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_Out" value="04-11" onchange="GenrateFilters('Out')" checked="">Morning(04:00 - 11:00) ( 3)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_Out" value="11-16" onchange="GenrateFilters('Out')" checked="">Afternoon(11:00 - 16:00) ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_Out" value="16-21" onchange="GenrateFilters('Out')" checked="">Evening(16:00 - 21:00) ( 3)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_Out" value="21 - 04" onchange="GenrateFilters('Out')" checked="">Night(21:00 - 04:00) ( 0)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of Departure -->
                                    <div class="line2"></div>
                                    <!--LayOvers -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#LayOversOut">LayOvers <span class="collapsearrow"></span></button>
                                    <div id="LayOversOut" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Layover_Out" value="Sharjah" onchange="GenrateFilters('Out')" checked="">Sharjah</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of LayOvers -->
                                    <div class="line2"></div>
                                    <!--Refund Type -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#LayOversOut">Refund Type <span class="collapsearrow"></span></button>
                                    <div id="LayOversOut" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="FareType_Out" value="non-Refundable" checked="" onchange="GenrateFilters('Out')">Refundable ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="FareType_Out" value="Refundable" checked="" onchange="GenrateFilters('Out')">non-Refundable ( 6)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of LayOvers -->
                                    <div class="line2"></div>
                                    <!--  Airlines -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#collapse3_Out">Airlines <span class="collapsearrow"></span></button>
                                    <div id="collapse3_Out" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Airlines_Out" value="AirArabia" onchange="GenrateFilters('Out')" checked="">AirArabia ( 6)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of Airlines -->
                                    <div class="line2"></div>
                                    <div class="clearfix"></div>
                                    <!-- Price range -->
                                    <button type="button" class="collapsebtn" data-toggle="collapse" data-target="#collapse2_Out">Price range <span class="collapsearrow"></span></button>
                                    <div id="collapse2_Out" class="collapse in">
                                        <div class="padding20">
                                            <div class="layout-slider wh100percent">
                                                <span class="cstyle09">
                                                    <input id="Slider1_Out" type="slider" name="price" value="20;5000" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Price range -->
                                    <div class="line2"></div>
                                    <!-- Price range -->
                                    <button type="button" class="collapsebtn" data-toggle="collapse" data-target="#collapseStop_Out">Stops<span class="collapsearrow"></span></button><div id="collapseStop_Out" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_Out" value="Direct" onchange="GenrateFilters('Out')" checked="">Direct ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_Out" value="1" onchange="GenrateFilters('Out')" checked="">One Stops ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_Out" value="2" onchange="GenrateFilters('Out')" checked="">Two Stops &amp; More ( 0)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Price range -->
                                    <div class="line2"></div>
                                </div>
                                <div class="tab-pane" id="TabIn">
                                    <!-- TOP TIP -->
                                    <div class="filtertip2">
                                        <div class="padding20">
                                            <p class="size13"><span class="size13 bold">6 </span>Flights found</p>
                                            <span class="size14">from</span>
                                            <p class="size13 mt10"><b>Delhi</b> to <b>Mumbai</b> </p>
                                        </div>
                                        <div class="tip-arrow2"></div>
                                    </div>
                                    <!-- END OF BOOK FILTERS -->
                                    <div class="padding20title" style="padding: 0px 20px 0px 20px;">
                                        <h3 class="opensans dark">Filter by</h3>
                                    </div>
                                    <div class="line2"></div>
                                    <!--Departure -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#collapse4_In">Departure time <span class="collapsearrow"></span></button>
                                    <div id="collapse4_In" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_In" value="04-11" onchange="GenrateFilters('In')" checked="">Morning(04:00 - 11:00) ( 3)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_In" value="11-16" onchange="GenrateFilters('In')" checked="">Afternoon(11:00 - 16:00) ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_In" value="16-21" onchange="GenrateFilters('In')" checked="">Evening(16:00 - 21:00) ( 3)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="DepartureTime_In" value="21 - 04" onchange="GenrateFilters('In')" checked="">Night(21:00 - 04:00) ( 0)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of Departure -->
                                    <div class="line2"></div>
                                    <!--LayOvers -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#LayOversIn">LayOvers <span class="collapsearrow"></span></button>
                                    <div id="LayOversIn" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Layover_In" value="Sharjah" onchange="GenrateFilters('In')" checked="">Sharjah</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of LayOvers -->
                                    <div class="line2"></div>
                                    <!--Refund Type -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#LayOversIn">Refund Type <span class="collapsearrow"></span></button>
                                    <div id="LayOversIn" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="FareType_In" value="non-Refundable" checked="" onchange="GenrateFilters('In')">Refundable ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="FareType_In" value="Refundable" checked="" onchange="GenrateFilters('In')">non-Refundable ( 6)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of LayOvers -->
                                    <div class="line2"></div>
                                    <!--  Airlines -->
                                    <button type="button" class="collapsebtn last" data-toggle="collapse" data-target="#collapse3_In">Airlines <span class="collapsearrow"></span></button>
                                    <div id="collapse3_In" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Airlines_In" value="AirArabia" onchange="GenrateFilters('In')" checked="">AirArabia ( 6)</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- End of Airlines -->
                                    <div class="line2"></div>
                                    <div class="clearfix"></div>
                                    <!-- Price range -->
                                    <button type="button" class="collapsebtn" data-toggle="collapse" data-target="#collapse2_In">Price range <span class="collapsearrow"></span></button>
                                    <div id="collapse2_In" class="collapse in">
                                        <div class="padding20">
                                            <div class="layout-slider wh100percent">
                                                <span class="cstyle09">
                                                    <input id="Slider1_In" type="slider" name="price" value="20;5000"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Price range -->
                                    <div class="line2"></div>
                                    <!-- Price range -->
                                    <button type="button" class="collapsebtn" data-toggle="collapse" data-target="#collapseStop_In">Stops<span class="collapsearrow"></span></button><div id="collapseStop_In" class="collapse in">
                                        <div class="hpadding20">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_In" value="Direct" onchange="GenrateFilters('In')" checked="">Direct ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_In" value="1" onchange="GenrateFilters('In')" checked="">One Stops ( 0)</label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="Stops_In" value="2" onchange="GenrateFilters('In')" checked="">Two Stops &amp; More ( 0)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Price range -->
                                    <div class="line2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9" id="div_Airline">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="sort-by-section clearfix box" style="padding: 1px 8px;">
                                        <ul class="sort-bar clearfix block-sm">
                                            <li class="sort-by-Time" style="padding: 15px 2px;"><a class="sort-by-container" href="#"><span>Depart </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Arrival </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Time</span></a></li>
                                            <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price </span></a></li>
                                        </ul>
                                    </div>
                                    <div class="flight-list listing-style3 flight">
                                        <article class="box">
                                            <figure class="col-xs-3 col-sm-2">
                                                <span>
                                                    <img src="../images/AirlineLogo/SG.gif" alt=""></span>
                                            </figure>
                                            <div class="details col-xs-9 col-sm-10">
                                                <div class="details-wrapper">
                                                    <div class="first-row">
                                                        <div>
                                                            <h5 class="box-title" style="float: none">Mumbai(BOM),India  <i class="soap-icon-longarrow-right"></i>Delhi(DEL),India</h5>
                                                        </div>
                                                        <div><span class="price" style="font-size: 16px;"><i class="fa fa-inr"></i>2,902.72</span></div>
                                                    </div>
                                                    <div class="second-row">
                                                        <div class="time">
                                                            <div class="take-off col-sm-3">
                                                                <div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                <div style="padding: 0px">7:20 AM</div>
                                                            </div>
                                                            <div class="landing col-sm-4">
                                                                <div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                <div style="padding: 0px">9:35 AM</div>
                                                            </div>
                                                            <div class="total-time col-sm-3">
                                                                <div class="icon" style="float: none"><i class="soap-icon-clock yellow-color"></i></div>
                                                                &nbsp;
                                                                <div style="padding: 0px">135 m</div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div style="padding: 0px">
                                                                    <small style="color: #ff3300">15 left</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="action">
                                                            <span class="skin-color">Fare Roules</span><br />
                                                            <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-xs-10 col-sm-10">
                                                        <div>Remark :<span class="skin-color"> this is a test from aditya.</span></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-10 col-sm-10">
                                                        <div>Baggage  :<span class="skin-color"> 10 Kg</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>


                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="sort-by-section clearfix box" style="padding: 1px 8px;">
                                        <ul class="sort-bar clearfix block-sm">
                                            <li class="sort-by-Time" style="padding: 15px 2px;"><a class="sort-by-container" href="#"><span>Depart </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Arrival </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Time</span></a></li>
                                            <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price </span></a></li>
                                        </ul>
                                    </div>
                                    <div class="flight-list listing-style3 flight">
                                        <article class="box">
                                            <figure class="col-xs-3 col-sm-2">
                                                <span>
                                                    <img src="../images/AirlineLogo/SG.gif" alt=""></span>
                                            </figure>
                                            <div class="details col-xs-9 col-sm-10">
                                                <div class="details-wrapper">
                                                    <div class="first-row">
                                                        <div>
                                                            <h5 class="box-title" style="float: none">Mumbai(BOM),India  <i class="soap-icon-longarrow-right"></i>Delhi(DEL),India</h5>
                                                        </div>
                                                        <div><span class="price" style="font-size: 16px;"><i class="fa fa-inr"></i>2,902.72</span></div>
                                                    </div>
                                                    <div class="second-row">
                                                        <div class="time">
                                                            <div class="take-off col-sm-3">
                                                                <div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                <div style="padding: 0px">7:20 AM</div>
                                                            </div>
                                                            <div class="landing col-sm-4">
                                                                <div class="icon" style="float: none"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                <div style="padding: 0px">9:35 AM</div>
                                                            </div>
                                                            <div class="total-time col-sm-3">
                                                                <div class="icon" style="float: none"><i class="soap-icon-clock yellow-color"></i></div>
                                                                &nbsp;
                                                                <div style="padding: 0px">135 m</div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div style="padding: 0px">
                                                                    <small style="color: #ff3300">15 left</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="action">
                                                            <span class="skin-color">Fare Roules</span><br />
                                                            <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-xs-10 col-sm-10">
                                                        <div>Remark :<span class="skin-color"> this is a test from aditya.</span></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-10 col-sm-10">
                                                        <div>Baggage  :<span class="skin-color"> 10 Kg</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="sort-by-section clearfix box">
                                        <ul class="sort-bar clearfix block-sm">
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Depart </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Arrival </span></a></li>
                                            <li class="sort-by-Time"><a class="sort-by-container" href="#"><span>Time</span></a></li>
                                            <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price </span></a></li>
                                        </ul>
                                    </div>
                                    <div class="flight-list listing-style3 flight">
                                        <div class="travelo-box">
                                            <article class="box" style="padding: 0px; margin-bottom: 0px">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span>
                                                        <img src="../images/AirlineLogo/6E.gif" alt=""></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h5 class="box-title">Mumbai(BOM),India<i class="soap-icon-longarrow-right"></i>Lucknow<b>(LKO)</b>,India</h5>
                                                            </div>
                                                            <div><span class="price" style="font-size: 16px;"><i class="fa fa-inr"></i>2,634.81</span></div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>11:50 AM</div>
                                                                </div>
                                                                <div class="landing col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>2:15 PM</div>
                                                                </div>
                                                                <div class="total-time col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>145 minutes</div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div>
                                                                        <span class="skin-color">Fare Roules</span><br>
                                                                        <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                                        <br>
                                                                        <small><b style="color: #ff3300">1Left</b></small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action"><a href="#" class="button btn-small sky-blue1" onclick="BookTicket('OB1')">Book</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                            </article>
                                            <div class="row">
                                                <div class="col-xs-9">
                                                    <div>Remark :<span class="skin-color"> Remarks for spl cpn.</span></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-9">
                                                    <div>Baggage  :<span class="skin-color"> 15 KG , 6E</span></div>
                                                </div>
                                            </div>

                                            <br />
                                            <article class="box" style="padding: 0px; margin-bottom: 0px">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span>
                                                        <img src="../images/AirlineLogo/6E.gif" alt=""></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div style="border-right: 0px">
                                                                <h5 class="box-title">Mumbai(BOM),India<i class="soap-icon-longarrow-right"></i>Lucknow<b>(LKO)</b>,India</h5>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time" style="border-right: 0px">
                                                                <div class="take-off col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>11:50 AM</div>
                                                                </div>
                                                                <div class="landing col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>2:15 PM</div>
                                                                </div>
                                                                <div class="total-time col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>145 minutes</div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div>
                                                                        <span class="skin-color">Fare Roules</span><br>
                                                                        <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                                        <br>
                                                                        <small><b style="color: #ff3300">1Left</b></small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                            </article>
                                            <div class="row">
                                                <div class="col-xs-9">
                                                    <div>Remark :<span class="skin-color"> Remarks for spl cpn.</span></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-9">
                                                    <div>Baggage  :<span class="skin-color"> 15 KG , 6E</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="travelo-box">
                                            <article class="box">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span>
                                                        <img src="../images/AirlineLogo/6E.gif" alt=""></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div>
                                                                <h5 class="box-title">Mumbai(BOM),India<i class="soap-icon-longarrow-right"></i>Lucknow<b>(LKO)</b>,India</h5>
                                                            </div>
                                                            <div><span class="price" style="font-size: 16px;"><i class="fa fa-inr"></i>2,634.81</span></div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time">
                                                                <div class="take-off col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>11:50 AM</div>
                                                                </div>
                                                                <div class="landing col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>2:15 PM</div>
                                                                </div>
                                                                <div class="total-time col-sm-3">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>145 minutes</div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div>
                                                                        <span class="skin-color">Fare Roules</span><br>
                                                                        <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                                        <br>
                                                                        <small><b style="color: #ff3300">1Left</b></small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action"><a href="#" class="button btn-small sky-blue1" onclick="BookTicket('OB1')">Book</a></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <div>Remark :<span class="skin-color"> Remarks for spl cpn.</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-9">
                                                            <div>Baggage  :<span class="skin-color"> 15 KG , 6E</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                            </article>
                                        </div>

                                        <div class="travelo-box">
                                            <article class="box" style="padding: 0px; margin-bottom: 0px">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span>
                                                        <img src="../images/AirlineLogo/6E.gif" alt=""></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div style="border-right: 0px">
                                                                <h5 class="box-title">Mumbai(BOM),India<i class="soap-icon-longarrow-right"></i>Lucknow<b>(LKO)</b>,India</h5>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time" style="border-right: 0px">
                                                                <div class="take-off col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>11:50 AM</div>
                                                                </div>
                                                                <div class="landing col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>2:15 PM</div>
                                                                </div>
                                                                <div class="total-time col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>145 min</div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <span class="skin-color">Fare Roules</span><br>
                                                                        <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <small><b style="color: #ff3300">1Left</b></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <a href="#" class="button btn-small sky-blue1" onclick="BookTicket('OB1')">Get Fare</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <br />
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div>Select Class :</div>
                                                    <div>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                            <article class="box" style="padding: 0px; margin-bottom: 0px">
                                                <figure class="col-xs-3 col-sm-2">
                                                    <span>
                                                        <img src="../images/AirlineLogo/6E.gif" alt=""></span>
                                                </figure>
                                                <div class="details col-xs-9 col-sm-10">
                                                    <div class="details-wrapper">
                                                        <div class="first-row">
                                                            <div style="border-right: 0px">
                                                                <h5 class="box-title">Mumbai(BOM),India<i class="soap-icon-longarrow-right"></i>Lucknow<b>(LKO)</b>,India</h5>
                                                            </div>
                                                        </div>
                                                        <div class="second-row">
                                                            <div class="time" style="border-right: 0px">
                                                                <div class="take-off col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>11:50 AM</div>
                                                                </div>
                                                                <div class="landing col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                                                    <div>2:15 PM</div>
                                                                </div>
                                                                <div class="total-time col-sm-2">
                                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                                    <div>145 min</div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <span class="skin-color">Fare Roules</span><br>
                                                                        <b><small style="color: #7db921"><b>Refundable </b></small></b>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <small><b style="color: #ff3300">1Left</b></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div style="padding-left: 0px">
                                                                        <a href="#" class="button btn-small sky-blue1" onclick="BookTicket('OB1')">Get Fare</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <br />
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div>Select Class :</div>
                                                    <div>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                        <a class="button btn-mini silver">F9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer" class="style5">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Address</h2>
                            <p style="text-align: justify">
                                Hi5Fly Diamond Hills Colony
                                Tolichowki<br>
                                Hyderabad 500008.
                            </p>

                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Contact Us</h2>
                            <ul class="travel-news">
                                <li>

                                    <address class="contact-details">
                                        <span class="contact-phone"><i class="soap-icon-phone"></i>+91 799 799 1000</span><br>
                                        <span class="contact-phone"><i class="soap-icon-phone"></i>+91 99593 00769</span>
                                        <br>
                                        <br>
                                        <h2><span class="contact-message"><i class="soap-icon-message"></i><a href="mailto:azam.faroqui@gmail.com">azam.faroqui@gmail.com</a></span></h2>

                                    </address>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>

                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email">
                            </div>
                            <br>
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Discover</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="Default.aspx">HOME</a></li>
                                <li class="col-xs-6"><a href="#">ABOUT US</a></li>
                                <li class="col-xs-6"><a href="#">PACKAGES</a></li>
                                <li class="col-xs-6"><a href="Contact.aspx">CONTACT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script type="text/javascript" src="js/gmap3.min.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">
        tjq(document).ready(function () {
            tjq("#price-range").slider({
                range: true,
                min: 0,
                max: 1000,
                values: [100, 800],
                slide: function (event, ui) {
                    tjq(".min-price-label").html("$" + ui.values[0]);
                    tjq(".max-price-label").html("$" + ui.values[1]);
                }
            });
            tjq(".min-price-label").html("$" + tjq("#price-range").slider("values", 0));
            tjq(".max-price-label").html("$" + tjq("#price-range").slider("values", 1));

            function convertTimeToHHMM(t) {
                var minutes = t % 60;
                var hour = (t - minutes) / 60;
                var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);
                var date = new Date("2014/01/01 " + timeStr + ":00");
                var hhmm = date.toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' });
                return hhmm;
            }
            tjq("#flight-times").slider({
                range: true,
                min: 0,
                max: 1440,
                step: 5,
                values: [360, 1200],
                slide: function (event, ui) {

                    tjq(".start-time-label").html(convertTimeToHHMM(ui.values[0]));
                    tjq(".end-time-label").html(convertTimeToHHMM(ui.values[1]));
                }
            });
            tjq(".start-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 0)));
            tjq(".end-time-label").html(convertTimeToHHMM(tjq("#flight-times").slider("values", 1)));
        });
    </script>
</body>
</html>
