﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="FlightSearch.aspx.cs" Inherits="CUTUK.FlightSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>
    <script src="scripts/login.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script src="scripts/flightAuto.js"></script>
    <script src="scripts/flightSearch.js?v=1.1"></script>
    <script src="js/modal.js"></script>
    <script src="js/moment.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="tour">
        <div id="slideshow" class="slideshow-bg full-screen">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background1.jpg);"></div>
                    </li>
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background2.jpg);"></div>
                    </li>
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background3.jpg);"></div>
                    </li>
                </ul>
            </div>
            <div class="container">
                <div class="table-wrapper full-width">
                    <div class="table-cell">
                        <div class="search-box">
                            <form>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h5 class="a-white" style="margin-top: 0px">Book Domestic &amp; International Flight Tickets</h5>
                                    </div>
                                    <div>
                                        <div class="col-sm-1">
                                            <input type="radio" name="JourneyType" id="radio01" value="O" title="One Way" onclick="OneWayRadioFunc()" checked="" />
                                            <label for="radio01">
                                                <span></span>
                                                <span class="a-white">One Way</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="radio" name="JourneyType" id="radio02" value="R" title="Round Way" onclick="RoundWayRadioFunc()" />
                                            <label for="radio02">
                                                <span></span>
                                                <span class="a-white">Round Way</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="radio" name="JourneyType" id="radio03" value="M" title="Multiple Destinations" onclick="MultiWayRadioFunc()" />
                                            <label for="radio03">
                                                <span></span>
                                                <span class="a-white">Multiple Destinations</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2" style="float: left">
                                            <input type="checkbox" name="NonStop" id="chkNonStop" value="NonStop" title="Non Stop Flights" />
                                            <label for="chkNonStop">
                                                <span></span>
                                                <span class="a-white">Non Stop Flights</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="display: none">
                                    <div class="col-sm-4">
                                    </div>
                                    <div>
                                        <div class="col-sm-2" style="display: none">
                                            <input type="radio" name="RoundWay" id="rdb_Normal" value="O" style="display: none" class="RoundWay" title="Normal Return" checked="" style="display: inline-block;" />
                                            <label for="rdb_Normal" class="RoundWay" style="display: none">
                                                <span></span>
                                                <span style="color: black">Normal Return</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2" style="display: none">
                                            <input type="radio" name="RoundWay" id="rdb_SP" value="R" style="display: none" class="RoundWay" title="Special Return" style="display: inline-block;" />
                                            <label for="rdb_SP" class="RoundWay" style="display: none">
                                                <span></span>
                                                <span style="color: black">LCC Special Returns</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2" style="display: none">
                                            <input type="radio" name="RoundWay" id="rdb_GDS" value="M" style="display: none" class="RoundWay" title="Special GDS Return" style="display: inline-block;" />
                                            <label for="rdb_GDS" class="RoundWay" style="display: none">
                                                <span></span>
                                                <span style="color: black">GDS Special Returns</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="GeneralSearch">
                                    <div class="row">
                                        <div class="col-md-8" id="fromToOnwardDateDiv">
                                            <div class="form-group row"></div>
                                        </div>
                                        <div class="col-md-4" style="display: none" id="returnDateDivId">
                                            <div class="form-group row">
                                                <div class="col-md-8">
                                                    <span class="text-left a-white">Return Date</span>
                                                    <div class="datepicker-wrap">
                                                        <input id="datepicker_Return" type="text" name="date_from" class="form-control" placeholder="return On" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div id="addDestinationDivId" class="row white" style="float: right; padding-right: 350px; margin-top: -60px; display: none; margin-bottom: 20px;">
                                            <i class="soap-icon-plus a-white" onclick="addDestination()" title="Add Destination" style="font-size: 40px; cursor: pointer"></i>
                                        </div>
                                        <div id="removeLastDestnDivId" class="row white" style="float: right; padding-right: 350px; margin-top: -60px; display: none; margin-bottom: 20px;">
                                            <i class="soap-icon-minus a-white" onclick="removeLastDestn()" title="Remove Destination" style="font-size: 40px; cursor: pointer"></i>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="display: none">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <span class="text-left a-white">Airlines</span>
                                                    <select id="sel_Airlines" class="form-control" multiple="multiple">
                                                        <option value="SG">Spice Jet</option>
                                                        <option value="6E">Indigo</option>
                                                        <option value="G8">Go Air</option>
                                                        <option value="G9">Air Arabia</option>
                                                        <option value="FZ">Fly Dubai</option>
                                                        <option value="IX">Air India Express</option>
                                                        <option value="AK">Air Asia</option>
                                                        <option value="LB">Air Costa</option>
                                                        <option value="UK">Air Vistara</option>
                                                        <option value="UK">Air Vistara</option>
                                                        <option value="AI">Air India</option>
                                                        <option value="9W">Jet Airways</option>
                                                        <option value="S2">JetLite</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <span class="text-left a-white">Class</span>
                                                    <select class="form-control" id="sel_Class">
                                                        <option selected="selected" value="1">All</option>
                                                        <option value="2">Economy</option>
                                                        <option value="3">Premium Economy</option>
                                                        <option value="4">Business</option>
                                                        <option value="5">Premium Business</option>
                                                        <option value="6">First</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <span class="text-left a-white">Adults</span>
                                                    <select class="form-control" id="sel_Adults">
                                                        <option selected="selected" value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                        <option value="">05</option>
                                                        <option value="1">06</option>
                                                        <option value="2">07</option>
                                                        <option value="3">08</option>
                                                        <option value="4">09</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="text-left a-white">Kids</span>
                                                    <select class="form-control" id="sel_Child">
                                                        <option selected="selected" value="0">00</option>
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                        <option value="">05</option>
                                                        <option value="1">06</option>
                                                        <option value="2">07</option>
                                                        <option value="3">08</option>
                                                        <option value="4">09</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="text-left a-white">Infants</span>
                                                    <select class="form-control" id="sel_Infant">
                                                        <option selected="selected" value="0">00</option>
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                        <option value="">05</option>
                                                        <option value="1">06</option>
                                                        <option value="2">07</option>
                                                        <option value="3">08</option>
                                                        <option value="4">09</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-xs-3">
                                                </div>
                                                <div class="col-xs-6 pull-right">
                                                    <input type="button" class="button btn-medium sky-blue1" onclick="SearchFlight()" value="Search" />
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</asp:Content>
