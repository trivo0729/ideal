﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitResponse.aspx.cs" Inherits="CUTUK.WaitResponse" %>

<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="">
<!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Wait For Response</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,200,300,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body class="soap-login-page style3 body-blank">
    <div id="page-wrapper" class="wrapper-blank">
        <header id="header" class="navbar-static-top">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle blue-bg">Mobile Menu Toggle</a>
            <div class="container">
                <h1 class="logo"></h1>
            </div>
        </header>
        <section id="content">
            <div class="container">
                <div id="main">
                    <h4 class="logo block">
                        <a href="Default.aspx" title="">
                            <img src="images/Logo.png" alt="logo" />
                        </a>
                    </h4>
                    <br />
                    <div class="welcome-text box" style="font-size: 1.7em;">WE'RE ON IT! SEARCHING THE BEST PRICES.</div>
                    <img src="images/loading.gif" style="height: 50px" />
                    <br />
                    <a href="HotelSearch.aspx" style="margin-left: 40%" title="">Back</a>
                    <form runat="server">
                        <div class="col-sm-8 col-md-12 col-lg-6 no-float no-padding center-block">
                            <div id="tour-details" class="travelo-box">
                                <div class="intro small-box table-wrapper full-width hidden-table-sms">

                                    <div class="col-sm-12 table-cell">
                                        <div class="detailed-features">
                                            <div class="flights table-wrapper">
                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>City :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Depart :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblDepart" runat="server"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Return :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblReturn" runat="server"></asp:Label>

                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Rooms :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblRoom" runat="server"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Adults :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblAdult" runat="server"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                                <div class="table-row">
                                                    <div class="table-cell">
                                                        <dl style="margin-right: 15px;">
                                                            <dt>Childs :</dt>
                                                        </dl>
                                                    </div>
                                                    <div class="table-cell">
                                                        <dl style="margin-left: 15px;">
                                                            <dt>
                                                                <asp:Label ID="lblChild" runat="server"></asp:Label>
                                                            </dt>
                                                        </dl>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>


    <script type="text/javascript">
        
        function HotelList() {
            var query = getParameterByName('session');
            $.ajax({
                type: "GET",
                url: "../HotelHandler.asmx/HotelList",
                data: { session: query },
                datatype: "json",
                success: function (response) {
                    if (response.Session == 1) {
                        if (response.retCode == 1) {
                            window.location.href = "HotelList.aspx?success=1";
                        }
                        else {
                            window.location.href = "HotelList.aspx?success=0";
                        }
                    }
                    else {
                        //window.location.href = "../Default.aspx";
                        alert | ("error")

                    }
                },
                error: function (xhr, error, statusCode) {
                    alert(xhr.responseText);
                }
            });
        }
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        $(document).ready(function () {
            HotelList();
        });
    </script>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">
        enableChaser = 0; //disable chaser
    </script>
</body>
</html>

