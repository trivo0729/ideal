﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="CUTUK.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/SendMail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content">
        <div class="container">
            <div id="main">
                <div class="travelo-google-map block"></div>
                <br />
                <div class="contact-address row block">
                    <div class="col-md-4">
                        <div class="icon-box style5" style="height: 95px">
                            <i class="soap-icon-phone"></i>
                            <div class="description">
                                <small>CALL</small>
                                <h5>+91 712 6649585 - 589 
                                    <br />
                                    +91 712 6641411 - 415
                                </h5>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box style5" style="height: 95px">
                            <i class="soap-icon-message"></i>
                            <div class="description">
                                <small>Send us email on</small>
                                <h5>ticketing@uniglobeidealtours.in<br />
                                    vacations@uniglobeidealtours.in
                                </h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box style5" style="height: 95px">
                            <i class="soap-icon-address"></i>
                            <div class="description">
                                <small>meet us at</small>
                                <h6>Golden Palace, WHC Road,Near Sudama Theatre, Dharampeth,Nagpur, Maharashtra 440010</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="travelo-box box-full">
                    <div class="contact-form">
                        <h2>Send us a Message</h2>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Your Name</label>
                                    <input type="text" name="name" id="name" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Your Email</label>
                                    <input type="text" name="email" id="email" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text" name="subject" id="subject" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Your Message</label>
                                    <textarea name="message" rows="8" class="form-control" id="message" placeholder="write message here"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sms-offset-6 col-sm-offset-6 col-md-offset-8 col-lg-offset-9">
                            <button class="btn-medium full-width" type="button" onclick="SendMail()">SEND MESSAGE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true&amp;key=AIzaSyCKJGgMtLA697jbQRoXXGyCtGiF65obDoo" type="text/javascript"></script>
  <%-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKJGgMtLA697jbQRoXXGyCtGiF65obDoo" type="text/javascript"></script>--%>
   <%--  <script type="text/javascript" src="js/jquery.googlemap.js"></script>--%>
     <script type="text/javascript" src="js/gmap3.min.js"></script>
    <script type="text/javascript">
        var tjq = jQuery;        tjq(".travelo-google-map").gmap3({
            map: {
                options: {
                    center: [21.1405278, 79.0609188],
                    zoom: 12
                }
            },
            marker: {
                values: [
                    //{ latLng: [21.1405278, 79.0609188], data: "Ideal Tours and Travels" }
                     { latLng: [21.145537, 79.074901], data: "Ideal Tours and Travels" }
                ],
                options: {
                    draggable: false
                },
            }
        });
    </script>
   <%-- <script type="text/javascript">
        $(function () {
            $(".travelo-google-map").googleMap({
                zoom: 12, // Initial zoom level (optional)
                coords: [21.145537, 79.074901], // Map center (optional)
                type: "ROADMAP" // Map type (optional)
            });
        })
    </script>--%>
</asp:Content>
