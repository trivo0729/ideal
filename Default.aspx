﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CUTUK.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <link href="css/owl.carousel.css" rel="stylesheet" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/moment.js"></script>
    <script src="scripts/Default.js?v=1.2"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        @keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-moz-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-webkit-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        #pS6Jyk4-1569738072448 {
            outline: none !important;
            visibility: visible !important;
            resize: none !important;
            box-shadow: none !important;
            overflow: visible !important;
            background: none !important;
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity1) !important;
            -moz-opacity: 1 !important;
            -khtml-opacity: 1 !important;
            top: auto !important;
            right: 10px !important;
            bottom: 90px !important;
            left: auto !important;
            position: fixed !important;
            border: 0 !important;
            min-height: 0 !important;
            min-width: 0 !important;
            max-height: none !important;
            max-width: none !important;
            padding: 0 !important;
            margin: 0 !important;
            -moz-transition-property: none !important;
            -webkit-transition-property: none !important;
            -o-transition-property: none !important;
            transition-property: none !important;
            transform: none !important;
            -webkit-transform: none !important;
            -ms-transform: none !important;
            width: auto !important;
            height: auto !important;
            display: none !important;
            z-index: 2000000000 !important;
            background-color: transparent !important;
            cursor: auto !important;
            float: none !important;
            border-radius: unset !important;
            pointer-events: auto !important;
        }

        #Q4gVTA5-1569738072452.open {
            animation: tawkMaxOpen .25s ease !important;
        }
    </style>



    <style>
        .box.listing-style3 p {
            margin: 3px 0px 5px 0px;
        }

        .box.listing-style3 .skin p {
            margin: 15px 0px 0px;
            line-height: 15px;
            font-size: 13px;
        }

        .box.listing-style3 .skin .box_new {
            font-size: 12px;
        }

        .box-title-2 {
            font-size: 15px;
            margin: 3px 9px 0px 0px;
        }

        .box.listing-style3.mng_box {
            display: inline-block;
        }

        .mng_img {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        . mng_box .box-title {
            font-size: 17px;
            color: #5d5b5b;
        }

        .mng_box {
            padding-bottom: 15px;
        }

            .mng_box input[type="button"].button {
                padding: 13px;
                height: 0px;
                line-height: 0px;
                font-size: 13px;
                font-weight: normal;
            }

                .mng_box input[type="button"].button:hover {
                    background: #9c9c9c;
                    color: #f9f9f9;
                }

        .skin_mng {
            border-right: 1px solid #ededed;
            height: 77px;
        }

        .skin {
            border-top: 1px solid #ededed;
            border-bottom: 1px solid #ededed;
            margin-top: 7px;
            margin-bottom: 7px;
            overflow: auto;
            text-align: center;
        }

        .owl_grip {
        }

            .owl_grip .owl-nav {
                position: absolute;
                top: 40%;
                width: 100%;
                font-size: 40px;
            }

                .owl_grip .owl-nav .owl-prev, .owl_grip .owl-nav .owl-next {
                    left: 0;
                    font-size: 25px;
                    position: absolute;
                    background: #a1a1a1 !important;
                    width: 26px;
                    height: 55px;
                    line-height: 25px !important;
                    color: #fff !important;
                }

                    .owl_grip .owl-nav .owl-prev:hover, .owl_grip .owl-nav .owl-next:hover {
                        background: #333 !important;
                    }

                .owl_grip .owl-nav .owl-next {
                    left: inherit;
                    right: 0;
                }

                    .owl_grip .owl-nav .owl-prev span, .owl_grip .owl-nav .owl-next span {
                        line-height: 30px;
                        height: 37px;
                        display: block;
                    }


        @media only screen and (min-device-width :320px) and (max-device-width :768px) {
            .mng_box input[type="button"].button, .review .button {
                padding: 14px 4px;
                height: inherit;
                margin: 0px;
                line-height: 0px;
                font-size: 12px;
            }

            .box.listing-style3 .skin p {
                margin-top: 5px;
            }

            .mng_mo_brd {
                border: 1px solid #ededed;
                border-left: 0px;
                border-right: 0px;
                . text-align:center;
                padding-bottom: 7px;
            }

            .skin_mng {
                height: inherit;
                padding: 3px 0px;
                text-align: center;
                padding-bottom: 7px;
            }

            .review {
            }
        }
    </style>
    <div id="slideshow">
        <div class="fullwidthbanner-container">
            <div class="revolution-slider rev_slider" style="height: 0; overflow: hidden;">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-16" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="http://slider.travelo.com/wp-content/" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="../../../slider.travelo.com/wp-content/plugins/revslider/admin/assets/images/transparent.html" style='background-color: #2f3e00' alt="" title="Slider1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-16-layer-1"
                            data-x="center" data-hoffset=""
                            data-y="center" data-voffset=""
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                            data-transform_out="opacity:0;s:300;s:300;"
                            data-start="500"
                            data-responsive_offset="on"
                            style="z-index: 5;">
                            <img src="../images/Promotion/bg3-1.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-16-layer-2"
                            data-x="587"
                            data-y="434"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="1100"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6;">
                            FARES
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-16-layer-3"
                            data-x="589"
                            data-y="337"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="1400"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 7;">
                            flight
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-16-layer-4"
                            data-x="590"
                            data-y="237"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="1700"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 8;">
                            ever
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-16-layer-5"
                            data-x="592"
                            data-y="138"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="2000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 9;">
                            BEST
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-16-layer-6"
                            data-x="39"
                            data-y="88"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:left;s:1500;e:Power3.easeInOut;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="2300"
                            data-responsive_offset="on"
                            style="z-index: 10;">
                            <img src="../images/Promotion/girls3.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-16-layer-7"
                            data-x="944"
                            data-y="144"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeInOut;"
                            data-transform_out="y:50px;opacity:0;s:300;s:300;"
                            data-start="2600"
                            data-responsive_offset="on"
                            style="z-index: 11;">
                            <img src="../images/Promotion/rectangle.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption black3_11   tp-resizeme"
                            id="slide-16-layer-8"
                            data-x="985"
                            data-y="185"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeInOut;"
                            data-transform_out="y:50px;opacity:0;s:300;s:300;"
                            data-start="2900"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 12;">
                            <a href="#">Read
                                <br>
                                More</a>
                        </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-17" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="http://slider.travelo.com/wp-content/" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="../../../slider.travelo.com/wp-content/plugins/revslider/admin/assets/images/transparent.html" style='background-color: #541262' alt="" title="Slider1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-17-layer-1"
                            data-x="center" data-hoffset=""
                            data-y="center" data-voffset=""
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                            data-transform_out="opacity:0;s:300;s:300;"
                            data-start="500"
                            data-responsive_offset="on"
                            style="z-index: 5;">
                            <img src="../images/Promotion/bg_22.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-17-layer-2"
                            data-x="482"
                            data-y="327"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:right;s:300;s:300;"
                            data-start="800"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6;">
                            deals
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-17-layer-3"
                            data-x="80"
                            data-y="134"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="y:top;s:1500;e:Power3.easeInOut;"
                            data-transform_out="y:50px;opacity:0;s:300;s:300;"
                            data-start="1100"
                            data-responsive_offset="on"
                            style="z-index: 7;">
                            <img src="../images/Promotion/girl_lap.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-17-layer-4"
                            data-x="271"
                            data-y="425"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                            data-transform_out="opacity:0;s:300;s:300;"
                            data-start="1400"
                            data-responsive_offset="on"
                            style="z-index: 8;">
                            <img src="../images/Promotion/logo22.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-17-layer-5"
                            data-x="483"
                            data-y="227"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="1700"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 9;">
                            holiday
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-17-layer-6"
                            data-x="483"
                            data-y="125"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="2000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 10;">
                            great
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-17-layer-7"
                            data-x="950"
                            data-y="341"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:50px;opacity:0;s:1500;e:Power3.easeInOut;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="2300"
                            data-responsive_offset="on"
                            style="z-index: 11;">
                            <img src="../images/Promotion/rectangle.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption black3_11   tp-resizeme"
                            id="slide-17-layer-8"
                            data-x="991"
                            data-y="382"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:Power3.easeInOut;"
                            data-transform_out="x:50px;opacity:0;s:300;s:300;"
                            data-start="2600"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 12;">
                            <a href="#">Read
                                <br>
                                More</a>
                        </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-18" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="http://slider.travelo.com/wp-content/" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="../../../slider.travelo.com/wp-content/plugins/revslider/admin/assets/images/transparent.html" style='background-color: #bd0000' alt="" title="Slider1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-18-layer-1"
                            data-x="center" data-hoffset=""
                            data-y="center" data-voffset=""
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="opacity:0;s:300;e:Power3.easeInOut;"
                            data-transform_out="opacity:0;s:300;s:300;"
                            data-start="500"
                            data-responsive_offset="on"
                            style="z-index: 5;">
                            <img src="../images/Promotion/slider_1_bg.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-18-layer-2"
                            data-x="676"
                            data-y="39"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:Power3.easeInOut;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="800"
                            data-responsive_offset="on"
                            style="z-index: 6;">
                            <img src="../images/Promotion/man_1.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-18-layer-3"
                            data-x="71"
                            data-y="354"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:left;s:1500;e:easeInOutBack;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="1100"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 7;">
                            deals
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-18-layer-4"
                            data-x="70"
                            data-y="252"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:left;s:1500;e:easeInOutBack;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="1400"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 8;">
                            business
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption black3_1   tp-resizeme"
                            id="slide-18-layer-5"
                            data-x="69"
                            data-y="158"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="x:right;s:1500;e:easeInOutBack;"
                            data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                            data-start="1700"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 9;">
                            exciting
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-18-layer-6"
                            data-x="528"
                            data-y="376"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-transform_idle="o:1;"
                            data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;"
                            data-transform_out="y:50px;opacity:0;s:300;s:300;"
                            data-start="2000"
                            data-responsive_offset="on"
                            style="z-index: 10;">
                            <img src="../images/Promotion/rectangle.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption black3_11   tp-resizeme"
                            id="slide-18-layer-7"
                            data-x="571"
                            data-y="419"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"
                            data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;"
                            data-transform_out="y:50px;opacity:0;s:300;s:300;"
                            data-start="2300"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 11;">
                            <a href="#">Read
                                <br>
                                More</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <%--<section id="content" class="tour">
        <div id="slideshow" class="slideshow-bg full-screen">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                         
                        <img class="slidebg" src="images/uploads/revslider1/holiday/package.jpg" />

                    </li>
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background2.jpg);"></div>
                        
                    </li>
                    <li>
                         
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background3.jpg);"></div>
                    </li>
                </ul>
            </div>
             
        </div>
    </section>--%>
    <section id="content">

        <div class="section container">
            <h2>Most Popular Tour Packages</h2>
            <%--<div class="image-carousel style2" data-animation="slide" data-item-width="370" data-item-margin="30">--%>
              <div class="image-carousel style2" data-animation="slide" data-item-width="370" data-item-margin="30">
                
            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                  <ul class="slides image-box style10" id="Static">
                </ul>
                </div>
                  </div>
          


            <%--<div class="owl_grip">
                <div class="owl-carousel owl-theme" id="Static">
                </div>
            </div>--%>

            <div class="row block">
                <div class="col-sm-12">
                    <h2>What Travelers Say?</h2>
                    <div class="testimonial style1 box" id="google-reviews">
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="icon-box style3 counters-box">
                        <div class="numbers">
                            <i class="soap-icon-places select-color"></i>
                            <span class="display-counter" data-value="3200">0</span>
                        </div>
                        <div class="description">Amazing Places To Visit</div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="icon-box style3 counters-box">
                        <div class="numbers">
                            <i class="soap-icon-hotel skin-color"></i>
                            <span class="display-counter" data-value="5738">0</span>
                        </div>
                        <div class="description">5 Star Hotels To Stay</div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="icon-box style3 counters-box">
                        <div class="numbers">
                            <i class="soap-icon-plane yellow-color"></i>
                            <span class="display-counter" data-value="4509">0</span>
                        </div>
                        <div class="description">Airlines To Travel the World</div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="icon-box style3 counters-box">
                        <div class="numbers">
                            <i class="soap-icon-car yellow-color"></i>
                            <span class="display-counter" data-value="3250">0</span>
                        </div>
                        <div class="description">VIP Transport Options</div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="global-map-area section parallax" id="div_show" style="padding-top: 80px; padding-bottom: 70px  ,!important;" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="description text-center">
                    <h1>Popular Destinations</h1>
                    <p>
                        <br />
                    </p>
                </div>
                <div class="image-carousel style3 flex-slider" data-item-width="400" data-item-margin="60">
                    <ul class="slides image-box style9">
                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="images/shortcodes/listings/style01/hotel/1.png" title="">
                                        <img width="270" height="161" src="images/shortcodes/listings/style01/hotel/1.png" alt=""></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title">Istanbul<small></small></h4>
                                    <div class="feedback">
                                        <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: 80%;"></span></div>
                                        <span class="review">270 reviews</span>
                                    </div>
                                    <p class="description">Istanbul is the largest and most populous city in Turkey. Spread across Bosporus Channel, Istanbul is one of the world’s greatest cities known for its magnificent historical monuments and scenic beauty. With an amazing cultural and architectural heritage, Istanbul is a major tourist attraction.</p>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0.6" style="animation-duration: 1s; animation-delay: 0.6s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img width="270" height="161" src="images/shortcodes/listings/style01/hotel/2.png" alt=""></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title">Europe <small>Park</small></h4>
                                    <div class="feedback">
                                        <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: 80%;"></span></div>
                                        <span class="review">170 reviews</span>
                                    </div>
                                    <p class="description">Europa-Park is the largest theme park in Germany, and the second most popular theme park resort in Europe, following Disneyland Paris. In the heart of Europe, between the Black Forest and the Vosges, lies one of the world's most beautiful theme parks - Europa-Park.</p>
                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0.9" style="animation-duration: 1s; animation-delay: 0.9s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img width="270" height="161" src="images/shortcodes/listings/style01/hotel/3.png" alt=""></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title">Turkey<small></small></h4>
                                    <div class="feedback">
                                        <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: 80%;"></span></div>
                                        <span class="review">485 reviews</span>
                                    </div>
                                    <p class="description">Turkey is a modern country with a captivating blend of antiquity and contemporary and East and West. Turkey offers the double attraction of a sunshine holiday and an abundance of sites of historical interest.</p>

                                </div>
                            </article>
                        </li>
                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="1.2" style="animation-duration: 1s; animation-delay: 1.2s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img width="270" height="161" src="images/shortcodes/listings/style01/hotel/4.png" alt=""></a>
                                </figure>
                                <div class="details">

                                    <h4 class="box-title">Abu Dhabi<small></small></h4>
                                    <div class="feedback">
                                        <div title="" class="five-stars-container" data-toggle="tooltip" data-placement="bottom" data-original-title="4 stars"><span class="five-stars" style="width: 80%;"></span></div>
                                        <span class="review">75 reviews</span>
                                    </div>
                                    <p class="description">ThisAbu Dhabi, the capital of the United Arab Emirates, sits off the mainland on an island in the Persian (Arabian) Gulf. Beneath white-marble domes, the vast Sheikh Zayed Grand Mosque features an immense Persian carpet, crystal chandeliers and capacity for 41,000 worshipers.</p>

                                </div>
                            </article>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <br />
        <div class="section parallax" id="div_Patners" style="padding-top: 80px; padding-bottom: 70px  ,!important;" data-stellar-background-ratio="0.5">

            <div class="investor-slideshow image-carousel style2 investor-list" data-animation="slide" data-item-width="170" data-item-margin="30">
                    <ul class="slides">
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/1.png" alt=""></a>
                            </div>
                        </li>
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/2.png" alt=""></a>
                            </div>
                        </li>
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/3.png" alt=""></a>
                            </div>
                        </li>
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/4.png" alt=""></a>
                            </div>
                        </li>
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/5.png" alt=""></a>
                            </div>
                        </li>
                        <li>
                            <div class="travelo-box">
                                <a href="#"><img src="images/investor/6.png" alt=""></a>
                            </div>
                        </li>
                    </ul>
                </div>
            <div class="image-carousel style3 flex-slider" data-item-width="400" data-item-margin="60">
                <div class="container">
                    <ul class="slides image-box style9" style="padding-left: 20px">
                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img style="width: 270px; height: 161px" src="images/TAAI LOGO.png" alt=""></a>
                                </figure>
                            </article>
                        </li>

                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img style="width: 270px; height: 161px" src="images/IATA LOGO.jpg" alt=""></a>
                                </figure>
                            </article>
                        </li>


                        <li>
                            <article class="box">
                                <figure class="animated fadeInDown" data-animation-type="fadeInDown" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">
                                    <a class="hover-effect popup-gallery" href="#" title="">
                                        <img style="width: 270px; height: 161px" src="images/UNIGLOBELOGO.jpeg" alt=""></a>
                                </figure>
                            </article>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <br />
        <div class="global-map-area promo-box no-margin parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="content-section description pull-right col-sm-9">
                    <div class="table-wrapper hidden-table-sm">
                        <div class="table-cell">
                            <h2 class="m-title animated" data-animation-type="fadeInDown" data-animation-duration="1.5">Tell us where you would like to go.<br />
                                <em>1,500+ Packages Available!</em>
                            </h2>
                        </div>
                        <div class="table-cell action-section col-md-4 no-float">
                            <form>
                                <div class="row">
                                    <div class="col-xs-6 col-md-12">
                                        <input type="text" id="PackageCity" class="input-text input-large full-width" value="" placeholder="Enter destination" />
                                    </div>
                                    <div class="col-xs-6 col-md-12">
                                        <button type="button" class="full-width btn-large" onclick="Search()">search Packages</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="image-container col-sm-4">
                    <img src="images/shortcodes/promo-image1.png" alt="" width="342" height="258" class="animated" data-animation-type="fadeInUp" />
                </div>
            </div>
        </div>

    </section>


    <script type="text/javascript">
        var tpj = jQuery;
        $(document).ready(function () {
            tpj("#PackageCity").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../PackageHandler.asmx/GetPackageCity",
                        data: "{'name':'" + document.getElementById('PackageCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnPCode').val(ui.item.id);
                }
            });
        });
    </script>
    <script>
        function Search() {

            window.location.href = "Packages.aspx?City=" + $("#PackageCity").val();
        }
    </script>

    <script>
        jQuery(document).ready(function ($) {
            $("#google-reviews").googlePlaces({
                placeId: 'ChIJSa1172DA1DsRFvhXQb1c6cI'
                , render: ['reviews']
                , min_rating: 1
                , max_rows: 5
                , map_plug_id: 'map-plug'
                , rotateTime: 5000
                , shorten_names: true
            });
        });
    </script>

    <%--<script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
    </script>--%>

    <%--<script>
        $('.portfolio-item').isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows'
        });
        $('.portfolio-menu ul li').click(function () {
            $('.portfolio-menu ul li').removeClass('active');
            $(this).addClass('active');

            var selector = $(this).attr('data-filter');
            $('.portfolio-item').isotope({
                filter: selector
            });
            return false;
        });
        $(document).ready(function () {
            var popup_btn = $('.popup-btn');
            popup_btn.magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });
    </script>--%>

    <style>
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            overflow-x: hidden;
            cursor: pointer;
        }

        .ui-menu-item {
            border-bottom: 1px dotted;
            border-bottom-color: #333;
        }

        .ui-corner-all {
            font-size: 13px;
        }

        input.input-text, select, textarea, span.custom-select {
            background: #f5f5f5;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.rawgit.com/stevenmonson/googleReviews/master/google-places.css" />
    <script src="https://cdn.jsdelivr.net/gh/stevenmonson/googleReviews@6e8f0d794393ec657dab69eb1421f3a60add23ef/google-places.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c&signed_in=true&libraries=places"></script>
    <script src="scripts/owl.carousel.js"></script>
</asp:Content>
