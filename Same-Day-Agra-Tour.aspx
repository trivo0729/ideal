﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Same-Day-Agra-Tour.aspx.cs" Inherits="CUTUK.Same_Day_Agra_Tour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">SAME DAY AGRA TOUR</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="#">HOME</a></li>

                <li class="active">SAME DAY AGRA TOUR</li>
            </ul>
        </div>
    </div>

    <section id="content" class="no-padding">
        <div class="banner imagebg-container" style="height: 350px; background-image: url(http://localhost:57896/images/agra-banner.jpg);">
            <div class="container">
                <h1 class="big-caption">Coverage for every step of <em><strong>your</strong></em> journey!</h1>
                <h2 class="med-caption">We all know the unexpected can occur, even when you are travelling.</h2>
            </div>
        </div>

        <div class="container md-section">
            <div class="row">
                <div id="main" class="col-md-12">


                    <div class="travelo-box box-full">
                        <div class="row">
                            <h1 class="text-center"><strong><u>Unisafe Tours Provide Local Activities Like :</u></strong></h1>
                            <hr />
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/tongaride.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Tonga Ride</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/Delhi-To-Agra1.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Cycling tour</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/biikeride.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Bike Tour</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/walkingtour.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Walking tour</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/tajmoonlight1.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Taj Mahal visit During Moonlight</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sms-4 col-sm-4">
                                <article>
                                    <figure class="small-box image-container">
                                        <img src="images/gallery/cookingclass.jpg" alt="" />
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Cooking Class’s</h4>

                                    </div>
                                </article>
                            </div>
                        </div>


                    </div>


                    <section id="content">
                        <div class="container tour-detail-page">
                            <div class="row">
                                <div id="main" class="col-md-12">
                                    <div id="tour-details" class="travelo-box">
                                        <div class="intro small-box table-wrapper full-width hidden-table-sms">
                                            <div class="col-sm-12 table-cell travelo-box">
                                                <dl class="term-description">
                                                    <dt>Our company registered with  IATA</dt>
                                                    <dd>TIDS CODE: 96-6 4520 5</dd>
                                                    <dt>LEGAL NAME:</dt>
                                                    <dd>UNISAFE TOURS</dd>
                                                </dl>
                                            </div>

                                        </div>
                                        <h2>Background & Objectives:</h2>
                                        <p>Unisafe tours and travels is an established and reputed company in Agra the field of Tours and Travels. The company has made its mark felt in tours and travels by adoption of innovative methods and strong customer base. The company has been promoted with the sole objective of developing business and pleasure travel. </p>
                                        <h2>Leisure Travel :</h2>
                                        <p>
                                            Company has a separate section and very well experienced staff to take care of the requirement of the local services for their vacation holidays with in Agra (Uttar Pradesh). We can provide the clients tailor made itineraries as per their requirements and budget.
                            <strong>Though we expertise in tailoring the packages as per our clients requirements.</strong>We provide guide services with all language guide in Agra. Our guides belong to Department of tourism. They are having more than 10 to 15 years experiences as a guide services in Agra.We wish to associate ourselves with your esteemed organization as your handling agency in Agra (Uttar Pradesh), India and once again assure you of our best services at all times & looking forward to the pleasure of working together.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="icon-box style7 box">
                                <i class="soap-icon-insurance"></i>
                                <div class="description">
                                    <h4 class="box-title"><a href="http://www.Vacaaay.com/company-certifications" target="_blank">Please check  &ndash; our company registration Proof</a></h4>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="icon-box style7 box">
                                <i class="soap-icon-friends"></i>
                                <div class="description">
                                    <h4 class="box-title"><a href="https://www.tripadvisor.in/Attraction_Review-g297683-d6453908-Reviews-Unisafe_Tours_India_Day_Tours-Agra_Uttar_Pradesh.html" target="_blank">Please check  &ndash; our company services reviews on Tripadviser.com</a></h4>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
