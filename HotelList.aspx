﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelList.aspx.cs" Inherits="CUTUK.HotelList" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Cheap Air Tickets, Flight Ticket Booking of Domestic Flights at Lowest Airfare: Ideal Tour and Travels</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="js/tooltip.js"></script>
    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <%--<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDF-uvsBH-yY5rLR7Fu9VtjtH0h47Gye0Y" type="text/javascript"></script>--%>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css" />

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css" />

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <script src="js/moment.js"></script>
    <script src="js/jquery-2.0.2.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="scripts/Alerts.js"></script>
    <script src="js/modal.js"></script>
    <script src="scripts/LoginAndRegister.js"></script>
    <!-- ........................................................................................... -->
    <script src="scripts/CountryCityCode.js?V=1.6" type="text/javascript"></script>
    <script src="scripts/HotelSearch.js?V=2.5" type="text/javascript"></script>
    <script src="scripts/HotelOccupancy.js?v=2.2"></script>
    <script src="scripts/HotelList.js?v=1.2"></script>
    <script src="scripts/GoogleMap.js"></script>
    <script type="text/javascript">
        $(document).on({
            ajaxStart: function () { $("#Hotelloders").css("display", "") },
            ajaxStop: function () { $("#Hotelloders").css("display", "none") }
        });
    </script>
    <style>
        .carousel img {
            display: block;
            float: left;
            height: 50%;
            width: 100%;
        }
    </style>
    <style>
        .tooltip-inner {
            white-space: pre;
            max-width: none;
        }
    </style>
    <script type="text/javascript">

        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        //tpj.noConflict();

        $(document).ready(function () {
            //.....................................................................................................//
            $("#txtCity").autocomplete({

                source: function (request, response) {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {

                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                }
            });
        });
    </script>

    <%--<script type="text/javascript">
        $body = $("body");
        $(document).on({
            ajaxStart: function () { $("#loders").css("display", "") },
            ajaxStop: function () { $("#loders").css("display", "none") }
        });
        $(document).ready(function () {
            GetNationality();
            GetCountry();
            GetHotelCategory();

        });
        $(function () {

            
            $('[data-toggle="tooltip"]').tooltip()
            $('[data-toggle="popover"]').popover();
        })
        function SearchCategory(Item) {
            if (Item.id == "HotelName") {
                document.getElementById("Select_StarRating").setAttribute("style", "display:none");
                document.getElementById("txt_HotelName").setAttribute("style", "display:''");
            }
            else {
                document.getElementById("txt_HotelName").setAttribute("style", "display:none");
                document.getElementById("Select_StarRating").setAttribute("style", "display:''");
            }
        }
    </script>--%>

    <script>
        $('.btn-close').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().fadeOut();
        });
        $('.btn-minimize').click(function (e) {
            e.preventDefault();
            var $target = $(this).parent().parent().next('.box-content');
            if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            else $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $target.slideToggle();
        });
        $('.btn-setting').click(function (e) {
            e.preventDefault();
            $('#myModal').modal('show');
        });
        function Cancel() {
            $("#ConformModal").modal("hide")
        }
        function close() {

            $("#AlertModal").modal("hide")
        }
        function Ok(Message, Method, arg) {
            
            $("#btnOk").css("display", "")
            var id = [];
            if (arg != null) {
                for (var i = 0; i < arg.length; i++) {
                    id.push('"' + arg[i] + '"')
                }
            }

            $("#ConformMessage").html(Message);
            if (arg == null)
                document.getElementById("btnOk").setAttribute("onclick", Method + "()")
            else
                document.getElementById("btnOk").setAttribute("onclick", Method + "(" + id + ")")
            $("#ConformModal").modal("show")
        }
        function Success(Message) {
            $("#AlertMessage").html(Message);
            //$("#btnOk").css("display", "none")
            //$("#Cancel").val("Close")
            $("#AlertModal").modal("show")
        }
    </script>

    <script type="text/javascript">
        var CurrencyClass = '<%=Session["CurrencyClass"]%>';
    </script>
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="Default.aspx" title="home">
                        <img src="images/logo.png" alt="logo" style="width: 306px; height: 80px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile" style="margin-top: 30px; margin-left: 10px;">
                        <li class="twitter"><a title="" href="https://twitter.com/travels_ideal" target="_blank" data-toggle="tooltip" data-original-title="twitter"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="" href="https://www.facebook.com/IdealToursandTravels" target="_blank" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                        <li class="instagram"><a title="" href="https://www.instagram.com/ideal_jungle_safaris/" target="_blank" data-toggle="tooltip" data-original-title="instagram"><i class="soap-icon-instagram"></i></a></li>
                        <li class="instagram"><a title="" href="https://www.instagram.com/uniglobe_idealtours_nagpur/" target="_blank" data-toggle="tooltip" data-original-title="instagram"><i class="soap-icon-instagram"></i></a></li>
                        <li class="linkedin"><a title="" href="https://www.linkedin.com/company/uniglobe-ideal-tours-and-travels" target="_blank" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                    </ul>
                    <span class="user-logged">
                        <a href="#" class="a-white"><i class="fa fa-phone ieffect"></i>+91 992 375 0110</a>&nbsp;&nbsp;
                    <a href="#" class="a-white"><i class="fa fa-envelope ieffect"></i>idealtoursandtravels@gmail.com</a>
                    </span>
                </div>
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>
            <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children active">
                                <a href="Default.aspx" class="a-white"><i class="soap-icon-address"></i>Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx" class="a-white"><i class="soap-icon-notice"></i>About Us</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a style="cursor: pointer" class="a-white"><i class="soap-icon-suitcase"></i>Holidays</a>
                                <ul>
                                    <li><a href="Packages.aspx?Type=Domestic" class="a-white"><i class="soap-icon-suitcase"></i>Holidays in India</a></li>
                                    <li><a href="Packages.aspx?Type=International" class="a-white"><i class="soap-icon-suitcase"></i>International Holidays</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="FlightSearch.aspx" class="a-white"><i class="soap-icon-plane-right takeoff-effect"></i>Flight</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="HotelSearch.aspx" class="a-white"><i class="soap-icon-hotel-3"></i>Hotel</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="Contact.aspx" class="a-white"><i class="soap-icon-phone"></i>Contact Us</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="Payment.aspx" class="a-white"><i class="fa fa-money" aria-hidden="true"></i>Payment</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a style="cursor: pointer" class="a-white account"><i class="soap-icon-user"></i>My account</a>
                                <ul>
                                    <li><a style="cursor: pointer" class="a-white logReg" onclick="SignUpModal()">Login / Register</a></li>
                                    <li><a style="cursor: pointer" onclick="Mybookings()" class="a-white">My Bookings</a></li>
                                    <li><a style="cursor: pointer; display: none" onclick="Logout()" class="a-white Out">Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children a-white">
                        <a href="Default.aspx" class="a-white">Home</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#" class="a-white">About Us</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-1"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Packages.aspx?Type=Domestic" class="a-white"><i class="soap-icon-star"></i>Holidays in India</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Packages.aspx?Type=International" class="a-white"><i class="soap-icon-star"></i>International Holidays</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="FlightSearch.aspx" class="a-white">Flight</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-3"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="HotelSearch.aspx" class="a-white">Hotel</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-4"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#" class="a-white">Contact Us</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-5"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Payment.aspx" class="a-white"><i class="fa fa-money" aria-hidden="true"></i>Payment</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a style="cursor: pointer" class="a-white account"><i class="soap-icon-user"></i>My account</a>
                        <ul>
                            <li><a style="cursor: pointer" class="a-white logReg" onclick="SignUpModal()">Login / Register</a></li>
                            <li><a style="cursor: pointer" onclick="Mybookings()" class="a-white">My Bookings</a></li>
                            <li><a style="cursor: pointer; display: none" onclick="Logout()" class="a-white Out">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </header>

        <section id="content">
            <img alt="" src="images/hotel.gif" id="Hotelloders" style="z-index: 9999; position: fixed; margin-top: 8%; margin-left: 50%; width: 10%">
            <div class="container">
                <div id="main">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <h4 class="search-results-title" id="FilterTrip"></h4>
                            <div class="toggle-container filters-container">
                                <%--<div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#amenities-filter" class="collapsed">Rate Type</a>
                                    </h4>
                                    <div id="amenities-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div class="form-group">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Refundable" name="chkRating" checked="checked" onclick="Refunplolicy(this)" id="Refundable">Refundable
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="nonRefundable" name="chkRating" checked="checked" onclick="Refunplolicy(this)" id="NonRefundable">Non Refundable
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#modify-search-panel" class="collapsed">Hotel Name</a>
                                    </h4>
                                    <div id="modify-search-panel" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <form method="post">
                                                <div class="form-group">
                                                    <label>Hotel Name</label>
                                                    <input type="text" class="form-control" placeholder="Eg: Xyz Hotel" id="txtSearchHotel" />
                                                </div>
                                                <br />
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="button" class="btn-medium icon-check uppercase full-width" onclick="HotelName()">search</button>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <button type="button" class="btn-medium icon-check uppercase full-width" id="spn_Hotel" onclick="ResetName()">Clear</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#price-filter" class="">Price range</a>
                                    </h4>
                                    <div id="price-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <div id="price-range"></div>
                                            <br />
                                            <span class="min-price-label pull-left" id="spn_Max"></span>
                                            <span class="max-price-label pull-right" onchange="HotelFilter();"></span>
                                            <div class="clearer"></div>
                                        </div>
                                        <!-- end content -->
                                    </div>
                                </div>

                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#accomodation-type-filter" class="">Star Ratings</a>
                                    </h4>
                                    <div id="accomodation-type-filter" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <ul class="check-square filters-option Ratings">
                                                <%-- <li class="" id="ALLstr" onclick="HotelFilter()"><a href="#">All<small><label id="Stars"></label></small></a></li>--%>
                                                <li class="active" onclick="HotelFilter()" id="0str"><a href="#">
                                                    <img src="HotelRating/0.png" /><small><label id="Star_0"></label></small></a>
                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="1str"><a href="#">
                                                    <img src="HotelRating/1.png" /><small><label id="Star_1"></label></small></a>
                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="2str"><a href="#">
                                                    <img src="HotelRating/2.png" /><small><label id="Star_2"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="3str"><a href="#">
                                                    <img src="HotelRating/3.png" /><small><label id="Star_3"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="4str"><a href="#">
                                                    <img src="HotelRating/4.png" /><small><label id="Star_4"></label></small></a>

                                                </li>
                                                <li class="active" onclick="HotelFilter()" id="5str"><a href="#">
                                                    <img src="HotelRating/5.png" /><small><label id="Star_5"></label></small></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="sort-by-section clearfix">
                                <h4 class="sort-by-title block-sm">Sort results by:</h4>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <br />
                                        <label id="lblHotelName" style="display: none">0</label>
                                        <input type="hidden" id="txt_Preferred" value="0" />
                                        <select class="form-control" id="sel_Class" onchange="SortBy(this.value)">
                                            <option selected="selected" value="Hotel">Hotel Name</option>
                                            <option value="Price">Price</option>
                                            <option value="Category">Category</option>
                                            <option value="Preferred">Preferred Hotels</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="text-left">Near By Location</span>
                                        <input type="text" style="cursor: pointer" id="txtSource" class="form-control" placeholder="" />
                                    </div>
                                    <div class="col-md-2">
                                        <br />
                                        <button type="button" style="background-color: #f5f5f5; color: #9e9e9e" class="full-width" id="btn_Show" onclick="Show();">Modify Search</button>
                                        <button type="button" style="background-color: #f5f5f5; color: #9e9e9e; display: none" class="full-width" id="btn_hide" onclick="Hide();">Modify Search</button>
                                    </div>
                                    <h1 style="cursor: pointer"><a class="sort-by-title block-sm" onclick="GetMapAll()"><i class="soap-icon-departure" title="Map View"></i></a></h1>
                                </div>
                                <input type="hidden" id="hdnlatitude" />
                                <input type="hidden" id="hdnlongitude" />
                                <input type="hidden" id="hdnnrbl" />
                                <%--<ul class="sort-bar clearfix block-sm">
                                    <li class="sort-by-name"><a class="sort-by-container" href="#"><span onclick="SortByHotel()" title="Sort By Hotel">Hotel Name</span></a>
                                        <label id="lblHotelName" style="display: none">0</label>
                                    </li>
                                    <li class="sort-by-price"><a class="sort-by-container" href="#"><span onclick="SortByPrice()" title="Sort By Pice">Price</span></a></li>
                                    <li class="clearer visible-sms"></li>
                                    <li class="sort-by-rating "><a class="sort-by-container" href="#"><span onclick="StarRating()" title="Sort By Category">Category</span></a></li>
                                    <li class="sort-by-popularity"><a class="sort-by-container" href="#"><span onclick="GetPreferred()" id="btn_Preffered">Preferred Hotels</span></a>
                                        <input type="hidden" id="txt_Preferred" value="0" />
                                    </li>
                                </ul>--%>
                                <br />
                            </div>
                            <div class="hotel-list listing-style3 hotel" style="margin-top: 0px; display: none" id="SearchDive">
                                <div>
                                    <div class="search-box-wrapper style2">
                                        <div class="search-box">

                                            <div class="search-tab-content">
                                                <div class="tab-pane fade active in" id="hotels-tab">
                                                    <form action="hotel-list-view.html" method="post">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <span class="text-left a-white">Where do you want to go?</span>
                                                                <br />
                                                                <div class="input-group" style="width: 100%">
                                                                    <input type="text" style="background-color: none" placeholder="Enter City Here" id="txtCity" class="form-control" />
                                                                </div>
                                                                <input type="hidden" id="hdnDCode" />
                                                            </div>

                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Check-In:</span>
                                                                <div class="datepicker-wrap">
                                                                    <input type="text" name="date_from" id="datepicker_HotelCheckin" class="input-text full-width" placeholder="Check In" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Check-Out:</span>
                                                                <div class="datepicker-wrap">
                                                                    <input type="text" name="date_to" id="datepicker_HotelCheckout" class="input-text full-width" placeholder="Check Out" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="text-left a-white">Total Nights:</span>
                                                                <br />
                                                                <select id="Select_TotalNights" class="form-control">
                                                                    <option selected="selected" value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <br />

                                                        <br id="br1" style="display: none" />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <span class="text-left a-white">Nationality:</span>
                                                                <br />
                                                                <select id="Select_Nationality" class="form-control">
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3" style="display: none">
                                                                <span class="text-left a-white">Star Rating:</span>
                                                                <br />
                                                                <select id="Select_StarRating" class="form-control"></select>
                                                            </div>

                                                            <div class="col-md-5" style="display: none">
                                                                <span class="text-left a-white">Hotel Name:</span>
                                                                <br />
                                                                <input class="form-control" type="text" placeholder="Enter Hotel Here" id="txt_HotelName" />
                                                                <input type="hidden" id="hdnHCode" />
                                                            </div>

                                                            <div class="col-md-1">
                                                                <span class="text-left a-white">Rooms:</span>
                                                                <select id="Select_Rooms" class="form-control">
                                                                    <option selected="selected" value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span class="text-left a-white">Room Type:</span>
                                                                <input type="text" id="spn_RoomType1" style="cursor: default;" disabled="disabled" class="form-control" value="Room 1" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span class="text-left a-white">Adults:</span>
                                                                <select id="Select_Adults1" class="form-control">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span class="text-left a-white">Child:</span>
                                                                <select id="Select_Children1" class="form-control">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst1" class="text-left a-white" style="display: none;">Age 1:</span>
                                                                <select id="Select_AgeChildFirst1" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond1" class="text-left a-white" style="display: none;">Age 2:</span>
                                                                <select id="Select_AgeChildSecond1" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>

                                                        </div>

                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType2" class="text-left a-white" style="display: none;">Room Type:</span>
                                                                <input type="text" id="spn_RoomType2" class="form-control" disabled="disabled" value="Room 2" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults2" class="text-left a-white" style="display: none;">Adults:</span>
                                                                <select id="Select_Adults2" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children2" class="text-left a-white" style="display: none;">Child:</span>
                                                                <select id="Select_Children2" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst2" class="text-left a-white" style="display: none;">Age 1:</span>
                                                                <select id="Select_AgeChildFirst2" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond2" class="text-left a-white" style="display: none;">Age 2:</span>
                                                                <select id="Select_AgeChildSecond2" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType3" class="text-left a-white" style="display: none;">Room Type:</span>
                                                                <input type="text" id="spn_RoomType3" class="form-control" disabled="disabled" value="Room 3" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults3" class="text-left a-white" style="display: none;">Adults:</span>
                                                                <select id="Select_Adults3" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children3" class="text-left a-white" style="display: none;">Child:</span>
                                                                <select id="Select_Children3" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst3" class="text-left a-white" style="display: none;">Age 1:</span>
                                                                <select id="Select_AgeChildFirst3" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond3" class="text-left a-white" style="display: none;">Age 2:</span>
                                                                <select id="Select_AgeChildSecond3" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <span id="Span_RoomType4" class="text-left a-white" style="display: none;">Room Type:</span>
                                                                <input type="text" id="spn_RoomType4" class="form-control" value="Room 4" disabled="disabled" style="display: none; cursor: default;" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Adults4" class="text-left a-white" style="display: none;">Adults:</span>
                                                                <select id="Select_Adults4" class="form-control" style="display: none">
                                                                    <option value="1">1</option>
                                                                    <option value="2" selected="selected">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_Children4" class="text-left a-white" style="display: none;">Child:</span>
                                                                <select id="Select_Children4" class="form-control" style="display: none">
                                                                    <option selected="selected" value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildFirst4" class="text-left a-white" style="display: none;">Age 1:</span>
                                                                <select id="Select_AgeChildFirst4" class="form-control" style="display: none">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <span id="Span_AgeChildSecond4" class="text-left a-white" style="display: none;">Age 2:</span>
                                                                <select id="Select_AgeChildSecond4" class="form-control" style="display: none;">
                                                                    <option selected="selected" value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="col-md-2" style="float: right">
                                                            <button type="button" class="full-width" id="btn_Search" onclick="Redirect();">SEARCH</button>
                                                        </div>
                                                        <br />
                                                        <br />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hotel-list listing-style3 hotel" id="Design">
                            </div>
                            <div class="hpadding20" id="divpagingbottom" style="float: right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>ABOUT US</h2>
                            <p class="p-white">
                                Ideal Tours and Travels had a modest start as a ticketing company in 2008 and gradually spread its wings to develop a huge client base of corporates and others dealing with everything related to tra...
                                <button type="button" class="full-width">READ MORE</button>
                            </p>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>POPULAR TOURS</h2>
                            <ul class="discover triangle hover row a-white">
                                <li class="col-xs-12"><a href="#">Romantic France</a></li>
                                <li class="col-xs-12"><a href="#">Wonderful Lodnon</a></li>
                                <li class="col-xs-12"><a href="#">Awesome Amsterdam</a></li>
                                <li class="col-xs-12"><a href="#">Wild Africa</a></li>
                                <li class="col-xs-12"><a href="#">Beach Goa</a></li>
                                <li class="col-xs-12"><a href="#">Casino Los Vages</a></li>
                                <li class="col-xs-12"><a href="#">Romantic France</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>OUR SERVICES</h2>
                            <ul class="discover triangle hover row a-white">
                                <li class="col-xs-12"><a href="#">Domestic Flights</a></li>
                                <li class="col-xs-12"><a href="#">International Flights</a></li>
                                <li class="col-xs-12"><a href="#">Tours &amp; Holidays</a></li>
                                <li class="col-xs-12"><a href="#">Domestic Hotels</a></li>
                                <li class="col-xs-12"><a href="#">International Hotels</a></li>
                                <li class="col-xs-12"><a href="#">Cruise Holidays</a></li>
                                <li class="col-xs-12"><a href="#">Car Rental</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>CONTACT US</h2>
                            <p class="p-white"><i class="soap-icon-address circle"></i>&nbsp;Golden Palace, SS8, W High Ct Rd, Near Sudhama Theatre, Dharampeth, Nagpur, Maharashtra 440010</p>
                            <p class="p-white"><i class="soap-icon-phone circle"></i><a href="#">+91 1800-00-0000</a></p>
                            <p class="p-white"><i class="soap-icon-message circle"></i><a href="mailto:info@uniglobeidealtours.in">info@uniglobeidealtours.in</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="bottom gray-area">
            <div class="container">
                <div class="copyright pull-left">
                    <p class="p-white">® All Rights Reserved © Copyright 2019 IDEAL TOUR & TRAVELS.</p>
                </div>
                <div class="pull-right">
                    <a id="back-to-top" href="#" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p class="p-white">Concept by Ideal Tours, Developed Trivo IT Solutions.</p>
                </div>
            </div>
        </div>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <%--<script type="text/javascript" src="js/bootstrap.js"></script>--%>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <!-- <script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>-->
    <!--<script type="text/javascript" src="js/gmap3.min.js"></script>-->

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <%--<script type="text/javascript">

        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj(document).ready(function () {

            tpj("#datepicker_HotelCheckin").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                maxDate: "+12M +10D"
            });
            tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
            var dateObject = tpj("#datepicker_HotelCheckin").datepicker("getDate", '+1d');
            dateObject.setDate(dateObject.getDate() + 1);
            tpj("#datepicker_HotelCheckout").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                beforeShow: insertDepartureDate,
                onSelect: insertDays,
                //dateFormat: "yy-m-d",
                minDate: "+1D",
                maxDate: "+12M +20D"
            });
            tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);

            tpj('#datepicker_HotelCheckin').change(function () {
                var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                chkinDate = date2.getDate();
                //chkinMonth = date2.getMonth();
                date2.setDate(date2.getDate() + 1);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                tpj('#Select_TotalNights').val("1");
            });
            tpj('#Select_TotalNights').change(function () {
                manage_date();
                //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
            });
            //....................................................................................................//

            function insertDays() {
                var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                if (days > 30) {
                    alert("You are allowed to book for a maximum 30 Nights");
                    days = tpj('#Select_TotalNights').val();
                    days = parseInt(days);
                    var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                    tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                    tpj('#Select_TotalNights').val(days);
                    //document.getElementById("sel_days").value = days;
                    return false;
                }
                tpj('#Select_TotalNights').val(days);
                //document.getElementById("sel_days").value = days;
            }

            function manage_date() {
                days = tpj('#Select_TotalNights').val();
                days = parseInt(days);
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                insertDepartureDate('', '', '');

            }
            function insertDepartureDate(value, date, inst) {
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var dateAdjust = tpj('#Select_TotalNights').val();
                var current_date = new Date();

                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                if (days < 0) {
                    var add_day = 1;
                } else {
                    var add_day = 2;
                }
                days = parseInt(days);

                tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                dateAdjust = parseInt(dateAdjust);
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
            }
            //....................................................................................................//
            tpj("#txtCity").autocomplete({
                source: function (request, response) {
                    tpj.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    tpj('#hdnDCode').val(ui.item.id);
                }
            });

        });


    </script>--%>

    <script type="text/javascript">

        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj.noConflict();

        tpj(document).ready(function () {

            if (tpj.fn.cssOriginal != undefined)
                tpj.fn.css = tpj.fn.cssOriginal;

            tpj('.fullscreenbanner').revolution(
                {
                    delay: 9000,
                    startwidth: 1170,
                    startheight: 600,

                    onHoverStop: "on",						// Stop Banner Timet at Hover on Slide on/off

                    thumbWidth: 100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                    thumbHeight: 50,
                    thumbAmount: 3,

                    hideThumbs: 0,
                    navigationType: "bullet",				// bullet, thumb, none
                    navigationArrows: "solo",				// nexttobullets, solo (old name verticalcentered), none

                    navigationStyle: false,				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


                    navigationHAlign: "left",				// Vertical Align top,center,bottom
                    navigationVAlign: "bottom",					// Horizontal Align left,center,right
                    navigationHOffset: 30,
                    navigationVOffset: 30,

                    soloArrowLeftHalign: "left",
                    soloArrowLeftValign: "center",
                    soloArrowLeftHOffset: 20,
                    soloArrowLeftVOffset: 0,

                    soloArrowRightHalign: "right",
                    soloArrowRightValign: "center",
                    soloArrowRightHOffset: 20,
                    soloArrowRightVOffset: 0,

                    touchenabled: "on",						// Enable Swipe Function : on/off


                    stopAtSlide: -1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                    stopAfterLoops: -1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

                    hideCaptionAtLimit: 0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                    hideAllCaptionAtLilmit: 0,				// Hide all The Captions if Width of Browser is less then this value
                    hideSliderAtLimit: 0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


                    fullWidth: "on",							// Same time only Enable FullScreen of FullWidth !!
                    fullScreen: "off",						// Same time only Enable FullScreen of FullWidth !!


                    shadow: 0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

                });

            tpj("#datepicker_Departure").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",


                minDate: "dateToday",

            });
            tpj("#datepicker_Return").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
            });
            tpj("#datepicker_HotelCheckin").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
            tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
            var dateObject = tpj("#datepicker_HotelCheckin").datepicker('getDate', '+1d');
            dateObject.setDate(dateObject.getDate() + 1);
            tpj("#datepicker_HotelCheckout").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                beforeShow: insertDepartureDate,
                onSelect: insertDays,
                //dateFormat: "yy-m-d",
                minDate: "+1D",
                //maxDate: "+3M +20D"
            });
            tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);
            Addyear();
            tpj('#datepicker_HotelCheckin').change(function () {
                var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                chkinDate = date2.getDate();
                //chkinMonth = date2.getMonth();
                date2.setDate(date2.getDate() + 1);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                tpj('#Select_TotalNights').val("1");
            });
            tpj('#Select_TotalNights').change(function () {
                manage_date();
                //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
            });
            //....................................................................................................//

            function insertDays() {
                var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                if (days > 30) {
                    alert("You are allowed to book for a maximum 30 Nights");
                    days = tpj('#Select_TotalNights').val();
                    days = parseInt(days);
                    var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                    tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                    tpj('#Select_TotalNights').val(days);
                    //document.getElementById("sel_days").value = days;
                    return false;
                }
                tpj('#Select_TotalNights').val(days);
                //document.getElementById("sel_days").value = days;
            }

            function manage_date() {
                days = tpj('#Select_TotalNights').val();
                days = parseInt(days);
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                insertDepartureDate('', '', '');

            }

            function insertDepartureDate(value, date, inst) {
                
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var dateAdjust = tpj('#Select_TotalNights').val();
                var current_date = new Date();

                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                if (days < 0) {
                    var add_day = 1;
                } else {
                    var add_day = 2;
                }
                days = parseInt(days);
                tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                dateAdjust = parseInt(dateAdjust);
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);

            }

            function Addyear() {
                var current_date = new Date();
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                days = parseInt(days);
                var i = 0
                tpj('#datepicker_HotelCheckin').datepicker("option", "minDate", days + i);
                tpj('#datepicker_HotelCheckin').datepicker("option", "maxDate", days + 365);
            }
        });
    </script>

    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="AlertMessage"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="size17" id="ConformMessage">
                            </p>
                            <table align="right" style="margin-left: -50%;">
                                <tbody>
                                    <tr>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="btn-search4 margtop20" style="float: none;">
                                        </td>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Ok" id="btnOk" class="btn-search4 margtop20" style="float: none;">
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade modal-lg" id="MapView" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Map View</h4>
                </div>
                <div class="modal-body">
                    <div id="map" class="img-responsive" style="height: 450px; width: 800px; width: 100%"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="Slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="SlideHotelName" style="text-align: left"></h4>
                </div>
                <div class="">
                    <div id="Img" class="container" style="width: 600px">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="PreviewModalRegisteration" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="tab-container style1">
                            <ul class="tabs">
                                <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="Login">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="size13 dark">Username / Email : </span>
                                            <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="size13 dark">Password : </span>
                                            <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" onclick="LoginCustomer()" class="button btn-small sky-blue1" value="Login" />
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Register">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">User Name : </span>
                                            <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Password: </span>
                                            <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Confirm Password: </span>
                                            <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Mobile : </span>
                                            <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <span class="size13 dark">Phone : </span>
                                            <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="size13 dark">Address : </span>
                                            <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                <b>* This field is required</b></label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="size13 dark">Country : </span>
                                            <select id="selCountry" class="form-control">
                                                <option selected="selected" value="-">Select Any Country</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">City : </span>
                                            <select id="selCity" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                <b>* This field is required</b></label>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="size13 dark">Pin Code : </span>
                                            <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                            <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                <b>* This field is required</b></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="block" style="float: right">
                                        <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidateRegistration()" />
                                        <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearRegistration();" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    //$(window).unload(function () {
    //    unloadPage()
    //});

    $(function () {
        $("form").validationEngine();
        var oldValue;
        var unsaved = false;
        //$(":input").change(function () {
        //    unsaved = true;
        //});
        $('input[type="text"]').keyup(function () {
            
            // = this.value()
            if (/^[A-Z0 -9]+$/i.test(this.value)) {
            }
            else {
                this.value = '';
            }
        });
    });

    //jQuery(window).bind('beforeunload', function () {
    //    return 'You sure you want to leave?';
    //});

    function GetPrepareRazorPayModal() {
        CheckSessionDefault();
        document.getElementById("rzp-button1").click();
        var total = $("#TotalPrice").text();
        var TotalFare = parseFloat(total.replace(",", ""));
        var options = {
            // "key": "rzp_live_Q8PFvYZQP356qb",
            "key": "rzp_live_0tFTmrMGQkOcZa",
            "order_id": "001",
            "amount": TotalFare * 100, // 2000 paise = INR 20
            "name": "VIZACafe",
            "description": "Hotel Booking",
            "currency": "INR",
            "image": "http://idealtours.in/assests/images/logo.png",
            "handler": function (response) {
                alert(response.razorpay_payment_id);
                CheckSessionOnPage();
                window.alert('Thank you For Booking.');
            },
            "prefill": {
                "name": "A Q",
                "email": "sheikh.quddus@gmail.com",
            },
            "notes": {
                "address": ""
            },
            "theme": {
                "color": "#f58634"
            }
        };
        var rzp1 = new Razorpay(options);
        document.getElementById('rzp-button1').onclick = function (e) {
            rzp1.open();
            e.preventDefault();
        }
    }
</script>
</html>
