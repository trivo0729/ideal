﻿using CUT.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUTUK
{
    public partial class VoucherPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)Session["LoginUser"];
            string ReservationId = Request.QueryString["ReservationID"];
            Int64 Uid = objGlobalDefaults.sid;
            string Night = "";
            string Supplier = "";
            string Latitude = "";
            string Longitude = "";

            string HotelAddress = "";
            string HotelCity = "";
            string HotelCountry = "";
            string HotelPhone = "";
            string HotelPostal = "";
            string City = "";
            string AllPassengers = "";
            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail, dtHotelAdd;


            SqlParameter[] SQLParams = new SqlParameter[2];
            SQLParams[0] = new SqlParameter("@ReservationID", ReservationId);
            SQLParams[1] = new SqlParameter("@sid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

            dtHotelReservation = ds.Tables[0];
            dtBookedPassenger = ds.Tables[1];
            dtBookedRoom = ds.Tables[2];
            dtBookingTransactions = ds.Tables[3];
            dtAgentDetail = ds.Tables[4];
            dtHotelAdd = ds.Tables[5];
            Night = dtHotelReservation.Rows[0]["NoOfDays"].ToString();
            Supplier = dtHotelReservation.Rows[0]["Source"].ToString();
            if (Supplier == "MGH" || Supplier == "DoTW")
            {
                Latitude = dtHotelAdd.Rows[0]["LatitudeMGH"].ToString();
                Longitude = dtHotelAdd.Rows[0]["LongitudeMGH"].ToString();
                HotelAddress = dtHotelAdd.Rows[0]["Address"].ToString();
                HotelCity = dtHotelAdd.Rows[0]["City"].ToString();
                HotelCountry = dtHotelAdd.Rows[0]["Country"].ToString();
                HotelPhone = dtHotelAdd.Rows[0]["ContactPhone"].ToString();
                HotelPostal = dtHotelAdd.Rows[0]["PostalCode"].ToString();

            }
            else
            {
                Latitude = dtHotelReservation.Rows[0]["Latitude"].ToString();
                Longitude = dtHotelReservation.Rows[0]["Longitude"].ToString();

                HotelAddress = dtHotelAdd.Rows[0]["Address"].ToString();
                City = dtHotelReservation.Rows[0]["City"].ToString();
                string[] CitySplit = City.Split(',');
                HotelCity = CitySplit[0];
                HotelCountry = CitySplit[1];
                HotelPhone = dtHotelAdd.Rows[0]["Number_"].ToString();
                HotelPostal = dtHotelAdd.Rows[0]["PostalCode"].ToString();
            }


            Double SalesTax = Convert.ToDouble(dtBookingTransactions.Rows[0]["SeviceTax"]);
            Double Ammount = Convert.ToDouble(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);

            string Hoteldestination = dtHotelReservation.Rows[0]["City"].ToString();
            string InvoiceID = dtHotelReservation.Rows[0]["InvoiceID"].ToString();
            string VoucherID = dtHotelReservation.Rows[0]["VoucherID"].ToString();
            string CheckIn = dtHotelReservation.Rows[0]["CheckIn"].ToString();


            CheckIn = CheckIn.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] CheckInDate = CheckIn.Split('/');
                CheckIn = CheckInDate[2] + '-' + CheckInDate[1] + '-' + CheckInDate[0];
            }
            string CheckOut = dtHotelReservation.Rows[0]["CheckOut"].ToString();
            CheckOut = CheckOut.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] CheckOutDate = CheckOut.Split('/');
                CheckOut = CheckOutDate[2] + '-' + CheckOutDate[1] + '-' + CheckOutDate[0];
            }


            string ReservationDate = dtHotelReservation.Rows[0]["ReservationDate"].ToString();
            ReservationDate = ReservationDate.Replace("00:00", "");
            if (CheckIn.Contains('/'))
            {
                string[] ReservationBookingDate = ReservationDate.Split('/');
                ReservationDate = ReservationBookingDate[2] + '-' + ReservationBookingDate[1] + '-' + ReservationBookingDate[0];

            }



            string HotelName = (dtHotelReservation.Rows[0]["HotelName"].ToString());
            string AgentRef = (dtHotelReservation.Rows[0]["AgentRef"].ToString());
            string AgencyName = dtAgentDetail.Rows[0]["AgencyName"].ToString();
            string Address = dtAgentDetail.Rows[0]["Address"].ToString();
            string Description = (dtAgentDetail.Rows[0]["Description"].ToString());
            string Countryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
            string phone = dtAgentDetail.Rows[0]["phone"].ToString();
            string email = (dtAgentDetail.Rows[0]["email"].ToString());
            string agentcode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
            string GstNo = dtAgentDetail.Rows[0]["GSTNumber"].ToString();
            string AgentCode = (dtAgentDetail.Rows[0]["Agentuniquecode"].ToString());
            string Pincode = dtAgentDetail.Rows[0]["Pincode"].ToString();
            string AgentCountryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
            string Fax = dtAgentDetail.Rows[0]["Fax"].ToString();
            if (Fax == "0")
            {
                Fax = "";
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\"/>");
            sb.Append("<title></title>");
            sb.Append("</head>");
            sb.Append("<body style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px; width:auto; border:2px solid gray\">");
            var AdminUrl = ConfigurationManager.AppSettings["AdminUrl"];
            sb.Append("<div style=\"background-color: #F7B85B; height: 13px\">");
            sb.Append("<span>&nbsp;</span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table style=\" height: 100px; width: 100%;\">");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<td style=\"width: auto;padding-left:15px\">");
            sb.Append("<img  src=\"" + AdminUrl + "/AgencyLogos/" + AgentCode + ".jpg\" height=\"auto\" width=\"auto\"></img>");
            sb.Append("</td>");
            sb.Append("<td style=\"width: 20%\"></td>");
            sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
            sb.Append("<span style=\"margin-right: 15px\">");
            sb.Append("<br>");
            sb.Append("" + Address + ",<br>");
            //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
            sb.Append("" + Description + " - " + Pincode + ", " + AgentCountryname + "<br>");
            sb.Append("Tel: " + phone + "<br> Fax: " + Fax + "<br>");
            sb.Append(" Email: " + email + "<br>");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"color: #57585A\"><span style=\"padding-left:10px\"> <b>GST No:</b> " + GstNo + "</span></td>");
            sb.Append("<td></td>");
            sb.Append("<td></td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
            sb.Append("<tr>");
            sb.Append("<td rowspan=\"3\" style=\"width:35%; color: #00CCFF; font-size: 30px; text-align: left; border: none; padding-right:35px; padding-left:10px\">");
            sb.Append("<b>HOTEL VOUCHER</b>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px;color: #57585A\">");
            sb.Append("<b> Booking Date &nbsp;&nbsp; &nbsp; &nbsp;     :</b><span style=\"color: #757575\">" + ReservationDate + "</span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px;color: #57585A\">");
            sb.Append("<b>Voucher No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     : </b><span style=\"color: #757575\">" + VoucherID + "</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
            sb.Append("<b> Agent Ref No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + AgentRef + " </span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
            sb.Append("<b>  Supplier Ref No &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\"color:#757575\">");
            sb.Append("436-985128");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>BOOKING DETAILS</b></span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; height: 250px; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td rowspan=\"4\" style=\"width: auto; border: none\" id=\"googleMap\">");
            sb.Append("<img  src='https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=400x250&sensor=false&maptype=roadmap&markers=color:red|" + Latitude + "," + Longitude + "' style='width:auto; height:auto' >");
            sb.Append("</td>");
            sb.Append("<td style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:20%\">");
            sb.Append("<span style=\"margin-left: 15px\"><b>Check In</span>");
            sb.Append("</br>");
            sb.Append("</td>");
            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:47%\">");
            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckIn + "</b></span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
            sb.Append("<span style=\"margin-left: 15px\"><b>Check Out</b></span>");
            sb.Append("</td>");
            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckOut + "</b></span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 24px; color: white; border: none\">");
            sb.Append("<span style=\" margin-left: 15px\"><b>Hotel Name</b></span>");
            sb.Append("</td>");
            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 22px; color: white; border: none\">");
            sb.Append(": <span style=\" margin-left: 15px\"><b>" + HotelName + "</b></span>");
            sb.Append("</td>");
            sb.Append("<td rowspan=\"2\" align=\"center\" style=\"background-color: #35C2F1; font-size: 22px; color: white; border: none; width:10%\">");
            sb.Append("<br> <br><b>Nights</b>");
            sb.Append("<br>");
            sb.Append("<span style=\"font-size:40px; font-weight:700\">" + Night + "</span>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr style=\"border: none\">");
            sb.Append("<td style=\"border: none;color: #57585A\">");
            sb.Append("<span style=\" margin-left: 15px\"><b>Address</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>City</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Country</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Phone</b></span><br>");
            sb.Append("<span style=\" margin-left: 15px\"><b>Postal Code</b></span>");
            sb.Append("</td>");
            sb.Append("<td style=\"border: none;color: #57585A\">");



            sb.Append(": <span style=\" margin-left: 15px\">" + HotelAddress + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelCity + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelCountry + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPhone + "</span><br>");
            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPostal + "</span>");
            sb.Append("</td>");
            sb.Append("<!--<td colspan=\"2\">Sum: $180</td>-->");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");


            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>GUEST & ROOM DETAILS</b></span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
            for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
            {
                AllPassengers = "";
                for (int p = 0; p < dtBookedPassenger.Rows.Count; p++)
                {
                    string brrn = (dtBookedRoom.Rows[i]["RoomNumber"]).ToString();
                    string bprn = (dtBookedPassenger.Rows[p]["RoomNumber"]).ToString();

                    if ((dtBookedPassenger.Rows[p]["RoomNumber"]).ToString() == (dtBookedRoom.Rows[i]["RoomNumber"]).ToString())
                    {
                        AllPassengers += dtBookedPassenger.Rows[p]["Name"] + " " + dtBookedPassenger.Rows[p]["LastName"] + ", ";

                    }

                }

                AllPassengers = AllPassengers.TrimEnd(' ');
                AllPassengers = AllPassengers.TrimEnd(',');
                string Chid_Age = dtBookedRoom.Rows[i]["ChildAge"].ToString();
                string Remark = dtBookedRoom.Rows[i]["Remark"].ToString();
                if (Chid_Age == "")
                {
                    Chid_Age = "-";
                }
                if (Remark == "")
                {
                    Remark = "-";
                }
                sb.Append("<tr style=\"border-spacing: 0px;\">");
                sb.Append("<td style=\"border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700\">" + dtBookedRoom.Rows[i]["RoomNumber"].ToString() + "</td>");
                //sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #f1d135; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                //  sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + dtBookedRoom.Rows[i]["LeadingGuest"].ToString() + "</span></td>");
                sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + AllPassengers + "</span></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Room Type</td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\"> Board </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Total Rooms</td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\"> Adults </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Child </td>");
                sb.Append("<td style=\"border: none; padding-top: 15px\">Child Age</td>");
                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px\">Remark</td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtBookedRoom.Rows[i]["RoomType"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["BoardText"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["TotalRooms"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["Adults"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom.Rows[i]["Child"].ToString() + "</td>");
                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + Chid_Age + "</td>");
                sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\">" + Remark + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</div>");

            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
            sb.Append("</div>");
            sb.Append("<hr style=\"border-top-width: 3px\">");
            sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
            sb.Append("<ul style=\"list-style-type: disc\">");
            sb.Append("<li>Please collect all extras directly from clients prior to departure</li>");
            sb.Append("<li>");
            sb.Append("It is mandatory to present valid Passport at the time of check-in for International");
            sb.Append("hotel booking & any valid photo ID for domestic hotel booking");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Hotel holds right to reject check in or charge supplement due to wrongly selection");
            sb.Append("of Nationality or Residency at the time of booking");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Early check-in & late check-out will be subject to availability and approval by");
            sb.Append("respective hotel only");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("Kindly inform hotel in advance for any late check-in, as hotel may automatically");
            sb.Append("cancel the room in not check-in by 18:00Hrs local time");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append(" You may need to deposit security amount at some destination as per the local rule");
            sb.Append("and regulations at the time of check-in");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append(" Tourism fees may apply at the time of check-in at some destinations like Dubai,Paris, etc...");

            sb.Append("</li>");
            sb.Append("<li>General check-in time is 14:00Hrs & check-out time is 12:00Hrs</li>");
            sb.Append("</ul>");
            sb.Append("</div>");
            sb.Append("<br>");
            sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 20px; color: white\">");
            sb.Append("<span>");
            sb.Append("Check your booking details carefully and inform us immediately before it’s too late...");
            sb.Append("</span>");
            sb.Append("</div><br>");
            sb.Append("</body>");
            sb.Append("</html>");

            HttpContext context = System.Web.HttpContext.Current;
            var htmlContent = String.Format(sb.ToString());
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
            pdfConverter.PdfToolPath = ConfigurationManager.AppSettings["rootPath"];
            //pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\httpdocs\\Agent";

            var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(pdfBytes);

        }
    }
}