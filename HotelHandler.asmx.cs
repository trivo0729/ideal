﻿using CUT.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;
using com = CUT.Common;
using System.Web.Script.Services;
using System.IO;
using System.Globalization;
using System.Transactions;
using System.Configuration;
using CUT.Common;
using MGHLib;

//using EANLib.Response;
using CUT.DataLayer;

//using System.Web.Script.Services;
using CUT.Models;
using MGHLib.Common;
using System.Threading.Tasks;
using System.Threading;

//using CUT.BL;
using CUT.Agent.DataLayer;
using CommonLib.Response;
using EANLib.Response;
using CUTUK.DataLayer;
using HotelLib.Request;
namespace CUTUK
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        CUT.HotelHandler obj = new CUT.HotelHandler();
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Destination

        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {

            return obj.GetDestinationCode(name);
            //string jsonString = "";
            //DataTable dtResult;
            //DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            //JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    List<CUT.Models.AutoComplete> list_autocomplete = new List<CUT.Models.AutoComplete>();
            //    list_autocomplete = dtResult.AsEnumerable()
            //    .Select(data => new CUT.Models.AutoComplete
            //    {
            //        id = data.Field<String>("DestinationCode"),
            //        value = data.Field<String>("Destination")
            //    }).ToList();

            //    jsonString = objSerlizer.Serialize(list_autocomplete);
            //    dtResult.Dispose();
            //}
            //else
            //{
            //    jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            //}
            //return jsonString;
        }

        // [WebMethod(EnableSession = true)]
        // public string GetDestinationCode(string name)
        //{
        //     string jsonString = "";
        //     JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
        //     using (var DB = new CUT.BL.TBOdbHandlerDataContext())
        //     {
        //         var List = (from obj in DB.tbl_TBOHotelCities
        //                     where obj.Destination.Contains(name)
        //                     select new CUT.Models.AutoComplete
        //                     {
        //                         id = obj.countrycode,
        //                         value = obj.Destination + ", " + obj.country
        //                     }).ToList();
        //         jsonString = objSerlizer.Serialize(List);
        //     }
        //     return jsonString;
        // }

        #endregion Destination

        #region Hotel

        [WebMethod(EnableSession = true)]
        public string GetHotel(string name, string destination)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CUT.DataLayer.HotelManager.Get(name, destination, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<CUT.Models.AutoComplete> list_autocomplete = new List<CUT.Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new CUT.Models.AutoComplete
                {
                    id = data.Field<String>("HotelCode"),
                    value = data.Field<String>("Name")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelCategory(string code)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CUT.DataLayer.HotelManager.GetCategory(code, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            List<CUT.Models.AutoComplete> list_autocomplete = new List<CUT.Models.AutoComplete>();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new CUT.Models.AutoComplete
                {
                    id = data.Field<String>("CategoryCode"),
                    value = data.Field<String>("CategoryName")
                }).ToList();
                //list_autocomplete.Insert(0, new Models.AutoComplete { id = "", value = "SELECT TYPE OF HOTEL" });
                list_autocomplete.Insert(0, new CUT.Models.AutoComplete { id = "", value = "SELECT" });
                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                //list_autocomplete.Insert(0, new Models.AutoComplete { id = "", value = "SELECT TYPE OF HOTEL" });
                list_autocomplete.Insert(0, new CUT.Models.AutoComplete { id = "", value = "SELECT" });
                jsonString = objSerlizer.Serialize(list_autocomplete);
            }
            return jsonString;
        }

        #endregion Destination

        #region Hotel Search

        [WebMethod(EnableSession = true)]
        public string GetResult()
        {
            JavaScriptSerializer JsonSerialize = new JavaScriptSerializer();
            CUT.Models.CommonHotels objCommonHotel = new CommonHotels();
            if (Session["Common"] != null)
            {
                objCommonHotel = (CUT.Models.CommonHotels)Session["Common"];
                return JsonSerialize.Serialize(new { arrHotel = b2cDesign.GetHotels(objCommonHotel, 1), retCode = 1 });

            }
            else
            {
                return JsonSerialize.Serialize(new { Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", });
            }

            //return Builder(objCommonHotel, 1);
        }


        [WebMethod(EnableSession = true)]
        public string HotelName(string HotelName)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            Session["HotelName"] = HotelName;
            return objSerialize.Serialize(new { retCode = 1 });
        }

        private string Builder(CUT.Models.CommonHotels objCommonHotel, int pageNo)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            if (Session["FiterHistory"] == null && objCommonHotel != null)
            {
                //...................................Common..................B........................................//
                List<CommonLib.Response.CommonHotelDetails> List_CommonHotel = objCommonHotel.CommonHotelDetails.Take(50).Skip(0).ToList();
                com.HTMLCommonAvailBuilder objHTMLCommonAvailBuilder = new com.HTMLCommonAvailBuilder();
                string m_filtertrip = objHTMLCommonAvailBuilder.GenerateFilterTrip(List_CommonHotel.Count, objCommonHotel.CommonHotelDetails.Count, objCommonHotel.DisplayRequest.Location);
                string m_StarRating = objHTMLCommonAvailBuilder.GenerateStarRating(objCommonHotel.Category.OrderBy(data => data.Name).ToList());
                string m_Facility = objHTMLCommonAvailBuilder.GenerateFacility(objCommonHotel.Facility);
                string m_Location = objHTMLCommonAvailBuilder.GenerateLocation(objCommonHotel.Location);
                com.Pager objPager = new com.Pager();
                objPager.currentPage = pageNo;
                objPager.TototalRowsCount = objCommonHotel.CommonHotelDetails.Count;
                string m_Paging = objPager.Generate();
                string m_Content = objHTMLCommonAvailBuilder.GenerateMainContainer(List_CommonHotel, "INR");
                string m_Supplier = objHTMLCommonAvailBuilder.GenerateSupplier(List_CommonHotel, objCommonHotel.CommonHotelDetails);
                string m_Price = objHTMLCommonAvailBuilder.GeneratePrice(objCommonHotel.MinPrice, objCommonHotel.MaxPrice);
                #region Map By Kashif
                List<CommonMap> MapList = objHTMLCommonAvailBuilder.GetMaps(objCommonHotel.CommonHotelDetails);
                return js.Serialize(new { FilterTrip = m_filtertrip, StarRating = m_StarRating, Facility = m_Facility, Paging = m_Paging, Content = m_Content, PriceMin = objCommonHotel.MinPrice, PriceMax = objCommonHotel.MaxPrice, Supplier = m_Supplier, PriceFilter = m_Price, Location = m_Location, DisplayRequest = objCommonHotel.DisplayRequest, MapName = MapList });
                //...................................Common...................E.......................................//
                #endregion
            }
            else if (Session["FiterHistory"] != null)
            {
                HotelFilterState objHotelFilterState = (HotelFilterState)Session["FiterHistory"];
                com.HTMLCommonAvailBuilder objHTMLCommonAvailBuilder = new com.HTMLCommonAvailBuilder();
                List<CommonLib.Response.CommonHotelDetails> List_HotelDetail_Filter = (List<CommonLib.Response.CommonHotelDetails>)Session["List_HotelDetail_Filter"];

                // Pagging  // 
                com.Pager objPager = new com.Pager();
                objPager.currentPage = objHotelFilterState.PageNo;
                objPager.TototalRowsCount = List_HotelDetail_Filter.Count;
                string m_Paging = objPager.Generate();
                //...................................Common..................B........................................//
                List<CommonLib.Response.CommonHotelDetails> List_CommonHotel = List_HotelDetail_Filter.Skip((objHotelFilterState.PageNo - 1) * objPager.PageSize).Take(objPager.PageSize).ToList();

                string m_filtertrip = objHTMLCommonAvailBuilder.GenerateFilterTrip(List_CommonHotel.Count, objPager.TototalRowsCount, objCommonHotel.DisplayRequest.Location);
                string m_StarRating = objHTMLCommonAvailBuilder.GenerateStarRating(objCommonHotel.Category.OrderBy(data => data.Name).ToList(), objHotelFilterState.StarRating);
                string m_Facility = objHTMLCommonAvailBuilder.GenerateFacility(objCommonHotel.Facility, objHotelFilterState.HotelFacility);
                string m_Location = objHTMLCommonAvailBuilder.GenerateLocation(objCommonHotel.Location, objHotelFilterState.NearByPlaces);

                string m_Content = objHTMLCommonAvailBuilder.GenerateMainContainer(List_CommonHotel, "INR");
                string m_Supplier = objHTMLCommonAvailBuilder.GenerateSupplier(List_CommonHotel, objCommonHotel.CommonHotelDetails, objHotelFilterState.Suppliers);
                string m_Price = objHTMLCommonAvailBuilder.GeneratePrice(objHotelFilterState.PriceRange.From, objHotelFilterState.PriceRange.To);
                #region Map By Kashif
                List<CommonMap> MapList = objHTMLCommonAvailBuilder.GetMaps(objCommonHotel.CommonHotelDetails);

                return js.Serialize(new { FilterTrip = m_filtertrip, StarRating = m_StarRating, Facility = m_Facility, Paging = m_Paging, Content = m_Content, PriceMin = objCommonHotel.MinPrice, PriceMax = objCommonHotel.MaxPrice, Supplier = m_Supplier, PriceFilter = m_Price, Location = m_Location, DisplayRequest = objCommonHotel.DisplayRequest, MapName = MapList, HotelName = Session["HotelName"] });
                //...................................Common...................E.......................................//
                #endregion
            }
            else
            {
                return js.Serialize(new { Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", });

            }
        }

        #endregion Hotel Search

        #region Hotel Parsing

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void HotelList(string session)
        {

            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            try
            {
                CUT.HotelHandler obj = new CUT.HotelHandler();
                obj.HotelList(session);
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod(EnableSession = true)]
        public string GetSession()
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            string session = (string)HttpContext.Current.Session["session"];
            string[] array_input = session.Split('_');

            HotelValuedAvailRQ objHotelValuedAvailRQ = new HotelValuedAvailRQ();
            CUT.Models.AvailRequest objAvail = new CUT.Models.AvailRequest();
            List<HotelLib.Request.HotelOccupancy> list_Occupancy = new List<HotelLib.Request.HotelOccupancy>();
            objHotelValuedAvailRQ.DestinationCode = array_input[0].ToString();
            objAvail.Location = array_input[1].ToString();
            objHotelValuedAvailRQ.DestinationType = "SIMPLE";
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            objHotelValuedAvailRQ.CheckInDate = fDate.ToString("yyyyMMdd");
            objAvail.CheckIn = fDate.ToString("dd/MM/yyyy");
            objHotelValuedAvailRQ.CheckOutDate = tDate.ToString("yyyyMMdd");
            objAvail.CheckOut = tDate.ToString("dd/MM/yyyy");
            string occpancy = array_input[5].ToString();
            objAvail.DestinationCode = array_input[0].ToString();
            try
            {
                objAvail.HotelCode = array_input[6].ToString();
                objAvail.HotelName = array_input[7].ToString();
                objAvail.StarRating = array_input[8].ToString();
                objAvail.NationalityCode = array_input[9].ToString();
            }
            catch { }
            //objAvail.NationalityCountry = array_input[10].ToString();
            string[] array_occupancy = occpancy.Split('$');
            int adult = 0;
            int child = 0;

            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Customer> list_Customer = new List<Customer>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        //...............................under test by maqsood...........................................//
                        int Age = Convert.ToInt32(n_array[i]);
                        //...............................under test by maqsood...........................................//
                        if (Age > 0)
                            list_Customer.Add(new Customer { Age = Age, type = "CH" });
                    }
                }
                list_Occupancy.Add(new HotelLib.Request.HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
            }

            objAvail.Night = Convert.ToInt32((tDate - fDate).TotalDays);

            if (list_Occupancy != null)
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    list_Occupancy = list_Occupancy,
                    DestinationCode = objHotelValuedAvailRQ.DestinationCode,
                    Location = objAvail.Location,
                    CheckIn = objAvail.CheckIn,
                    CheckOut = objAvail.CheckOut,
                    Night = objAvail.Night,
                    HotelCode = objAvail.HotelCode,
                    HotelName = objAvail.HotelName,
                    StarRating = objAvail.StarRating,
                    NationalityCode = objAvail.NationalityCode
                });
            else
                return objSerlizer.Serialize(new { retCode = 0 });
        }

        #region Rate Per Rooms
        [WebMethod(EnableSession = true)]
        public string GetRoomPrice(string Supplier, string HotelId, string CutCode, List<string> RoomId, List<string> RoomDesc, int RoomCount, List<float> _CutPrice, List<int> _RoomNo, List<int> _noRooms)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            float Total = 0;
            Supplier = TooltipManager.GetSupplier(Supplier);
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            Total = Convert.ToSingle(CUT.Common.GetRoomPrice.GetAPIRoomPrice(Supplier, HotelId, CutCode, RoomId, RoomDesc, RoomCount, _CutPrice, _RoomNo, _noRooms));
            //string sTotal = "<span class=\"size20  strong orange \"  style=\"MARGIN-LEFT: 20PX;\" class=\"\" data-toggle=\"tooltip\" data-placement=\"left\" data-container=\"body\"  data-original-title=\"" + TooltipManager.GetCurencyTip(objGlobalDefault.ExchangeRate, PreferedCurrency, Total) + "\"><i class=\"" + ExchangeRateManger.GetCurrency() + "\" style=\"font-size: 11px;\"></i><b > " + HTMLCommonAvailBuilder.NumberWithComma(Math.Round((Decimal)(Total), 2, MidpointRounding.AwayFromZero)) + " </b></span>";
            string sTotal = "<span class=\"size20  strong orange \"  style=\"MARGIN-LEFT: 20PX;\" class=\"\" data-toggle=\"tooltip\" data-placement=\"left\" data-container=\"body\"  data-original-title=\"" + TooltipManager.GetCurencyTip(objGlobalDefault.ExchangeRate, PreferedCurrency, Total) + "\">RM <b > " + HTMLCommonAvailBuilder.NumberWithComma(Math.Round((Decimal)(Total), 2, MidpointRounding.AwayFromZero)) + " </b></span>";
            if (Total != 0)
                return objSerlizer.Serialize(new { retCode = 1, Total = sTotal });
            else
                return objSerlizer.Serialize(new { retCode = 0, Total = 0 });
        }
        #endregion
        [WebMethod(EnableSession = true)]
        public string GenerateRoomDetails(string HotelCode, string Supplier)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            string RoomDetails = "";

            List<object> ListRateGroup = new List<object>();

            objSerlizer.MaxJsonLength = Int32.MaxValue;
            ListRateGroup = b2cDesign.GenerateRoomDetails(HotelCode, Supplier);
            if (ListRateGroup != null)
                return objSerlizer.Serialize(new { retCode = 1, ListRateGroup = ListRateGroup });
            else
                return objSerlizer.Serialize(new { retCode = 0, ListRateGroup = "" });

        }


        #region HotelList commented
        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public void HotelList(string session)
        //{
        //    GlobalDefault objGlobalDefault = null;
        //    string jsonString = "";
        //    bool bResponse_MGH = false;
        //    JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
        //    objSerlizer.MaxJsonLength = Int32.MaxValue;
        //    List<CommonLib.Response.Facility> lstFacility = new List<CommonLib.Response.Facility>();
        //    List<CommonLib.Response.Category> lstCategory = new List<CommonLib.Response.Category>();
        //    List<CommonLib.Response.Location> lstLocation = new List<CommonLib.Response.Location>();
        //    if (com.Common.Session(out objGlobalDefault))
        //    {
        //        //................................HotelBeds..........B..............................................//
        //        CUT.Models.StatusCode objStatusCode;
        //        bool bResponse = CUT.Common.XMLHelper.AvailRequest(session, out objStatusCode);
        //        LogManager.Add(0, objStatusCode.RequestHeader, objStatusCode.Request, objStatusCode.ResponseHeader, objStatusCode.Response, objGlobalDefault.sid, objStatusCode.Status);

        //        // CUT.Models.MGHStatusCode objMGHStatusCode;
        //        //bResponse_MGH = CUT.Common.MghXMLHelper.AvailRequest(session, out objMGHStatusCode);
        //        //Session["MGHStatus"] = objMGHStatusCode;
        //        //LogManager.Add(0, objMGHStatusCode.RequestHeader, objMGHStatusCode.Request, objMGHStatusCode.ResponseHeader, objMGHStatusCode.Response, objGlobalDefault.sid, objMGHStatusCode.Status);

        //        //Dotw Response / /
        //        CUT.Models.DOTWStatusCode objDOTStatus;
        //        string citycode = "2014";
        //        DataTable dtResult;
        //        string[] array_input = session.Split('_');
        //        string[] Country = array_input[1].Split(',');
        //        string cty = Country[0];
        //        DBHelper.DBReturnCode retCode = DOTWManager.GetDOTWCityCode(cty, out dtResult);
        //        if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //        {
        //            citycode = dtResult.Rows[0]["code"].ToString();
        //        }
        //        bool bResponse_Dotw = CUT.Common.DOTWXMLHelper.AvailRequest(session, citycode, out objDOTStatus);
        //        // end respons/

        //        CUT.Models.CommonStatusCode objCommonStatusCode;
        //        bool bResponse_Common = CUT.Common.CommonXMLHelper.AvailRequest(session, out objCommonStatusCode);
        //        //objStatusCode = new Models.CommonStatusCode {  DisplayRequest = objMGHAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
        //        int count;
        //        int countCommon;
        //        DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        List<HotelLib.Response.HotelDetail> List_HotelDetail;
        //        float min_price;
        //        float max_price;
        //        int m_counthotel;
        //        float min_price_Common;
        //        float max_price_Common;
        //        int m_counthotel_Common;
        //        Models.Hotel objHotel = new Models.Hotel { };
        //        Models.MGHHotel objMGHHotel;
        //        if (bResponse)
        //        {
        //            count = objStatusCode.DisplayRequest.Night;

        //            Common.ParseAvailResponse objParseAvailResponse = new com.ParseAvailResponse(objStatusCode.Response, count, objUser.sid, objStatusCode.DisplayRequest.Room, objStatusCode.Occupancy);
        //            //List<HotelLib.Response.HotelDetail> List_HotelDetail = objParseAvailResponse.GetServiceHotel();
        //            List_HotelDetail = objParseAvailResponse.GetServiceHotel();
        //            min_price = List_HotelDetail.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //            max_price = List_HotelDetail.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //            m_counthotel = List_HotelDetail.Where(data => data.CUTPrice == min_price).Count();
        //            objHotel = new Models.Hotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseAvailResponse.Facility, HotelDetail = List_HotelDetail, CountHotel = m_counthotel, Location = objParseAvailResponse.Location, Category = objParseAvailResponse.Category, DisplayRequest = objStatusCode.DisplayRequest };
        //            Session["Avail"] = objHotel;
        //            //jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
        //        }
        //        //if (bResponse_MGH)
        //        //{
        //        //    count = objMGHStatusCode.DisplayRequest.Night;
        //        //    Common.ParseMGHResponse objParseMGHResponse = new com.ParseMGHResponse();
        //        //    bool bParse = objParseMGHResponse.ParseXML(objMGHStatusCode.Response);
        //        //    List<MGHLib.Response.MGHHotelDetails> List_MGHHotelDetails = objParseMGHResponse.GetServiceHotel();
        //        //    min_price = List_MGHHotelDetails.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //        //    max_price = List_MGHHotelDetails.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //        //    m_counthotel = List_MGHHotelDetails.Where(data => data.CUTPrice == min_price).Count();

        //        //    objMGHHotel = new Models.MGHHotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseMGHResponse.Facility, HotelDetail = List_MGHHotelDetails, CountHotel = m_counthotel, Location = objParseMGHResponse.Location, Category = objParseMGHResponse.Category, DisplayRequest = objMGHStatusCode.DisplayRequest };
        //        //    Session["MGHAvail"] = objMGHHotel;

        //        //    countCommon = objStatusCode.DisplayRequest.Night;
        //        //    Common.ParseCommonResponse objParseCommonResponse = new com.ParseCommonResponse();
        //        //    List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = objParseCommonResponse.GetCommon(objHotel, objMGHHotel, out lstFacility, out lstCategory, out lstLocation);
        //        //    min_price_Common = List_CommonHotelDetails.OrderBy(data => data.Room[0].CancellationPolicy[0].CutRoomAmount).Select(data => data.Room[0].CancellationPolicy[0].CutRoomAmount).FirstOrDefault();
        //        //    max_price_Common = List_CommonHotelDetails.OrderByDescending(data => data.Room[0].CancellationPolicy[0].CutRoomAmount).Select(data => data.Room[0].CancellationPolicy[0].CutRoomAmount).FirstOrDefault();
        //        //    m_counthotel_Common = List_CommonHotelDetails.Where(data => data.Room[0].CancellationPolicy[0].CutRoomAmount == min_price_Common).Count();
        //        //    Models.CommonHotels objCommonHotels = new Models.CommonHotels { MinPrice = min_price_Common, MaxPrice = max_price_Common, CountHotel = m_counthotel_Common, CommonHotelDetails = List_CommonHotelDetails, Facility = lstFacility, Category = lstCategory, Location = lstLocation, DisplayRequest = objCommonStatusCode.DisplayRequest };

        //        //    Session["Common"] = objCommonHotels;
        //        //    //jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
        //        //}
        //        if (bResponse == false && bResponse_MGH == false)
        //        {
        //            jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
        //        }
        //        else
        //            jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1 });


        //        //................................HotelBeds...............E........................................//
        //        //...................................MGH..................B........................................//

        //        //CUT.Models.MGHStatusCode objMGHStatusCode;
        //        //bool bResponse_MGH = CUT.Common.MghXMLHelper.AvailRequest(session, out objMGHStatusCode);
        //        //Session["MGHStatus"] = objMGHStatusCode;
        //        //LogManager.Add(0, objMGHStatusCode.RequestHeader, objMGHStatusCode.Request, objMGHStatusCode.ResponseHeader, objMGHStatusCode.Response, objGlobalDefault.sid, objMGHStatusCode.Status);
        //        //if (bResponse_MGH)
        //        //{
        //        //    int count = objMGHStatusCode.DisplayRequest.Night;
        //        //    DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

        //        //    Common.ParseMGHResponse objParseMGHResponse = new com.ParseMGHResponse();
        //        //    //MGHHotelDetails[] objMGHHotelDetails;
        //        //    bool bParse = objParseMGHResponse.ParseXML(objMGHStatusCode.Response);
        //        //    List<MGHLib.Response.MGHHotelDetails> List_MGHHotelDetails = objParseMGHResponse.GetServiceHotel();
        //        //    float min_price = List_MGHHotelDetails.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //        //    float max_price = List_MGHHotelDetails.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
        //        //    int m_counthotel = List_MGHHotelDetails.Where(data => data.CUTPrice == min_price).Count();
        //        //    Models.MGHHotel objMGHHotel = new Models.MGHHotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseMGHResponse.Facility, HotelDetail = List_MGHHotelDetails, CountHotel = m_counthotel, Location = objParseMGHResponse.Location, Category = objParseMGHResponse.Category, DisplayRequest = objMGHStatusCode.DisplayRequest };
        //        //    Session["MGHAvail"] = objMGHHotel;
        //        //    jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1 });
        //        //}
        //        //else
        //        //    jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });

        //        //...................................MGH...................E.......................................//
        //    }
        //    HttpContext.Current.Response.ContentType = "application/json";
        //    HttpContext.Current.Response.Write(jsonString);
        //    HttpContext.Current.Response.End();
        //}
        #endregion HotelList commented

        #endregion Hotel Parsing

        #region Country

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"City\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string GetNationalityCOR()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityManager.GetNationalityCOR(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"NationalityMaster\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCityCode(string Description)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityManager.GetCityCode(Description, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityCode\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion Country

        #region Hotel Filter

        //[WebMethod(EnableSession = true)]
        //public string HotelFilter(float MinPrice, float MaxPrice, List<Int32> HotelRating, List<string> HotelFacility, List<string> HotelLocation, string HotelName, Int32 SortBy, Int32 PageNo, string currency, int Categorys)
        //{
        //    Session["OtherCurrency"] = currency;
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    js.MaxJsonLength = Int32.MaxValue;
        //    Models.Hotel objHotel = (Models.Hotel)Session["Avail"];
        //    Common.HTMLAvailBuilder objHTMLAvailBuilder = new com.HTMLAvailBuilder();
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail = objHotel.HotelDetail;
        //    List_HotelDetail_Filter = FilterPrice(MinPrice, MaxPrice, objHotel.HotelDetail);
        //    if (HotelRating.Count > 0)
        //        List_HotelDetail_Filter = FilterHotelRating(HotelRating.ToArray(), List_HotelDetail_Filter);
        //    else
        //    {
        //        string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(0, objHotel.HotelDetail.Count, objHotel.DisplayRequest.Location);
        //        return js.Serialize(new { retCode = 0, Paging = "", Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", FilterTrip = m_filtertrip });
        //    }
        //    if (Categorys != null)
        //    {
        //        List_HotelDetail_Filter = FilterHotelRatingByCategory(Categorys, HotelRating.ToArray(), List_HotelDetail_Filter);
        //    }
        //    if (HotelFacility.Count > 0)
        //        List_HotelDetail_Filter = FilterHotelFacility(HotelFacility.ToArray(), List_HotelDetail_Filter);
        //    else
        //    {
        //        string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(0, objHotel.HotelDetail.Count, objHotel.DisplayRequest.Location);
        //        return js.Serialize(new { retCode = 0, Paging = "", Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", FilterTrip = m_filtertrip });
        //        //List_HotelDetail_Filter = FilterHotelFacility(HotelFacility.ToArray(), List_HotelDetail_Filter);
        //        //if (List_HotelDetail_Filter.Count > 0)
        //        //{
        //        //    string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(List_HotelDetail_Filter.Count, objHotel.HotelDetail.Count, objHotel.DisplayRequest.Location);
        //        //}
        //        //else
        //        //{
        //        //}
        //    }
        //    if (HotelLocation.Count > 0)
        //        List_HotelDetail_Filter = FilterHotelLocation(HotelLocation.ToArray(), List_HotelDetail_Filter);
        //    else
        //    {
        //        string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(0, objHotel.HotelDetail.Count, objHotel.DisplayRequest.Location);
        //        return js.Serialize(new { retCode = 0, Paging = "", Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", FilterTrip = m_filtertrip });
        //    }
        //    List_HotelDetail_Filter = FilterHotelName(HotelName, List_HotelDetail_Filter);
        //    List_HotelDetail_Filter = SortHotel(SortBy, List_HotelDetail_Filter);
        //    Common.Pager objPager = new Common.Pager();
        //    objPager.currentPage = PageNo;
        //    objPager.TototalRowsCount = List_HotelDetail_Filter.Count;

        //    if (List_HotelDetail_Filter.Count > 0)
        //    {
        //        string m_Paging = objPager.Generate();
        //        List<HotelLib.Response.HotelDetail> List_Hotel = List_HotelDetail_Filter.Skip((PageNo - 1) * objPager.PageSize).Take(objPager.PageSize).ToList();
        //        string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(List_Hotel.Count, objPager.TototalRowsCount, objHotel.DisplayRequest.Location);
        //        string m_Content = objHTMLAvailBuilder.GenerateMainContainer(List_Hotel, currency);
        //        return js.Serialize(new { retCode = 1, Paging = m_Paging, Content = m_Content, FilterTrip = m_filtertrip });
        //    }
        //    else
        //    {
        //        string m_filtertrip = objHTMLAvailBuilder.GenerateFilterTrip(0, objHotel.HotelDetail.Count, objHotel.DisplayRequest.Location);
        //        return js.Serialize(new { retCode = 0, Paging = "", Content = "<span class=\"opensans grey size24\" style=\"margin: 100px;\">No hotels were found for this location!</span>", FilterTrip = m_filtertrip });
        //    }
        //}

        //private List<HotelLib.Response.HotelDetail> FilterPrice(float MinPrice, float MaxPrice, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (MinPrice > 0 && MaxPrice > 0)
        //    {
        //        foreach (HotelLib.Response.HotelDetail objHotel in List_HotelDetail)
        //        {
        //            bool IsAdd = false;
        //            List<HotelLib.Response.RoomOccupancy> List_Rooms = objHotel.sRooms;
        //            foreach (HotelLib.Response.RoomOccupancy objAvailableRooms in List_Rooms)
        //            {
        //                if (objAvailableRooms.CUTRoomPrice >= MinPrice && objAvailableRooms.CUTRoomPrice <= MaxPrice)
        //                {
        //                    IsAdd = true;
        //                    break;
        //                }
        //            }
        //            if (IsAdd)
        //                List_HotelDetail_Filter.Add(objHotel);
        //        }
        //    }
        //    else
        //    {
        //        List_HotelDetail_Filter = List_HotelDetail;
        //    }
        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> FilterHotelRating(int[] HotelRating, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (HotelRating.Length > 0)
        //        List_HotelDetail_Filter = List_HotelDetail.Where(data => HotelRating.Contains(data.CategoryType)).ToList();
        //    else
        //        List_HotelDetail_Filter = List_HotelDetail;

        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> FilterHotelRatingByCategory(int Categorys, int[] HotelRating, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (Categorys == 0)
        //    {
        //        var newArr = new int[HotelRating.Length];
        //        int counter = 1;
        //        for (int i = 0; i < HotelRating.Length; i++)
        //        {
        //            newArr[i] = HotelRating[HotelRating.Length - counter];
        //            counter++;
        //        }
        //        if (newArr.Length > 0)
        //            // List_HotelDetail_Filter = List_HotelDetail.Where(data => newArr.Contains(data.CategoryType)).ToList();
        //            List_HotelDetail_Filter = List_HotelDetail.Where(data => newArr.Contains(data.CategoryType)).OrderByDescending(data => data.CategoryType).ToList();
        //        else
        //            List_HotelDetail_Filter = List_HotelDetail;
        //    }
        //    else
        //    {
        //        if (HotelRating.Length > 0)
        //            List_HotelDetail_Filter = List_HotelDetail.Where(data => HotelRating.Contains(data.CategoryType)).ToList();
        //        else
        //            List_HotelDetail_Filter = List_HotelDetail;

        //    }

        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> FilterHotelFacility(string[] HotelFacility, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (HotelFacility.Length > 0)
        //    {
        //        foreach (HotelLib.Response.HotelDetail objHotelDetail in List_HotelDetail)
        //        {
        //            List<string> List_Facility = objHotelDetail.Facility;
        //            bool IsAdd = false;
        //            foreach (string facility in HotelFacility)
        //            {
        //                var FacilityExist = List_Facility.Where(data => data == facility).ToList();
        //                if (FacilityExist != null && FacilityExist.Count > 0)
        //                {
        //                    IsAdd = true;
        //                    break;
        //                }
        //            }
        //            if (IsAdd)
        //                List_HotelDetail_Filter.Add(objHotelDetail);
        //        }
        //    }
        //    else
        //    {
        //        List_HotelDetail_Filter = List_HotelDetail;
        //        //foreach (HotelLib.Response.HotelDetail objHotelDetail in List_HotelDetail)
        //        //{
        //        //    if (objHotelDetail.Facility.Count == 0)
        //        //    {
        //        //        List_HotelDetail_Filter.Add(objHotelDetail);
        //        //    }
        //        //}           
        //    }
        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> FilterHotelLocation(string[] HotelLocation, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (HotelLocation.Length > 0)
        //    {
        //        foreach (HotelLib.Response.HotelDetail objHotelDetail in List_HotelDetail)
        //        {
        //            List<string> List_Location = objHotelDetail.Location;
        //            bool IsAdd = false;
        //            foreach (string location in HotelLocation)
        //            {
        //                var LocationExist = List_Location.Where(data => data == location).ToList();
        //                if (LocationExist != null && LocationExist.Count > 0)
        //                {
        //                    IsAdd = true;
        //                    break;
        //                }
        //            }
        //            if (IsAdd)
        //                List_HotelDetail_Filter.Add(objHotelDetail);
        //        }

        //    }
        //    else
        //    {
        //        List_HotelDetail_Filter = List_HotelDetail;
        //    }
        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> FilterHotelName(string HotelName, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (HotelName.Length > 0)
        //    {
        //        List_HotelDetail_Filter = List_HotelDetail.Where(data => data.Name.ToLower().Contains(HotelName.ToLower())).ToList();
        //    }
        //    else
        //    {
        //        List_HotelDetail_Filter = List_HotelDetail;
        //    }
        //    return List_HotelDetail_Filter;
        //}

        //private List<HotelLib.Response.HotelDetail> SortHotel(int SortBy, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Filter = new List<HotelLib.Response.HotelDetail>();
        //    if (SortBy == 1)
        //        List_HotelDetail_Filter = List_HotelDetail.OrderBy(data => data.Price).ToList();
        //    else if (SortBy == 2)
        //        List_HotelDetail_Filter = List_HotelDetail.OrderByDescending(data => data.Price).ToList();
        //    else if (SortBy == 3)
        //        List_HotelDetail_Filter = List_HotelDetail.OrderBy(data => data.Name).ToList();
        //    else if (SortBy == 4)
        //        List_HotelDetail_Filter = List_HotelDetail.OrderByDescending(data => data.Name).ToList();
        //    else
        //        List_HotelDetail_Filter = List_HotelDetail;
        //    return List_HotelDetail_Filter;
        //}

        #endregion Hotel Filter

        #region Hotel Sorting

        //[WebMethod(EnableSession = true)]
        //public string HotelSort(string HotelRating, string name, string price)
        //{
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    js.MaxJsonLength = Int32.MaxValue;
        //    List<HotelLib.Response.HotelDetail> List_HotelDetail_Sort = new List<HotelLib.Response.HotelDetail>();
        //    if (Session["FilterHotel"] == null)
        //    {
        //        Models.Hotel objHotel = (Models.Hotel)Session["Avail"];
        //        List_HotelDetail_Sort = objHotel.HotelDetail;
        //    }
        //    else
        //    {
        //        List_HotelDetail_Sort = (List<HotelLib.Response.HotelDetail>)Session["FilterHotel"];
        //    }
        //    List_HotelDetail_Sort = SortByRating(HotelRating, List_HotelDetail_Sort);
        //    List_HotelDetail_Sort = SortByNamePrice(name, price, List_HotelDetail_Sort);
        //    if (List_HotelDetail_Sort.Count > 0)
        //    {
        //        Common.HTMLAvailBuilder objHTMLAvailBuilder = new com.HTMLAvailBuilder();
        //        string m_Content = objHTMLAvailBuilder.GenerateMainContainer(List_HotelDetail_Sort.Take(10).Skip(0).ToList(), "INR");
        //        return js.Serialize(new { retCode = 1, Content = m_Content });
        //    }
        //    else
        //    {
        //        return js.Serialize(new { retCode = 0 });
        //    }
        //}

        //private List<HotelLib.Response.HotelDetail> SortByRating(string HotelRating, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    if (!String.IsNullOrEmpty(HotelRating))
        //        List_HotelDetail = List_HotelDetail.Where(data => data.UserRating == float.Parse(HotelRating)).ToList();
        //    return List_HotelDetail;SSSS
        //}

        //private List<HotelLib.Response.HotelDetail> SortByNamePrice(string Name, string Price, List<HotelLib.Response.HotelDetail> List_HotelDetail)
        //{
        //    if (!String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Price))
        //    {
        //        if (Name == "A" && Price == "A")
        //            List_HotelDetail = List_HotelDetail.OrderBy(data => data.Name).OrderBy(data => data.Price).ToList();
        //        else if (Name == "A" && Price == "D")
        //            List_HotelDetail = List_HotelDetail.OrderBy(data => data.Name).OrderByDescending(data => data.Price).ToList();
        //        else if (Name == "D" && Price == "A")
        //            List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.Name).OrderBy(data => data.Price).ToList();
        //        else
        //            List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.Name).OrderByDescending(data => data.Price).ToList();
        //    }
        //    else if (!String.IsNullOrEmpty(Name) && String.IsNullOrEmpty(Price))
        //    {
        //        if (Name == "A")
        //            List_HotelDetail = List_HotelDetail.OrderBy(data => data.Name).ToList();
        //        else
        //            List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.Name).ToList();
        //    }
        //    else if (!String.IsNullOrEmpty(Price) && String.IsNullOrEmpty(Name))
        //    {
        //        if (Price == "A")
        //            List_HotelDetail = List_HotelDetail.OrderBy(data => data.Price).ToList();
        //        else
        //            List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.Price).ToList();
        //    }
        //    return List_HotelDetail;
        //}

        #endregion Hotel Sorting



        #region Common Hotel Filter

        [WebMethod(EnableSession = true)]
        public string CommonHotelFilter(float MinPrice, float MaxPrice, List<string> HotelSupplier, List<Int32> HotelRating, List<string> HotelFacility, List<string> HotelLocation, string HotelName, Int32 SortBy, Int32 PageNo, string currency, string Preferred, string latitude, string longitude)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            List<CommonMap> MapList = new List<CommonMap>();
            string m_filtertrip = string.Empty;
            string m_Paging = string.Empty;
            string m_Content = string.Empty;
            List<CommonLib.Response.CommonHotelDetails> List_HotelDetail_Filter =
                                                   HotelFilterManager.GetFilteredHotel(
                                                   MinPrice, MaxPrice,
                                                   HotelSupplier, HotelRating,
                                                   HotelFacility, HotelLocation,
                                                   HotelName, SortBy, PageNo,
                                                   currency, Preferred,
                                                   out m_filtertrip, out MapList,
                                                   out m_Paging, out m_Content, latitude, longitude);
            CUT.Models.CommonHotels objCommonHotel = new CommonHotels();
            object arrHotels = b2cDesign.GetHotels(objCommonHotel, 1);
            return js.Serialize(new { retCode = 1, arrHotels = arrHotels });
        }
        #endregion Common Hotel Filter


        ////////////////

        ///////////////






        #region Common Hotel Sorting

        [WebMethod(EnableSession = true)]
        public string CommonHotelSort(string HotelRating, string name, string price)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            List<CommonLib.Response.CommonHotelDetails> List_HotelDetail_Sort = new List<CommonLib.Response.CommonHotelDetails>();
            if (Session["FilterHotel"] == null)
            {
                CUT.Models.CommonHotels objHotel = (CUT.Models.CommonHotels)Session["Common"];
                List_HotelDetail_Sort = objHotel.CommonHotelDetails;
            }
            else
            {
                List_HotelDetail_Sort = (List<CommonLib.Response.CommonHotelDetails>)Session["FilterHotel"];
            }
            List_HotelDetail_Sort = CommonSortByRating(HotelRating, List_HotelDetail_Sort);
            List_HotelDetail_Sort = CommonSortByNamePrice(name, price, List_HotelDetail_Sort);
            if (List_HotelDetail_Sort.Count > 0)
            {
                com.HTMLCommonAvailBuilder objHTMLAvailBuilder = new com.HTMLCommonAvailBuilder();
                string m_Content = objHTMLAvailBuilder.GenerateMainContainer(List_HotelDetail_Sort.Take(10).Skip(0).ToList(), "INR");
                return js.Serialize(new { retCode = 1, Content = m_Content });
            }
            else
            {
                return js.Serialize(new { retCode = 0 });
            }
        }

        private List<CommonLib.Response.CommonHotelDetails> CommonSortByRating(string HotelRating, List<CommonLib.Response.CommonHotelDetails> List_HotelDetail)
        {
            if (!String.IsNullOrEmpty(HotelRating))
                List_HotelDetail = List_HotelDetail.Where(data => data.UserRating == float.Parse(HotelRating)).ToList();
            return List_HotelDetail;
        }

        private List<CommonLib.Response.CommonHotelDetails> CommonSortByNamePrice(string Name, string Price, List<CommonLib.Response.CommonHotelDetails> List_HotelDetail)
        {
            if (!String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Price))
            {
                if (Name == "A" && Price == "A")
                    List_HotelDetail = List_HotelDetail.OrderBy(data => data.HotelName).OrderBy(data => data.CUTPrice).ToList();
                else if (Name == "A" && Price == "D")
                    List_HotelDetail = List_HotelDetail.OrderBy(data => data.HotelName).OrderByDescending(data => data.CUTPrice).ToList();
                else if (Name == "D" && Price == "A")
                    List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.HotelName).OrderBy(data => data.CUTPrice).ToList();
                else
                    List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.HotelName).OrderByDescending(data => data.CUTPrice).ToList();
            }
            else if (!String.IsNullOrEmpty(Name) && String.IsNullOrEmpty(Price))
            {
                if (Name == "A")
                    List_HotelDetail = List_HotelDetail.OrderBy(data => data.HotelName).ToList();
                else
                    List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.HotelName).ToList();
            }
            else if (!String.IsNullOrEmpty(Price) && String.IsNullOrEmpty(Name))
            {
                if (Price == "A")
                    List_HotelDetail = List_HotelDetail.OrderBy(data => data.CUTPrice).ToList();
                else
                    List_HotelDetail = List_HotelDetail.OrderByDescending(data => data.CUTPrice).ToList();
            }
            return List_HotelDetail;
        }

        #endregion Common Hotel Sorting

        #region Get Hotel Images
        [WebMethod(EnableSession = true)]
        public string GetHotelImages(string HotelCode)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            try
            {
                object ImageSlider = b2cDesign.GelImageSlider(HotelCode);
                return objSerializer.Serialize(new { retCode = 1, Session = 1, Slider = ImageSlider });
            }
            catch
            {
                return objSerializer.Serialize(new { retCode = 0, Session = 1 });
            }


        }
        #endregion

        #region Room Details
        [WebMethod(EnableSession = true)]
        public string GetHotelInfo(string HotelID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            try
            {
                CUT.Models.CommonHotels objCommonHotels = (CUT.Models.CommonHotels)Session["Common"];
                CommonLib.Response.CommonHotelDetails objHotelDetails = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == HotelID).FirstOrDefault();
                if (objHotelDetails == null)
                    throw new Exception("Not math Found");
                string HotelInfo = b2cDesign.GetHotelInfo(objHotelDetails);
                return objSerializer.Serialize(new { retCode = 1, Sesssion = 1, objHotelDetails = objHotelDetails, HotelInfo = HotelInfo });
            }
            catch
            {
                return objSerializer.Serialize(new { retCode = 1, Sesssion = 0 });
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetRoomFacilit(string HotelCode, string CutCode, string RoomTypeId, string RoomDescriptionId, string Supplier)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            try
            {
                string sb = string.Empty;
                if (Supplier == "GTA")
                    CutCode = HotelCode;
                List<string> List_Facilities = HotelDataManager.RoomAminites(CutCode, Supplier, RoomTypeId, RoomDescriptionId);
                #region Room Aminites
                if (List_Facilities.Count != 0)
                {
                    string RoomAmenities1 = "";
                    string RoomAmenities2 = "";
                    string RoomAmenity = "";
                    for (int k = 0; k < List_Facilities.Count; k++)
                    {
                        if (List_Facilities[k] == null)
                        {
                            continue;
                        }
                        RoomAmenity = List_Facilities[k];
                        if (RoomAmenity.Length > 2)
                        {
                            if (k >= 0 && k < 8)
                                RoomAmenities1 += "<li>" + RoomAmenity + "</li>";
                            else if (k >= 8 && k < 12)
                                RoomAmenities2 += "<li>" + RoomAmenity + "</li>";
                            //else if (k >= 8 && k < 12)
                            //    RoomAmenities3 += "<li>" + RoomAmenity + "</li>";
                        }
                    }
                    sb += "<div class='col-md-6'>";
                    sb += "<ul class='checklist'>";
                    sb += RoomAmenities1;
                    sb += "</ul>";
                    sb += "</div>";
                    sb += "<div class='col-md-6'>";
                    sb += "<ul class='checklist'>";

                    sb += RoomAmenities2;
                    sb += "</ul>";
                    sb += "</div>";
                    //RoomAminity +="<div class='col-md-4'>";
                    //RoomAminity +="<ul class='checklist'>";
                    //RoomAminity +=RoomAmenities3;
                    //RoomAminity +="</ul>";
                    //RoomAminity +="</div>";
                }
                else
                {
                    sb = "Not available.";
                }

                #endregion

                return objSerializer.Serialize(new { retCode = 1, Session = 1, Details = sb.ToString() });
            }
            catch
            {
                return objSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region Common Hotel Paging

        [WebMethod(EnableSession = true)]
        public string CommonHotelPager(int PageNo)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            List<CommonLib.Response.CommonHotelDetails> List_HotelDetail_Pager = new List<CommonLib.Response.CommonHotelDetails>();
            if (Session["FilterHotel"] == null)
            {
                CUT.Models.CommonHotels objHotel = (CUT.Models.CommonHotels)Session["Common"];
                List_HotelDetail_Pager = objHotel.CommonHotelDetails.Take(PageNo * 10).Skip((PageNo - 1) * 10).ToList();
            }
            else
            {
                List_HotelDetail_Pager = (List<CommonLib.Response.CommonHotelDetails>)Session["FilterHotel"];
                List_HotelDetail_Pager = List_HotelDetail_Pager.Take(PageNo * 10).Skip((PageNo - 1) * 10).ToList();
            }
            if (List_HotelDetail_Pager.Count > 0)
            {
                com.HTMLCommonAvailBuilder objHTMLAvailBuilder = new com.HTMLCommonAvailBuilder();
                string m_Content = objHTMLAvailBuilder.GenerateMainContainer(List_HotelDetail_Pager, "INR");
                return js.Serialize(new { retCode = 1, Content = m_Content });
            }
            else
            {
                return js.Serialize(new { retCode = 0 });
            }
        }

        #endregion Common Hotel Paging

        [WebMethod(EnableSession = true)]
        public string HotelRoom(string HotelCode, string CutCode, string RoomID, string Supplier, string RoomDescriptionId, string RoomPrice, string noRooms)
        {
            Supplier = TooltipManager.GetSupplier(Supplier);
            if (Supplier.Contains("GRN"))
            {
                HotelCode = HotelCode.Replace("_", "!");
                CutCode = CutCode.Replace("_", "!");
            }
            CUT.Models.CommonHotels objCommonHotels = new CUT.Models.CommonHotels();
            HTMLCommonDetailBuilder objBuilder = new HTMLCommonDetailBuilder();
            objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
            CommonLib.Response.CommonHotelDetails objHotelDetails = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == CutCode).FirstOrDefault();
            //string hotelroom = objBuilder.CommonDetailBuilder(objHotelDetails);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            string HotelDetails = HTMLCommonDetailBuilder.GetCurrentHotel(objHotelDetails);

            List<float> _CutPrice = new List<float>();
            foreach (string CPrice in RoomPrice.Split(','))
            {
                _CutPrice.Add(Convert.ToSingle(CUT.Common.Cryptography.DecryptText(CPrice)));
            }

            string HotelInfo = HTMLCommonDetailBuilder.GetHotelInfo(objHotelDetails);

            string RoomInfo = HTMLCommonDetailBuilder.RoomInfo(objHotelDetails, HotelCode, Supplier, RoomID.Split(',').ToList(), RoomDescriptionId.Split(',').ToList(), _CutPrice);

            string roomrates = HTMLCommonDetailBuilder.GetRateGroups(CutCode, objHotelDetails.List_RateGroup, 0);

            string MapImage = "";
            if (objHotelDetails.Image.Count != 0)
                MapImage = objHotelDetails.Image[0].Url;
            string maps = HTMLCommonDetailBuilder.Map(objHotelDetails.Langitude, objHotelDetails.Latitude, objHotelDetails.HotelName, MapImage);

            string BookedRate = HTMLCommonDetailBuilder.GetRateBrackups(objHotelDetails.HotelId, "");
            BookedRate += "<div class=\"pagecontainer2 opensans needassistancebox hpadding20 mt20\" style=\"padding-bottom: 20px;\">";
            BookedRate += "<a href=\"#\" onclick=\"RedirectBooking('" + HotelCode.Replace("!", "_") + "','" + CutCode.Replace("!", "_") + "','" + RoomID + "','" + RoomDescriptionId + "','" + RoomPrice + "','" + Supplier + "','" + noRooms + "');\" class=\"booknow margtop20 btnmarg\">Book now</a>        <div class=\"clearfix\"></div>";


            return js.Serialize(new
            {
                HotelDetails = HotelDetails,
                HotelInfo = HotelInfo,
                RoomInfo = RoomInfo,
                roomrates = roomrates,
                maps = maps,
                BookedRate = BookedRate
            });
        }
        [WebMethod(EnableSession = true)]
        public string GetRoomDetails(string HotelCode, string CutCode, List<string> RoomID, string Supplier, List<string> RoomDescriptionId, List<float> CUTRoomPrice, List<int> _noRooms)
        //public string GetRoomDetails(DOTWLib.Response.DOTWHotelDetails HotelDetails)
        {
            CUT.HotelHandler obj = new CUT.HotelHandler();
            return obj.GetRoomDetails(HotelCode, CutCode, RoomID, Supplier, RoomDescriptionId, CUTRoomPrice, _noRooms);

        }

        [WebMethod(EnableSession = true)]
        public string CheckForRoomOccupancyChanged(string HotelCode, string CutCode, List<string> RoomID, string Supplier, List<string> RoomDescriptionId, List<float> CUTRoomPrice, List<int> _noRooms)
        {

            #region Method Variables
            bool status = false;

            JavaScriptSerializer js = new JavaScriptSerializer();
            DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails = new DOTWLib.Response.DOTWHotelDetails();
            com.HotelBookingHelperDoTW objBuilderDoTW = new com.HotelBookingHelperDoTW();

            DOTWLib.Response.Room objRoomDoTW = new DOTWLib.Response.Room();
            List<DOTWLib.Response.RoomType> Hotel_RoomType = new List<DOTWLib.Response.RoomType>();

            #endregion

            #region DoTW

            List<CommonLib.Response.Room> objRoomType = new List<CommonLib.Response.Room>();
            status = CUT.Common.GetRoomPrice.CheckForRoomOccupancyChanged(Supplier, CutCode, RoomID, RoomDescriptionId, CUTRoomPrice, out objRoomType);
            //........................
            if (status)
            {
                JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
                objSerlizer.MaxJsonLength = Int32.MaxValue;
                return objSerlizer.Serialize(new { Session = 1, retCode = 1, Hotel_RoomType = objRoomType });
                //return "{\"Session\":\"1\",\"retCode\":\"1\",\"Hotel_RoomType\":\"" + Hotel_RoomType + "\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }

            #endregion DoTW


        }

        [WebMethod(EnableSession = true)]
        public string CheckOtherOption(string HotelCode, string[] RoomID, string Supplier, string[] RoomDescriptionId, string[] CUTRoomPrice, string PvRoomid, string PvRoomDescriptionId, string PVCUTRoomPrice)
        {
            CUT.HotelHandler obj = new CUT.HotelHandler();
            return obj.CheckOtherOption(HotelCode, RoomID, Supplier, RoomDescriptionId, CUTRoomPrice, PvRoomid, PvRoomDescriptionId, PVCUTRoomPrice);
        }


        [WebMethod(EnableSession = true)]
        public string HotelBooking(string HotelCode, string CutCode, string RoomID, string Supplier, string RoomDescriptionId, string RoomPrice, string _noRooms)
        {

            Supplier = TooltipManager.GetSupplier(Supplier);
            #region Method Variables
            DateTime midnight = DateTime.Today.AddHours(24);
            DateTime currenttime = DateTime.Now;
            TimeSpan difference = (midnight - currenttime);
            double HoursLeft = Math.Round(((difference.TotalHours) / 1.00), 4, MidpointRounding.AwayFromZero);
            //string APIType = "HotelBeds";
            string APIType = Supplier;
            object DisplayRequest = "";
            string OtherCurrency = "";
            float amount = 0;
            string TotalCharges = "";
            string TotalPayable = "";
            string AvailableCredit = "";
            string Balance = "";
            string TypeOFRoom = "";
            string holding_message = "";
            string alert_message = "";
            bool bProceedToBooking = false;
            string HotelRequest = "";//objBuilder.GenerateHotelRequest(TypeOFRoom, objHotel.DisplayRequest.Room, objHotel.DisplayRequest.Night, amount);
            string RoomDetails = "";
            string HotelRoomID = "";
            float lowestAmount = 0.0F;
            float AgentCancellationMarkup = 0.0F;
            float AgentRoomMarkup = 0.0F;
            float CancellationAmount = 0.0F;
            float CUTCancellationAmount = 0.0F;
            float CutRoomAmount = 0.0F;
            object HotelDetails = "";
            bool bValid = false;
            string Confirmation = "";
            DateTime Compare = DateTime.Now;

            //float AvailableCreditFloat = 0.0F;
            //object Initiallization
            CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new CUT.DataLayer.MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (CUT.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (CUT.DataLayer.MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];

            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            JavaScriptSerializer js = new JavaScriptSerializer();
            CUT.Models.Hotel objHotel = new CUT.Models.Hotel();
            CUT.Models.MGHHotel objMGHHotel = new CUT.Models.MGHHotel();
            CUT.Models.DoTWHotel objDoTWHotel = new CUT.Models.DoTWHotel();
            CUT.Models.EANHotels objEanHotel = new CUT.Models.EANHotels();

            HotelLib.Response.HotelDetail objHotelDetail = new HotelLib.Response.HotelDetail();
            MGHLib.Response.MGHHotelDetails objMGHHotelDetail = new MGHLib.Response.MGHHotelDetails();
            DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails = new DOTWLib.Response.DOTWHotelDetails();
            EANLib.Response.HotelRoomAvailabilityResponse objExpHotelDetails = new EANLib.Response.HotelRoomAvailabilityResponse();

            com.HotelBookingHelper objBuilder = new com.HotelBookingHelper();
            com.HotelBookingHelperDoTW objBuilderDoTW = new com.HotelBookingHelperDoTW();
            com.ExpediaHotelBookingHelper objBuilderEan = new com.ExpediaHotelBookingHelper();
            CUT.Models.MGHStatusCode objMGHStatusCode = new CUT.Models.MGHStatusCode();

            CUT.Models.CommonHotels objCommonHotels = (CUT.Models.CommonHotels)HttpContext.Current.Session["Common"];
            CommonLib.Response.CommonHotelDetails objHotelDetails = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == CutCode).FirstOrDefault();
            List<float> _CutPrice = new List<float>();
            #endregion
            #region APIType

            #region Common Booking Design
            if (APIType != "Expdia")
            {

                if (Supplier.Contains("GRN"))
                {
                    CutCode = CutCode.Replace("_", "!");
                    HotelCode = HotelCode.Replace("_", "!");
                }

                objHotelDetails = objCommonHotels.CommonHotelDetails.Where(d => d.HotelId == CutCode).FirstOrDefault();
                _CutPrice = new List<float>();
                foreach (string Price in RoomPrice.Split(','))
                {
                    _CutPrice.Add(Convert.ToSingle(CUT.Common.Cryptography.DecryptText(Price)));
                }
                string stat = "";
                bool bstat = true;
                bool nonRefundable = false;
                RoomDetails = "";
                List<object> RoomsDetails = b2cDesign.GenerateDisplayRequest(objHotelDetails, HotelCode, CutCode, Supplier, RoomID.Split(',').ToList(), RoomDescriptionId.Split(',').ToList(), _CutPrice, _noRooms.Split(',').ToList());
                DisplayRequest = b2cDesign.GetRateBrackups(objHotelDetails.HotelId, objHotelDetails.Supplier);
                Confirmation = HtmlCommonBookingHelper.GetHotelDetails(CutCode) + HtmlCommonBookingHelper.ConfirmationDetails(CutCode, Supplier);
                HotelDetails = b2cDesign.GetCurrentHotel(objHotelDetails);
                Compare = CUT.Common.GetRoomPrice.GetHoldDate(HotelCode, CutCode, Supplier, RoomID.Split(',').ToList(), RoomDescriptionId.Split(',').ToList(), _CutPrice, out nonRefundable);
                AvailableCredit = HtmlCommonBookingHelper.GetAvailableCredit(out stat, out bstat);
                TotalCharges = Convert.ToSingle(CUT.Common.GetRoomPrice.GetTotalCharge(Supplier, HotelCode, CutCode, RoomID.Split(',').ToList(), RoomDescriptionId.Split(',').ToList(), _CutPrice, _noRooms.Split(',').ToList())).ToString();
                Balance = Math.Round((Decimal)((float.Parse(AvailableCredit) - float.Parse(TotalCharges)) / 1.00), 2, MidpointRounding.AwayFromZero).ToString();
                if (float.Parse(Balance) < 0)
                    Balance = "";
                TypeOFRoom = String.Empty;
                holding_message = "";
                alert_message = "";
                bProceedToBooking = false;
                // Compare = CUT.Common.GetRoomPrice.GetHoldDate(HotelCode, RoomID.Split(',').ToList(), RoomDescriptionId.Split(',').ToList(), _CutPrice);
                if (Compare > currenttime && nonRefundable == false)
                {
                    if (currenttime.AddHours(48 + HoursLeft).AddSeconds(-1) > Compare)
                    {
                        holding_message = "This booking will be on HOLD till: " + (currenttime.AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "<input type=\"hidden\" id=\"hdnholdtime\" value=\"" + (currenttime.AddHours(48 + HoursLeft).AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "\" />";// till " + DateTime.Now.AddDays(2);
                        alert_message = " You can hold this booking till " + Compare.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + ".";
                        bProceedToBooking = true;
                    }
                    else
                    {
                        holding_message = "This booking will be on HOLD till: " + (Compare.AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "<input type=\"hidden\" id=\"hdnholdtime\" value=\"" + (Compare.AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "\" />";// till " + DateTime.Now.AddDays(2);
                        alert_message = "You can hold this booking till " + Compare.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + ".";
                        bProceedToBooking = true;
                    }
                }
                //else if (Compare < currenttime.AddHours(48) && Compare >= currenttime && nonRefundable == false)
                //{
                //    holding_message = "This booking will be on HOLD till: " + (currenttime.AddHours((Compare - currenttime).TotalHours).AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "<input type=\"hidden\" id=\"hdnholdtime\" value=\"" + (currenttime.AddHours((Compare - currenttime).TotalHours).AddSeconds(-1)).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "\" />";// till " + DateTime.Now.AddDays(2);
                //    alert_message = "You can hold this booking till " + Compare.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + ".";
                //    bProceedToBooking = true;
                //}
                else if (nonRefundable == true)
                {
                    holding_message = "<input type=\"hidden\" id=\"hdnholdtime\" value=\"" + DateTime.Now.ToString("dd/MM/yyy HH:mm") + "\" />";
                    alert_message = "Time limit is insufficient! You cannot hold this booking.";
                    bProceedToBooking = true;
                }
                return js.Serialize(new { RoomsDetails = RoomsDetails, HotelDetails = HotelDetails, DisplayRequest = DisplayRequest, HoldingPolicy = holding_message, AlertMessage = alert_message, HCode = HotelCode, RCode = RoomID, HotelRoom = HotelRoomID, ProceedToBooking = bProceedToBooking, TotalPayable = TotalPayable, ConfirmationDetails = Confirmation });
                //return js.Serialize(new { Request = DisplayRequest, AvailRequest = objCommonHotels.DisplayRequest, HotelName = objHotelDetails.HotelName, HRequest = HotelRequest, RoomDetail = RoomDetails, Amount = amount, AgentRoomMarkup = AgentRoomMarkup, TotalAmount = TotalCharges, ServiceTaxPer = objMarkupsAndTaxes.PerServiceTax, AvailableCredit = AvailableCredit, AvailableBalance = Balance, HoldingPolicy = holding_message, AlertMessage = alert_message, HCode = HotelCode, RCode = RoomID, HotelRoom = HotelRoomID, ProceedToBooking = bProceedToBooking, TotalPayable = TotalPayable, HotelDetails = HotelDetails, ConfirmationDetails = Confirmation });
            }
            #endregion
            #endregion
            js.MaxJsonLength = Int32.MaxValue;
            return js.Serialize(new { Request = DisplayRequest, AvailRequest = objMGHHotel.DisplayRequest, HotelName = objMGHHotelDetail.HotelName, HRequest = HotelRequest, RoomDetail = RoomDetails, Amount = amount, AgentRoomMarkup = AgentRoomMarkup, TotalAmount = TotalCharges, ServiceTaxPer = objMarkupsAndTaxes.PerServiceTax, AvailableCredit = AvailableCredit, AvailableBalance = Balance, HoldingPolicy = holding_message, AlertMessage = alert_message, HCode = HotelCode, RCode = RoomID, HotelRoom = HotelRoomID, ProceedToBooking = bProceedToBooking, TotalPayable = TotalPayable, ConfirmationDetails = Confirmation });

        }

        #region Hotel Confirm

        [WebMethod(EnableSession = true)]
        public string GetAvaiCredit(float Price, string HotelCode, string CutID, string Supplier, List<string> RoomID, List<string> RoomDescriptionId, string _Price, List<int> _noRooms, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<CommonLib.Request.Guest> List_Guest, string ServiceTax, string HoldTime)
        {
            CUT.HotelHandler OBJ = new CUT.HotelHandler();
            string JSON = OBJ.GetAvaiCredit(Price, HotelCode, CutID, Supplier, RoomID, RoomDescriptionId, _Price, _noRooms, AgencyRefernce, MobileNo, Remark, BookingStatus, Email, List_Guest, ServiceTax, HoldTime);
            return JSON;
        }

        //[WebMethod(EnableSession = true)]
        //public string ConfirmBooking(string HotelCode, string CutID, string Supplier, List<string> RoomID, List<string> RoomDescriptionId, string _Price, List<int> _noRooms, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<CommonLib.Request.Guest> List_Guest, string ServiceTax, string HoldTime, string ConfirmationId)
        //{
        //    if (Supplier.Contains("GRN"))
        //    {
        //        HotelCode = HotelCode.Replace("_", "!");
        //        CutID = CutID.Replace("_", "!");
        //    }
        //    //CommonLib.Response.CommonHotelDetails objCommHotelDetail = ((List<CommonLib.Response.CommonHotelDetails>)Session["Common"]).Single(d => d.HotelId == HotelCode);
        //    _Price = _Price.Replace(" ", "+");
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    string APIType = Supplier;
        //    bool IsValidRoomID = true;
        //    ReservationDetails objReserDetails = new ReservationDetails();
        //    objReserDetails = (ReservationDetails)Session["ConfirmationData" + ConfirmationId];
        //    if (objReserDetails == null)
        //        return js.Serialize(new { session = 0, retCode = 0, Message = "Invalid Booking" });
        //    #region Variables & object
        //    CUTUK.DataLayer.HotelManager.objReserDetails = new ReservationDetails();
        //    CUTUK.DataLayer.HotelManager.objReserDetails = objReserDetails;
        //    string AffilateCode = "";
        //    string ReservationId = "";
        //    float ServiceCharges = 0.0F;
        //    string ComparedCurrency = "";
        //    string BookingDate;
        //    string CurrentStatus = "V";
        //    Int64 Sid = 0;
        //    //CUT.Occupancy objOccupancy = new Occupancy();
        //    // objects--common

        //    GlobalDefault objGlobalDefault = null;
        //    CUT.DataLayer.AgentDetailsOnAdmin objGlobalAgent = null;
        //    #endregion
        //    js = new JavaScriptSerializer();
        //    string errorMessage = "";
        //    //DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
        //    DBHelper.DBReturnCode retCode = CUT.Agent.DataLayer.AccountManager.CheckAvilCreditByBooking();
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        objReserDetails.HoldingTime = Convert.ToDateTime(HoldTime);
        //        #region Booking By Supplier
        //        try
        //        {
        //            objReserDetails.List_Guest = List_Guest;
        //            //BookingStatus = objReserDetails.BookingStatus;
        //            #region Save BOOKING lOG
        //            retCode = HotelBookingTxnManager.SaveCommonRoomLog(objReserDetails, List_Guest);
        //            #endregion

        //            #region Common Booking Methods
        //            #region Transaction
        //            var List_Customer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
        //            var LeadingGuestName = List_Customer[0].Name + List_Customer[0].LastName;
        //            using (TransactionScope objTransactionScope0 = new TransactionScope())
        //            {
        //                retCode = HotelBookingTxnManager.HotelBookingTxn(objReserDetails.ReservationId, objReserDetails.BookingDate, objReserDetails.TotalAmount, ServiceCharges, objReserDetails.TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, objReserDetails.Supplier, objReserDetails.SupplierTotalAmount, objReserDetails.AgentComm, objReserDetails.Cut_Comm, "", objReserDetails.BookingStatus);
        //                if (retCode != DBHelper.DBReturnCode.SUCCESS)
        //                {
        //                    objTransactionScope0.Dispose();
        //                    throw new Exception("Error while adding transaction details");
        //                }
        //                else
        //                {
        //                    objTransactionScope0.Complete();
        //                }
        //            }
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {
        //                using (TransactionScope objTransactionScope1 = new TransactionScope())
        //                {
        //                    DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
        //                    if (objReserDetails.Supplier != "MGH" && objReserDetails.Supplier != "GRN")
        //                        retcode = HotelReservationManager.HotelReservationAddUpdate(Sid, objReserDetails.sHotelCode.Split('_')[0], objReserDetails.Supplier, objReserDetails.objHotelDetails.HotelName, objReserDetails.DisplayRequest.Location, objReserDetails.sCheckIn, objReserDetails.sCheckOut, objReserDetails.DisplayRequest.Night, objReserDetails.DisplayRequest.Room, objReserDetails.TotalPayable, objReserDetails.ComparedCurrency, objReserDetails.DisplayRequest.Adult, objReserDetails.DisplayRequest.Child, objReserDetails.ChargeDate, objReserDetails.objHotelDetails.Description, objReserDetails.DeadLineDate, out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, objReserDetails.SupplierTotalAmount, MobileNo, Email, Remark, HoldTime);
        //                    else
        //                        retcode = HotelReservationManager.HotelReservationAddUpdateMGH(Sid, ConfirmationId, objReserDetails.sHotelCode, objReserDetails.objHotelDetails.HotelName, objReserDetails.DisplayRequest.Location, objReserDetails.sCheckIn, objReserDetails.DisplayRequest.CheckOut, objReserDetails.DisplayRequest.Night, objReserDetails.DisplayRequest.Room, objReserDetails.TotalPayable, objReserDetails.ComparedCurrency, objReserDetails.DisplayRequest.Adult, objReserDetails.DisplayRequest.Child, objReserDetails.ChargeDate.ToString(), objReserDetails.objHotelDetails.Description, objReserDetails.DeadLineDate.ToString(), out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, objReserDetails.SupplierTotalAmount, MobileNo, Email, Remark, objReserDetails.objHotelDetails.Latitude, objReserDetails.objHotelDetails.Langitude, 0, 0, new List<CUT.tbl_OfferRate>(), HoldTime);
        //                    objReserDetails.AffilateCode = AffilateCode;
        //                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //                    {

        //                        CurrentStatus = "Reservation Details Added";


        //                        retCode = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, objReserDetails.DumReservation);
        //                        objTransactionScope1.Complete();
        //                    }
        //                    else if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        CurrentStatus = "Error while Inserting Reservation Details";
        //                        objTransactionScope1.Dispose();
        //                        retCode = HotelBookingTxnManager.HotelBookingTxnCancel(ReservationId, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), objReserDetails.TotalAmount, ServiceCharges, objReserDetails.TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "Hotelbeds", objReserDetails.SupplierTotalAmount, objReserDetails.AgentComm, objReserDetails.Cut_Comm, "", BookingStatus);
        //                        DBHelper.DBReturnCode MyRetCode = HotelBookingTxnManager.UpdateBookingStatus(ReservationId);
        //                        throw new Exception("Error while Inserting Reservation Details");
        //                    }
        //                }
        //            }

        //            #endregion

        //            #region Request For Booking
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {
        //                //bool bResponse = GenrateRequest.BookingRequest();
        //                bool bResponse = CUTUK.DataLayer.HotelManager.BookingRequest();
        //                if (bResponse)
        //                {
        //                    #region Integration
        //                    //HotelReservationManager.SaveToIntegrationDB(objReserDetails);
        //                    #endregion
        //                    #region Update Mgh Hotel Details

        //                    #endregion
        //                    if (objReserDetails.Supplier == "MGH")
        //                    {
        //                        CUT.Models.MGHHotel objMGHHotel = (CUT.Models.MGHHotel)Session["MGHAvail"];
        //                        MGHLib.Response.MGHHotelDetails objMGHHotelDetail = objMGHHotel.HotelDetail.Where(data => data.HotelId == Convert.ToInt32(objReserDetails.objHotelDetails.HotelId)).FirstOrDefault();
        //                        string latitude = (objMGHHotelDetail.Latitude).ToString();
        //                        string Longitude = (objMGHHotelDetail.Longitude).ToString();
        //                        string HotelCodeMGH = (objMGHHotelDetail.HotelId).ToString();
        //                        string HotelNameMGH = (objMGHHotelDetail.HotelName);
        //                        string HotelAdd = (objMGHHotelDetail.Address);
        //                        string Zip = (objMGHHotelDetail.Zip);
        //                        string Description = (objMGHHotelDetail.Description);
        //                        string ContactPerson = (objMGHHotelDetail.ContactDetails.ContactName);
        //                        string ContactEmail = (objMGHHotelDetail.ContactDetails.ContactEmail);
        //                        string ContactMobile = (objMGHHotelDetail.ContactDetails.ContactMobile);
        //                        string ContactPhone = (objMGHHotelDetail.ContactDetails.ContactPhone);
        //                        string Country = objMGHHotelDetail.Location[0].Country;
        //                        string City = objMGHHotelDetail.Location[0].LocationName;
        //                        retCode = HotelReservationManager.HotelMGHAdd(objReserDetails.ReservationId, HotelCode, objReserDetails.objHotelDetails.HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, Country, City);
        //                    }
        //                    #region Update  Dotw Hotel Details
        //                    if (objReserDetails.Supplier == "DoTW")
        //                    {
        //                        retCode = HotelReservationManager.HotelReservationUpdate(objReserDetails.AffilateCode, BookingStatus, objReserDetails.Supplier, "", objReserDetails.ReservationId, objReserDetails.PurchaseToken, 1, objReserDetails.Comments);
        //                        CUT.Models.DoTWHotel objDoTWHotel = new CUT.Models.DoTWHotel();
        //                        DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails = new DOTWLib.Response.DOTWHotelDetails();
        //                        objDoTWHotel = (CUT.Models.DoTWHotel)Session["DoTWAvail"];
        //                        objDOTWHotelDetails = (DOTWLib.Response.DOTWHotelDetails)Session["DoTWRoomDetails"];
        //                        string latitude = (objDOTWHotelDetails.Lat).ToString();
        //                        string Longitude = (objDOTWHotelDetails.Lng).ToString();
        //                        string HotelCodeMGH = (objDOTWHotelDetails.HotelId).ToString();
        //                        string HotelNameMGH = (objDOTWHotelDetails.HotelName);
        //                        string HotelAdd = (objDOTWHotelDetails.FullAddress.hotelStreetAddress);
        //                        string Zip = (objDOTWHotelDetails.FullAddress.hotelZipCode);
        //                        string Description = (objDOTWHotelDetails.Description);
        //                        string ContactPerson = objReserDetails.fullName;
        //                        string ContactEmail = "";
        //                        string ContactMobile = objReserDetails.Officecode;
        //                        string ContactPhone = (objDOTWHotelDetails.HotelPhone);
        //                        string session = (string)Session["session"];
        //                        string citycode = "";
        //                        string[] array_input = session.Split('_');
        //                        string[] Country = array_input[1].Split(',');
        //                        string cty = Country[0];
        //                        string CountryDoTW = Country[1];
        //                        string City = Country[0];
        //                        retCode = HotelReservationManager.HotelMGHAdd(objReserDetails.ReservationId, objReserDetails.sHotelCode, objReserDetails.objHotelDetails.HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, CountryDoTW, City);
        //                    }
        //                    #endregion
        //                    using (TransactionScope objTransactionScope2 = new TransactionScope())
        //                    {
        //                        retCode = HotelBookingTxnManager.SaveBookedRoom(objReserDetails, List_Guest);
        //                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                        {

        //                            objTransactionScope2.Complete();
        //                        }
        //                        else
        //                        {
        //                            objTransactionScope2.Dispose();
        //                        }
        //                    }
        //                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        Int64 noPax = objReserDetails.Adult + objReserDetails.Child;
        //                        GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //                        CUTUK.DataLayer.HotelManager.AddB2C_CustomerBooking(objReserDetails.objHotelDetails.HotelName, objReserDetails.ReservationId, "", noPax.ToString(), objReserDetails.DisplayRequest.CheckIn, objReserDetails.DisplayRequest.CheckOut, BookingStatus, objReserDetails.DeadLineDate.ToString(), objReserDetails.TotalPayable.ToString(), objGlobalDefaults.City);
        //                        using (TransactionScope objTransactionScope2 = new TransactionScope())
        //                        {
        //                            retCode = HotelBookingTxnManager.SaveGuest(objReserDetails, List_Guest);
        //                            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                            {

        //                                objReserDetails.CurrentStatus = "Booking Success";
        //                                retCode = HotelReservationManager.UpdateBookingStatus(objReserDetails.AffilateCode, objReserDetails.CurrentStatus, objReserDetails.DumReservation);
        //                                retCode = HotelReservationManager.UpdateTransactionStatus(objReserDetails.DumReservation, objReserDetails.ReservationId);

        //                                objTransactionScope2.Complete();
        //                                return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = BookingStatus, Message = objReserDetails.CurrentStatus });

        //                            }
        //                            else
        //                            {
        //                                objReserDetails.CurrentStatus = "Unable to Save Guest Details";
        //                                retCode = HotelReservationManager.UpdateBookingStatus(objReserDetails.AffilateCode, objReserDetails.CurrentStatus, objReserDetails.DumReservation);
        //                                retCode = HotelReservationManager.UpdateTransactionStatus(objReserDetails.DumReservation, objReserDetails.ReservationId);
        //                                objTransactionScope2.Complete();
        //                                return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = BookingStatus, Message = objReserDetails.CurrentStatus });
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        objReserDetails.CurrentStatus = "Unable to Save Rooms";
        //                        retCode = HotelReservationManager.UpdateBookingStatus(objReserDetails.AffilateCode, objReserDetails.CurrentStatus, objReserDetails.DumReservation);
        //                        retCode = HotelReservationManager.UpdateTransactionStatus(objReserDetails.DumReservation, objReserDetails.ReservationId);
        //                        return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = objReserDetails.CurrentStatus });

        //                    }
        //                }
        //                else
        //                    return js.Serialize(new { session = 1, retCode = 1, Message = objReserDetails.CurrentStatus });

        //            }
        //            else
        //            {
        //                return js.Serialize(new { session = 1, retCode = 1, Message = objReserDetails.CurrentStatus });
        //            }
        //            #endregion  Request For Booking
        //            #endregion


        //        }
        //        catch (Exception ex)
        //        {
        //            return js.Serialize(new { session = 0, retCode = 0, Message = ex.Message });
        //        }

        //        #endregion
        //    }
        //    else if (retCode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
        //    {
        //        return js.Serialize(new { session = 0, retCode = 0, Message = "Your Balance is insufficient to Make this booking" });
        //    }
        //    else if (retCode == DBHelper.DBReturnCode.DUPLICATEENTRY)
        //    {
        //        return js.Serialize(new { session = 0, retCode = 0, Message = "Your booking is already under Process!!" });
        //    }
        //    else
        //    {
        //        return js.Serialize(new { session = 0, retCode = 0, Message = errorMessage });
        //    }


        //}


        [WebMethod(EnableSession = true)]
        public string ConfirmBooking(string ConfirmId)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            ReservationDetails objReservationDetails = (ReservationDetails)Session["ConfirmationData" + ConfirmId];
            try
            {
                if (Session["ConfirmationData" + ConfirmId] == null)
                    throw new Exception("Invalid booking");
                else
                    Session["ConfirmationData" + ConfirmId] = "";
                string HotelCode = objReservationDetails.sHotelCode;
                string CutID = objReservationDetails.CutCode;
                string Supplier = objReservationDetails.Supplier;
                List<string> RoomID = objReservationDetails.sRoomId;
                List<string> RoomDescriptionId = objReservationDetails.RoomDescriptionId;
                string _Price = objReservationDetails.objCharge.TotalPrice.ToString();
                List<int> _noRooms = objReservationDetails._noRooms;
                string AgencyRefernce = objReservationDetails.AgencyRefernce;
                string MobileNo = objReservationDetails.MobileNo;
                string Remark = objReservationDetails.Remark;
                string BookingStatus = objReservationDetails.BookingStatus;
                string Email = objReservationDetails.Email;
                List<CommonLib.Request.Guest> List_Guest = objReservationDetails.List_Guest;
                string ServiceTax = objReservationDetails.ServiceTax.ToString();
                string HoldTime = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                string ConfirmationId = objReservationDetails.PurchaseToken;
                if (Supplier.Contains("GRN"))
                {
                    HotelCode = HotelCode.Replace("_", "!");
                    CutID = CutID.Replace("_", "!");
                }
                //CommonLib.Response.CommonHotelDetails objCommHotelDetail = ((List<CommonLib.Response.CommonHotelDetails>)Session["Common"]).Single(d => d.HotelId == HotelCode);
                _Price = _Price.Replace(" ", "+");

                string APIType = Supplier;
                bool IsValidRoomID = true;
                ReservationDetails objReserDetails = new ReservationDetails();
                // objReserDetails = (ReservationDetails)Session["ConfirmationData" + ConfirmationId];
                objReserDetails = objReservationDetails;
                objReserDetails.BookingStatus = "Vouchered";
                if (objReserDetails == null)
                    return js.Serialize(new { session = 0, retCode = 0, Message = "Invalid Booking" });
                #region Variables & object
                CUTUK.DataLayer.HotelManager.objReserDetails = new ReservationDetails();
                CUTUK.DataLayer.HotelManager.objReserDetails = objReserDetails;
                CUT.DataLayer.BookingManager.objReservation = objReserDetails;

                string AffilateCode = ""; ;
                string ReservationId = "";
                float ServiceCharges = 0.0F;
                string ComparedCurrency = "";
                string BookingDate;
                string CurrentStatus = "V";
                Int64 Sid = 0;
                //CUT.Occupancy objOccupancy = new Occupancy();
                // objects--common

                GlobalDefault objGlobalDefault = null;
                CUT.DataLayer.AgentDetailsOnAdmin objGlobalAgent = null;
                #endregion
                js = new JavaScriptSerializer();
                string errorMessage = "";
                DBHelper.DBReturnCode retCode = CUT.DataLayer.BookingManager.ValidateTransaction(true);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    objReserDetails.HoldingTime = Convert.ToDateTime(HoldTime);
                    #region Booking By Supplier

                    objReserDetails.List_Guest = List_Guest;
                    BookingStatus = "Vouchered";
                    #region Save BOOKING lOG
                    retCode = HotelBookingTxnManager.SaveCommonRoomLog(objReserDetails, List_Guest);
                    #endregion

                    #region Common Booking Methods
                    var List_Customer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
                    var LeadingGuestName = List_Customer[0].Name + List_Customer[0].LastName;
                    #region Request For Booking
                    //bool bResponse = GenrateRequest.BookingRequest();
                    bool bResponse = true;
                    if (bResponse)
                    {
                        retCode = CUT.DataLayer.BookingManager.BookingTransact("Hotel");
                        objReserDetails = CUT.DataLayer.BookingManager.objReservation;
                        GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                        if (objGlobalDefaultCustomer == null)
                        {
                            objGlobalDefaultCustomer = new GlobalDefault();
                            objGlobalDefaultCustomer.City = "";
                        }


                        CUTUK.DataLayer.HotelManager.AddB2C_CustomerBooking(objReserDetails.objHotelDetails.HotelName, objReserDetails.ReservationId, "", objReserDetails.List_Guest.Count.ToString(), objReserDetails.sCheckIn, objReserDetails.DisplayRequest.CheckOut, BookingStatus, objReserDetails.DeadLineDate.ToString("dd/MM/yyyy hh:mm"), objReserDetails.TotalPayable.ToString(), objGlobalDefaultCustomer.City);
                        #region Static Hotel
                        if (objReserDetails.Supplier == "MGH")
                        {
                            CUT.Models.MGHHotel objMGHHotel = (CUT.Models.MGHHotel)Session["MGHAvail"];
                            MGHLib.Response.MGHHotelDetails objMGHHotelDetail = objMGHHotel.HotelDetail.Where(data => data.HotelId == Convert.ToInt32(objReserDetails.objHotelDetails.HotelId)).FirstOrDefault();
                            string latitude = (objMGHHotelDetail.Latitude).ToString();
                            string Longitude = (objMGHHotelDetail.Longitude).ToString();
                            string HotelCodeMGH = (objMGHHotelDetail.HotelId).ToString();
                            string HotelNameMGH = (objMGHHotelDetail.HotelName);
                            string HotelAdd = "";
                            if (objMGHHotelDetail.Address != null)
                            {
                                HotelAdd = (objMGHHotelDetail.Address);
                            }
                            string Zip = "";
                            if (objMGHHotelDetail.Zip != null)
                            {
                                Zip = (objMGHHotelDetail.Zip);
                            }

                            string Description = (objMGHHotelDetail.Description);
                            string ContactPerson = (objMGHHotelDetail.ContactDetails.ContactName);
                            string ContactEmail = (objMGHHotelDetail.ContactDetails.ContactEmail);
                            string ContactMobile = (objMGHHotelDetail.ContactDetails.ContactMobile);
                            string ContactPhone = (objMGHHotelDetail.ContactDetails.ContactPhone);
                            string Country = objMGHHotelDetail.Location[0].Country;
                            string City = objMGHHotelDetail.Location[0].LocationName;
                            retCode = HotelReservationManager.HotelMGHAdd(objReserDetails.ReservationId, HotelCode, objReserDetails.objHotelDetails.HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, Country, City);
                        }
                        #region Update  Dotw Hotel Details
                        if (objReserDetails.Supplier == "DoTW")
                        {
                            retCode = HotelReservationManager.HotelReservationUpdate(objReserDetails.AffilateCode, BookingStatus, objReserDetails.Supplier, "", objReserDetails.ReservationId, objReserDetails.PurchaseToken, 1, objReserDetails.Comments);
                            CUT.Models.DoTWHotel objDoTWHotel = new CUT.Models.DoTWHotel();
                            DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails = new DOTWLib.Response.DOTWHotelDetails();
                            objDoTWHotel = (CUT.Models.DoTWHotel)Session["DoTWAvail"];
                            objDOTWHotelDetails = (DOTWLib.Response.DOTWHotelDetails)Session["DoTWRoomDetails"];
                            string latitude = (objDOTWHotelDetails.Lat).ToString();
                            string Longitude = (objDOTWHotelDetails.Lng).ToString();
                            string HotelCodeMGH = (objDOTWHotelDetails.HotelId).ToString();
                            string HotelNameMGH = (objDOTWHotelDetails.HotelName);
                            string HotelAdd = (objDOTWHotelDetails.FullAddress.hotelStreetAddress);
                            string Zip = (objDOTWHotelDetails.FullAddress.hotelZipCode);
                            string Description = (objDOTWHotelDetails.Description);
                            string ContactPerson = objReserDetails.fullName;
                            string ContactEmail = "";
                            string ContactMobile = objReserDetails.Officecode;
                            string ContactPhone = (objDOTWHotelDetails.HotelPhone);
                            string session = (string)Session["session"];
                            string citycode = "";
                            string[] array_input = session.Split('_');
                            string[] Country = array_input[1].Split(',');
                            string cty = Country[0];
                            string CountryDoTW = Country[1];
                            string City = Country[0];
                            retCode = HotelReservationManager.HotelMGHAdd(objReserDetails.ReservationId, objReserDetails.sHotelCode, objReserDetails.objHotelDetails.HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, CountryDoTW, City);
                        }
                        #endregion
                        #endregion

                        #region Saved Booked Room
                        retCode = HotelBookingTxnManager.SaveBookedRoom(objReserDetails, List_Guest);
                        #endregion

                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            using (TransactionScope objTransactionScope2 = new TransactionScope())
                            {
                                retCode = HotelBookingTxnManager.SaveGuest(objReserDetails, List_Guest);
                                if (retCode == DBHelper.DBReturnCode.SUCCESS || retCode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
                                {

                                    objTransactionScope2.Complete();
                                }
                                else
                                {
                                    objReserDetails.CurrentStatus = "Unable to Save Guest Details";
                                    objTransactionScope2.Complete();
                                    return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = BookingStatus, Message = objReserDetails.CurrentStatus });
                                }
                            }
                            CUT.DataLayer.EmailManager.SendResevation(objReserDetails.ReservationId);
                            return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = BookingStatus, Message = objReserDetails.CurrentStatus });
                        }
                        else
                        {
                            objReserDetails.CurrentStatus = "Unable to Save Rooms";
                            return js.Serialize(new { session = 1, retCode = 1, ReservationID = objReserDetails.ReservationId, Status = objReserDetails.CurrentStatus });

                        }
                    }
                    else
                        return js.Serialize(new { session = 1, retCode = 1, Message = objReserDetails.CurrentStatus });

                    #endregion  Request For Booking
                    #endregion

                    #endregion
                }
                else if (retCode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
                {
                    return js.Serialize(new { session = 0, retCode = 0, Message = "Your Balance is insufficient to Make this booking" });
                }
                else if (retCode == DBHelper.DBReturnCode.DUPLICATEENTRY)
                {
                    return js.Serialize(new { session = 0, retCode = 0, Message = "Your booking is already under Process!!" });
                }
                else
                {
                    return js.Serialize(new { session = 0, retCode = 0, Message = errorMessage });
                }
            }
            catch (Exception ex)
            {
                return js.Serialize(new { session = 0, retCode = 0, Message = ex.Message });
            }
        }


        #endregion

        [WebMethod(EnableSession = true)]
        public string HotelConfirmBooking(string HotelCode, string RoomID, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<MGHLib.Request.Guest> Guest_listMGH, string ServiceTax, string Supplier, string RoomDescriptionId)
        {
            switch (Supplier)
            {
                //case "A":
                //    Supplier = "HotelBeds";
                //    break;
                case "B":
                    Supplier = "MGH";
                    break;
                    //case "C":
                    //    Supplier = "DoTW";
                    //    break;
                    //case "D":
                    //    Supplier = "Expedia";
                    //    break;
            }
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            #region Variables & object
            //string APIType = "HOTELBEDS";
            string APIType = Supplier;
            bool IsValidRoomID = true;
            string ReservationId = "";
            float TotalAmount = 0.0F;
            float ServiceCharges = 0.0F;
            float TotalPayable = 0.0F;
            string AffilateCode = "";
            string affcode = "";
            string Lang = "";
            string UserName = "";
            string CompanyId = "";
            string Password = "";
            string CheckIn = "";
            string CheckOut = "";
            string sCheckIn = "";
            string sCheckOut = "";
            string DesitinationCode = "";
            string DesitionationType = "";
            string HotelName = "";
            Int64 HotelId = 0;
            string HotelType = "";
            string AvailToken = "";
            string SerViceType = "";
            int RoomCount = 0;
            int Adult = 0;
            int Child = 0;
            string OnRequest = "";
            string EchoToken = "";
            float rate = 0;
            string m_xml = "";
            string m_response = "";
            string m_p_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            int Status = 0;
            string PurchaseToken = String.Empty;
            int HolderAge = 0;
            string HolderName = "";
            string HolderLastName = "";
            string Suppliername = "";
            string VatNumber = "";
            string Comments = "";
            string Purchasetoken = "";
            int Officecode = 0;
            float SupplierTotalAmount = 0;
            float CutComm = 0;
            float AgentComm = 0;
            string RoomCode = "";
            int RoomNumber = 0;
            string SUPCancellationDate = "";
            string CUTCancellationDate = "";
            string CancellationAmount = "";
            string CanServiceTax = "";
            string CanAmountWithServiceTax = "";
            float SerTax = 0;
            float CanAmtWithTax = 0;
            float RoomSerTax = 0;
            float RoomAmtWithTax = 0;
            string ChildAge = "";
            string Currency = "";
            string ComparedCurrency = "";
            float ComparedTotalAmount = 0;
            string LeadingGuestName = "";
            DateTime DeadLineDate;
            DateTime ChargeDate;
            string BookingDate;
            string DumReservation;
            string CurrentStatus = "Request";
            string session;
            Int64 selectedRateBasis = 0;
            string tariffName = "";
            string fullName = "";
            string phone = "";
            DBHelper.DBReturnCode RetCodeStatus;
            bool bValid = true;

            MGHLib.Request.Occupancy objOccupancy = new MGHLib.Request.Occupancy();
            // objects--common
            JavaScriptSerializer js = new JavaScriptSerializer();
            //GlobalDefault objGlobalDefault = null;
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();

            // objects--HOTELBEDS
            //Models.Hotel objHotel = new Models.Hotel();
            //HotelLib.Response.HotelDetail objHotelDetail = new HotelLib.Response.HotelDetail();
            //List<Customer> List_Customer = new List<Customer>();
            //List<HotelLib.Response.Contract> List_Contract = new List<HotelLib.Response.Contract>();
            //HotelLib.Response.RoomOccupancy objRoomOccupancy = new HotelLib.Response.RoomOccupancy();
            //List<HotelLib.Response.Room> Hotel_Occupancy = new List<HotelLib.Response.Room>();
            //HotelLib.Request.ServiceAddRequest objService = new HotelLib.Request.ServiceAddRequest();
            //HotelLib.Response.DateTimeFrom objDateTimeFrom = new HotelLib.Response.DateTimeFrom();
            //HotelLib.Response.CancellationPolicy objCancellationPolicy = new HotelLib.Response.CancellationPolicy();
            //HotelLib.Response.RoomType objRoomType = new HotelLib.Response.RoomType();
            //HotelLib.Response.Price objPrice = new HotelLib.Response.Price();
            //HotelLib.Response.Location objLocation = new HotelLib.Response.Location();


            CUT.Models.MGHHotel objMGHHotel = new CUT.Models.MGHHotel();
            CUT.Models.MGHStatusCode objMGHStatusCode = new CUT.Models.MGHStatusCode();
            MGHLib.Response.MGHHotelDetails objMGHHotelDetail = new MGHLib.Response.MGHHotelDetails();

            //Dotw
            //Models.DoTWHotel objDoTWHotel = new Models.DoTWHotel();
            //DOTWLib.Response.DOTWHotelDetails objDOTWHotelDetails = new DOTWLib.Response.DOTWHotelDetails();
            //List<DOTWLib.Request.Customers> List_CustomerDoTW = new List<DOTWLib.Request.Customers>();
            //DOTWLib.Response.Room objRoomDoTW = new DOTWLib.Response.Room();
            //List<DOTWLib.Response.RoomType> Hotel_RoomType = new List<DOTWLib.Response.RoomType>();
            //DOTWLib.Request.ServiceAddRequest objDoTWService = new DOTWLib.Request.ServiceAddRequest();
            //DOTWLib.Response.cancellationRules objcancellationRules = new DOTWLib.Response.cancellationRules();
            //DOTWLib.Response.Location objDoTWLocation = new DOTWLib.Response.Location();


            //Models.EANHotels objEANHotels = new Models.EANHotels();
            //com.ExpediaHotelBookingHelper objBookingHelper = new ExpediaHotelBookingHelper();
            //Models.EANStatusCode objEANStatusCode = new Models.EANStatusCode();
            //EANLib.Response.HotelRoomAvailabilityResponse objEanHotelDetails = new EANLib.Response.HotelRoomAvailabilityResponse();

            #endregion
            string VoucherID = "";
            int Passenger = 0;
            #region MGH
            if (APIType == "MGH")
            {
                string CD = "";
                string DD = "";

                js = new JavaScriptSerializer();
                #region Checking Space
                //IsValidRoomID = true;
                //if (Session["HotelRoomIDs"] != null)
                //{
                //    string[] arrHotelRoomIDs = Session["HotelRoomIDs"].ToString().Split('|');
                //    foreach (var objGuest in List_Guest)
                //    {
                //        if (objGuest.ID.Contains(' '))
                //        {
                //            IsValidRoomID = false;
                //            objGuest.ID = objGuest.ID.Replace(' ', '+');
                //            for (int i = 0; i < arrHotelRoomIDs.Length; i++)
                //            {
                //                if (objGuest.ID == arrHotelRoomIDs[i])
                //                    IsValidRoomID = true;
                //            }
                //        }
                //    }
                //}
                //if (!IsValidRoomID)
                //    return js.Serialize(new { session = 1, retCode = 0, Message = "Something went wrong! We recommend you to select other room options" });
                #endregion Checking Space

                if (HttpContext.Current.Session["markups"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
                else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
                ComparedCurrency = (Session["OtherCurrency"] != null) ? Session["OtherCurrency"].ToString() : "";
                ComparedTotalAmount = 0;
                if (AgencyRefernce == "")
                    AgencyRefernce = "Test AgencyRef";
                BookingDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);


                if (com.Common.Session(out objGlobalDefault))
                {
                    objMGHHotel = (CUT.Models.MGHHotel)Session["MGHAvail"];
                    objMGHHotelDetail = objMGHHotel.HotelDetail.Where(data => data.HotelId == Convert.ToInt32(HotelCode)).FirstOrDefault();
                    objMGHStatusCode = (CUT.Models.MGHStatusCode)Session["MGHStatus"];
                    ReservationId = "Dummy-" + GenerateRandomString(4);
                    DumReservation = ReservationId;
                    float SupplierAmount = 0.0F;
                    float CutRoomAmount = 0.0F;
                    float AgentRoomMarkup = 0.0F;
                    string BlockID = "";
                    bool bResponseBooking = false;
                    DBHelper.DBReturnCode retCode;
                    DBHelper.DBReturnCode retCodeUpdateTXN;
                    List<string> lstLeadingGuests = new List<string>();
                    List<string> lstChildAge = new List<string>();
                    DBHelper.DBReturnCode retcode;
                    string leadingGuest = "";

                    if (BookingStatus == "Vouchered")
                    {
                        for (int k = 0; k < objMGHHotelDetail.rate.Count; k++)
                        {
                            for (int j = 0; j < objMGHHotelDetail.rate[k].roomCategory.Count; j++)
                            {
                                if (objMGHHotelDetail.rate[k].roomCategory[j].RoomCategoryId == Convert.ToInt64(RoomID))
                                {
                                    SupplierAmount = objMGHHotelDetail.rate[k].roomInfo[j].roomRate[0].amount;
                                    CutRoomAmount = objMGHHotelDetail.rate[k].refundPolicy[j].CutRoomAmount;
                                    AgentRoomMarkup = objMGHHotelDetail.rate[k].refundPolicy[j].AgentRoomMarkup;
                                    break;

                                }


                            }

                        }

                        TotalAmount = CutRoomAmount + AgentRoomMarkup;
                        SupplierTotalAmount = CutRoomAmount;
                        // SupplierTotalAmount = SupplierAmount;
                        CutComm = CutRoomAmount - SupplierTotalAmount;
                        AgentComm = AgentRoomMarkup;
                        ServiceCharges = (float)(Convert.ToSingle(ServiceTax));
                        bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(GroupSupplier => GroupSupplier.Supplier == "HotelBeds").TaxOnMarkup;
                        if (TaxOnMarkup == false)
                        {
                            TotalPayable = TotalAmount - (AgentComm) + ServiceCharges;
                        }
                        else
                        {
                            TotalPayable = TotalAmount - (AgentComm);
                        }
                        retCode = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationId, BookingDate, TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "MGH", SupplierTotalAmount, AgentComm, CutComm, "", BookingStatus);

                        if (retCode != DBHelper.DBReturnCode.SUCCESS)
                        {

                            return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding transaction details" });
                        }
                    }

                    #region TransactionScope

                    #region URL For Block Room
                    UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
                    Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];
                    AffilateCode = "";
                    affcode = "";

                    DesitinationCode = (objMGHHotelDetail.Location[0].LocationId).ToString();
                    string CheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
                    string sCheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                    string CheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
                    string sCheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                    HotelName = objMGHHotelDetail.HotelName;
                    HotelType = objMGHHotelDetail.Category[0].CategoryName;

                    var List_MGHCustomer = Guest_listMGH.Select(data => data.sCustomer).FirstOrDefault();

                    //List_MGHCustomer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
                    Lang = "ENG";
                    string latitude = (objMGHHotelDetail.Latitude).ToString();
                    string Longitude = (objMGHHotelDetail.Longitude).ToString();


                    RoomCount = objMGHHotel.DisplayRequest.Room;
                    Adult = objMGHHotel.DisplayRequest.Adult;
                    Child = objMGHHotel.DisplayRequest.Child;
                    int Adult_Room1 = 0;
                    int Adult_Room2 = 0;
                    int Adult_Room3 = 0;
                    int Adult_Room4 = 0;
                    int Child_Room1 = 0;
                    int Child_Room2 = 0;
                    int Child_Room3 = 0;
                    int Child_Room4 = 0;
                    OnRequest = "N";
                    MGHLib.Response.Rate objRate = new MGHLib.Response.Rate();

                    string Room1_ChidAge = "";
                    string Room2_ChidAge = "";
                    string Room3_ChidAge = "";
                    string Room4_ChidAge = "";
                    if (RoomCount == 1)
                    {
                        if (Child == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (Child == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult + "&occupant_child=" + Child + Room1_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "&rooms=" + RoomCount;
                    }

                    if (RoomCount == 2)
                    {
                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID;

                    }

                    if (RoomCount == 3)
                    {

                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }
                        // for romm 3 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 1)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 2)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Adult_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        Child_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "," + Adult_Room3 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "," + Child_Room3 + Room3_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID + "," + RoomID;

                    }


                    if (RoomCount == 4)
                    {

                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }
                        // for romm 3 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 1)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 2)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[1].Age).ToString();
                        }

                        // for romm 4 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[3].ChildCount == 1)
                        {
                            Room4_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[3].ChildCount == 2)
                        {
                            Room4_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Adult_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Adult_Room4 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        Child_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        Child_Room4 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "," + Adult_Room3 + "," + Adult_Room4 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "," + Child_Room3 + Room3_ChidAge + "," + Child_Room4 + Room4_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID + "," + RoomID + "," + RoomID;

                    }



                    #endregion URL For Block Room

                    if (m_xml != null)
                    {
                        Int64 Sid = 0;
                        if (ComparedCurrency != "" && ComparedCurrency != "INR")
                        {
                            //net.webservicex.www.Currency cur = CUT.Common.Common.GetCurrency(ComparedCurrency);
                            //net.webservicex.www.CurrencyConvertor objCurrency = new net.webservicex.www.CurrencyConvertor();
                            //rate = Convert.ToInt32(objCurrency.ConversionRate(cur, net.webservicex.www.Currency.INR));
                            rate = objGlobalDefault.ExchangeRate.Single(data => data.Currency == ComparedCurrency).Rate;
                            ComparedTotalAmount = Convert.ToSingle(Math.Round((Decimal)(TotalAmount / rate), 2, MidpointRounding.AwayFromZero));
                        }
                        LeadingGuestName = List_MGHCustomer[0].Name + " " + List_MGHCustomer[0].LastName;
                        DateTime ChkInDeadLine = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB"));
                        for (int j = 0; j < objMGHHotelDetail.RoomCategory.Count; j++)
                        {

                            for (int k = 0; k < objMGHHotelDetail.rate.Count; k++)
                            {
                                //  for (int j = 0; j < objMGHHotelDetail.rate[k].roomInfo.Count; j++)


                                if (objMGHHotelDetail.RoomCategory[j].RoomCategoryId == Convert.ToInt64(RoomID))
                                {
                                    int q = (-(24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[0].dayMax)));
                                    int r = (-(24 + 24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[0].dayMax)));
                                    DateTime TimeChargeDate = ChkInDeadLine.AddHours(q);
                                    DateTime TimeDeadLineDate = ChkInDeadLine.AddHours(r);
                                    string s1 = TimeChargeDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                                    string s2 = TimeDeadLineDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                                    CD = s1.Replace(" 00:00", "");
                                    DD = s2.Replace(" 00:00", "");
                                }
                            }
                        }

                        using (TransactionScope objTransactionScope1 = new TransactionScope())
                        {

                            //DBHelper.DBReturnCode retcodeMGH = HotelReservationManager.HotelReservationAddUpdateMGH(Sid, objMGHHotelDetail.HotelId.ToString(), objMGHHotelDetail.HotelName, objMGHHotel.DisplayRequest.Location, objMGHHotel.DisplayRequest.CheckIn, objMGHHotel.DisplayRequest.CheckOut, objMGHHotel.DisplayRequest.Night, objMGHHotel.DisplayRequest.Room, TotalAmount, objMGHHotelDetail.Currency, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, CD, objMGHHotelDetail.Description, DD, out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, ComparedTotalAmount, MobileNo, Email, Remark, latitude, Longitude, out VoucherID);
                            DBHelper.DBReturnCode retcodeMGH = DBHelper.DBReturnCode.SUCCESS;
                            affcode = AffilateCode;
                            if (retcodeMGH == DBHelper.DBReturnCode.SUCCESS)
                            {

                                CurrentStatus = "Reservation Details Added";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope1.Complete();
                            }
                            else if (retcodeMGH != DBHelper.DBReturnCode.SUCCESS)
                            {
                                CurrentStatus = "Error while Inserting Reservation Details";
                                bValid = false;
                                objTransactionScope1.Dispose();

                            }
                        }

                    }
                    if (bValid)
                    {
                        m_response = "";
                        RequestHeader = "";
                        ResponseHeader = "";
                        Status = 0;
                        PurchaseToken = String.Empty;

                        MGHLib.Common.ServiceAddRequestMGH webclient = new MGHLib.Common.ServiceAddRequestMGH();

                        bool bResponse = webclient.Post(m_xml, out ResponseHeader, out Status);
                        using (TransactionScope objTransactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                        {
                            LogManager.Add(1, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
                            if (bResponse)
                            {
                                CurrentStatus = "Service Add Request Success";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope2.Complete();
                            }
                            else
                            {
                                CurrentStatus = "Service Add Request Fail";
                                bValid = false;
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope2.Dispose();
                            }

                        }


                        if (bValid)
                        {
                            if (bResponse)
                            {
                                string leadingGeust = Guest_listMGH[0].sCustomer[0].Name + " " + Guest_listMGH[0].sCustomer[0].LastName;
                                ParseMGHResponse objBlockID = new ParseMGHResponse();
                                bResponseBooking = objBlockID.GetBlockID(ResponseHeader, out BlockID);
                                m_xml = "bookRoom&token=" + Password + "&hotelid=" + HotelCode + "&room_category_id =" + RoomID + "&block_ref_no=" + BlockID + "&guest_name=" + leadingGeust + "&guest_email=dubai%40clickurtrip.com&guest_phone=+971-4-2977792&guest_ident_type=&guest_ident=&guest_ident=";
                                bResponseBooking = webclient.Post(m_xml, out ResponseHeader, out Status);
                                try
                                {
                                    ReservationId = objBlockID.GetBookingID(ResponseHeader);
                                }
                                catch
                                {
                                    return js.Serialize(new { session = 1, retCode = 0, Message = "Set of Rooms are not Available." });
                                }

                                using (TransactionScope objTransactionScope3 = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    LogManager.Add(2, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
                                    if (bResponseBooking)
                                    {
                                        CurrentStatus = "Purchase Request Success";
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        objTransactionScope3.Complete();
                                    }
                                    else
                                    {
                                        CurrentStatus = "Purchase Request Fail";
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        bValid = false;
                                        objTransactionScope3.Dispose();
                                    }
                                }
                                js.MaxJsonLength = Int32.MaxValue;

                            }
                            if (bValid)
                            {
                                if (bResponseBooking)
                                {
                                    // code for database insertion here 
                                    using (TransactionScope objTransactionScope4 = new TransactionScope())
                                    {
                                        DBHelper.DBReturnCode retcode1;
                                        retcode1 = HotelReservationManager.HotelReservationUpdate(affcode, BookingStatus, Suppliername = "MGH", VatNumber, ReservationId, Purchasetoken, Officecode, Comments);

                                        // retcode = HotelReservationManager.HotelReservationUpdate("123", "Vouchered", "HotelBeds Spain S.L.U", "456", "789", "101112", 121212, "Test 12 Jan 2016");
                                        if (retcode1 == DBHelper.DBReturnCode.SUCCESS)
                                        {
                                            CurrentStatus = "Reservation Details Updated";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            latitude = (objMGHHotelDetail.Latitude).ToString();
                                            Longitude = (objMGHHotelDetail.Longitude).ToString();
                                            string HotelCodeMGH = (objMGHHotelDetail.HotelId).ToString();
                                            string HotelNameMGH = (objMGHHotelDetail.HotelName);
                                            string HotelAdd = (objMGHHotelDetail.Address);
                                            string Zip = (objMGHHotelDetail.Zip);
                                            string Description = (objMGHHotelDetail.Description);
                                            string ContactPerson = (objMGHHotelDetail.ContactDetails.ContactName);
                                            string ContactEmail = (objMGHHotelDetail.ContactDetails.ContactEmail);
                                            string ContactMobile = (objMGHHotelDetail.ContactDetails.ContactMobile);
                                            string ContactPhone = (objMGHHotelDetail.ContactDetails.ContactPhone);
                                            string Country = objMGHHotelDetail.Location[0].Country;
                                            string City = objMGHHotelDetail.Location[0].LocationName;

                                            DBHelper.DBReturnCode retcode2;
                                            retcode2 = HotelReservationManager.HotelMGHAdd(ReservationId, HotelCode, HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, Country, City);
                                            //if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
                                            //{

                                            //    //return js.Serialize(new { session = 1, retCode = 0, Message = "Error while Adding Hotel  details" });
                                            //}

                                            objTransactionScope4.Complete();

                                        }
                                        else if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
                                        {
                                            CurrentStatus = "Error while updating reservation details";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            bValid = false;
                                            objTransactionScope4.Dispose();
                                        }
                                    }
                                    if (bValid)
                                    {
                                        if (BookingStatus == "Vouchered")
                                        {
                                            using (TransactionScope objTransactionScope5 = new TransactionScope())
                                            {

                                                retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnUpdate(ReservationId, DumReservation);
                                                if (retCodeUpdateTXN == DBHelper.DBReturnCode.SUCCESS)
                                                {
                                                    CurrentStatus = "Reservation-Id Added Success";
                                                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                    objTransactionScope5.Complete();
                                                }
                                                else if (retCodeUpdateTXN != DBHelper.DBReturnCode.SUCCESS)
                                                {
                                                    CurrentStatus = "Error while Updating transaction details";
                                                    bValid = false;
                                                    objTransactionScope5.Dispose();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                    if (bValid)
                                    {
                                        RoomCode = RoomID;
                                        RoomNumber = 1;
                                        int r = 0;
                                        bool retbit = true;
                                        foreach (MGHLib.Request.Guest guestroom in Guest_listMGH)
                                        {
                                            Passenger = Passenger + guestroom.sCustomer.Count;
                                            if (retbit == false)
                                            {
                                                break;
                                            }

                                            foreach (MGHLib.Request.Customer customer in guestroom.sCustomer)
                                            {
                                                if (r == 0)
                                                {
                                                    lstLeadingGuests.Add(customer.Name + " " + customer.LastName);
                                                    leadingGuest = customer.Name + " " + customer.LastName;
                                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 1, ReservationId);
                                                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                                    {
                                                        retbit = false;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 0, ReservationId);
                                                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                                    {
                                                        retbit = false;
                                                        break;
                                                    }
                                                }
                                                r++;
                                            }
                                            RoomNumber++;
                                        }

                                        using (TransactionScope objTransactionScope6 = new TransactionScope())
                                        {
                                            if (retbit == false)
                                            {
                                                CurrentStatus = "Error while adding PASSENGER";
                                                bValid = false;
                                                objTransactionScope6.Dispose();
                                            }
                                            else
                                            {
                                                CurrentStatus = "PASSENGER Added Success";
                                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                bValid = true;
                                                objTransactionScope6.Complete();
                                            }
                                        }


                                        #region old
                                        //using (TransactionScope objTransactionScope6 = new TransactionScope())
                                        //{
                                        //    foreach (MGHLib.Request.Guest guestroom in Guest_listMGH)
                                        //    {
                                        //        RoomCode = RoomID;
                                        //        RoomNumber = 1;
                                        //        int k = 0;

                                        //        foreach (MGHLib.Request.Customer customer in guestroom.sCustomer)
                                        //        {

                                        //            //foreach (var scustomer in List_Guest)
                                        //            //{

                                        //            if (k == 0)
                                        //            {
                                        //                retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 1, ReservationId);
                                        //                lstLeadingGuests.Add(customer.Name + " " + customer.LastName);
                                        //                leadingGuest = customer.Name + " " + customer.LastName;
                                        //            }
                                        //            else
                                        //                retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 0, ReservationId);

                                        //            if (retcode == DBHelper.DBReturnCode.SUCCESS)
                                        //            {
                                        //                CurrentStatus = "PASSENGER Added Success";
                                        //                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        //                //if (k == (guestroom.sCustomer.Count - 1))
                                        //                //  objTransactionScope6.Complete();
                                        //            }
                                        //            if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                        //            {

                                        //                //CurrentStatus = "PASSENGER Insert Fail";
                                        //                CurrentStatus = "Error while adding PASSENGER";
                                        //                bValid = false;
                                        //                objTransactionScope6.Dispose();
                                        //                //RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        //                //return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                        //            }


                                        //            k++;
                                        //            //}

                                        //        }
                                        //    }
                                        //    objTransactionScope6.Complete();

                                        //}
                                        #endregion
                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                    if (bValid)
                                    {
                                        int l = 0;
                                        int n = 0;
                                        bool RoomBit = false;

                                        int Nights = objMGHHotel.DisplayRequest.Night;
                                        int Rooms = objMGHHotel.DisplayRequest.Room;
                                        for (int rc = 0; rc < RoomCount; rc++)
                                        {


                                            SUPCancellationDate = "";
                                            CUTCancellationDate = "";
                                            CancellationAmount = "";
                                            CanServiceTax = "";
                                            int TotalRoom = objMGHHotelDetail.rate[0].roomInfo.Count;
                                            string SrTax = "";
                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {

                                                        CUTCancellationDate += (Convert.ToDateTime(objMGHHotelDetail.DateFrom)).AddHours(-(objMarkupsAndTaxes.TimeGap + (Convert.ToInt32(objMGHHotelDetail.rate[r].refundPolicy[j].dayMax) * 24))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {

                                                        SUPCancellationDate += (Convert.ToDateTime(objMGHHotelDetail.DateFrom)).AddHours(-((Convert.ToInt32(objMGHHotelDetail.rate[r].refundPolicy[j].dayMax) * 24))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "|";
                                                    }
                                                }

                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        CancellationAmount += String.Format("{0:#,##0}", Math.Round((Decimal)((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount + objMGHHotelDetail.rate[r].refundPolicy[j].AgentCancellationMarkup) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero).ToString()) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        SrTax += ((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount * objMarkupsAndTaxes.PerServiceTax) / (Nights * Rooms)) / 100 + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        CanAmountWithServiceTax += String.Format("{0:#,##0}", Math.Round((Decimal)(((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount + objMGHHotelDetail.rate[r].refundPolicy[j].AgentCancellationMarkup) + ((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount * objMarkupsAndTaxes.PerServiceTax) / 100)) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero).ToString()) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID))
                                                {
                                                    CutRoomAmount = (float)(Math.Round((Decimal)((objMGHHotelDetail.rate[r].refundPolicy[0].CutRoomAmount) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero));
                                                }
                                            }


                                            ChildAge = "";




                                            // for Room 1 child ages
                                            if (objMGHHotel.DisplayRequest.List_Occupancy[l].ChildCount == 1)
                                            {
                                                //remember to focus on it...
                                                string age = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[0].Age).ToString();
                                                ChildAge = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[0].Age).ToString();
                                            }
                                            if (objMGHHotel.DisplayRequest.List_Occupancy[l].ChildCount == 2)
                                            {
                                                ChildAge = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[l].Age).ToString() + "," + (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[(l + 1)].Age).ToString();
                                            }
                                            l++;




                                            //for (int z = 0; z < objMGHHotel.DisplayRequest.Child; z++)
                                            //{
                                            //    ChildAge = ChildAge + "|" + lstChildAge[l];
                                            //    l++;
                                            //}
                                            //ChildAge = ChildAge.TrimStart('|');
                                            string MEALPLAN = objMGHHotelDetail.rate[0].mealPlan[0].text;
                                            string RoomType = objMGHHotelDetail.rate[0].roomType[0].RoomTypeName;
                                            retcode = HotelReservationManager.BookedRoomAdd(RoomCode, RoomType, "Room " + (n + 1), MEALPLAN, objMGHHotel.DisplayRequest.Room, leadingGuest, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, SrTax, CanAmountWithServiceTax, CutRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
                                            if (retcode == DBHelper.DBReturnCode.SUCCESS)
                                            {
                                                RoomBit = true;
                                            }
                                            else
                                            {
                                                RoomBit = false;
                                            }
                                            n++;
                                        }
                                        using (TransactionScope objTransactionScope7 = new TransactionScope())
                                        {
                                            //retcode = HotelReservationManager.BookedRoomAdd(objRoom.SHRUI, objRoom.Roomtext, "Room " + (n + 1), objRoom.Boardtext, objRoom.RoomCount, lstLeadingGuests[n], objRoom.AdultCount, objRoom.ChildCount, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, CanServiceTax, CanAmountWithServiceTax, objRoom.CUTRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
                                            if (RoomBit == true)
                                            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                                            {
                                                CurrentStatus = "Room Insert Success";
                                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                //if (n == (RoomCount - 1))
                                                objTransactionScope7.Complete();
                                            }
                                            else if (RoomBit == false)
                                            //else if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                            {

                                                CurrentStatus = "Error while adding ROOM";
                                                //RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                bValid = false;
                                                objTransactionScope7.Dispose();
                                                //return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                            }




                                        }
                                        if (bValid && (n == RoomCount))
                                        {
                                            //objTransactionScope.Complete();
                                            CurrentStatus = "Booking Success";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            retcode = HotelReservationManager.UpdateTransactionStatus(DumReservation, ReservationId);
                                            GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                                            CUTUK.DataLayer.HotelManager.AddB2C_CustomerBooking(HotelName, ReservationId, VoucherID, Passenger.ToString(), objMGHHotel.DisplayRequest.CheckIn, objMGHHotel.DisplayRequest.CheckOut, BookingStatus, CUTCancellationDate, TotalAmount.ToString(), objGlobalDefaultCustomer.City);
                                            DataTable dt = null;
                                            CUT.Common.SMSWebClient objSMS = new SMSWebClient();
                                            DBHelper.DBReturnCode RetSMS = HotelReservationManager.BookingDetailForSMS(ReservationId, out dt);

                                            string Mobile = dt.Rows[0]["Mobile"].ToString();
                                            string InvoiceId = dt.Rows[0]["InvoiceID"].ToString();
                                            string CIn = dt.Rows[0]["CheckIn"].ToString();
                                            string COut = dt.Rows[0]["CheckOut"].ToString();
                                            string SMS = "Your Booking No." + InvoiceId + "is confirmed.Your Check-In:" + CIn + " and Check-Out;" + COut + ". Thank You.";
                                            string Sms_Request = "http://www.smsjust.com/blank/sms/user/urlsms.php?username=ClickurtripNew&pass=88888&senderid=257147&dest_mobileno=917666249241&message=" + SMS + "&response=Y";
                                            string Sms_Response = "";
                                            int status = 0;
                                            bool SmsResponse = objSMS.Post(Sms_Request, out Sms_Response, out status);
                                            return js.Serialize(new { session = 1, retCode = 1, ReservationID = ReservationId, Status = BookingStatus });
                                        }

                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                }
                            }
                            else
                            {
                                CurrentStatus = "Error during Purchase Confirm";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, ReservationId, DumReservation);
                                return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                            }

                        }
                        else
                        {
                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                            return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                        }
                    }
                    else
                    {
                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                    }
                    #endregion TransactionScope
                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                    return js.Serialize(new { session = 1, retCode = 1, Message = CurrentStatus, ReservationID = ReservationId, Status = Status });


                }
                else
                {
                    js.MaxJsonLength = Int32.MaxValue;
                    return js.Serialize(new { session = 0, retCode = 0, Message = "Session Expired!" });
                }


            }
            #endregion MGH





            else
            {
                return js.Serialize(new { Message = "No API!" });
            }
        }

        #region Salman Work
        //[WebMethod(EnableSession = true)]
        //public string HotelConfirmBooking(string HotelCode, string RoomID, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<Guest> List_Guest, List<MGHLib.Request.Guest> Guest_listMGH)
        //{
        //    #region Variables & object
        //    string APIType = "HOTELBEDS";
        //    bool IsValidRoomID = true;
        //    string ReservationId = "";
        //    float TotalAmount = 0.0F;
        //    float ServiceCharges = 0.0F;
        //    float TotalPayable = 0.0F;
        //    string AffilateCode;
        //    string affcode = "";
        //    string Lang = "";
        //    string UserName = "";
        //    string Password = "";
        //    string CheckIn = "";
        //    string CheckOut = "";
        //    string sCheckIn = "";
        //    string sCheckOut = "";
        //    string DesitinationCode = "";
        //    string DesitionationType = "";
        //    string HotelName = "";
        //    string HotelType = "";
        //    string AvailToken = "";
        //    string SerViceType = "";
        //    int RoomCount = 0;
        //    int Adult = 0;
        //    int Child = 0;
        //    string OnRequest = "";
        //    string EchoToken = "";
        //    int rate = 0;
        //    string m_xml = "";
        //    string m_response = "";
        //    string RequestHeader = "";
        //    string ResponseHeader = "";
        //    int Status = 0;
        //    string PurchaseToken = String.Empty;
        //    int HolderAge = 0;
        //    string HolderName = "";
        //    string HolderLastName = "";
        //    string Suppliername = "";
        //    string VatNumber = "";
        //    string Comments = "";
        //    string Purchasetoken = "";
        //    int Officecode = 0;
        //    float SupplierTotalAmount = 0;
        //    float CutComm = 0;
        //    float AgentComm = 0;
        //    string RoomCode = "";
        //    int RoomNumber = 0;
        //    string SUPCancellationDate = "";
        //    string CUTCancellationDate = "";
        //    string CancellationAmount = "";
        //    string CanServiceTax = "";
        //    string CanAmountWithServiceTax = "";
        //    float SerTax = 0;
        //    float CanAmtWithTax = 0;
        //    float RoomSerTax = 0;
        //    float RoomAmtWithTax = 0;
        //    string ChildAge = "";
        //    string ComparedCurrency = "";
        //    float ComparedTotalAmount = 0;
        //    string LeadingGuestName = "";
        //    DateTime DeadLineDate;
        //    DateTime ChargeDate;
        //    string BookingDate;
        //    string DumReservation;
        //    string CurrentStatus = "Request";
        //    DBHelper.DBReturnCode RetCodeStatus;

        //    Occupancy objOccupancy = new Occupancy();
        //    // objects--common
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    GlobalDefault objGlobalDefault = null;
        //    MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();

        //    // objects--HOTELBEDS
        //    Models.Hotel objHotel = new Models.Hotel();
        //    HotelLib.Response.HotelDetail objHotelDetail = new HotelLib.Response.HotelDetail();
        //    List<Customer> List_Customer = new List<Customer>();
        //    List<HotelLib.Response.Contract> List_Contract = new List<HotelLib.Response.Contract>();
        //    HotelLib.Response.RoomOccupancy objRoomOccupancy = new HotelLib.Response.RoomOccupancy();
        //    List<HotelLib.Response.Room> Hotel_Occupancy = new List<HotelLib.Response.Room>();
        //    HotelLib.Request.ServiceAddRequest objService = new HotelLib.Request.ServiceAddRequest();
        //    HotelLib.Response.DateTimeFrom objDateTimeFrom = new HotelLib.Response.DateTimeFrom();
        //    HotelLib.Response.CancellationPolicy objCancellationPolicy = new HotelLib.Response.CancellationPolicy();
        //    HotelLib.Response.RoomType objRoomType = new HotelLib.Response.RoomType();
        //    HotelLib.Response.Price objPrice = new HotelLib.Response.Price();
        //    HotelLib.Response.Location objLocation = new HotelLib.Response.Location();


        //    Models.MGHHotel objMGHHotel = new Models.MGHHotel();

        //    com.HotelBookingHelper objBuilder = new com.HotelBookingHelper();
        //    com.HotelBookingHelperMGH objBuilderMGH = new HotelBookingHelperMGH();
        //    Models.MGHStatusCode objMGHStatusCode = new Models.MGHStatusCode();

        //    MGHLib.Response.MGHHotelDetails objMGHHotelDetail = new MGHLib.Response.MGHHotelDetails();
        //    #endregion

        //    #region HOTELBEDS
        //    if (APIType == "HOTELBEDS")
        //    {
        //        BookingDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //        js = new JavaScriptSerializer();
        //        IsValidRoomID = true;
        //        if (Session["HotelRoomIDs"] != null)
        //        {
        //            string[] arrHotelRoomIDs = Session["HotelRoomIDs"].ToString().Split('|');
        //            foreach (var objGuest in List_Guest)
        //            {
        //                if (objGuest.ID.Contains(' '))
        //                {
        //                    IsValidRoomID = false;
        //                    objGuest.ID = objGuest.ID.Replace(' ', '+');
        //                    for (int i = 0; i < arrHotelRoomIDs.Length; i++)
        //                    {
        //                        if (objGuest.ID == arrHotelRoomIDs[i])
        //                            IsValidRoomID = true;
        //                    }
        //                }
        //            }
        //        }
        //        if (!IsValidRoomID)
        //            return js.Serialize(new { session = 1, retCode = 0, Message = "Something went wrong! We recommend you to select other room options" });

        //        if (HttpContext.Current.Session["markups"] != null)
        //            objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
        //        else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
        //            objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
        //        ComparedCurrency = (Session["OtherCurrency"] != null) ? Session["OtherCurrency"].ToString() : "";
        //        ComparedTotalAmount = 0;
        //        if (AgencyRefernce == "")
        //            AgencyRefernce = "Test AgencyRef";
        //        if (com.Common.Session(out objGlobalDefault))
        //        {
        //            AffilateCode = "";
        //            affcode = "";
        //            objHotel = (Models.Hotel)Session["Avail"];
        //            objHotelDetail = (HotelLib.Response.HotelDetail)objHotel.HotelDetail.Where(data => data.HotelID == HotelCode).Select(data => data).FirstOrDefault();
        //            List_Customer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
        //            Lang = "ENG";
        //            UserName = System.Configuration.ConfigurationManager.AppSettings["APIUserName"];
        //            Password = System.Configuration.ConfigurationManager.AppSettings["APIPassword"];
        //            CheckIn = Convert.ToDateTime(objHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //            CheckOut = Convert.ToDateTime(objHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //            sCheckIn = Convert.ToDateTime(objHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //            sCheckOut = Convert.ToDateTime(objHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //            DesitinationCode = objHotelDetail.DestinationCode;
        //            DesitionationType = objHotelDetail.DestionationType;
        //            HotelName = objHotelDetail.Name;
        //            HotelType = objHotelDetail.HotelType;
        //            AvailToken = objHotelDetail.AvailToken;
        //            SerViceType = objHotelDetail.ServiceType;
        //            RoomCount = objHotel.DisplayRequest.Room;
        //            Adult = objHotel.DisplayRequest.Adult;
        //            Child = objHotel.DisplayRequest.Child;
        //            OnRequest = "N";
        //            List_Contract = objHotelDetail.ContractList;
        //            EchoToken = Guid.NewGuid().ToString().Replace("-", "");
        //            objRoomOccupancy = objHotelDetail.sRooms.Where(data => data.RoomID == RoomID).Select(data => data).First();
        //            Hotel_Occupancy = objRoomOccupancy.sRoom;
        //            ReservationId = "Dummy-" + GenerateRandomString(4);
        //            DumReservation = ReservationId;
        //            if (BookingStatus == "Vouchered")
        //            {

        //                TotalAmount = (objRoomOccupancy.CUTRoomPrice * objHotel.DisplayRequest.Night) + (objRoomOccupancy.AgentMarkup);
        //                SupplierTotalAmount = objRoomOccupancy.RoomPrice * objHotel.DisplayRequest.Night;
        //                CutComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
        //                AgentComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerAgentMarkup) / 100;
        //                ServiceCharges = (float)(TotalAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //                TotalPayable = TotalAmount + ServiceCharges;
        //                DBHelper.DBReturnCode retcode = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationId, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "Hotelbeds", SupplierTotalAmount, AgentComm, CutComm, "");

        //            }
        //            //till this point my ammount will be reduced......... now going to purchase ticket.....
        //            using (TransactionScope objTransactionScope = new TransactionScope())
        //            {
        //                objService = new ServiceAddRequest { AdultCount = Adult, AvailToken = AvailToken, CheckInDate = CheckIn, CheckOutDate = CheckOut, ChildCount = Child, ContractList = List_Contract, CustomerList = List_Guest, DestinationCode = DesitinationCode, DestinationType = DesitionationType, EchoToken = EchoToken, HotelCode = objHotelDetail.Code, HotelType = HotelType, Language = Lang, onRequest = OnRequest, Password = Password, RoomCount = RoomCount, RoomID = RoomID, ServiceType = SerViceType, UserName = UserName, Currency = "INR", HotelOccupancy = Hotel_Occupancy };
        //                m_xml = objService.GenerateXML(List_Guest);
        //                if (m_xml != null)
        //                {
        //                    Int64 Sid = 0;
        //                    TotalAmount = (objRoomOccupancy.CUTRoomPrice * objHotel.DisplayRequest.Night) + (objRoomOccupancy.AgentMarkup) + ((float)(TotalAmount * objMarkupsAndTaxes.PerServiceTax) / 100);
        //                    if (ComparedCurrency != "" && ComparedCurrency != "INR")
        //                    {
        //                        net.webservicex.www.Currency cur = CUT.Common.Common.GetCurrency(ComparedCurrency);
        //                        net.webservicex.www.CurrencyConvertor objCurrency = new net.webservicex.www.CurrencyConvertor();
        //                        rate = Convert.ToInt32(objCurrency.ConversionRate(cur, net.webservicex.www.Currency.INR));
        //                        ComparedTotalAmount = Convert.ToSingle(Math.Round((Decimal)(TotalAmount / rate), 2, MidpointRounding.AwayFromZero));
        //                    }
        //                    LeadingGuestName = List_Customer[0].Name + " " + List_Customer[0].LastName;
        //                    DeadLineDate = objRoomOccupancy.sRoom[0].NoChargeDate;
        //                    ChargeDate = objRoomOccupancy.sRoom[0].ChargeDate[0];


        //                    DBHelper.DBReturnCode retcode = HotelReservationManager.HotelReservationAddUpdate(Sid, objHotelDetail.Code, objHotelDetail.Name, objHotel.DisplayRequest.Location, sCheckIn, sCheckOut, objHotel.DisplayRequest.Night, objHotel.DisplayRequest.Room, TotalAmount, objHotelDetail.Currency, objHotel.DisplayRequest.Adult, objHotel.DisplayRequest.Child, ChargeDate, objHotelDetail.Description, DeadLineDate, out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, ComparedTotalAmount, MobileNo, Email, Remark);
        //                    affcode = AffilateCode;
        //                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        CurrentStatus = "Reservation Details Added";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);

        //                    }

        //                    else if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        objTransactionScope.Dispose();
        //                        CurrentStatus = "Reservation Details Insert Fail";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                        return js.Serialize(new { session = 1, retCode = 0, Message = "Error while inserting reservation details" });
        //                    }
        //                }
        //                m_response = "";
        //                RequestHeader = "";
        //                ResponseHeader = "";
        //                Status = 0;
        //                PurchaseToken = String.Empty;
        //                bool bResponse = objService.Post(m_xml, out m_response, out RequestHeader, out ResponseHeader, out Status);

        //                /*.... to comment/uncomment ..Start..*/
        //                //string txt = System.IO.File.ReadAllText(@"C:\Users\User1\Desktop\NiyazBhaiTest_Dubai\ServiceAddRS.xml");
        //                //m_response = txt;
        //                //bool bResponse = false;
        //                //if (m_response != "")
        //                //    bResponse = true;
        //                //Status = 1;
        //                /*.... to comment/uncomment ..End..*/

        //                using (TransactionScope objTransactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
        //                {
        //                    LogManager.Add(1, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
        //                    if (bResponse)
        //                    {
        //                        CurrentStatus = "Service Add Request Success";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                    }
        //                    else
        //                    {
        //                        CurrentStatus = "Service Add Request Fail";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                    }
        //                    objTransactionScope2.Complete();
        //                }
        //                if (bResponse)
        //                {
        //                    com.ParseServiceAddResponse objParseServiceAddResponse = new com.ParseServiceAddResponse(m_response);
        //                    HotelLib.Response.ServiceResponse objServiceResponse = objParseServiceAddResponse.GetServiceResponse();
        //                    EchoToken = Guid.NewGuid().ToString().Replace("-", "");
        //                    HolderAge = List_Customer.Where(data => data.type == "AD").Select(data => data.Age).FirstOrDefault();
        //                    HolderName = List_Customer.Where(data => data.type == "AD").Select(data => data.Name).FirstOrDefault();
        //                    HolderLastName = List_Customer.Where(data => data.type == "AD").Select(data => data.LastName).FirstOrDefault();
        //                    HotelLib.Request.PurchaseConfirmRequest objPurchaseConfirmRequest = new PurchaseConfirmRequest { AgencyReference = AgencyRefernce, CustomerList = List_Customer, EchoToken = EchoToken, HolderAge = HolderAge.ToString(), HolderName = HolderName, HolderLastName = HolderLastName, Language = Lang, Password = Password, PurchaseToken = objServiceResponse.PurchaseToken, RoomID = objServiceResponse.SPUI, UserName = UserName };
        //                    string m_p_xml = objPurchaseConfirmRequest.GenerateXML();
        //                    bResponse = objPurchaseConfirmRequest.Post(m_p_xml, out m_response, out RequestHeader, out ResponseHeader, out Status);


        //                    /*.... to comment/uncomment ..Start..*/
        //                    //txt = System.IO.File.ReadAllText(@"C:\Users\User1\Desktop\NiyazBhaiTest_Dubai\PurchaseConfirmRS.xml");
        //                    //m_response = txt;
        //                    //bResponse = true;
        //                    //Status = 1;
        //                    /*.... to comment/uncomment ..End..*/

        //                    using (TransactionScope objTransactionScope3 = new TransactionScope(TransactionScopeOption.Suppress))
        //                    {
        //                        LogManager.Add(2, RequestHeader, m_p_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
        //                        if (bResponse)
        //                        {
        //                            CurrentStatus = "Purchase Request Success";
        //                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                        }
        //                        else
        //                        {
        //                            CurrentStatus = "Purchase Request Fail";
        //                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                        }
        //                        objTransactionScope3.Complete();
        //                    }
        //                    js.MaxJsonLength = Int32.MaxValue;
        //                    if (bResponse)
        //                    {
        //                        DBHelper.DBReturnCode retcode;
        //                        com.ParsePurchaseConfirmResponse objParsePurchaseConfirmResponse = new com.ParsePurchaseConfirmResponse(m_response);
        //                        HotelLib.Response.PurchaseConfirmResponse objPurchaseConfirmResponse = objParsePurchaseConfirmResponse.GetPurchaseConfirmResponse();
        //                        Suppliername = objPurchaseConfirmResponse.Service[0].Supplier.Name;
        //                        //......... BBB... Under Test By Maqsood.........//
        //                        VatNumber = objPurchaseConfirmResponse.Service[0].Supplier.VatNumber;
        //                        //string Comments = "";
        //                        Comments = objPurchaseConfirmResponse.Service[0].Contract.Comments[0].Text;
        //                        Comments = HotelReservationManager.TrimWhiteSpace(Comments);


        //                        //......... EEE... Under Test By Maqsood.........//
        //                        Purchasetoken = objPurchaseConfirmResponse.PurchaseToken;
        //                        Officecode = objPurchaseConfirmResponse.Service[0].Reference.IncomingOfficeCode;
        //                        ReservationId = objPurchaseConfirmResponse.Service[0].Reference.FileNumber;
        //                        if (ReservationId.LastIndexOf("-") > 0)
        //                            ReservationId = ReservationId.Substring(0, ReservationId.LastIndexOf("-"));
        //                        // code for database insertion here 

        //                        retcode = HotelReservationManager.HotelReservationUpdate(affcode, BookingStatus, Suppliername, VatNumber, ReservationId, Purchasetoken, Officecode, Comments);
        //                        // retcode = HotelReservationManager.HotelReservationUpdate("123", "Vouchered", "HotelBeds Spain S.L.U", "456", "789", "101112", 121212, "Test 12 Jan 2016");
        //                        if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //                        {
        //                            CurrentStatus = "Reservation Details Updated";
        //                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);

        //                        }
        //                        else if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //                        {
        //                            objTransactionScope.Dispose();
        //                            CurrentStatus = "Reservation Details Update Fail";
        //                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                            return js.Serialize(new { session = 1, retCode = 0, Message = "Error while updating reservation details" });
        //                        }

        //                        if (BookingStatus == "Vouchered")
        //                        {
        //                            DBHelper.DBReturnCode retCodeUpdateTXN;
        //                            SupplierTotalAmount = objRoomOccupancy.RoomPrice * objHotel.DisplayRequest.Night;
        //                            CutComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
        //                            AgentComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerAgentMarkup) / 100;
        //                            ServiceCharges = (float)(TotalAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //                            TotalPayable = TotalAmount + ServiceCharges;
        //                            retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnUpdate(ReservationId, DumReservation);

        //                            if (retCodeUpdateTXN == DBHelper.DBReturnCode.SUCCESS)
        //                            {
        //                                CurrentStatus = "Reservation-Id Added Success";
        //                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                            }

        //                            else if (retCodeUpdateTXN != DBHelper.DBReturnCode.SUCCESS)
        //                            {
        //                                objTransactionScope.Dispose();
        //                                CurrentStatus = "Reservation-Id Insert Fail";
        //                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                                return js.Serialize(new { session = 1, retCode = 0, Message = "Error while Updating transaction details" });
        //                            }


        //                        }

        //                        List<string> lstLeadingGuests = new List<string>();
        //                        List<string> lstChildAge = new List<string>();
        //                        foreach (HotelLib.Request.Guest guestroom in List_Guest)
        //                        {
        //                            RoomCode = guestroom.ID;
        //                            RoomNumber = guestroom.RoomNumber;
        //                            int k = 0;
        //                            foreach (HotelLib.Request.Customer customer in guestroom.sCustomer)
        //                            {
        //                                if (customer.type == "CH")
        //                                    lstChildAge.Add(customer.Age.ToString());
        //                                if (k == 0)
        //                                {
        //                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 1, ReservationId);
        //                                    lstLeadingGuests.Add(customer.Name + " " + customer.LastName);
        //                                }
        //                                else
        //                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 0, ReservationId);
        //                                if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //                                {
        //                                    CurrentStatus = "PASSENGER Added Success";
        //                                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                                }

        //                                if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //                                {
        //                                    objTransactionScope.Dispose();
        //                                    CurrentStatus = "PASSENGER Inset Fail";
        //                                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding PASSENGER" });
        //                                }

        //                                k++;
        //                            }

        //                        }

        //                        int n = 0;
        //                        int l = 0;
        //                        foreach (HotelLib.Response.Room objRoom in objRoomOccupancy.sRoom)
        //                        {
        //                            //..... BBB ..... Under test by Maqsood...................//
        //                            //for (int y = 0; y < objRoom.RoomCount; y++)
        //                            //{
        //                            SUPCancellationDate = "";
        //                            CUTCancellationDate = "";
        //                            CancellationAmount = "";
        //                            CanServiceTax = "";
        //                            CanAmountWithServiceTax = "";
        //                            for (int i = 0; i < objRoom.CUTCancellationAmount.Count; i++)
        //                            {
        //                                SerTax = 0;
        //                                CanAmtWithTax = 0;
        //                                SerTax = (objRoom.CUTCancellationAmount[i] * objMarkupsAndTaxes.PerServiceTax) / 100;
        //                                CanAmtWithTax = SerTax + objRoom.CUTCancellationAmount[i];
        //                                SUPCancellationDate += "|" + objRoom.ChargeDate[i].ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //                                CUTCancellationDate += "|" + objRoom.ChargeDate[i].AddHours(-24).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //                                CancellationAmount += "|" + objRoom.CUTCancellationAmount[i].ToString();
        //                                CanServiceTax += "|" + SerTax.ToString();
        //                                CanAmountWithServiceTax += "|" + CanAmtWithTax.ToString();
        //                            }
        //                            SUPCancellationDate = SUPCancellationDate.TrimStart('|');
        //                            CUTCancellationDate = CUTCancellationDate.TrimStart('|');
        //                            CancellationAmount = CancellationAmount.TrimStart('|');
        //                            CanServiceTax = CanServiceTax.TrimStart('|');
        //                            CanAmountWithServiceTax = CanAmountWithServiceTax.TrimStart('|');
        //                            RoomSerTax = (objRoom.CUTRoomAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //                            RoomAmtWithTax = RoomSerTax + objRoom.CUTRoomAmount;
        //                            ChildAge = "";
        //                            for (int z = 0; z < objRoom.ChildCount; z++)
        //                            {
        //                                ChildAge = ChildAge + "|" + lstChildAge[l];
        //                                l++;
        //                            }
        //                            ChildAge = ChildAge.TrimStart('|');


        //check here


        //                            retcode = HotelReservationManager.BookedRoomAdd(objRoom.SHRUI, objRoom.Roomtext, "Room " + (n + 1), objRoom.Boardtext, objRoom.RoomCount, lstLeadingGuests[n], objRoom.AdultCount, objRoom.ChildCount, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, CanServiceTax, CanAmountWithServiceTax, objRoom.CUTRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
        //                            n++;
        //                            if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //                            {
        //                                CurrentStatus = "Room Insert Success";
        //                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                            }
        //                            else if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //                            {
        //                                objTransactionScope.Dispose();
        //                                CurrentStatus = "Room Insert Fail";
        //                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                                return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding ROOM" });
        //                            }

        //                            //}
        //                            //..... EEE ..... Under test by Maqsood...................//          
        //                        }
        //                        objTransactionScope.Complete();
        //                        CurrentStatus = "Booking Success";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                        return js.Serialize(new { session = 1, retCode = 1, ReservationID = ReservationId, Status = BookingStatus });
        //                    }
        //                    else
        //                    {
        //                        objTransactionScope.Dispose();
        //                        CurrentStatus = "Purchase confirm Fail";
        //                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
        //                        return js.Serialize(new { session = 1, retCode = 0, Message = "Error during purchase confirm" });
        //                    }
        //                }
        //                else
        //                {
        //                    objTransactionScope.Dispose();
        //                    js.MaxJsonLength = Int32.MaxValue;
        //                    CurrentStatus = "Error during service add";
        //                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);

        //                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error during service add" });
        //                }
        //            }
        //        }
        //        else
        //        {
        //            js.MaxJsonLength = Int32.MaxValue;

        //            return js.Serialize(new { session = 0, retCode = 0, Message = "Session Expired!" });
        //        }

        //    }
        //    #endregion

        //    #region MGH
        //    //if (APIType == "MGH")
        //    //{

        //    //    //List<MGHLib.Request.Guest> Guest_listMGH=new List<MGHLib.Request.Guest>();
        //    //    //Guest_listMGH= (MGHLib.Request.Guest) List_Guest;
        //    //    js = new JavaScriptSerializer();
        //    //    IsValidRoomID = true;
        //    //    if (Session["HotelRoomIDs"] != null)
        //    //    {
        //    //        string[] arrHotelRoomIDs = Session["HotelRoomIDs"].ToString().Split('|');
        //    //        foreach (var objGuest in List_Guest)
        //    //        {
        //    //            if (objGuest.ID.Contains(' '))
        //    //            {
        //    //                IsValidRoomID = false;
        //    //                objGuest.ID = objGuest.ID.Replace(' ', '+');
        //    //                for (int i = 0; i < arrHotelRoomIDs.Length; i++)
        //    //                {
        //    //                    if (objGuest.ID == arrHotelRoomIDs[i])
        //    //                        IsValidRoomID = true;
        //    //                }
        //    //            }
        //    //        }
        //    //    }
        //    //    if (!IsValidRoomID)
        //    //        return js.Serialize(new { session = 1, retCode = 0, Message = "Something went wrong! We recommend you to select other room options" });

        //    //    if (HttpContext.Current.Session["markups"] != null)
        //    //        objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
        //    //    else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
        //    //        objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
        //    //    ComparedCurrency = (Session["OtherCurrency"] != null) ? Session["OtherCurrency"].ToString() : "";
        //    //    ComparedTotalAmount = 0;
        //    //    if (AgencyRefernce == "")
        //    //        AgencyRefernce = "Test AgencyRef";
        //    //    BookingDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
        //    //    objMGHHotel = (Models.MGHHotel)Session["MGHAvail"];
        //    //    objMGHHotelDetail = objMGHHotel.HotelDetail.Where(data => data.HotelId == Convert.ToInt32(HotelCode)).FirstOrDefault();
        //    //    objMGHStatusCode = (Models.MGHStatusCode)Session["MGHStatus"];
        //    //    TotalAmount = objMGHHotelDetail.rate[0].refundPolicy[0].CutRoomAmount * objMGHHotel.DisplayRequest.Night;
        //    //    ReservationId = "Dummy-" + GenerateRandomString(4);
        //    //    if (com.Common.Session(out objGlobalDefault))
        //    //    {

        //    //        DBHelper.DBReturnCode retCode;
        //    //        if (BookingStatus == "Vouchered")
        //    //        {
        //    //            CutComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
        //    //            AgentComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerAgentMarkup) / 100;
        //    //            ServiceCharges = (float)(TotalAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //    //            TotalPayable = TotalAmount + ServiceCharges;

        //    //            retCode = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationId, BookingDate, TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "MGH", SupplierTotalAmount, AgentComm, CutComm, "");
        //    //            if (retCode != DBHelper.DBReturnCode.SUCCESS)
        //    //            {
        //    //                //objTransactionScope.Dispose();
        //    //                return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding transaction details" });
        //    //            }
        //    //        }



        //    //        using (TransactionScope objTransactionScope = new TransactionScope())
        //    //        {
        //    //            AffilateCode = "";
        //    //            affcode = "";



        //    //            string latitude = (objMGHHotelDetail.Latitude).ToString();
        //    //            string Longitude = (objMGHHotelDetail.Longitude).ToString();

        //    //            List_Customer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
        //    //            Lang = "ENG";
        //    //            UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
        //    //            Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];

        //    //            //CheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //    //            //CheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("ddMMyyy");

        //    //            //CheckOut = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //    //            //sCheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
        //    //            //sCheckOut = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
        //    //            //sCheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("dd-MM-yyyy HH:mm", CultureInfo.CurrentCulture);


        //    //            DesitinationCode = (objMGHHotelDetail.Location[0].LocationId).ToString();
        //    //            CheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("ddMMyyy");
        //    //            sCheckIn = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("dd/MM/yyyy HH:mm", CultureInfo.CurrentCulture);
        //    //            CheckOut = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("ddMMyyy");
        //    //            sCheckOut = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("dd/MM/yyyy HH:mm", CultureInfo.CurrentCulture);


        //    //            string CheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //    //            string sCheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
        //    //            string CheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
        //    //            string sCheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
        //    //            HotelName = objMGHHotelDetail.HotelName;
        //    //            HotelType = objMGHHotelDetail.Category[0].CategoryName;



        //    //            RoomCount = objMGHHotel.DisplayRequest.Room;
        //    //            Adult = objMGHHotel.DisplayRequest.Adult;
        //    //            Child = objMGHHotel.DisplayRequest.Child;
        //    //            OnRequest = "N";
        //    //            MGHLib.Response.Rate objRate = new MGHLib.Response.Rate();
        //    //            //m_xml = "search&rooms=1&token=8d75e20b9e2e2c96dfc94a284fde249d&location_id=25662&locality_id=&facility=1&checkin_date=2015-12-03&checkout_date=2015-12-04&occupant_adult=1&occupant_child=0";
        //    //            m_xml = "blockRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&occupant_adult=" + Adult + "&occupant_child=" + Child + "&room_category_id=" + RoomID + "&class_type=1|Single&hotelid= " + HotelCode + "&clientBookingID=XYZ123456&payment_model=BTC";
        //    //            // m_xml = "blockRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&occupant_adult=" + Adult + "&occupant_child=" + Child + "&room_category_id=" + RoomID + "&class_type=1|Single&hotelid= " + HotelCode + "&clientBookingID = XYZ123456 & payment_model = BTC";
        //    //            // m_xml = "blockRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&occupant_adult=" + Adult + "&occupant_child=" + Child + "&room_category_id=9234&class_type=1|Single&hotelid=4462&clientBookingID=XYZ123456&payment_model=BTC";


        //    //            if (m_xml != null)
        //    //            {
        //    //                Int64 Sid = 0;
        //    //                TotalAmount = objMGHHotelDetail.rate[0].refundPolicy[0].CutRoomAmount * objMGHHotel.DisplayRequest.Night;

        //    //                if (ComparedCurrency != "" && ComparedCurrency != "INR")
        //    //                {
        //    //                    net.webservicex.www.Currency cur = CUT.Common.Common.GetCurrency(ComparedCurrency);
        //    //                    net.webservicex.www.CurrencyConvertor objCurrency = new net.webservicex.www.CurrencyConvertor();
        //    //                    rate = Convert.ToInt32(objCurrency.ConversionRate(cur, net.webservicex.www.Currency.INR));
        //    //                    ComparedTotalAmount = Convert.ToSingle(Math.Round((Decimal)(TotalAmount / rate), 2, MidpointRounding.AwayFromZero));
        //    //                }
        //    //                LeadingGuestName = List_Customer[0].Name + " " + List_Customer[0].LastName;

        //    //                //for timing
        //    //                ChargeDate = Convert.ToDateTime((Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn).AddHours(-(24 * Convert.ToInt32(objMGHHotelDetail.rate[0].refundPolicy[0].dayMax))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture)));
        //    //                DeadLineDate = Convert.ToDateTime((Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn).AddHours(-(24 + 24 * Convert.ToInt32(objMGHHotelDetail.rate[0].refundPolicy[0].dayMax))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture)));
        //    //                //ChargeDate = Convert.ToDateTime(Convert.ToDateTime(objRate.checkout).AddHours(-24).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
        //    //                //  DeadLineDate =Convert.ToDateTime (Convert.ToDateTime(objRate.checkin).AddHours(-24).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
        //    //                DBHelper.DBReturnCode retcode = HotelReservationManager.HotelReservationAddUpdateMGH(Sid, objMGHHotelDetail.HotelId.ToString(), objMGHHotelDetail.HotelName, objMGHHotel.DisplayRequest.Location, sCheckIn, sCheckOut, objMGHHotel.DisplayRequest.Night, objMGHHotel.DisplayRequest.Room, TotalAmount, objMGHHotelDetail.Currency, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, ChargeDate, objMGHHotelDetail.Description, DeadLineDate, out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, ComparedTotalAmount, MobileNo, Email, Remark, latitude, Longitude);

        //    //                if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //    //                {
        //    //                    objTransactionScope.Dispose();
        //    //                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while inserting reservation details" });
        //    //                }
        //    //                affcode = AffilateCode;
        //    //            }
        //    //            m_response = "";
        //    //            RequestHeader = "";
        //    //            ResponseHeader = "";
        //    //            Status = 0;
        //    //            PurchaseToken = String.Empty;





        //    //            MGHLib.Common.ServiceAddRequestMGH webclient = new MGHLib.Common.ServiceAddRequestMGH();

        //    //            bool bResponse = webclient.Post(m_xml, out ResponseHeader, out Status);

        //    //            using (TransactionScope objTransactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
        //    //            {

        //    //                LogManager.Add(1, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
        //    //                objTransactionScope2.Complete();
        //    //            }

        //    //            string BlockID = "";
        //    //            if (bResponse)
        //    //            {
        //    //                string leadingGeust = Guest_listMGH[0].sCustomer[0].Name + " " + Guest_listMGH[0].sCustomer[0].LastName;
        //    //                ParseMGHResponse objBlockID = new ParseMGHResponse();
        //    //                BlockID = objBlockID.GetBlockID(ResponseHeader);
        //    //                //m_xml = "blockRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&occupant_adult=" + Adult + "&occupant_child=" + Child + "&room_category_id=" + RoomID + "&class_type=1|Single&hotelid= " + HotelCode + "&clientBookingID = XYZ123456 & payment_model = BTC";
        //    //                //m_xml = "bookRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&hotelid=4462&room_category_id = 9234 &block_ref_no=" + BlockID + "&guest_name=Test&guest_email=test%40salman.com&guest_phone=9898989898&guest_ident_type=&guest_ident=&guest_ident=";
        //    //                m_xml = "bookRoom&token=8d75e20b9e2e2c96dfc94a284fde249d&hotelid=4462&room_category_id =" + RoomID + "&block_ref_no=" + BlockID + "&guest_name=" + leadingGeust + "&guest_email=test%40salman.com&guest_phone=9898989898&guest_ident_type=&guest_ident=&guest_ident=";
        //    //                bool bResponseBooking = webclient.Post(m_xml, out ResponseHeader, out Status);
        //    //                ReservationId = objBlockID.GetBookingID(ResponseHeader);
        //    //                //bool bResponseBooking = true;
        //    //                //ReservationId = GenerateRandomString(4);
        //    //                DBHelper.DBReturnCode retcode1;
        //    //                retcode1 = HotelReservationManager.HotelReservationUpdate(affcode, BookingStatus, Suppliername = "MGH", VatNumber, ReservationId, Purchasetoken, Officecode, Comments);
        //    //                if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
        //    //                {
        //    //                    objTransactionScope.Dispose();
        //    //                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while updating reservation details" });
        //    //                }
        //    //                latitude = (objMGHHotelDetail.Latitude).ToString();
        //    //                Longitude = (objMGHHotelDetail.Longitude).ToString();
        //    //                string HotelCodeMGH = (objMGHHotelDetail.HotelId).ToString();
        //    //                string HotelNameMGH = (objMGHHotelDetail.HotelName);
        //    //                string HotelAdd = (objMGHHotelDetail.Address);
        //    //                string Zip = (objMGHHotelDetail.Zip);
        //    //                string Description = (objMGHHotelDetail.Description);
        //    //                string ContactPerson = (objMGHHotelDetail.ContactDetails.ContactName);
        //    //                string ContactEmail = (objMGHHotelDetail.ContactDetails.ContactEmail);
        //    //                string ContactMobile = (objMGHHotelDetail.ContactDetails.ContactMobile);
        //    //                string ContactPhone = (objMGHHotelDetail.ContactDetails.ContactPhone);
        //    //                string Country = objMGHHotelDetail.Location[0].Country;
        //    //                string City = objMGHHotelDetail.Location[0].LocationName;

        //    //                DBHelper.DBReturnCode retcode2;
        //    //                retcode2 = HotelReservationManager.HotelMGHAdd(ReservationId, HotelCode, HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, Country, City);
        //    //                if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
        //    //                {
        //    //                    objTransactionScope.Dispose();
        //    //                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while Adding Hotel  details" });
        //    //                }


        //    //                using (TransactionScope objTransactionScope3 = new TransactionScope(TransactionScopeOption.Suppress))
        //    //                {
        //    //                    LogManager.Add(2, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
        //    //                    objTransactionScope3.Complete();
        //    //                }
        //    //                js.MaxJsonLength = Int32.MaxValue;
        //    //                if (bResponseBooking)
        //    //                {
        //    //                    DBHelper.DBReturnCode retcode;

        //    //                    List<string> lstLeadingGuests = new List<string>();
        //    //                    List<string> lstChildAge = new List<string>();

        //    //                    DBHelper.DBReturnCode retCodeUpdateTXN;
        //    //                    if (BookingStatus == "Vouchered")
        //    //                    {

        //    //                        CutComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
        //    //                        AgentComm = (SupplierTotalAmount * objMarkupsAndTaxes.PerAgentMarkup) / 100;
        //    //                        ServiceCharges = (float)(TotalAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //    //                        TotalPayable = TotalAmount + ServiceCharges;
        //    //                        retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnUpdate(ReservationId, BookingDate, TotalPayable, BookingDate);

        //    //                        // retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationId, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "MGH", SupplierTotalAmount, AgentComm, CutComm, "");
        //    //                        if (retCodeUpdateTXN != DBHelper.DBReturnCode.SUCCESS)
        //    //                        {
        //    //                            objTransactionScope.Dispose();
        //    //                            return js.Serialize(new { session = 1, retCode = 0, Message = "Error while Updating transaction details" });
        //    //                        }
        //    //                    }
        //    //                    string leadingGuest = "";

        //    //                    foreach (MGHLib.Request.Guest guestroom in Guest_listMGH)
        //    //                    {
        //    //                        RoomCode = RoomID;
        //    //                        RoomNumber = 1;
        //    //                        int k = 0;

        //    //                        foreach (MGHLib.Request.Customer customer in guestroom.sCustomer)
        //    //                        {

        //    //                            foreach (var scustomer in List_Guest)
        //    //                            {

        //    //                                if (k == 0)
        //    //                                {
        //    //                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 1, ReservationId);
        //    //                                    lstLeadingGuests.Add(customer.Name + " " + customer.LastName);
        //    //                                    leadingGuest = customer.Name + " " + customer.LastName;
        //    //                                }
        //    //                                else
        //    //                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 0, ReservationId);
        //    //                                if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //    //                                {
        //    //                                    objTransactionScope.Dispose();
        //    //                                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding PASSENGER" });
        //    //                                }
        //    //                                k++;
        //    //                            }

        //    //                        }
        //    //                    }
        //    //                    int l = 0;
        //    //                    int n = 0;
        //    //                    for (int k = 0; k < objMGHHotelDetail.rate.Count; k++)
        //    //                    {

        //    //                        SUPCancellationDate = "";
        //    //                        CUTCancellationDate = "";
        //    //                        CancellationAmount = "";
        //    //                        CanServiceTax = "";

        //    //                        for (int j = 0; j < objMGHHotelDetail.rate[k].roomInfo.Count; j++)
        //    //                        {

        //    //                            if (objMGHHotelDetail.RoomCategory[k].RoomCategoryId == Convert.ToInt64(RoomID))
        //    //                            {


        //    //                                SerTax = 0;
        //    //                                CanAmtWithTax = 0;
        //    //                                SerTax = (objMGHHotelDetail.rate[k].refundPolicy[j].CUTCancellationAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //    //                                CanAmtWithTax = SerTax + objMGHHotelDetail.rate[k].refundPolicy[j].CUTCancellationAmount; ;
        //    //                                SUPCancellationDate += "|" + (Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn).AddHours(-(24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[j].dayMax))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
        //    //                                CUTCancellationDate += "|" + (Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn).AddHours(-(24 + 24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[j].dayMax))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
        //    //                                CancellationAmount += "|" + objMGHHotelDetail.rate[k].refundPolicy[j].CUTCancellationAmount.ToString();
        //    //                                CanServiceTax += "|" + SerTax.ToString();
        //    //                                CanAmountWithServiceTax += "|" + CanAmtWithTax.ToString();

        //    //                                SUPCancellationDate = SUPCancellationDate.TrimStart('|');
        //    //                                CUTCancellationDate = CUTCancellationDate.TrimStart('|');
        //    //                                CancellationAmount = CancellationAmount.TrimStart('|');
        //    //                                CanServiceTax = CanServiceTax.TrimStart('|');
        //    //                                CanAmountWithServiceTax = CanAmountWithServiceTax.TrimStart('|');
        //    //                                RoomSerTax = (objMGHHotelDetail.rate[k].refundPolicy[j].CutRoomAmount * objMarkupsAndTaxes.PerServiceTax) / 100;
        //    //                                RoomAmtWithTax = RoomSerTax + objMGHHotelDetail.rate[k].refundPolicy[j].CutRoomAmount;
        //    //                                float CutRoomAmount = objMGHHotelDetail.rate[k].refundPolicy[j].CutRoomAmount;
        //    //                                ChildAge = "";

        //    //                                //for (int z = 0; z < objMGHHotel.DisplayRequest.Child; z++)
        //    //                                //{
        //    //                                //    ChildAge = ChildAge + "|" + lstChildAge[l];
        //    //                                //    l++;
        //    //                                //}
        //    //                                ChildAge = ChildAge.TrimStart('|');
        //    //                                string MEALPLAN = objMGHHotelDetail.rate[0].mealPlan[0].text;
        //    //                                string RoomType = objMGHHotelDetail.rate[0].roomType[0].RoomTypeName;
        //    //                                retcode = HotelReservationManager.BookedRoomAdd(RoomCode, RoomType, "Room " + (n + 1), MEALPLAN, objMGHHotel.DisplayRequest.Room, leadingGuest, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, CanServiceTax, CanAmountWithServiceTax, CutRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
        //    //                                n++;
        //    //                                if (retcode != DBHelper.DBReturnCode.SUCCESS)
        //    //                                {
        //    //                                    objTransactionScope.Dispose();
        //    //                                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding ROOM" });
        //    //                                }
        //    //                            }
        //    //                        }
        //    //                    }
        //    //                    objTransactionScope.Complete();
        //    //                    return js.Serialize(new { session = 1, retCode = 1, ReservationID = ReservationId, Status = BookingStatus });
        //    //                }
        //    //                else
        //    //                {
        //    //                    objTransactionScope.Dispose();
        //    //                    return js.Serialize(new { session = 1, retCode = 0, Message = "Error during purchase confirm" });
        //    //                }
        //    //            }
        //    //            else
        //    //            {
        //    //                objTransactionScope.Dispose();
        //    //                js.MaxJsonLength = Int32.MaxValue;
        //    //                return js.Serialize(new { session = 1, retCode = 0, Message = "Error during service add" });
        //    //            }
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        js.MaxJsonLength = Int32.MaxValue;
        //    //        return js.Serialize(new { session = 0, retCode = 0, Message = "Session Expired!" });
        //    //    }

        //    //}
        //    #endregion MGH
        //    else
        //    {
        //        return js.Serialize(new { Message = "No API!" });
        //    }
        //}
        #endregion Salman Work

        [WebMethod(EnableSession = true)]
        public string HotelCancelBooking(string ReservationID, string ReferenceCode, string CancellationAmount, string BookingStatus, string Remark, string TotalFare, string ServiceCharge, string Total, string Type)
        {

            switch (Type)
            {
                case "A":
                    Type = "HotelBeds";
                    break;
                case "B":
                    Type = "MGH";
                    break;
                case "C":
                    Type = "DoTW";
                    break;
                case "D":
                    Type = "Expedia";
                    break;
            }

            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            ParentID = objGlobalDefault.sid;
            if (ParentID != 0)
            {
                #region MGH
                if (Type == "MGH")
                {
                    string m_response = "";
                    string RequestHeader = "";
                    string ResponseHeader = "";
                    int Status = 0;
                    string UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
                    string Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];
                    MGHLib.Common.ServiceAddRequestMGH webclient = new MGHLib.Common.ServiceAddRequestMGH();
                    ParseMGHResponse objBlockID = new ParseMGHResponse();
                    string m_xml = "processCancelBooking&token=" + Password + "&book_ref_no=" + ReservationID;
                    bool bResponseCancel = webclient.Post(m_xml, out ResponseHeader, out Status);
                    LogManager.Add(3, RequestHeader, m_xml, ResponseHeader, m_response, ParentID, Status);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = Int32.MaxValue;
                    if (bResponseCancel)
                    {
                        bool CancelStatus = objBlockID.GetCancelStatus(ResponseHeader);

                        if (CancelStatus)
                        {
                            //string CanDetails = objBlockID.GetCancelDetail(ResponseHeader);
                            //string[] arrCanDetails = CanDetails.Split('|');

                            //CancellationAmount = arrCanDetails[1];
                            //string CanFrefNo = arrCanDetails[2];
                            BookingStatus = "Vouchered";
                            DBHelper.DBReturnCode retCode = HotelReservationManager.CancelBooking(ReservationID, CancellationAmount, BookingStatus, Remark, TotalFare, ServiceCharge, Total);

                            if (retCode != DBHelper.DBReturnCode.SUCCESS)
                            {
                                return js.Serialize(new { session = 1, retCode = 0, Message = "Error on cancel transaction" });
                            }
                            else
                            {
                                CUT.Common.SMSWebClient objSMS = new SMSWebClient();
                                DataTable dt = null;
                                DBHelper.DBReturnCode RetSMS = HotelReservationManager.BookingDetailForSMS(ReservationID, out dt);
                                string Mobile = dt.Rows[0]["Mobile"].ToString();
                                string InvoiceId = dt.Rows[0]["InvoiceID"].ToString();
                                string CIn = dt.Rows[0]["CheckIn"].ToString();
                                string COut = dt.Rows[0]["CheckOut"].ToString();
                                string SMS = "Your Booking No." + InvoiceId + " has been cancelled.Thank You.";
                                string Sms_Request = "http://www.smsjust.com/blank/sms/user/urlsms.php?username=ClickurtripNew&pass=88888&senderid=257147&dest_mobileno=" + Mobile + "&message=" + SMS + "&response=Y";
                                string Sms_Response = "";
                                int status = 0;
                                bool SmsResponse = objSMS.Post(Sms_Request, out Sms_Response, out status);
                                return js.Serialize(new { session = 1, retCode = 1 });
                            }
                        }
                        else
                            return js.Serialize(new { session = 1, retCode = 0, Message = "Request successfully completed! But not cancelled from supplier." });
                    }
                    else
                        return js.Serialize(new { session = 1, retCode = 0, Message = "Error while cancelling booking" });
                }
                #endregion MGH
            }

            JavaScriptSerializer JS = new JavaScriptSerializer();
            JS.MaxJsonLength = Int32.MaxValue;
            return JS.Serialize(new { session = 0, retCode = 0, Message = "Session Expired!" });


        }

        [WebMethod(EnableSession = true)]
        public string ConfirmHoldBooking(string ReservationID)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //js.MaxJsonLength = Int32.MaxValue;
            //DBHelper.DBReturnCode retcode;
            //DataTable dtResult;
            //MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            //if (HttpContext.Current.Session["markups"] != null)
            //    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            //else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
            //    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
            //using (TransactionScope objTransactionScope = new TransactionScope())
            //{
            //    DBHelper.DBReturnCode retCode = HotelReservationManager.HotelReservationLoadByResID(ReservationID, out dtResult);
            //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        float BookingAmtWithTax = Convert.ToSingle(dtResult.Rows[0]["TotalFare"]) + Convert.ToSingle(dtResult.Rows[0]["Servicecharge"]);
            //        GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //        float AvailableCrdit = objGlobalDefaults.AvailableCrdit;
            //        if (AvailableCrdit < BookingAmtWithTax)
            //        {
            //            objTransactionScope.Dispose();
            //            return js.Serialize(new { session = 1, retCode = 2, Message = "Insufficient Balance to complete this booking." });
            //        }
            //        else
            //        {
            //            float BookingAmtWithoutTax = Convert.ToSingle(dtResult.Rows[0]["TotalFare"]);
            //            float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["Servicecharge"]);

            //            string BookingDate = dtResult.Rows[0]["ReservationDate"].ToString();
            //            string BookingRemark = dtResult.Rows[0]["AgentRemark"].ToString();
            //            //string Supplier = dtResult.Rows[0]["Source"].ToString();
            //            float SupplierBasicAmt = (BookingAmtWithoutTax / (objMarkupsAndTaxes.PerCutMarkup + 100)) * 100; ;
            //            float CutComm = (SupplierBasicAmt * objMarkupsAndTaxes.PerCutMarkup) / 100;
            //            float AgentComm = (SupplierBasicAmt * objMarkupsAndTaxes.PerAgentMarkup) / 100;
            //            string BookedBy = dtResult.Rows[0]["AgentRef"].ToString();
            //            retcode = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationID, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "Hotelbeds", SupplierBasicAmt, AgentComm, CutComm, BookedBy, BookingStatus);
            //            if (retcode != DBHelper.DBReturnCode.SUCCESS)
            //            {
            //                objTransactionScope.Dispose();
            //                return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding transaction details" });
            //            }
            //            else
            //            {
            //                objTransactionScope.Complete();
            //                return js.Serialize(new { session = 1, retCode = 1 });
            //            }
            //        }
            //    }
            //    else
            //    {
            //        objTransactionScope.Dispose();
            //        return js.Serialize(new { session = 0, retCode = 0, Message = "Error occured during loading hotel details!" });
            //    }
            //}
            return "";
        }

        [WebMethod(EnableSession = true)]
        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        [WebMethod(EnableSession = true)]
        public string GetCancellationDetails(string ReservationID)
        {
            DataSet dsResult;
            DataTable dtHotelReservation, dtBookedRooms, dtBookingTransactions;
            string sHotelReservation, sBookedRooms;
            int Iscancelable = 0;
            int IsConfirmable = 0;
            //string sCancellationDate = "";
            float fCancellationAmount = 0;
            string TotalFare = "";
            string Total = "";
            string ServiceCharge = "";
            string[] arrAmount;
            string[] arrCanDate;
            string Source = "";
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
            DBHelper.DBReturnCode retCode = HotelReservationManager.GetCancellationDetails(ReservationID, out dsResult);
            if (DBHelper.DBReturnCode.SUCCESS != retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            else
            {

                dtHotelReservation = dsResult.Tables[0];
                dtBookedRooms = dsResult.Tables[1];
                dtBookingTransactions = dsResult.Tables[2];
                Source = dtHotelReservation.Rows[0]["Source"].ToString();
                TotalFare = dtHotelReservation.Rows[0]["TotalFare"].ToString();
                //ServiceCharge = dtHotelReservation.Rows[0]["ServiceCharge"].ToString();
                ServiceCharge = dtBookingTransactions.Rows[0]["SeviceTax"].ToString();
                //Total = dtBookedRooms.Rows[0]["CanAmtWithTax"].ToString();
                string CCD = dtBookedRooms.Rows[0]["CutCancellationDate"].ToString();
                arrCanDate = CCD.Split('|');
                string chkdt = dtHotelReservation.Rows[0]["CheckIn"].ToString();

                if (Source == "MGH")
                {
                    if (chkdt.Contains("-") == true)
                    {
                        chkdt = chkdt.Replace("-", "/");
                    }
                    if (chkdt.Contains("00:00") != true)
                    {
                        chkdt = chkdt + " 00:00";
                    }
                    string CanDate = arrCanDate[0];
                    if (CanDate.Contains("-") == true)
                    {
                        CanDate = CanDate.Replace("-", "/");
                    }
                    if (DateTime.Now > DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))
                    {
                        Iscancelable = 0;
                        IsConfirmable = 0;
                    }

                    else if (DateTime.ParseExact(CanDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                    {
                        Iscancelable = 1;
                        IsConfirmable = 1;
                    }
                    if (arrCanDate.Length == 1)
                    {
                        CanDate = dtBookedRooms.Rows[0]["CutCancellationDate"].ToString();
                        if (CanDate.Contains("-") == true)
                        {
                            CanDate = CanDate.Replace("-", "/");
                        }
                        //  if (DateTime.ParseExact(dtBookedRooms.Rows[0]["CutCancellationDate"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        if (DateTime.ParseExact(CanDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                                //fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CanAmtWithTax"]);
                                fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CancellationAmount"], System.Globalization.CultureInfo.InvariantCulture);
                        }
                        Iscancelable = 1;
                    }
                    else if (arrCanDate.Length == 2)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[0] != "" && DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }


                    else if (arrCanDate.Length == 3)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[0] != "" && (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now))
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }


                    else if (arrCanDate.Length == 4)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[3].Contains("-"))
                        {
                            arrCanDate[3] = arrCanDate[3].Replace("-", "/");
                        }


                        if (arrCanDate[0] != "" && DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[3] != "" && DateTime.ParseExact(arrCanDate[3], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[3], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }





                    }




                    else if (arrCanDate.Length == 5 || arrCanDate.Length == 6)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[3].Contains("-"))
                        {
                            arrCanDate[3] = arrCanDate[3].Replace("-", "/");
                        }

                        if (arrCanDate[4].Contains("-"))
                        {
                            arrCanDate[4] = arrCanDate[4].Replace("-", "/");
                        }

                        if (arrCanDate[0] != "" && (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) < DateTime.Now) && (DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now))
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        //else if (DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[3] != "" && DateTime.ParseExact(arrCanDate[3], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[3], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[4] != "" && DateTime.ParseExact(arrCanDate[4], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[4], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                    }
                }
                else if (Source == "Expedia")
                {
                    if (chkdt.Contains("-") == true)
                    {
                        chkdt = chkdt.Replace("-", "/");
                    }
                    if (chkdt.Contains("00:00") != true)
                    {
                        chkdt = chkdt + " 00:00";
                    }
                    string CanDate = arrCanDate[0];
                    if (CanDate.Contains("-") == true)
                    {
                        CanDate = CanDate.Replace("-", "/");
                    }
                    if (DateTime.Now > DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))
                    {
                        Iscancelable = 0;
                        IsConfirmable = 0;
                    }

                    else if (DateTime.ParseExact(CanDate, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture) > DateTime.Now)
                    {
                        Iscancelable = 1;
                        IsConfirmable = 1;
                    }
                    if (arrCanDate.Length == 1)
                    {
                        CanDate = dtBookedRooms.Rows[0]["CutCancellationDate"].ToString();
                        if (CanDate.Contains("-") == true)
                        {
                            CanDate = CanDate.Replace("-", "/");
                        }
                        //  if (DateTime.ParseExact(dtBookedRooms.Rows[0]["CutCancellationDate"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        if (DateTime.ParseExact(CanDate, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                                //fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CanAmtWithTax"]);
                                fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CancellationAmount"], System.Globalization.CultureInfo.InvariantCulture);
                        }
                        Iscancelable = 1;
                    }
                    else if (arrCanDate.Length == 2)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[0] != "" && DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }


                    else if (arrCanDate.Length == 3)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[0] != "" && (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now))
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }


                    else if (arrCanDate.Length == 4)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[3].Contains("-"))
                        {
                            arrCanDate[3] = arrCanDate[3].Replace("-", "/");
                        }


                        if (arrCanDate[0] != "" && DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[3] != "" && DateTime.ParseExact(arrCanDate[3], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[3], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }





                    }




                    else if (arrCanDate.Length == 5 || arrCanDate.Length == 6)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (arrCanDate[3].Contains("-"))
                        {
                            arrCanDate[3] = arrCanDate[3].Replace("-", "/");
                        }

                        if (arrCanDate[4].Contains("-"))
                        {
                            arrCanDate[4] = arrCanDate[4].Replace("-", "/");
                        }

                        if (arrCanDate[0] != "" && (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) < DateTime.Now) && (DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now))
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (arrCanDate[1] != "" && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        //else if (DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        else if (arrCanDate[2] != "" && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[3] != "" && DateTime.ParseExact(arrCanDate[3], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[3], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                        else if (arrCanDate[4] != "" && DateTime.ParseExact(arrCanDate[4], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) >= DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {

                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[4], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }


                    }
                }
                else
                {

                    if (chkdt.Contains("-") == true)
                    {
                        chkdt = chkdt.Replace("-", "/");
                    }
                    string CanDate = arrCanDate[0];
                    if (CanDate.Contains("-") == true)
                    {
                        CanDate = CanDate.Replace("-", "/");
                    }
                    if (DateTime.Now > DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))
                    {
                        Iscancelable = 0;
                        IsConfirmable = 0;
                    }

                    else if (DateTime.ParseExact(CanDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                    {
                        Iscancelable = 1;
                        IsConfirmable = 1;
                    }
                    if (arrCanDate.Length == 1)
                    {
                        CanDate = dtBookedRooms.Rows[0]["CutCancellationDate"].ToString();
                        if (CanDate.Contains("-") == true)
                        {
                            CanDate = CanDate.Replace("-", "/");
                        }
                        //  if (DateTime.ParseExact(dtBookedRooms.Rows[0]["CutCancellationDate"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        if (DateTime.ParseExact(CanDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(chkdt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        //if (DateTime.ParseExact(CanDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.ParseExact(DateTime.Now.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.ParseExact(DateTime.Now.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                                //fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CanAmtWithTax"]);
                                fCancellationAmount += Convert.ToSingle(dtBookedRooms.Rows[i]["CancellationAmount"], System.Globalization.CultureInfo.InvariantCulture);
                            Iscancelable = 1;
                        }

                    }
                    else if (arrCanDate.Length == 2)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }
                    else if (arrCanDate.Length == 3)
                    {
                        if (arrCanDate[0].Contains("-"))
                        {
                            arrCanDate[0] = arrCanDate[0].Replace("-", "/");
                        }
                        if (arrCanDate[1].Contains("-"))
                        {
                            arrCanDate[1] = arrCanDate[1].Replace("-", "/");
                        }
                        if (arrCanDate[2].Contains("-"))
                        {
                            arrCanDate[2] = arrCanDate[2].Replace("-", "/");
                        }
                        if (DateTime.ParseExact(arrCanDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[0], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (DateTime.ParseExact(arrCanDate[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[1], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                        else if (DateTime.ParseExact(arrCanDate[2], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) <= DateTime.Now && DateTime.ParseExact(dtHotelReservation.Rows[0]["CheckIn"].ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) > DateTime.Now)
                        {
                            for (int i = 0; i < dtBookedRooms.Rows.Count; i++)
                            {
                                //arrAmount = dtBookedRooms.Rows[i]["CanAmtWithTax"].ToString().Split('|');
                                arrAmount = dtBookedRooms.Rows[i]["CancellationAmount"].ToString().Split('|');
                                fCancellationAmount += Convert.ToSingle(arrAmount[2], System.Globalization.CultureInfo.InvariantCulture);
                            }
                            Iscancelable = 1;
                        }
                    }
                    Session["Iscancelable"] = Iscancelable;
                }


                sHotelReservation = "";
                foreach (DataRow dr in dtHotelReservation.Rows)
                {
                    sHotelReservation += "{";
                    foreach (DataColumn dc in dtHotelReservation.Columns)
                    {
                        sHotelReservation += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sHotelReservation = sHotelReservation.Trim(',') + "},";
                }

                sBookedRooms = "";
                foreach (DataRow dr in dtBookedRooms.Rows)
                {
                    sBookedRooms += "{";
                    foreach (DataColumn dc in dtBookedRooms.Columns)
                    {
                        sBookedRooms += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sBookedRooms = sBookedRooms.Trim(',') + "},";
                }
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"IsConfirmable\":\"" + IsConfirmable + "\",\"IsCancelable\":\"" + Iscancelable + "\",\"CancellationAmount\":\"" + fCancellationAmount + "\",\"HotelReservation\":[" + sHotelReservation.Trim(',') + "],\"BookedRooms\":[" + sBookedRooms.Trim(',') + "],\"TotalFare\":\"" + TotalFare + "\",\"ServiceCharge\":\"" + ServiceCharge + "\",\"Total\":\"" + Total + "\",\"TimeGap\":\"" + objMarkupsAndTaxes.TimeGap + "\"}";
            }
        }


        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

        [WebMethod(EnableSession = true)]
        //public string HotelConfirmSession(string HotelCode, string RoomID, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<Guest> List_Guest, List<MGHLib.Request.Guest> Guest_listMGH, List<DOTWLib.Request.Guest> Guest_listDoTW, string ServiceTax)
        public string HotelConfirmSession(float Price, string HotelCode, string CutID, string Supplier, List<string> RoomID, List<string> RoomDescriptionId, string _Price, List<int> _noRooms, string AgencyRefernce, string MobileNo, string Remark, string BookingStatus, string Email, List<CommonLib.Request.Guest> List_Guest, string ServiceTax, string HoldTime)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            string BookingParameters = HotelCode + "$" + CutID + "$" + RoomID + "$" + RoomDescriptionId + "$" + _Price + "$" + AgencyRefernce + "$" + MobileNo + "$" + Remark + "$" + BookingStatus + "$" + Email + "$" + ServiceTax;
            Session["BookingParameters"] = BookingParameters;
            Session["Guest_listMGH"] = List_Guest;
            try
            {
                CUTUK.BL.dbHelperDataContext Db = new BL.dbHelperDataContext();
                bool LogedInUser = true;
                var Data = (from obj in Db.tbl_B2C_CustomerLogins where obj.Email == Email select obj).FirstOrDefault();
                if (Data == null)
                    LogedInUser = false;
                return objSerialize.Serialize(new
                {
                    Session = 1,
                    retCode = 1,
                    IsLogedIn = LogedInUser
                });

            }
            catch
            {
                return objSerialize.Serialize(new
                {
                    Session = 1,
                    retCode = 0,
                });
            }

        }

        [WebMethod(EnableSession = true)]
        public string OnlinePayment(string RoomDescriptionId, string RoomPrice)
        {
            string sessionArray = (string)Session["BookingParameters"];
            List<MGHLib.Request.Guest> Guest_listMGH = (List<MGHLib.Request.Guest>)Session["Guest_listMGH"];
            string APIType = "MGH";



            string[] Array = sessionArray.Split('$');
            string HotelCode = Array[0].ToString();
            string RoomID = Array[1].ToString();
            string AgencyRefernce = "";
            string MobileNov = "";
            string Remark = "";
            string BookingStatus = Array[5].ToString();
            string Email = "";
            string VoucherId = "";

            string ServiceTax = Array[7].ToString();
            string MobileNo = MobileNov;
            #region Variables & object
            //string APIType = "HOTELBEDS";
            bool IsValidRoomID = true;
            string ReservationId = "";
            float TotalAmount = 0.0F;
            float ServiceCharges = 0.0F;
            float TotalPayable = 0.0F;
            string AffilateCode;
            string affcode = "";
            string Lang = "";
            string UserName = "";
            string CompanyId = "";
            string Password = "";
            string CheckIn = "";
            string CheckOut = "";
            string sCheckIn = "";
            string sCheckOut = "";
            string DesitinationCode = "";
            string DesitionationType = "";
            string HotelName = "";
            Int64 HotelId = 0;
            string HotelType = "";
            string AvailToken = "";
            string SerViceType = "";
            int RoomCount = 0;
            int Adult = 0;
            int Child = 0;
            string OnRequest = "";
            string EchoToken = "";
            float rate = 0;
            string m_xml = "";
            string m_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            int Status = 0;
            string PurchaseToken = String.Empty;
            int HolderAge = 0;
            string HolderName = "";
            string HolderLastName = "";
            string Suppliername = "";
            string VatNumber = "";
            string Comments = "";
            string Purchasetoken = "";
            int Officecode = 0;
            float SupplierTotalAmount = 0;
            float CutComm = 0;
            float AgentComm = 0;
            string RoomCode = "";
            int RoomNumber = 0;
            string SUPCancellationDate = "";
            string CUTCancellationDate = "";
            string CancellationAmount = "";
            string CanServiceTax = "";
            string CanAmountWithServiceTax = "";
            float SerTax = 0;
            float CanAmtWithTax = 0;
            float RoomSerTax = 0;
            float RoomAmtWithTax = 0;
            string ChildAge = "";
            string Currency = "";
            string ComparedCurrency = "";
            float ComparedTotalAmount = 0;
            string LeadingGuestName = "";
            DateTime DeadLineDate;
            DateTime ChargeDate;
            string BookingDate;
            string DumReservation;
            string CurrentStatus = "Request";
            string session;
            Int64 selectedRateBasis = 0;
            string tariffName = "";
            string fullName = "";
            string phone = "";
            DBHelper.DBReturnCode RetCodeStatus;
            bool bValid = true;

            //Occupancy objOccupancy = new Occupancy();
            MGHLib.Request.Occupancy objOccupancy = new MGHLib.Request.Occupancy();
            // objects--common
            JavaScriptSerializer js = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();

            // objects--HOTELBEDS
            //Models.Hotel objHotel = new Models.Hotel();
            //HotelLib.Response.HotelDetail objHotelDetail = new HotelLib.Response.HotelDetail();
            //List<Customer> List_Customer = new List<Customer>();
            //List<HotelLib.Response.Contract> List_Contract = new List<HotelLib.Response.Contract>();
            //HotelLib.Response.RoomOccupancy objRoomOccupancy = new HotelLib.Response.RoomOccupancy();
            //List<HotelLib.Response.Room> Hotel_Occupancy = new List<HotelLib.Response.Room>();
            //HotelLib.Request.ServiceAddRequest objService = new HotelLib.Request.ServiceAddRequest();
            //HotelLib.Response.DateTimeFrom objDateTimeFrom = new HotelLib.Response.DateTimeFrom();
            //HotelLib.Response.CancellationPolicy objCancellationPolicy = new HotelLib.Response.CancellationPolicy();
            //HotelLib.Response.RoomType objRoomType = new HotelLib.Response.RoomType();
            //HotelLib.Response.Price objPrice = new HotelLib.Response.Price();
            //HotelLib.Response.Location objLocation = new HotelLib.Response.Location();


            CUT.Models.MGHHotel objMGHHotel = new CUT.Models.MGHHotel();
            CUT.Models.MGHStatusCode objMGHStatusCode = new CUT.Models.MGHStatusCode();
            MGHLib.Response.MGHHotelDetails objMGHHotelDetail = new MGHLib.Response.MGHHotelDetails();

            #endregion


            #region MGH
            if (APIType == "MGH")
            {
                string CD = "";
                string DD = "";

                js = new JavaScriptSerializer();
                #region Checking Space
                //IsValidRoomID = true;
                //if (Session["HotelRoomIDs"] != null)
                //{
                //    string[] arrHotelRoomIDs = Session["HotelRoomIDs"].ToString().Split('|');
                //    foreach (var objGuest in List_Guest)
                //    {
                //        if (objGuest.ID.Contains(' '))
                //        {
                //            IsValidRoomID = false;
                //            objGuest.ID = objGuest.ID.Replace(' ', '+');
                //            for (int i = 0; i < arrHotelRoomIDs.Length; i++)
                //            {
                //                if (objGuest.ID == arrHotelRoomIDs[i])
                //                    IsValidRoomID = true;
                //            }
                //        }
                //    }
                //}
                //if (!IsValidRoomID)
                //    return js.Serialize(new { session = 1, retCode = 0, Message = "Something went wrong! We recommend you to select other room options" });
                #endregion Checking Space

                if (HttpContext.Current.Session["markups"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
                else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];
                ComparedCurrency = (Session["OtherCurrency"] != null) ? Session["OtherCurrency"].ToString() : "";
                ComparedTotalAmount = 0;
                if (AgencyRefernce == "")
                    AgencyRefernce = "Test AgencyRef";
                BookingDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);


                if (com.Common.Session(out objGlobalDefault))
                {
                    objMGHHotel = (CUT.Models.MGHHotel)Session["MGHAvail"];
                    objMGHHotelDetail = objMGHHotel.HotelDetail.Where(data => data.HotelId == Convert.ToInt32(HotelCode)).FirstOrDefault();
                    objMGHStatusCode = (CUT.Models.MGHStatusCode)Session["MGHStatus"];
                    ReservationId = "Dummy-" + GenerateRandomString(4);
                    DumReservation = ReservationId;
                    float SupplierAmount = 0.0F;
                    float CutRoomAmount = 0.0F;
                    float AgentRoomMarkup = 0.0F;
                    string BlockID = "";
                    bool bResponseBooking = false;
                    DBHelper.DBReturnCode retCode;
                    DBHelper.DBReturnCode retCodeUpdateTXN;
                    List<string> lstLeadingGuests = new List<string>();
                    List<string> lstChildAge = new List<string>();
                    DBHelper.DBReturnCode retcode;
                    string leadingGuest = "";

                    if (BookingStatus == "Vouchered")
                    {
                        for (int k = 0; k < objMGHHotelDetail.rate.Count; k++)
                        {
                            if (objMGHHotelDetail.RoomCategory[k].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                            {

                                for (int j = 0; j < objMGHHotelDetail.RoomCategory.Count; j++)
                                {


                                    SupplierAmount = objMGHHotelDetail.rate[k].roomInfo[j].roomRate[0].amount;
                                    CutRoomAmount = objMGHHotelDetail.rate[k].refundPolicy[j].CutRoomAmount;
                                    AgentRoomMarkup = objMGHHotelDetail.rate[k].refundPolicy[j].AgentRoomMarkup;
                                    break;
                                }
                            }
                        }

                        TotalAmount = CutRoomAmount + AgentRoomMarkup;
                        SupplierTotalAmount = CutRoomAmount;
                        // SupplierTotalAmount = SupplierAmount;
                        CutComm = CutRoomAmount - SupplierTotalAmount;
                        AgentComm = AgentRoomMarkup;
                        ServiceCharges = (float)(Convert.ToSingle(ServiceTax, System.Globalization.CultureInfo.InvariantCulture));
                        TotalPayable = TotalAmount - (AgentComm) + ServiceCharges;


                        //retCode = HotelBookingTxnManager.HotelBookingTxnAdd(ReservationId, BookingDate, TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "MGH", SupplierTotalAmount, AgentComm, CutComm, "", BookingStatus);
                        retCode = HotelBookingTxnManager.HotelBookingTxnAddOnline(ReservationId, BookingDate, TotalAmount, ServiceCharges, TotalPayable, 0, 0, 0, 0, 0, 0, 0, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), Remark, 0, "MGH", SupplierTotalAmount, AgentComm, CutComm, "");
                        if (retCode != DBHelper.DBReturnCode.SUCCESS)
                        {

                            return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding transaction details" });
                        }
                    }

                    #region TransactionScope

                    #region URL For Block Room
                    UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
                    Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];
                    AffilateCode = "";
                    affcode = "";

                    DesitinationCode = (objMGHHotelDetail.Location[0].LocationId).ToString();
                    string CheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
                    string sCheckInMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                    string CheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyyMMdd");
                    string sCheckOutMGH = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckOut, new System.Globalization.CultureInfo("en-GB")).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                    HotelName = objMGHHotelDetail.HotelName;
                    HotelType = objMGHHotelDetail.Category[0].CategoryName;



                    var List_MGHCustomer = Guest_listMGH.Select(data => data.sCustomer).FirstOrDefault();

                    //List_MGHCustomer =(HotelLib.Request.Customer)  List_MGHCustomer;
                    //List_MGHCustomer = List_Guest.Select(data => data.sCustomer).FirstOrDefault();
                    Lang = "ENG";
                    string latitude = (objMGHHotelDetail.Latitude).ToString();
                    string Longitude = (objMGHHotelDetail.Longitude).ToString();


                    RoomCount = objMGHHotel.DisplayRequest.Room;
                    Adult = objMGHHotel.DisplayRequest.Adult;
                    Child = objMGHHotel.DisplayRequest.Child;
                    int Adult_Room1 = 0;
                    int Adult_Room2 = 0;
                    int Adult_Room3 = 0;
                    int Adult_Room4 = 0;
                    int Child_Room1 = 0;
                    int Child_Room2 = 0;
                    int Child_Room3 = 0;
                    int Child_Room4 = 0;
                    OnRequest = "N";
                    MGHLib.Response.Rate objRate = new MGHLib.Response.Rate();

                    string Room1_ChidAge = "";
                    string Room2_ChidAge = "";
                    string Room3_ChidAge = "";
                    string Room4_ChidAge = "";
                    if (RoomCount == 1)
                    {
                        if (Child == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (Child == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult + "&occupant_child=" + Child + Room1_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "&rooms=" + RoomCount;
                    }

                    if (RoomCount == 2)
                    {
                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID;

                    }

                    if (RoomCount == 3)
                    {

                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }
                        // for romm 3 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 1)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 2)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Adult_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        Child_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "," + Adult_Room3 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "," + Child_Room3 + Room3_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID + "," + RoomID;

                    }


                    if (RoomCount == 4)
                    {

                        // for Room 1 child ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 1)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount == 2)
                        {
                            Room1_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[0].GuestList[1].Age).ToString();
                        }
                        // for romm 2 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 1)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount == 2)
                        {
                            Room2_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[1].GuestList[1].Age).ToString();
                        }
                        // for romm 3 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 1)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount == 2)
                        {
                            Room3_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[2].GuestList[1].Age).ToString();
                        }

                        // for romm 4 chid ages
                        if (objMGHHotel.DisplayRequest.List_Occupancy[3].ChildCount == 1)
                        {
                            Room4_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[0].Age).ToString();
                        }
                        if (objMGHHotel.DisplayRequest.List_Occupancy[3].ChildCount == 2)
                        {
                            Room4_ChidAge = "-" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[0].Age).ToString() + ";" + (objMGHHotel.DisplayRequest.List_Occupancy[3].GuestList[1].Age).ToString();
                        }

                        Adult_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].AdultCount;
                        Adult_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].AdultCount;
                        Adult_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Adult_Room4 = objMGHHotel.DisplayRequest.List_Occupancy[2].AdultCount;
                        Child_Room1 = objMGHHotel.DisplayRequest.List_Occupancy[0].ChildCount;
                        Child_Room2 = objMGHHotel.DisplayRequest.List_Occupancy[1].ChildCount;
                        Child_Room3 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        Child_Room4 = objMGHHotel.DisplayRequest.List_Occupancy[2].ChildCount;
                        string RoomTypeName = "";
                        string RoomTypeID = "";
                        for (int i = 0; i < objMGHHotelDetail.rate.Count; i++)
                        {
                            if (objMGHHotelDetail.rate[i].roomCategory[0].RoomCategoryId == Convert.ToInt32(RoomID))
                            {

                                RoomTypeName = objMGHHotelDetail.rate[i].roomType[0].RoomTypeName;
                                RoomTypeID = (objMGHHotelDetail.rate[i].roomType[0].RoomTypeId).ToString();
                            }
                        }

                        m_xml = "blockRoom?token=" + Password + "&rooms=" + RoomCount + "&checkin_date=" + sCheckInMGH + "&checkout_date=" + sCheckOutMGH + "&class_type=" + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "," + RoomTypeID + "|" + RoomTypeName + "&clientBookingID=&hotelid=" + HotelCode + "&occupant_adult=" + Adult_Room1 + "," + Adult_Room2 + "," + Adult_Room3 + "," + Adult_Room4 + "&occupant_child=" + Child_Room1 + Room1_ChidAge + "," + Child_Room2 + Room2_ChidAge + "," + Child_Room3 + Room3_ChidAge + "," + Child_Room4 + Room4_ChidAge + "&payment_model=BTC&room_category_id=" + RoomID + "," + RoomID + "," + RoomID + "," + RoomID;

                    }



                    #endregion URL For Block Room

                    if (m_xml != null)
                    {
                        Int64 Sid = 0;
                        if (ComparedCurrency != "" && ComparedCurrency != "INR")
                        {
                            //net.webservicex.www.Currency cur = CUT.Common.Common.GetCurrency(ComparedCurrency);
                            //net.webservicex.www.CurrencyConvertor objCurrency = new net.webservicex.www.CurrencyConvertor();
                            //rate = Convert.ToInt32(objCurrency.ConversionRate(cur, net.webservicex.www.Currency.INR));
                            rate = objGlobalDefault.ExchangeRate.Single(data => data.Currency == ComparedCurrency).Rate;
                            ComparedTotalAmount = Convert.ToSingle(Math.Round((Decimal)(TotalAmount / rate), 2, MidpointRounding.AwayFromZero), System.Globalization.CultureInfo.InvariantCulture);
                        }
                        LeadingGuestName = List_MGHCustomer[0].Name + " " + List_MGHCustomer[0].LastName;
                        DateTime ChkInDeadLine = Convert.ToDateTime(objMGHHotel.DisplayRequest.CheckIn, new System.Globalization.CultureInfo("en-GB"));
                        for (int j = 0; j < objMGHHotelDetail.RoomCategory.Count; j++)
                        {

                            for (int k = 0; k < objMGHHotelDetail.rate.Count; k++)
                            {
                                //  for (int j = 0; j < objMGHHotelDetail.rate[k].roomInfo.Count; j++)


                                if (objMGHHotelDetail.RoomCategory[j].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                {
                                    int q = (-(24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[0].dayMax, System.Globalization.CultureInfo.InvariantCulture)));
                                    int r = (-(24 + 24 * Convert.ToInt32(objMGHHotelDetail.rate[k].refundPolicy[0].dayMax, System.Globalization.CultureInfo.InvariantCulture)));
                                    DateTime TimeChargeDate = ChkInDeadLine.AddHours(q);
                                    DateTime TimeDeadLineDate = ChkInDeadLine.AddHours(r);
                                    string s1 = TimeChargeDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                                    string s2 = TimeDeadLineDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                                    CD = s1.Replace(" 00:00", "");
                                    DD = s2.Replace(" 00:00", "");
                                }
                            }
                        }

                        using (TransactionScope objTransactionScope1 = new TransactionScope())
                        {
                            // DBHelper.DBReturnCode retcodeMGH = HotelReservationManager.HotelReservationAddUpdateMGH(Sid, objMGHHotelDetail.HotelId.ToString(), objMGHHotelDetail.HotelName, objMGHHotel.DisplayRequest.Location, objMGHHotel.DisplayRequest.CheckIn, objMGHHotel.DisplayRequest.CheckOut, objMGHHotel.DisplayRequest.Night, objMGHHotel.DisplayRequest.Room, TotalAmount, objMGHHotelDetail.Currency, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, CD, objMGHHotelDetail.Description, DD, out AffilateCode, BookingStatus, AgencyRefernce, LeadingGuestName, ComparedCurrency, ComparedTotalAmount, MobileNo, Email, Remark, latitude, Longitude, out VoucherId);
                            DBHelper.DBReturnCode retcodeMGH = DBHelper.DBReturnCode.SUCCESS;
                            affcode = AffilateCode;
                            if (retcodeMGH == DBHelper.DBReturnCode.SUCCESS)
                            {

                                CurrentStatus = "Reservation Details Added";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope1.Complete();
                            }
                            else if (retcodeMGH != DBHelper.DBReturnCode.SUCCESS)
                            {
                                CurrentStatus = "Error while Inserting Reservation Details";
                                bValid = false;
                                objTransactionScope1.Dispose();

                            }
                        }

                    }
                    if (bValid)
                    {
                        m_response = "";
                        RequestHeader = "";
                        ResponseHeader = "";
                        Status = 0;
                        PurchaseToken = String.Empty;

                        MGHLib.Common.ServiceAddRequestMGH webclient = new MGHLib.Common.ServiceAddRequestMGH();

                        bool bResponse = webclient.Post(m_xml, out ResponseHeader, out Status);
                        using (TransactionScope objTransactionScope2 = new TransactionScope(TransactionScopeOption.Suppress))
                        {
                            LogManager.Add(1, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
                            if (bResponse)
                            {
                                CurrentStatus = "Service Add Request Success";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope2.Complete();
                            }
                            else
                            {
                                CurrentStatus = "Service Add Request Fail";
                                bValid = false;
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                objTransactionScope2.Dispose();
                            }

                        }


                        if (bValid)
                        {
                            if (bResponse)
                            {

                                string leadingGeust = Guest_listMGH[0].sCustomer[0].Name + " " + Guest_listMGH[0].sCustomer[0].LastName;
                                ParseMGHResponse objBlockID = new ParseMGHResponse();
                                bResponseBooking = objBlockID.GetBlockID(ResponseHeader, out BlockID);
                                m_xml = "bookRoom&token=" + Password + "&hotelid=" + HotelCode + "&room_category_id =" + RoomID + "&block_ref_no=" + BlockID + "&guest_name=" + leadingGeust + "&guest_email=dubai%40clickurtrip.com&guest_phone=+971-4-2977792&guest_ident_type=&guest_ident=&guest_ident=";
                                bResponseBooking = webclient.Post(m_xml, out ResponseHeader, out Status);
                                try
                                {
                                    ReservationId = objBlockID.GetBookingID(ResponseHeader);
                                }
                                catch
                                {
                                    return js.Serialize(new { session = 1, retCode = 0, Message = "Set of Rooms are not Available." });
                                }

                                using (TransactionScope objTransactionScope3 = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    LogManager.Add(2, RequestHeader, m_xml, ResponseHeader, m_response, objGlobalDefault.sid, Status);
                                    if (bResponseBooking)
                                    {
                                        CurrentStatus = "Purchase Request Success";
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        objTransactionScope3.Complete();
                                    }
                                    else
                                    {
                                        CurrentStatus = "Purchase Request Fail";
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        bValid = false;
                                        objTransactionScope3.Dispose();
                                    }
                                }
                                js.MaxJsonLength = Int32.MaxValue;

                            }
                            if (bValid)
                            {
                                if (bResponseBooking)
                                {
                                    // code for database insertion here 
                                    using (TransactionScope objTransactionScope4 = new TransactionScope())
                                    {
                                        DBHelper.DBReturnCode retcode1;
                                        retcode1 = HotelReservationManager.HotelReservationUpdate(affcode, BookingStatus, Suppliername = "MGH", VatNumber, ReservationId, Purchasetoken, Officecode, Comments);

                                        // retcode = HotelReservationManager.HotelReservationUpdate("123", "Vouchered", "HotelBeds Spain S.L.U", "456", "789", "101112", 121212, "Test 12 Jan 2016");
                                        if (retcode1 == DBHelper.DBReturnCode.SUCCESS)
                                        {
                                            CurrentStatus = "Reservation Details Updated";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            latitude = (objMGHHotelDetail.Latitude).ToString();
                                            Longitude = (objMGHHotelDetail.Longitude).ToString();
                                            string HotelCodeMGH = (objMGHHotelDetail.HotelId).ToString();
                                            string HotelNameMGH = (objMGHHotelDetail.HotelName);
                                            string HotelAdd = (objMGHHotelDetail.Address);
                                            string Zip = (objMGHHotelDetail.Zip);
                                            string Description = (objMGHHotelDetail.Description);
                                            string ContactPerson = (objMGHHotelDetail.ContactDetails.ContactName);
                                            string ContactEmail = (objMGHHotelDetail.ContactDetails.ContactEmail);
                                            string ContactMobile = (objMGHHotelDetail.ContactDetails.ContactMobile);
                                            string ContactPhone = (objMGHHotelDetail.ContactDetails.ContactPhone);
                                            string Country = objMGHHotelDetail.Location[0].Country;
                                            string City = objMGHHotelDetail.Location[0].LocationName;

                                            DBHelper.DBReturnCode retcode2;
                                            retcode2 = HotelReservationManager.HotelMGHAdd(ReservationId, HotelCode, HotelName, HotelAdd, Zip, Description, ContactPerson, ContactEmail, ContactMobile, ContactPhone, latitude, Longitude, Country, City);
                                            //if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
                                            //{

                                            //    //return js.Serialize(new { session = 1, retCode = 0, Message = "Error while Adding Hotel  details" });
                                            //}

                                            objTransactionScope4.Complete();

                                        }
                                        else if (retcode1 != DBHelper.DBReturnCode.SUCCESS)
                                        {
                                            CurrentStatus = "Error while updating reservation details";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            bValid = false;
                                            objTransactionScope4.Dispose();
                                        }
                                    }
                                    if (bValid)
                                    {
                                        if (BookingStatus == "Vouchered")
                                        {
                                            using (TransactionScope objTransactionScope5 = new TransactionScope())
                                            {

                                                retCodeUpdateTXN = HotelBookingTxnManager.HotelBookingTxnUpdate(ReservationId, DumReservation);
                                                if (retCodeUpdateTXN == DBHelper.DBReturnCode.SUCCESS)
                                                {
                                                    CurrentStatus = "Reservation-Id Added Success";
                                                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                    objTransactionScope5.Complete();
                                                }
                                                else if (retCodeUpdateTXN != DBHelper.DBReturnCode.SUCCESS)
                                                {
                                                    CurrentStatus = "Error while Updating transaction details";
                                                    bValid = false;
                                                    objTransactionScope5.Dispose();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                    if (bValid)
                                    {
                                        RoomCode = RoomID;
                                        RoomNumber = 1;
                                        int r = 0;
                                        bool retbit = true;
                                        foreach (MGHLib.Request.Guest guestroom in Guest_listMGH)
                                        {
                                            if (retbit == false)
                                            {
                                                break;
                                            }

                                            foreach (MGHLib.Request.Customer customer in guestroom.sCustomer)
                                            {
                                                if (r == 0)
                                                {
                                                    lstLeadingGuests.Add(customer.Name + " " + customer.LastName);
                                                    leadingGuest = customer.Name + " " + customer.LastName;
                                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 1, ReservationId);
                                                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                                    {
                                                        retbit = false;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    retcode = HotelReservationManager.BookedPaseengerAdd(RoomCode, "Room " + RoomNumber, customer.Name, customer.LastName, customer.Age, customer.type, 0, ReservationId);
                                                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                                    {
                                                        retbit = false;
                                                        break;
                                                    }
                                                }
                                                r++;
                                            }
                                            RoomNumber++;
                                        }

                                        using (TransactionScope objTransactionScope6 = new TransactionScope())
                                        {
                                            if (retbit == false)
                                            {
                                                CurrentStatus = "Error while adding PASSENGER";
                                                bValid = false;
                                                objTransactionScope6.Dispose();
                                            }
                                            else
                                            {
                                                CurrentStatus = "PASSENGER Added Success";
                                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                bValid = true;
                                                objTransactionScope6.Complete();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                    if (bValid)
                                    {
                                        int l = 0;
                                        int n = 0;
                                        bool RoomBit = false;

                                        int Nights = objMGHHotel.DisplayRequest.Night;
                                        int Rooms = objMGHHotel.DisplayRequest.Room;
                                        for (int rc = 0; rc < RoomCount; rc++)
                                        {


                                            SUPCancellationDate = "";
                                            CUTCancellationDate = "";
                                            CancellationAmount = "";
                                            CanServiceTax = "";
                                            int TotalRoom = objMGHHotelDetail.rate[0].roomInfo.Count;
                                            string SrTax = "";
                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {

                                                        CUTCancellationDate += (Convert.ToDateTime(objMGHHotelDetail.DateFrom)).AddHours(-(objMarkupsAndTaxes.TimeGap + (Convert.ToInt32(objMGHHotelDetail.rate[r].refundPolicy[j].dayMax) * 24))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {

                                                        SUPCancellationDate += (Convert.ToDateTime(objMGHHotelDetail.DateFrom)).AddHours(-((Convert.ToInt32(objMGHHotelDetail.rate[r].refundPolicy[j].dayMax, System.Globalization.CultureInfo.InvariantCulture) * 24))).ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture) + "|";
                                                    }
                                                }

                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        CancellationAmount += String.Format("{0:#,##0}", Math.Round((Decimal)((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount + objMGHHotelDetail.rate[r].refundPolicy[j].AgentCancellationMarkup) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero).ToString()) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        SrTax += ((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount * objMarkupsAndTaxes.PerServiceTax) / (Nights * Rooms)) / 100 + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    for (int j = 0; j < objMGHHotelDetail.rate[r].refundPolicy.Count; j++)
                                                    {
                                                        CanAmountWithServiceTax += String.Format("{0:#,##0}", Math.Round((Decimal)(((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount + objMGHHotelDetail.rate[r].refundPolicy[j].AgentCancellationMarkup) + ((objMGHHotelDetail.rate[r].refundPolicy[j].CUTCancellationAmount * objMarkupsAndTaxes.PerServiceTax) / 100)) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero).ToString()) + "|";
                                                    }
                                                }
                                            }

                                            for (int r = 0; r < objMGHHotelDetail.rate.Count; r++)
                                            {
                                                if (objMGHHotelDetail.rate[r].roomCategory[0].RoomCategoryId == Convert.ToInt64(RoomID, System.Globalization.CultureInfo.InvariantCulture))
                                                {
                                                    CutRoomAmount = (float)(Math.Round((Decimal)((objMGHHotelDetail.rate[r].refundPolicy[0].CutRoomAmount) / (Nights * Rooms)), 2, MidpointRounding.AwayFromZero));
                                                }
                                            }


                                            ChildAge = "";




                                            // for Room 1 child ages
                                            if (objMGHHotel.DisplayRequest.List_Occupancy[l].ChildCount == 1)
                                            {
                                                string age = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[l].Age).ToString();
                                                ChildAge = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[l].Age).ToString();
                                            }
                                            if (objMGHHotel.DisplayRequest.List_Occupancy[l].ChildCount == 2)
                                            {
                                                ChildAge = (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[l].Age).ToString() + "," + (objMGHHotel.DisplayRequest.List_Occupancy[l].GuestList[(l + 1)].Age).ToString();
                                            }
                                            l++;




                                            //for (int z = 0; z < objMGHHotel.DisplayRequest.Child; z++)
                                            //{
                                            //    ChildAge = ChildAge + "|" + lstChildAge[l];
                                            //    l++;
                                            //}
                                            //ChildAge = ChildAge.TrimStart('|');
                                            string MEALPLAN = objMGHHotelDetail.rate[0].mealPlan[0].text;
                                            string RoomType = objMGHHotelDetail.rate[0].roomType[0].RoomTypeName;
                                            retcode = HotelReservationManager.BookedRoomAdd(RoomCode, RoomType, "Room " + (n + 1), MEALPLAN, objMGHHotel.DisplayRequest.Room, leadingGuest, objMGHHotel.DisplayRequest.Adult, objMGHHotel.DisplayRequest.Child, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, SrTax, CanAmountWithServiceTax, CutRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
                                            if (retcode == DBHelper.DBReturnCode.SUCCESS)
                                            {
                                                RoomBit = true;
                                            }
                                            else
                                            {
                                                RoomBit = false;
                                            }
                                            n++;
                                        }
                                        using (TransactionScope objTransactionScope7 = new TransactionScope())
                                        {
                                            //retcode = HotelReservationManager.BookedRoomAdd(objRoom.SHRUI, objRoom.Roomtext, "Room " + (n + 1), objRoom.Boardtext, objRoom.RoomCount, lstLeadingGuests[n], objRoom.AdultCount, objRoom.ChildCount, ChildAge, Remark, CUTCancellationDate, SUPCancellationDate, CancellationAmount, CanServiceTax, CanAmountWithServiceTax, objRoom.CUTRoomAmount, RoomSerTax, RoomAmtWithTax, ReservationId);
                                            if (RoomBit == true)
                                            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                                            {
                                                CurrentStatus = "Room Insert Success";
                                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                //if (n == (RoomCount - 1))
                                                objTransactionScope7.Complete();
                                            }
                                            else if (RoomBit == false)
                                            //else if (retcode != DBHelper.DBReturnCode.SUCCESS)
                                            {

                                                CurrentStatus = "Error while adding ROOM";
                                                //RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                                bValid = false;
                                                objTransactionScope7.Dispose();
                                                //return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                            }




                                        }
                                        if (bValid && (n == RoomCount))
                                        {
                                            //objTransactionScope.Complete();
                                            CurrentStatus = "Booking Success";
                                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                            retcode = HotelReservationManager.UpdateTransactionStatusOnline(DumReservation, ReservationId);
                                            DataTable dt = null;
                                            com.SMSWebClient objSMS = new SMSWebClient();
                                            DBHelper.DBReturnCode RetSMS = HotelReservationManager.BookingDetailForSMS(ReservationId, out dt);
                                            string Mobile = dt.Rows[0]["Mobile"].ToString();
                                            string InvoiceId = dt.Rows[0]["InvoiceID"].ToString();
                                            string CIn = dt.Rows[0]["CheckIn"].ToString();
                                            string COut = dt.Rows[0]["CheckOut"].ToString();
                                            string SMS = "Your Booking No." + InvoiceId + "is confirmed.Your Check-In:" + CIn + " and Check-Out;" + COut + ". Thank You.";
                                            string Sms_Request = "http://www.smsjust.com/blank/sms/user/urlsms.php?username=ClickurtripNew&pass=88888&senderid=257147&dest_mobileno=917666249241&message=" + SMS + "&response=Y";
                                            string Sms_Response = "";
                                            int status = 0;
                                            bool SmsResponse = objSMS.Post(Sms_Request, out Sms_Response, out status);
                                            return js.Serialize(new { session = 1, retCode = 1, ReservationID = ReservationId, Status = BookingStatus });
                                        }

                                    }
                                    else
                                    {
                                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                                    }

                                }
                            }
                            else
                            {
                                CurrentStatus = "Error during Purchase Confirm";
                                RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, ReservationId, DumReservation);
                                return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                            }

                        }
                        else
                        {
                            RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                            return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                        }
                    }
                    else
                    {
                        RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                        return js.Serialize(new { session = 1, retCode = 0, Message = CurrentStatus });
                    }
                    #endregion TransactionScope
                    RetCodeStatus = HotelReservationManager.UpdateBookingStatus(AffilateCode, CurrentStatus, DumReservation);
                    return js.Serialize(new { session = 1, retCode = 1, Message = CurrentStatus, ReservationID = ReservationId, Status = Status });


                }
                else
                {
                    js.MaxJsonLength = Int32.MaxValue;
                    return js.Serialize(new { session = 0, retCode = 0, Message = "Session Expired!" });
                }


            }
            #endregion MGH



            else
            {
                return js.Serialize(new { Message = "No API!" });
            }
        }

        [WebMethod(EnableSession = true)]
        public string Exit()
        {
            Environment.Exit(Environment.ExitCode);
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }

        //[WebMethod(EnableSession = true)]
        //public string AddB2C_CustomerBooking(string HotelName, string Passenger, string CheckIn, string CheckOut, string Status, string DeadLine, string TotalFare, string City)
        //{
        //    string json="";
        //    DBHelper.DBReturnCode retCode;
        //    retCode = HotelManager.AddB2C_CustomerBooking(HotelName, Passenger, CheckIn, CheckOut, Status, DeadLine, TotalFare, City);
        //    if(retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        json="{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    else
        //    {
        //        json="{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}
    }
}
