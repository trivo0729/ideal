﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="PackageDetails.aspx.cs" Inherits="CUTUK.PackageDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    <style type="text/css">
        @keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-moz-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-webkit-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        #pS6Jyk4-1569738072448 {
            outline: none !important;
            visibility: visible !important;
            resize: none !important;
            box-shadow: none !important;
            overflow: visible !important;
            background: none !important;
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity1) !important;
            -moz-opacity: 1 !important;
            -khtml-opacity: 1 !important;
            top: auto !important;
            right: 10px !important;
            bottom: 90px !important;
            left: auto !important;
            position: fixed !important;
            border: 0 !important;
            min-height: 0 !important;
            min-width: 0 !important;
            max-height: none !important;
            max-width: none !important;
            padding: 0 !important;
            margin: 0 !important;
            -moz-transition-property: none !important;
            -webkit-transition-property: none !important;
            -o-transition-property: none !important;
            transition-property: none !important;
            transform: none !important;
            -webkit-transform: none !important;
            -ms-transform: none !important;
            width: auto !important;
            height: auto !important;
            display: none !important;
            z-index: 2000000000 !important;
            background-color: transparent !important;
            cursor: auto !important;
            float: none !important;
            border-radius: unset !important;
            pointer-events: auto !important;
        }

        #Q4gVTA5-1569738072452.open {
            animation: tawkMaxOpen .25s ease !important;
        }
    </style>
    <style>
        .owl-stage-outer .item {
            height: 70vh;
        }

        section#content.con_mng {
            padding: 0px;
            margin: 0px;
        }

        .slide_cov {
            position: relative;
            height: 70vh;
        }

        .caption_slider {
            background: rgba(0, 0, 0, 0.6);
            color: #fff;
            padding: 25px;
        }

            .caption_slider form .form-group {
                margin: 15px 0px;
            }

                .caption_slider form .form-group .form-control::placeholder {
                    color: #fff;
                }

        .caption_slider1 {
            position: absolute;
            left: 0;
            z-index: 999;
            top: 42%;
            right: 0;
            text-align: center;
            /* bottom: 0; */
            line-height: 50%;
        }

        .cap_bu {
            display: inline-block;
            background: rgba(0, 0, 0, 0.8);
            padding: 19px 19px 19px;
            border-radius: 38px;
            color: #fff;
        }

        .checkbox .label {
            color: #000 !important;
            margin: 0px 0px 0px 15px;
        }

        .cap_bu a:hover span {
            background: red;
        }

        .per_detail {
            color: #fff;
            font-size: 16px;
            /* border-bottom: 1px solid #fff; */
            margin: 0px 0px 12px 0px;
            padding: 3px 0px 12px;
            line-height: 0px;
        }

        .cap_bu span {
            background: #ff9800;
            padding: 5px 15px;
            border-radius: 15px;
            width: 100%;
            /* display: inline; */
            color: #fff;
        }

        .offer {
            font-family: 'Montserrat', sans-serif;
            color: #000;
            font-size: 25px;
        }


        .btnContactSubmit {
            width: 50%;
            border-radius: 1rem;
            padding: 1.5%;
            color: #fff;
            background-color: red;
            border: none;
            cursor: pointer;
            margin-right: 6%;
            background-color: white;
            color: blue;
            margin-top: 4%;
        }

        .register .nav-tabs .nav-link:hover {
            border: none;
        }

        .text-align {
        }

        .form-new {
        }

        .register-heading {
            color: #000;
            text-transform: capitalize;
            font-size: 26px;
            font-style: italic;
            border-bottom: 1px solid #fff;
            padding-bottom: 7px;
            margin: 0px 0px 25px 0px;
            padding: 0px;
        }

            .register-heading h1 {
                color: #e9ecef;
            }

        .tab-content {
            clear: both;
        }

        .btnLoginSubmit {
            border: none;
            padding: 2%;
            width: 25%;
            cursor: pointer;
            background: #29abe2;
            color: #fff;
        }

        .btnForgetPwd {
            cursor: pointer;
            margin-right: 5%;
            color: #f8f9fa;
        }

        .register {
            background: -webkit-linear-gradient(left, #3931af, #00c6ff);
            margin-top: 3%;
            padding: 3%;
            border-radius: 2.5rem;
        }

        .nav-tabs .nav-link {
            border: 1px solid transparent;
            border-top-left-radius: .25rem;
            border-top-right-radius: .25rem;
            color: white;
        }

        .left_sec .nav-tabs > li > a {
            background: #ddd;
            color: #000;
        }

            .left_sec .nav-tabs > li.active > a, .left_sec .nav-tabs > li.active > a:focus, .left_sec .nav-tabs > li.active > a:hover, .left_sec .nav-tabs > li > a:hover {
                background: #333;
                color: #fff;
                border-color: transparent;
            }

        .section-padding {
            padding: 25px 0;
        }

        .section-dark {
            z-index: -2;
        }

        .modal-header.strip h1 {
            margin: 0px;
            color: #fff;
        }

        .modal-header.strip {
            background: #989595;
            border-radius: 5px 5px 0px 0px;
        }

        .mng_form .form-control,
        .mng_form .form-group .form-control, select.mng_select {
            border: 0px;
            -webkit-transition: background 0s ease-out;
            -o-transition: background 0s ease-out;
            transition: background 0s ease-out;
            float: none;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
            border-bottom: 1px solid #ccc;
            background: transparent;
            height: inherit;
            padding: inherit;
            width: 100%;
            color: #333;
            font-size: 16px;
        }

            .mng_form .form-control::-moz-placeholder,
            .mng_form .form-group .form-control::-moz-placeholder {
                color: #333;
                font-weight: 400;
            }

            .mng_form .form-control:-ms-input-placeholder,
            .mng_form .form-group .form-control:-ms-input-placeholder {
                color: #333;
                ;
                font-weight: 400;
            }

        .form-control::-webkit-input-placeholder,
        .form-group .form-control::-webkit-input-placeholder {
            color: #333;
            font-weight: 400;
        }

        .mng_form .form-control[disabled],
        .mng_form .form-control[readonly],
        .mng_form .form-group .form-control[disabled],
        .mng_form .form-group .form-control[readonly],
        fieldset[disabled] .form-control,
        fieldset[disabled] .form-group .form-control {
            background-color: rgba(0, 0, 0, 0);
        }

        .mng_form .form-control[disabled],
        .mng_form.form-group .form-control[disabled],
        fieldset[disabled] .form-control,
        fieldset[disabled] .form-group .form-control {
            background-image: none;
            border-bottom: 1px dotted #D2D2D2;
        }

        .mng_form .form-group {
            position: relative;
        }

            .mng_form .form-group.label-floating label.control-label,
            .mng_form .form-group.label-placeholder label.control-label,
            .mng_form .form-group.label-static label.control-label {
                position: absolute;
                pointer-events: none;
                -webkit-transition: .3s ease all;
                -o-transition: .3s ease all;
                transition: .3s ease all;
            }

            .mng_form .form-group.label-floating label.control-label {
                will-change: left, top, contents;
            }

            .mng_form .form-group.label-placeholder:not(.is-empty) label.control-label {
                display: none;
            }

            .mng_form .form-group .help-block {
                position: absolute;
                display: none;
            }

            .mng_form .form-group.is-focused .form-control {
                outline: 0;
                background-image: -webkit-gradient(linear, left top, left bottom, from(#009688), to(#009688)), -webkit-gradient(linear, left top, left bottom, from(#D2D2D2), to(#D2D2D2));
                background-image: -webkit-linear-gradient(#009688, #009688), -webkit-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: -o-linear-gradient(#009688, #009688), -o-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: linear-gradient(#009688, #009688), linear-gradient(#D2D2D2, #D2D2D2);
                -webkit-background-size: 100% 2px, 100% 1px;
                background-size: 100% 2px, 100% 1px;
                -webkit-box-shadow: none;
                box-shadow: none;
                -webkit-transition-duration: .3s;
                -o-transition-duration: .3s;
                transition-duration: .3s;
            }

                .mng_form .form-group.is-focused .form-control .material-input:after {
                    background-color: #009688;
                }

            .mng_form .form-group.is-focused label,
            .mng_form.form-group.is-focused label.control-label {
                color: #009688;
            }

            .mng_form .form-group.is-focused.label-placeholder label,
            .mng_form .form-group.is-focused.label-placeholder label.control-label {
                color: #333;
            }

            .mng_form .form-group.is-focused .help-block {
                display: block;
            }

            .mng_form .form-group.has-warning .form-control {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

            .mng_form .form-group.has-warning.is-focused .form-control {
                background-image: -webkit-gradient(linear, left top, left bottom, from(#ff5722), to(#ff5722)), -webkit-gradient(linear, left top, left bottom, from(#D2D2D2), to(#D2D2D2));
                background-image: -webkit-linear-gradient(#ff5722, #ff5722), -webkit-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: -o-linear-gradient(#ff5722, #ff5722), -o-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: linear-gradient(#ff5722, #ff5722), linear-gradient(#D2D2D2, #D2D2D2);
            }

            .mng_form .form-group.has-warning .help-block,
            .mng_form .form-group.has-warning label.control-label {
                color: #ff5722;
            }

            .mng_form .form-group.has-error .form-control {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

            .mng_form .form-group.has-error .help-block,
            .mng_form .form-group.has-error label.control-label {
                color: #f44336;
            }

            .mng_form .form-group.has-success .form-control {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

            .mng_form .form-group.has-success.is-focused .form-control {
                background-image: -webkit-gradient(linear, left top, left bottom, from(#4caf50), to(#4caf50)), -webkit-gradient(linear, left top, left bottom, from(#D2D2D2), to(#D2D2D2));
                background-image: -webkit-linear-gradient(#4caf50, #4caf50), -webkit-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: -o-linear-gradient(#4caf50, #4caf50), -o-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: linear-gradient(#4caf50, #4caf50), linear-gradient(#D2D2D2, #D2D2D2);
            }

            .mng_form .form-group.has-success .help-block,
            .mng_form .form-group.has-success label.control-label {
                color: #4caf50;
            }

            .mng_form .form-group.has-info .form-control {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

            .mng_form .form-group.has-info.is-focused .form-control {
                background-image: -webkit-gradient(linear, left top, left bottom, from(#03a9f4), to(#03a9f4)), -webkit-gradient(linear, left top, left bottom, from(#D2D2D2), to(#D2D2D2));
                background-image: -webkit-linear-gradient(#03a9f4, #03a9f4), -webkit-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: -o-linear-gradient(#03a9f4, #03a9f4), -o-linear-gradient(#D2D2D2, #D2D2D2);
                background-image: linear-gradient(#03a9f4, #03a9f4), linear-gradient(#D2D2D2, #D2D2D2);
            }

            .mng_form .form-group.has-info .help-block,
            .mng_form .form-group.has-info label.control-label {
                color: #03a9f4;
            }

            .mng_form .form-group textarea {
                resize: none;
            }

                .mng_form .form-group textarea ~ .form-control-highlight {
                    margin-top: -11px;
                }

            .mng_form .form-group select {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }

                .mng_form .form-group select ~ .material-input:after {
                    display: none;
                }

        .mng_form .form-control {
            margin-bottom: 7px;
        }

            .mng_form .form-control::-moz-placeholder {
                font-size: 16px;
                line-height: 1.42857143;
                color: #333;
                ;
                font-weight: 400;
            }

            .mng_form .form-control:-ms-input-placeholder {
                font-size: 16px;
                line-height: 1.42857143;
                color: #333;
                ;
                font-weight: 400;
            }

            .mng_form .form-control::-webkit-input-placeholder {
                font-size: 16px;
                line-height: 1.42857143;
                color: #333;
                ;
                font-weight: 400;
            }

        .checkbox label,
        .radio label,
        label {
            font-size: 16px;
            line-height: 1.42857143;
            color: #333;
            ;
            font-weight: 400;
        }

            label.control-label {
                font-size: 12px;
                line-height: 1.07142857;
                font-weight: 400;
                margin: 16px 0 0 0;
            }

        #stickThis {
        }

            #stickThis.stick {
                position: fixed;
                top: 55px;
                z-index: 9999;
            }

        .help-block {
            margin-top: 0;
            font-size: 12px;
        }

        .mng_form .form-group {
            padding-bottom: 7px;
            margin: 0px;
        }

            .mng_form .form-group .form-control {
                margin-bottom: 7px;
            }

                .mng_form .form-group .form-control::-moz-placeholder {
                    font-size: 16px;
                    line-height: 1.42857143;
                    color: #333;
                    ;
                    font-weight: 400;
                }

                .mng_form .form-group .form-control:-ms-input-placeholder {
                    font-size: 16px;
                    line-height: 1.42857143;
                    color: #333;
                    ;
                    font-weight: 400;
                }

                .mng_form .form-group .form-control::-webkit-input-placeholder {
                    font-size: 16px;
                    line-height: 1.42857143;
                    color: #333;
                    ;
                    font-weight: 400;
                }

            .mng_form .form-group .checkbox label,
            .mng_form .form-group .radio label,
            .mng_form .form-group label {
                font-size: 16px;
                line-height: 1.42857143;
                color: #333;
                ;
                font-weight: 400;
            }

                .mng_form .form-group label.control-label {
                    font-size: 12px;
                    line-height: 1.07142857;
                    font-weight: 400;
                    margin: 16px 0 0 0;
                }

            .mng_form .form-group .help-block {
                margin-top: 0;
                font-size: 12px;
            }

            .mng_form .form-group.label-floating label.control-label,
            .mng_form .form-group.label-placeholder label.control-label {
                top: -7px;
                font-size: 16px;
                line-height: 1.42857143;
            }

            .mng_form .form-group.label-floating.is-focused label.control-label,
            .mng_form .form-group.label-floating:not(.is-empty) label.control-label,
            .mng_form .form-group.label-static label.control-label {
                top: -30px;
                left: 0;
                font-size: 12px;
                line-height: 1.07142857;
            }

            .mng_form .form-group.label-floating input.form-control:-webkit-autofill ~ label.control-label label.control-label {
                top: -30px;
                left: 0;
                font-size: 12px;
                line-height: 1.07142857;
            }

            .mng_form .form-group.form-group-sm {
                padding-bottom: 3px;
                margin: 21px 0 0 0;
            }

                .mng_form .form-group.form-group-sm .form-control {
                    margin-bottom: 3px;
                }

                    .mng_form .form-group.form-group-sm .form-control::-moz-placeholder {
                        font-size: 11px;
                        line-height: 1.5;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                    .mng_form .form-group.form-group-sm .form-control:-ms-input-placeholder {
                        font-size: 11px;
                        line-height: 1.5;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                    .mng_form .form-group.form-group-sm .form-control::-webkit-input-placeholder {
                        font-size: 11px;
                        line-height: 1.5;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                .mng_form .form-group.form-group-sm .checkbox label,
                .mng_form .form-group.form-group-sm .radio label,
                .mng_form .form-group.form-group-sm label {
                    font-size: 11px;
                    line-height: 1.5;
                    color: #333;
                    ;
                    font-weight: 400;
                }

                    .mng_form .form-group.form-group-sm label.control-label {
                        font-size: 9px;
                        line-height: 1.125;
                        font-weight: 400;
                        margin: 16px 0 0 0;
                    }

                .mng_form .form-group.form-group-sm .help-block {
                    margin-top: 0;
                    font-size: 9px;
                }

                .mng_form .form-group.form-group-sm.label-floating label.control-label,
                .mng_form .form-group.form-group-sm.label-placeholder label.control-label {
                    top: -11px;
                    font-size: 11px;
                    line-height: 1.5;
                }

                .mng_form .form-group.form-group-sm.label-floating.is-focused label.control-label,
                .mng_form .form-group.form-group-sm.label-floating:not(.is-empty) label.control-label,
                .mng_form .form-group.form-group-sm.label-static label.control-label {
                    top: -25px;
                    left: 0;
                    font-size: 9px;
                    line-height: 1.125;
                }

                .mng_form .form-group.form-group-sm.label-floating input.form-control:-webkit-autofill ~ label.control-label label.control-label {
                    top: -25px;
                    left: 0;
                    font-size: 9px;
                    line-height: 1.125;
                }

            .mng_form .form-group.form-group-lg {
                padding-bottom: 9px;
                margin: 30px 0 0 0;
            }

                .mng_form .form-group.form-group-lg .form-control {
                    margin-bottom: 9px;
                }

                    .mng_form .form-group.form-group-lg .form-control::-moz-placeholder {
                        font-size: 18px;
                        line-height: 1.3333333;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                    .mng_form .form-group.form-group-lg .form-control:-ms-input-placeholder {
                        font-size: 18px;
                        line-height: 1.3333333;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                    .mng_form .form-group.form-group-lg .form-control::-webkit-input-placeholder {
                        font-size: 18px;
                        line-height: 1.3333333;
                        color: #333;
                        ;
                        font-weight: 400;
                    }

                .mng_form .form-group.form-group-lg .checkbox label,
                .mng_form .form-group.form-group-lg .radio label,
                .mng_form .form-group.form-group-lg label {
                    font-size: 18px;
                    line-height: 1.3333333;
                    color: #333;
                    ;
                    font-weight: 400;
                }

                    .mng_form .form-group.form-group-lg label.control-label {
                        font-size: 14px;
                        line-height: .99999998;
                        font-weight: 400;
                        margin: 16px 0 0 0;
                    }

                .mng_form .form-group.form-group-lg .help-block {
                    margin-top: 0;
                    font-size: 14px;
                }

                .mng_form .form-group.form-group-lg.label-floating label.control-label,
                .mng_form .form-group.form-group-lg.label-placeholder label.control-label {
                    top: -5px;
                    font-size: 18px;
                    line-height: 1.3333333;
                }

                .mng_form .form-group.form-group-lg.label-floating.is-focused label.control-label,
                .mng_form .form-group.form-group-lg.label-floating:not(.is-empty) label.control-label,
                .mng_form .form-group.form-group-lg.label-static label.control-label {
                    top: -32px;
                    left: 0;
                    font-size: 14px;
                    line-height: .99999998;
                }

                .mng_form .form-group.form-group-lg.label-floating input.form-control:-webkit-autofill ~ label.control-label label.control-label {
                    top: -32px;
                    left: 0;
                    font-size: 14px;
                    line-height: .99999998;
                }

        .mng_form select.form-control {
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        .mng_form .form-group.is-focused select.form-control {
            -webkit-box-shadow: none;
            box-shadow: none;
            border-color: #D2D2D2;
        }

            .mng_form .form-group.is-focused select.form-control[multiple],
            select.form-control[multiple] {
                height: 85px;
            }

        .mng_form .input-group-btn .btn {
            margin: 0 0 7px 0;
        }

        .mng_form .form-group.form-group-sm .input-group-btn .btn {
            margin: 0 0 3px 0;
        }

        .mng_form .form-group.form-group-lg .input-group-btn .btn {
            margin: 0 0 9px 0;
        }

        .mng_form .input-group .input-group-btn {
            padding: 0 12px;
        }

        .mng_form .input-group .input-group-addon {
            border: 0;
            background: 0 0;
        }

        .mng_form .form-group input[type=file] {
            opacity: 0;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 100;
        }

        .contact-widget-section .single-contact-widget {
            background: #f9f9f9;
            padding: 20px 25px;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.26);
            height: 260px;
            margin-top: 25px;
            transition: all 0.3s ease-in-out;
        }

            .contact-widget-section .single-contact-widget i {
                font-size: 75px;
            }

            .contact-widget-section .single-contact-widget h3 {
                font-size: 20px;
                color: #333;
                font-weight: 700;
                padding-bottom: 10px;
            }

            .contact-widget-section .single-contact-widget p {
                line-height: 16px;
            }

            .contact-widget-section .single-contact-widget:hover {
                background: #fff;
                box-shadow: 0 1px 4px rgba(0, 0, 0, 0.46);
                cursor: pointer;
                transition: all 0.3s ease-in-out;
            }

        #contactForm {
            margin-top: -10px;
        }

            #contactForm .form-group label.control-label {
                color: #8c8c8c;
            }

            #contactForm .form-control {
                font-weight: 500;
                height: auto;
            }



        .portfolio-menu {
            text-align: center;
        }

            .portfolio-menu ul li {
                display: inline-block;
                margin: 0;
                list-style: none;
                padding: 10px 15px;
                cursor: pointer;
                -webkit-transition: all 05s ease;
                -moz-transition: all 05s ease;
                -ms-transition: all 05s ease;
                -o-transition: all 05s ease;
                transition: all .5s ease;
            }

        .portfolio-item img {
            width: 100%;
        }

        .portfolio-item .item {
            /*width:303px;*/
            float: left;
            margin-bottom: 10px;
        }



        .sidebar-item {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            /* Position the items */
            // &:nth-child(2);

        {
            top: 25%;
        }

        // &:nth-child(3) {
            top: 50%;
        }

        // &:nth-child(4) {
            top: 75%;
        }

        }


        .make-me-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            padding: 0 15px;
        }

        .dis_cost {
            text-decoration: line-through;
        }

        .dis_cover .head {
            display: inline;
            color: #fff;
        }

        .brd {
            border: 1px solid #fff;
            padding: 15px 25px;
            margin-top: 15px;
        }

            .brd ul {
                list-style-type: disc;
            }

        .caption_slider .brd h3 {
            margin: 0px 0px 15px;
            color: #fff;
            font-size: 25px;
        }

        .caption_slider h3 {
            color: #fff;
            font-size: 35px;
            margin: 0px;
        }

        .dis_cover {
            /* background: #fff; */
            margin: 8px 0px 15px;
        }

        .caption_slider h4 {
            color: #fff;
            font-weight: bold;
            font-style: italic;
            margin: 0px;
        }

        .sec_bg {
            display: inline-block;
            width: 100%;
            background: #fff;
            margin: 35px 0px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .btn_hold {
            text-align: center;
            background: #333;
            position: absolute;
            bottom: 0;
            z-index: 999;
            width: 100%;
        }

        a.secondary:hover {
            background: #fff;
        }

        a.secondary {
            padding: 1px;
            background: #ff9800;
            margin: 5px 0px;
        }

        .what_more li {
            list-style-type: disc;
            font-weight: bold;
            margin: 0px 0px 0px 25px;
            font-size: 14px;
        }

        .panel-headin .nav-tabs {
            border: 0px;
        }

        .points {
            margin: 0px 0px 22px;
        }

            .points span {
                display: block;
                margin-bottom: 7px;
            }

        .mng_panel {
            border: 1px solid #ccc;
            border-top: 0px;
        }

        .special {
            border-top: 1px solid #ccc;
            padding-top: 15px;
        }

        .img_float {
            width: 250px;
            float: left;
            padding-right: 15px;
        }

        .day_duration:last-child {
            border: 0px;
        }

        .day_duration {
        }

            .day_duration h1 {
                margin: 2px 0px 4px;
                padding: 0px;
                font-size: 21px;
            }

            .day_duration p {
                margin-top: 0px;
            }

            .day_duration h4 {
                font-size: 23px;
                margin: 5px 0px;
            }

        .special span {
            margin: 0px 26px 0px 0px;
            font-weight: bold;
            font-size: 15px;
        }

            .special span i {
                font-weight: normal;
                color: #fff;
                margin: 0px 4px 0px 0px;
                background: #908e8e;
                width: 31px;
                border-radius: 15px;
                height: 31px;
                line-height: 31px;
                text-align: center;
            }

        .clr {
            clear: both;
        }

        .left_sec h3 {
            font-size: 23px;
            /* font-weight: bold; */
        }

        .listing {
            list-style-type: disc;
        }

            .listing li {
                padding: 25px;
            }

            .listing li {
                margin: 11px 25px;
                background: #f8f8f8;
                padding: 7px;
            }
        /*css for timeline start here*/

        .timeline {
            list-style: none;
            padding: 20px 0 20px;
            position: relative;
        }

            .timeline:before {
                top: 0;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 3px;
                background-color: #eeeeee;
                left: 50%;
                margin-left: -1.5px;
            }

            .timeline > li {
                margin-bottom: 20px;
                position: relative;
            }

                .timeline > li:before,
                .timeline > li:after {
                    content: " ";
                    display: table;
                }

                .timeline > li:after {
                    clear: both;
                }

                .timeline > li:before,
                .timeline > li:after {
                    content: " ";
                    display: table;
                }

                .timeline > li:after {
                    clear: both;
                }

                .timeline > li > .timeline-panel {
                    width: 46%;
                    float: left;
                    border: 1px solid #d4d4d4;
                    border-radius: 2px;
                    padding: 20px;
                    position: relative;
                    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
                    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
                }

                    .timeline > li > .timeline-panel:before {
                        position: absolute;
                        top: 26px;
                        right: -15px;
                        display: inline-block;
                        border-top: 15px solid transparent;
                        border-left: 15px solid #ccc;
                        border-right: 0 solid #ccc;
                        border-bottom: 15px solid transparent;
                        content: " ";
                    }

                    .timeline > li > .timeline-panel:after {
                        position: absolute;
                        top: 27px;
                        right: -14px;
                        display: inline-block;
                        border-top: 14px solid transparent;
                        border-left: 14px solid #fff;
                        border-right: 0 solid #fff;
                        border-bottom: 14px solid transparent;
                        content: " ";
                    }

                .timeline > li > .timeline-badge {
                    color: #fff;
                    width: 50px;
                    height: 50px;
                    line-height: 50px;
                    font-size: 1.4em;
                    text-align: center;
                    position: absolute;
                    top: 16px;
                    left: 50%;
                    margin-left: -25px;
                    background-color: #999999;
                    z-index: 100;
                    border-top-right-radius: 50%;
                    border-top-left-radius: 50%;
                    border-bottom-right-radius: 50%;
                    border-bottom-left-radius: 50%;
                }

                .timeline > li.timeline-inverted > .timeline-panel {
                    float: right;
                }

                    .timeline > li.timeline-inverted > .timeline-panel:before {
                        border-left-width: 0;
                        border-right-width: 15px;
                        left: -15px;
                        right: auto;
                    }

                    .timeline > li.timeline-inverted > .timeline-panel:after {
                        border-left-width: 0;
                        border-right-width: 14px;
                        left: -14px;
                        right: auto;
                    }

        .timeline-badge.primary {
            background-color: #2e6da4 !important;
        }

        .timeline-badge.success {
            background-color: #3f903f !important;
        }

        .timeline-badge.warning {
            background-color: #f0ad4e !important;
        }

        .timeline-badge.danger {
            background-color: #d9534f !important;
        }

        .timeline-badge.info {
            background-color: #5bc0de !important;
        }

        .timeline-title {
            margin-top: 0;
            color: inherit;
        }

        .timeline-body > p,
        .timeline-body > ul {
            margin-bottom: 0;
        }

            .timeline-body > p + p {
                margin-top: 5px;
            }

        @media (max-width: 767px) {
            .owl_slide {
                display: none;
            }

            .special span {
                margin: 0px;
            }

            .sec_bg {
                display: inline-block;
                margin: 0px;
            }

            .slide_cov {
                height: inherit;
            }

            .caption_slider {
                width: 100%;
                position: relative;
                right: 0px;
                left: 0px;
                top: 0px;
                bottom: 0px;
                padding: 15px;
            }

            .caption_slider1 {
                width: 94%;
                position: relative;
                margin: 0 auto;
                padding: 5% 0px;
            }

            .offer {
                font-size: 23px;
            }

            .caption_slider h3 {
                font-size: 27px;
            }

            .per_detail {
                color: #000;
            }

            .cap_bu {
                display: inline-block;
                background: transparent;
                padding: 0px;
                border-radius: 0px;
                color: #000;
            }

            .btn_hold {
                display: none;
            }

            ul.timeline:before {
                left: 40px;
            }

            .cap_bu a:hover span {
            }

            .caption_slider .brd h3 {
                margin: 0px;
                clear: both;
                padding: 15px 0px 5px;
            }

            .left_sec .nav-tabs > li > a {
                padding: 8px 10px;
            }

            .left_sec .nav-tabs {
                display: inline-block;
            }

            .portfolio-menu ul li {
                padding: 10px 8px;
            }

            ul.timeline > li > .timeline-panel {
                width: calc(100% - 49px);
                width: -moz-calc(100% - 49px);
                width: -webkit-calc(100% - 49px);
            }

            .timeline::before {
                margin-left: -35.5px;
            }

            ul.timeline > li > .timeline-badge {
                left: -15px;
                margin-left: 0;
                top: 16px;
            }

            ul.timeline > li > .timeline-panel {
                float: right;
            }

                ul.timeline > li > .timeline-panel:before {
                    border-left-width: 0;
                    border-right-width: 15px;
                    left: -15px;
                    right: auto;
                }

                ul.timeline > li > .timeline-panel:after {
                    border-left-width: 0;
                    border-right-width: 14px;
                    left: -14px;
                    right: auto;
                }
        }

        @media only screen and (min-device-width :480px) and (max-device-width : 767px) {
            .panel-headin .nav-tabs.nav-justified > li {
                width: 32%;
                display: inline-block;
            }

            .boxi .nav-tabs.nav-justified > li {
                float: none;
                display: inline-block;
            }
        }
    </style>
 
    <style>
       ul.cross li:before {
    content: "\e890";
    margin-right: 10px;
    color: #e52f48;
    font-family: "soap-icons";
    font-size: 1.3333em;
    line-height: 2em;
    font-size: 1.1em;
}
    </style>
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>
    <script src="scripts/PackageDetails.js?v=1.5"></script>

    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
        };
    </script>
    <style>
        .itinere-head {
            background-color: #EEE;
            padding-top: 1px;
            padding-bottom: 0;
            font-weight: 600;
            margin-bottom: 9px !important;
            color: #666;
            font-size: 14px;
            border-bottom: 1px solid #ccc;
        }

        .info-box {
            padding: 20px 0px !important;
            border-bottom: 1px solid #ff9800 !important;
            border-top: none !important;
            border-right: none !important;
            border-left: none !important;
            position: relative;
        }

        .dayss {
            background-color: #ff9800;
            padding: 3px 9px;
            color: #fff;
            margin-right: 5px;
        }

        #Days div.date {
            width: 72px;
            height: 72px;
            background: #f5f5f5;
            text-align: center;
            float: left;
            margin-right: 20px;
        }

            #Days div.date > * {
                margin: 0;
                display: block;
            }

            #Days div.date > .month {
                background: #e44049;
                height: 20px;
                line-height: 20px;
                font-size: 0.8333em;
                color: #fff;
            }

            #Days div.date > .date {
                font-size: 2em;
                line-height: 1em;
                color: #e44049;
                font-weight: bold;
                margin-top: 6px;
            }

            #Days div.date > .day {
                font-size: 0.8333em;
                font-weight: normal;
                line-height: 1.25em;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="con_mng" id="content">
        <%--<div class="container">
            <div class="col-md-12">
                <div class="row">
                    <img src="images/Loader.gif" id="packagedloader" style="margin-left: 35%; margin-top: 80px; display: none" />

                    <div class="col-md-9">
                        <div id="PackgImg">
                        </div>
                        <br />
                    </div>

                    <div class="booking-details travelo-box" style="display: none" id="sdb">
                        <div id="PackageRight"></div>

                        <button type="button" class="btn-medium" style="padding: 0px 15px" onclick="Booking()">Book now</button>&nbsp;&nbsp;&nbsp;
                       <button type="button" class="btn-medium" style="padding: 0px 15px" onclick="PackageMailModal()">Send Mail</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id="PackgDescription" class="tab-container box">
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="container clr">
            <div class="row">
                <div class="sec_bg col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="PackgImg">
                            </div>
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="panel-body left_sec" id="PackgDescription">
                            </div>
                        </div>
                        <div id="stick-here" class="col-md-4 col-sm-4 col-xs-12">
                            <div id="stickThis">
                                <div class="booking-details travelo-box" style="display: none; box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175); margin-top: 15px;" id="sdb">
                                    <div id="PackageRight"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn-medium full-width" style="padding: 0px 15px; border-radius: 15px; cursor: pointer;" onclick="Booking()">Book now</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn-medium full-width" style="padding: 0px 15px; border-radius: 15px; cursor: pointer;" onclick="PackageMailModal()">Send Mail</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="cap_bu" style="margin-left: 45px;">
                                    <h5 class="per_detail"><i>Want Us to Call You ?</i></h5>
                                    <a data-toggle="modal" data-target="#myModal" href="#"><span>Special Offer</span></a>
                                    <a data-toggle="modal" data-target="#myModal2" href="#"><span>Jumbo Discount</span><div></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade bs-example-modal-lg" id="BookingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form method="post" name="customerData" action="ccavRequestHandler.aspx">
                <input type="hidden" name="tid" id="tid" />
                <input type="hidden" name="amount" id="Amount" />
                <input type="hidden" name="order_id" id="order_id" />
                <input type="hidden" name="merchant_id" id="merchant_id" value="174467" />
                <input type="hidden" name="currency" id="currency" value="INR" />
                <input type="hidden" name="redirect_url" value="Packages.aspx" id="redirect_url" />
                <input type="hidden" name="cancel_url" value="ccavResponseHandler.aspx" id="cancel_url" />
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                        <%--<h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Booking Details</h4>--%>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                            <div class="">

                                <table>
                                    <tbody>

                                        <tr>
                                            <td style="border-top: 1px solid rgb(255, 255, 255); padding: 10px"><span class="dark bold">Package Name: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspPackageName"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">City: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCitys"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">Days: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspNDays"></span></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid rgb(255, 255, 255); padding: 10px"><span class="dark bold">Starts From: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCheckIn"></span></td>
                                            <td style="padding-left: 20px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark bold">Up To: </span></td>
                                            <td style="padding-left: 15px; border-top: 1px solid rgb(255, 255, 255);"><span class="dark" id="dspCheckOut"></span></td>

                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div style="background: rgba(255, 153, 0, 0.4); color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>
                            <div class="frow3">
                                <div class="col-md-6">

                                    <label class="margtop10">Name</label>
                                    <input type="text" id="txt_name" name="billing_name" placeholder="Name" class="form-control logpadding" />
                                    <label class="margtop10">Phone</label>
                                    <input type="text" name="billing_tel" placeholder="Phone" id="txt_phone" class="form-control logpadding" />
                                    <label class="margtop10">E-mail</label>
                                    <input type="text" name="billing_email" placeholder="E-mail" id="txt_email" class="form-control logpadding" />
                                    <label class="margtop20">Adults</label>
                                    <select id="Select_Adults" class="form-control logpadding">
                                        <option value="1">1</option>
                                        <option value="2" selected="selected">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>


                                    <div class="row">
                                        <div class="col-md-4" id="Childs">
                                            <label class="margtop20">Childs</label>
                                            <select id="Select_CHILD" class="form-control logpadding " onchange="NoChild(this.value)">
                                                <option value="0" selected="selected">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>

                                            </select>
                                        </div>
                                        <div class="col-md-4" id="div_FirstChild" style="display: none">
                                            <label class="margtop20">Ages</label>
                                            <select id="Select_AgeChildSecond1" class="form-control logpadding ">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4" id="div_SecondChild" style="display: none">
                                            <label class="margtop20">
                                                <br />
                                            </label>
                                            <select id="Select_AgeChildSecond2" class="form-control logpadding ">
                                                <option selected="selected" value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="margtop10">Travel date</label>
                                    <input type="text" id="txt_date" placeholder="dd-mm-yy" class="form-control logpadding" />
                                    <%-- <div class="datepicker-wrap">
                                        <input type="text" name="date_from" id="txt_date" style="cursor: pointer" class="form-control logpadding" placeholder="Check In" />
                                    </div>--%>
                                    <label class="margtop10">Add on required</label>
                                    <textarea rows="13" id="txt_Remark" class="form-control"></textarea>
                                    <br />
                                    <button id="Btn_CCAvenue" type="submit" class="button btn-small orange">Book</button>
                                    <br />
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #fff">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" title="Close">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="size17" id="AlertMessage">
                            </p>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-example-modal-lg" id="PackageMailModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="margin-top: 150px; margin-left: 150px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="size13 dark">Email  : </span>
                                <input type="text" id="Email" data-trigger="focus" autocomplete="off" data-placement="top" placeholder="Email" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="col-md-6">
                                <span class="size13 dark">Name : </span>
                                <input type="text" id="Name" data-trigger="focus" autocomplete="off" data-placement="top" placeholder="Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                            </div>

                            <div class="col-md-12">
                                <br />
                                <span class="size13 dark">Remark: </span>
                                <textarea id="Remark" data-trigger="focus" data-placement="top" placeholder="Remark" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address"></textarea>
                            </div>
                        </div>
                        <br />
                        <div class="block" style="float: right">
                            <input type="button" id="btn_Cancel" value="Send" onclick="SendPackageMail()" class="button btn-small orange" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>

    <script>
        function sticktothetop() {
            var window_top = $(window).scrollTop();
            var top = $('#stick-here').offset().top;
            if (window_top > top) {
                $('#stickThis').addClass('stick');
                $('#stick-here').height($('#stickThis').outerHeight());
            } else {
                $('#stickThis').removeClass('stick');
                $('#stick-here').height(0);
            }
        }
        $(function () {
            $(window).scroll(sticktothetop);
            sticktothetop();
        });
    </script>
</asp:Content>
