﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace CUTUK.DataLayer
{
    public class PackageMail
    {
        public static bool BookPackage(string nID, string CategoryID, string Name, string MobileNo, string Email, Int64 noAdults, Int64 noChild, string noInfant, string ChildAges, string TarvelDate, string AddOnReq, string PackageName, string StartDate, string TillDate, string CategoryType)
        {
            //Work start
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            StringBuilder AdminMail = new StringBuilder();
            AdminMail.Append("<div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\">");
            AdminMail.Append("<div style=\"min-height:50px;width:auto\">");
            AdminMail.Append("<br>");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div style=\"margin-left:10%\">  ");
            AdminMail.Append("<p> <span>Your Booking Details.</span><br></p>  ");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div><table>");
            AdminMail.Append("<tbody><tr>");
            AdminMail.Append("<td><b>Passenger Name</b></td>");
            AdminMail.Append("<td>: " + Name + "</td>");
            AdminMail.Append("");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>E-Mail</b></td>");
            AdminMail.Append("<td>:  " + Email + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Mobile No.</b></td>");
            AdminMail.Append("<td>:  " + MobileNo + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Booking Date</b></td>");
            AdminMail.Append("<td>: " + DateTime.Now.ToString() + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<td><b> No Adults</b></td>");
            AdminMail.Append("<td>: " + noAdults + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<td><b>No Child</b></td>");
            AdminMail.Append("<td>: " + noChild + "</td>");
            AdminMail.Append("</tr>");
            if (noChild != 0)
            {
                AdminMail.Append("<tr>");
                AdminMail.Append("<td><b> Child Ages</b></td>");
                AdminMail.Append("<td>: " + ChildAges + "</td>");
                AdminMail.Append("</tr>");
            }
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b> Details</b></td>");
            AdminMail.Append("<td>: " + AddOnReq + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Package Name</b></td>");
            AdminMail.Append("<td>:  " + PackageName + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            string PackageType = "";
            if (CategoryID == "1")
            {
                PackageType = "Standard";
            }
            else if (CategoryID == "2")
            {
                PackageType = "Deluxe";
            }
            else if (CategoryID == "3")
            {
                PackageType = "Premium";
            }
            else if (CategoryID == "4")
            {
                PackageType = "Luxury";
            }
            AdminMail.Append("<td><b>Package Type</b></td>");
            AdminMail.Append("<td>:  " + PackageType + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Start From</b></td>");
            AdminMail.Append("<td>:  " + StartDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Till</b></td>");
            AdminMail.Append("<td>:  " + TillDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Travel Date  </b></td>");
            AdminMail.Append("<td>:  " + TarvelDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("</tbody></table></div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<p><span>For any further clarification & query kindly feel free to contact us</span><br></p>");
            AdminMail.Append("");
            AdminMail.Append("<span>");
            AdminMail.Append("<b>Thank You,</b><br>");
            AdminMail.Append("</span>");
            AdminMail.Append("<span>");
            AdminMail.Append("Administrator");
            AdminMail.Append("</span>");
            AdminMail.Append("");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div>");
            AdminMail.Append("<span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><table border=\"0\" style=\"height:30px;width:100%;border-spacing:0px;border-top-color:white;border-left:none;border-right:none\">");
            AdminMail.Append("<tbody><tr style=\"border:none;border-bottom-color:gray;color:#57585a\">");
            AdminMail.Append("</font></span></div><span class=\"HOEnZb adL\"><font color=\"#888888\">");

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in Email.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", AdminMail.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static bool AdminMail(string nID, string CategoryID, string Name, string MobileNo, string Email, Int64 noAdults, Int64 noChild, string noInfant, string ChildAges, string TarvelDate, string AddOnReq, string PackageName, string StartDate, string TillDate, string CategoryType)
        {
            //Work start
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            StringBuilder AdminMail = new StringBuilder();
            AdminMail.Append("<div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\">");
            AdminMail.Append("<div style=\"min-height:50px;width:auto\">");
            AdminMail.Append("<br>");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div style=\"margin-left:10%\">  ");
            AdminMail.Append("<p> <span>Booking Details.</span><br></p>  ");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div><table>");
            AdminMail.Append("<tbody><tr>");
            AdminMail.Append("<td><b>Passenger Name</b></td>");
            AdminMail.Append("<td>: " + Name + "</td>");
            AdminMail.Append("");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>E-Mail</b></td>");
            AdminMail.Append("<td>:  " + Email + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Mobile No.</b></td>");
            AdminMail.Append("<td>:  " + MobileNo + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Booking Date</b></td>");
            AdminMail.Append("<td>: " + DateTime.Now.ToString() + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<td><b> No Adults</b></td>");
            AdminMail.Append("<td>: " + noAdults + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<td><b>No Child</b></td>");
            AdminMail.Append("<td>: " + noChild + "</td>");
            AdminMail.Append("</tr>");
            if (noChild != 0)
            {
                AdminMail.Append("<tr>");
                AdminMail.Append("<td><b> Child Ages</b></td>");
                AdminMail.Append("<td>: " + ChildAges + "</td>");
                AdminMail.Append("</tr>");
            }
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b> Details</b></td>");
            AdminMail.Append("<td>: " + AddOnReq + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Package Name</b></td>");
            AdminMail.Append("<td>:  " + PackageName + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            string PackageType = "";
            if (CategoryID == "1")
            {
                PackageType = "Standard";
            }
            else if (CategoryID == "2")
            {
                PackageType = "Deluxe";
            }
            else if (CategoryID == "3")
            {
                PackageType = "Premium";
            }
            else if (CategoryID == "4")
            {
                PackageType = "Luxury";
            }
            AdminMail.Append("<td><b>Package Type</b></td>");
            AdminMail.Append("<td>:  " + PackageType + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Start From</b></td>");
            AdminMail.Append("<td>:  " + StartDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Till</b></td>");
            AdminMail.Append("<td>:  " + TillDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Travel Date  </b></td>");
            AdminMail.Append("<td>:  " + TarvelDate + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("</tbody></table></div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<span>");
            AdminMail.Append("<b>Thank You,</b><br>");
            AdminMail.Append("</span>");
            AdminMail.Append("");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div>");
            AdminMail.Append("<span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><table border=\"0\" style=\"height:30px;width:100%;border-spacing:0px;border-top-color:white;border-left:none;border-right:none\">");
            AdminMail.Append("<tbody><tr style=\"border:none;border-bottom-color:gray;color:#57585a\">");
            AdminMail.Append("</font></span></div><span class=\"HOEnZb adL\"><font color=\"#888888\">");

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                string MailTo = Convert.ToString(ConfigurationManager.AppSettings["supportMail"]);

                foreach (string mail in MailTo.Split(','))
                {
                    Email1List.Add(mail, mail);
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", AdminMail.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }
    }
}