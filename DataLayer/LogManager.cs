﻿using CUTUK.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class LogManager
    {
        public static DBHelper.DBReturnCode Add(int Resource, string RequestHeader, string RequestBody, string ResponseHeader, string ResponseBody, Int64 UserID, int Status)
        {
            int rowsAffected = 0;

            SqlParameter[] SQLParams = new SqlParameter[7];
            SQLParams[0] = new SqlParameter("@Resource", Resource);
            SQLParams[1] = new SqlParameter("@RequestHeader", RequestHeader);
            SQLParams[2] = new SqlParameter("@RequestBody", RequestBody);
            SQLParams[3] = new SqlParameter("@ResponseHeader", ResponseHeader);
            SQLParams[4] = new SqlParameter("@ResponseBody", ResponseBody);
            SQLParams[5] = new SqlParameter("@UserID", UserID);
            SQLParams[6] = new SqlParameter("@Status", Status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_AddAPILog", out rowsAffected, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode AddTrans(int Resource, string RequestHeader, string RequestBody, string ResponseHeader, string ResponseBody, Int64 UserID, int Status)
        {
            int rowsAffected = 0;

            SqlParameter[] SQLParams = new SqlParameter[7];
            SQLParams[0] = new SqlParameter("@Resource", Resource);
            SQLParams[1] = new SqlParameter("@RequestHeader", RequestHeader);
            SQLParams[2] = new SqlParameter("@RequestBody", RequestBody);
            SQLParams[3] = new SqlParameter("@ResponseHeader", ResponseHeader);
            SQLParams[4] = new SqlParameter("@ResponseBody", ResponseBody);
            SQLParams[5] = new SqlParameter("@UserID", UserID);
            SQLParams[6] = new SqlParameter("@Status", Status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_AddAPILogTrans", out rowsAffected, SQLParams);
            return retCode;
        }

    }
}