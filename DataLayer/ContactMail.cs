﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class ContactMail
    {

        const string SERVER = "EvidentNet";
        const string URL = "http://www.evidentnet.com";
        const string EmailTemplate = "<html><body>" +
        "<div align=\"center\">" +
        "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
        "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
        "</tr>" +

        "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"> Name: #name#</td>" +
        "</tr>" +

          "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Email: #email#</td>" +
        "</tr>" +


        "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">subject: #subject#</td>" +
        "</tr>" +

         "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">message:#message#</td>" +
        "</tr>" +

        "<tr>" +
        "<td width=\"100%\" style=\"vertical-align:middle;text-align:left;padding:20px 15px 20px 15px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><b>Thank You</b><br/>Adminstrator</td>" +
        "</tr>" +

        "<tr>" +
        "<td width=\"100%\" style=\"padding:8px 5px 8px 5px;text-align:center;FONT-SIZE: 8pt;color:#007CC2;FONT-FAMILY: Tahoma;background-color:Silver\">  © 2018 - 2019 IdealTours.in</td>" +
        "</tr>" +

        "</table>" +
        "</div>" +
        "</html></body>";

        public static bool SendEmail(string name, string email, string subject, string message)
        {

            try
            {
                string sMailMessage = EmailTemplate.Replace("#title#", "Information Received");
                sMailMessage = sMailMessage.Replace("#name#", name);
                sMailMessage = sMailMessage.Replace("#email#", email);
                sMailMessage = sMailMessage.Replace("#subject#", subject);
                sMailMessage = sMailMessage.Replace("#message#", message);

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in email.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CUT.DataLayer.MailManager.SendMail(accessKey, Email1List, "Enquiry", sMailMessage, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }
    }
}