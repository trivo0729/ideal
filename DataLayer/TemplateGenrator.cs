﻿using CutAdmin;
using CutAdmin.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace CUTUK.DataLayer
{
    public class TemplateGenrator
    {
        static readonly Regex re = new Regex(@"\$(\w+)\$", RegexOptions.Compiled);
        public class Token
        {
            public string TokenValue { get; set; }
            public string SampleValue { get; set; }
        }
        public static List<Token> GetTokenList()
        {
            List<Token> tokens = new List<Token>();
            foreach (var Parms in arrParms)
            {
                tokens.Add(new Token() { TokenValue = Parms.Key, SampleValue = Parms.Value });
            }
            return tokens;
        }
        public static string Path { get; set; }
        public static string GetTemplate
        {
            get
            {

                Path = HttpContext.Current.Server.MapPath(Path);
                string sTemplate = "";
                using (var reader = new StreamReader(Path))
                {
                    while (!reader.EndOfStream)
                    {
                        sTemplate += reader.ReadLine();
                    }
                }
                sTemplate = re.Replace(sTemplate, match => arrParms[match.Groups[1].Value]);
                return sTemplate.ToString();
            }
        }
        public static Dictionary<string, string> arrParms { get; set; }

        public static bool Post(string data, out string response)
        {
            response = "";
            try
            {
                var oRequest = WebRequest.Create(data);
                oRequest.Method = "GET";
                oRequest.Timeout = 300000;
                using (var oResponse = oRequest.GetResponse())
                {
                    using (var oResponseStream = oResponse.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                        if (response.Contains("error"))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            catch (WebException oWebException)
            {
                if (oWebException.Response != null)
                {
                    using (var oResponseStream = oWebException.Response.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                    }
                }
                return false;
            }
            catch (Exception oException)
            {
                response = oException.Message;
                return false;
            }
        }

        public static string GetTemplatePath(Int64 AdminID, string TemplateName)
        {
            string sTemplate = string.Empty;
            try
            {
                using (var DB = new Trivo_AmsHelperDataContext())
                {
                    var List = (from obj in DB.tbl_Admins where obj.ID == AdminID select obj).FirstOrDefault();
                    var ListName = (from obj in DB.tbl_EmailTemplates where obj.nAdminID == AdminID && obj.sTemplateName == TemplateName select obj).FirstOrDefault();
                    string ServerPath = "";
                    if (ListName != null)
                    {
                        Path = ListName.sPath;

                    }
                    else
                    {
                        string pathDownload = Common.GetAppServerFolder;
                        DirectoryInfo directory = new DirectoryInfo(pathDownload);
                        DirectoryInfo[] directories = directory.GetDirectories();
                        foreach (DirectoryInfo folder in directories)
                        {
                            string Folder = folder.Name;
                            var arrList = EmailTemplate.GetSubMenu(folder.Name);
                            foreach (var Menu in arrList)
                            {
                                if (TemplateName == Menu.Name)
                                {
                                    if (Menu.ChildItem.Count == 0)
                                    {
                                        Path = Menu.Path;
                                    }
                                    else
                                    {
                                        Path = Menu.ChildItem.FirstOrDefault().Path;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                //throw;
            }
            return sTemplate;
        }
    }
}