﻿using CUT.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CUTUK.DataLayer
{
    public class B2CCustomerLoginManager
    {
        public static DBHelper.DBReturnCode RegisterUser(string UserName, string Password, string Contact1, string Contact2, string Address, string CityId)
        {
            DBHelper.DBReturnCode Retcode = DBHelper.DBReturnCode.EXCEPTION;
            try
            {
                DataTable dtResult = new DataTable();
                SqlParameter[] sqlParamsLogin = new SqlParameter[1];
                sqlParamsLogin[0] = new SqlParameter("@UserName", UserName);
                Retcode = DBHelper.GetDataTable("Proc_tbl_B2C_CustomerLoginCheckUser", out dtResult, sqlParamsLogin);
                if (DBHelper.DBReturnCode.SUCCESS != Retcode)
                {
                    int RowEffected;
                    string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(Password);
                    string TodayTime = DateTime.Now.ToShortTimeString();
                    DateTime TodayDate = DateTime.Now.Date;
                    string MyDate = TodayDate.ToString("dd-MM-yyyy");
                    string CreatedDate = MyDate + " " + TodayTime;
                    string UniqueCode = "CUK-" + GenerateRandomString(8);

                    SqlParameter[] Sqlparams = new SqlParameter[9];
                    Sqlparams[0] = new SqlParameter("@B2C_Id", UniqueCode);
                    Sqlparams[1] = new SqlParameter("@UserName", UserName);
                    Sqlparams[2] = new SqlParameter("@Password", sEncryptedPassword);
                    Sqlparams[3] = new SqlParameter("@Email", UserName);
                    Sqlparams[4] = new SqlParameter("@Contact1", Contact1);
                    Sqlparams[5] = new SqlParameter("@Contact2", Contact2);
                    Sqlparams[6] = new SqlParameter("@Address", Address);
                    Sqlparams[7] = new SqlParameter("@CityId", CityId);
                    Sqlparams[8] = new SqlParameter("@CreatedDate", CreatedDate);

                    Retcode = DBHelper.ExecuteNonQuery("Proc_tbl_B2C_CustomerLogin", out RowEffected, Sqlparams);

                    if (Retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        DataLayer.DefaultManager.B2CUserLogin(UserName, Password);
                        string UniqueCodeUpdate = "";
                        dtResult = new DataTable();
                        SqlParameter[] sqlParamsUniqueCode = new SqlParameter[1];
                        sqlParamsUniqueCode[0] = new SqlParameter("@B2C_Id", UniqueCode);
                        Retcode = DBHelper.GetDataTable("Proc_tbl_CustomerBookingLoadByB2CId", out dtResult, sqlParamsUniqueCode);
                        if (Retcode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            Int64 nReceivedSid = Convert.ToInt64(dtResult.Rows[0]["Sid"]);
                            Int64 UniqueCodeLength = nReceivedSid.ToString().Length;

                            if (UniqueCodeLength <= 4)
                            {
                                UniqueCodeUpdate = "CUK-" + nReceivedSid.ToString("D" + 4);
                            }
                            else
                                UniqueCodeUpdate = "CUK-" + nReceivedSid.ToString("D" + 6);
                            SqlParameter[] sqlParamsCodeUpdate = new SqlParameter[2];
                            sqlParamsCodeUpdate[0] = new SqlParameter("@Sid", nReceivedSid);
                            sqlParamsCodeUpdate[1] = new SqlParameter("@B2C_Id", UniqueCodeUpdate);
                            Retcode = DBHelper.ExecuteNonQuery("Proc_tbl_CustomerBookingUpdateByB2CId", out RowEffected, sqlParamsCodeUpdate);
                        }
                    }
                }
                else
                {
                    Retcode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                Retcode = DBHelper.DBReturnCode.EXCEPTION;
            }


            return Retcode;
        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }
    }
}