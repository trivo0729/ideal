﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Packages.aspx.cs" Inherits="CUTUK.Packages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/PackageList.js?v=1.2"></script>
    <%--<script src="scripts/Package.js?v=1.5"></script>
    <script src="scripts/PackageDetail.js?v=1.5"></script>--%>
      <link href="css/style.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.min.css"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style type="text/css">
        @keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-moz-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        @-webkit-keyframes tawkMaxOpen {
            0% {
                opacity: 0;
                transform: translate(0, 30px);
                ;
            }

            to {
                opacity: 1;
                transform: translate(0, 0px);
            }
        }

        #pS6Jyk4-1569738072448 {
            outline: none !important;
            visibility: visible !important;
            resize: none !important;
            box-shadow: none !important;
            overflow: visible !important;
            background: none !important;
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity1) !important;
            -moz-opacity: 1 !important;
            -khtml-opacity: 1 !important;
            top: auto !important;
            right: 10px !important;
            bottom: 90px !important;
            left: auto !important;
            position: fixed !important;
            border: 0 !important;
            min-height: 0 !important;
            min-width: 0 !important;
            max-height: none !important;
            max-width: none !important;
            padding: 0 !important;
            margin: 0 !important;
            -moz-transition-property: none !important;
            -webkit-transition-property: none !important;
            -o-transition-property: none !important;
            transition-property: none !important;
            transform: none !important;
            -webkit-transform: none !important;
            -ms-transform: none !important;
            width: auto !important;
            height: auto !important;
            display: none !important;
            z-index: 2000000000 !important;
            background-color: transparent !important;
            cursor: auto !important;
            float: none !important;
            border-radius: unset !important;
            pointer-events: auto !important;
        }

        #Q4gVTA5-1569738072452.open {
            animation: tawkMaxOpen .25s ease !important;
        }
    </style>

    <style>
        .box.listing-style3 p {
            margin: 3px 0px 5px 0px;
        }

        .box.listing-style3 .skin p {
            margin: 15px 0px 0px;
            line-height: 15px;
            font-size: 13px;
        }

        .box.listing-style3 .skin .box_new {
            font-size: 12px;
        }

        .box-title-2 {
            font-size: 15px;
            margin: 3px 9px 0px 0px;
        }

        .box.listing-style3.mng_box {
            display: inline-block;
        }

        .mng_img {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        . mng_box .box-title {
            font-size: 17px;
            color: #5d5b5b;
        }

        .mng_box {
            padding-bottom: 15px;
        }

            .mng_box input[type="button"].button {
                padding: 13px;
                height: 0px;
                line-height: 0px;
                font-size: 13px;
                font-weight: normal;
            }

                .mng_box input[type="button"].button:hover {
                    background: #9c9c9c;
                    color: #f9f9f9;
                }

        .skin_mng {
            border-right: 1px solid #ededed;
            height: 77px;
        }

        .skin {
            border-top: 1px solid #ededed;
            border-bottom: 1px solid #ededed;
            margin-top: 7px;
            margin-bottom: 7px;
            overflow: auto;
            text-align: center;
        }

        .owl_grip {
        }

            .owl_grip .owl-nav {
                position: absolute;
                top: 40%;
                width: 100%;
                font-size: 40px;
            }

                .owl_grip .owl-nav .owl-prev, .owl_grip .owl-nav .owl-next {
                    left: 0;
                    font-size: 25px;
                    position: absolute;
                    background: #a1a1a1 !important;
                    width: 26px;
                    height: 55px;
                    line-height: 25px !important;
                    color: #fff !important;
                }

                    .owl_grip .owl-nav .owl-prev:hover, .owl_grip .owl-nav .owl-next:hover {
                        background: #333 !important;
                    }

                .owl_grip .owl-nav .owl-next {
                    left: inherit;
                    right: 0;
                }

                    .owl_grip .owl-nav .owl-prev span, .owl_grip .owl-nav .owl-next span {
                        line-height: 30px;
                        height: 37px;
                        display: block;
                    }


        @media only screen and (min-device-width :320px) and (max-device-width :768px) {
            .mng_box input[type="button"].button, .review .button {
                padding: 14px 4px;
                height: inherit;
                margin: 0px;
                line-height: 0px;
                font-size: 12px;
            }

            .box.listing-style3 .skin p {
                margin-top: 5px;
            }

            .mng_mo_brd {
                border: 1px solid #ededed;
                border-left: 0px;
                border-right: 0px;
                . text-align:center;
                padding-bottom: 7px;
            }

            .skin_mng {
                height: inherit;
                padding: 3px 0px;
                text-align: center;
                padding-bottom: 7px;
            }

            .review {
            }
        }
    </style>
    
    
      <section id="content">
        <div class="container">
            <div id="main">
                <div id="div_package">

                </div>
                <%--<div class="row">

                    <div class="col-sm-12 col-md-12">
                       
                        <div class="cruise-list listing-style3 cruise" id="div_package">
                        </div>
                        <div class="owl_grip"  id="div_package" >
                        </div>
                        
                    </div>
                </div>--%>
                <img src="images/Loader.gif" id="packageloader" style="margin-left: 35%; margin-top: 80px; display: none" />
                        
                        <div class="cruise-list listing-style3 cruise" id="Nopackage" style="display: none">
                            <article class="box" id="div_Nopackage">
                            </article>
                        </div>
            </div>
        </div>
    </section>
</asp:Content>
