﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="CUTUK.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/Transfer.js?v=1.2"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="gray-area">
        <div class="container shortcode">
            <div class="row image-box style3">
                <div class="col-md-12">
                    <div id="main" class="col-sms-12 col-sm-12 col-md-12">
                        <div class="booking-section travelo-box">
                            <div class="card-information">
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-6">
                                        <label>Full Name :</label>
                                        <input type="text" id="Name" name="billing_name" class="form-control" value="" placeholder="Enter Your Name" />
                                        <br />
                                        <label>Email :</label>
                                        <input type="text" id="Email" name="billing_email" class="form-control" placeholder="Enter Your Email" />
                                        <br />
                                        <label>Mobile :</label>
                                        <input type="text" id="Mobile" name="billing_tel" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Enter Your Mobile No" />
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <label>Address :</label>
                                        <textarea id="Address" name="billing_address" rows="5" class="form-control" placeholder="Enter Your Address"></textarea>
                                        <br />
                                        <label>Amount :</label>
                                        <input type="text" id="txt_Amount" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" placeholder="Enter Amount" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6" style="float: right">
                                    <button id="PayAmount" type="button" class="button btn-medium sky-blue1" onclick="Pay()">Make payment   </button>
                                    <button id="rzp-button1" type="submit" style="display: none" class="button btn-medium sky-blue1">Pay</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
