﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CalendarSearch.aspx.cs" Inherits="CUTUK.CalendarSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/CalendarSearch.js"></script>
    <script src="fullcalendar/jquery-2.0.2.min.js"></script>
    <script src="fullcalendar/fullcalendar.min.js"></script>
    <script src="fullcalendar/moment.js"></script>
    <link href="fullcalendar/fullcalendar.min.css" rel="stylesheet" />
    <script src="js/modal.js"></script>
    <script type="text/javascript">
        $(document).on({
            ajaxStart: function () { $("#loders").css("display", "") },
            ajaxStop: function () { $("#loders").css("display", "none") }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <img alt="" src="images/Processing.gif" id="loders" style="z-index: 9999; position: fixed; margin-top: 5%; margin-left: 40%" />
    <section id="content">
        <div class="container flight-detail-page">
            <div class="row">
                <div id="main" class="col-md-12">
                    <div class="tab-container style1 box" id="flight-main-content">
                        <div class="tab-content">
                            <div id="calendar-tab" class="tab-pane fade in active" style="background: none">
                                <div id="fullCalendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade " id="ModalBook" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="lblRoomFacility" style="text-align: left">Booking</h4>
                </div>
                <div class="modal-body" style="height: auto">

                    <div class="form-group clearfix">
                        <div class="col-sm-2">
                            <label>Adult</label><br>
                            <select class="form-control" id="sel_Adults">
                                <option value="1" selected="selected">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label>Children</label><br>
                            <select class="form-control  " id="sel_Child">
                                <option value="0" selected="selected">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label>Infants</label><br>
                            <select class="form-control" id="sel_Infant">
                                <option value="0" selected="selected">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label></label>
                            <br>
                            <input type="button" style="float: right" class="button btn-small orange" value="Search" onclick="Book()" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%--  <div class="container">
        <div id="fullCalendar"></div>
    </div>--%>
    <script>
        $(document).ready(function () {
            var calendar = $('#fullCalendar').fullCalendar();
        });
    </script>
</asp:Content>
