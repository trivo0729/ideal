﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="HotelSearch.aspx.cs" Inherits="CUTUK.HotelSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="scripts/HotelSearch.js?v=1.5"></script>
    <script src="scripts/B2CCustomerLogin.js"></script>
    <script src="scripts/login.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/moment.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="tour">
        <div id="slideshow" class="slideshow-bg full-screen">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background1.jpg);"></div>
                    </li>
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background2.jpg);"></div>
                    </li>
                    <li>
                        <div class="slidebg" style="background-image: url(images/uploads/revslider1/holiday/background3.jpg);"></div>
                    </li>
                </ul>
            </div>
            <div class="container">
                <div class="table-wrapper full-width">
                    <div class="table-cell">
                        <div class="search-box">
                            <form>
                                <div class="row">
                                    <div class="col-md-4">
                                        <span class="text-left a-white">Where do you want to go?</span>
                                        <br />
                                        <div class="input-group" style="width: 100%">
                                            <input type="text" style="background-color: none" placeholder="Enter City Here" id="txtCity" class="form-control" />
                                        </div>
                                        <input type="hidden" id="hdnDCode" />
                                    </div>
                                    <div class="col-md-3">
                                        <span class="text-left a-white">Check-In:</span>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="date_from" id="datepicker_HotelCheckin" style="cursor: pointer" class="form-control" placeholder="Check In" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="text-left a-white">Check-Out:</span>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="date_to" id="datepicker_HotelCheckout" style="cursor: pointer" class=" form-control" placeholder="Check Out" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="text-left a-white">Total Nights:</span>
                                        <br />
                                        <select id="Select_TotalNights" class="form-control">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <br id="br1" style="display: none" />
                                <div class="row">
                                    <div class="col-md-4">
                                        <span class="text-left a-white">Nationality:</span>
                                        <br />
                                        <select id="Select_Nationality" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-md-3" style="display: none">
                                        <span class="text-left a-white">Star Rating:</span>
                                        <br />
                                        <select id="Select_StarRating" class="form-control"></select>
                                    </div>

                                    <div class="col-md-5">
                                        <span class="text-left a-white">Hotel Name:</span>
                                        <br />
                                        <input class="form-control" type="text" placeholder="Enter Hotel Here" id="txt_HotelName" />
                                        <input type="hidden" id="hdnHCode" />
                                    </div>

                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2">
                                        <span class="text-left a-white">Rooms:</span>
                                        <select id="Select_Rooms" class="form-control">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="text-left a-white">Room Type:</span>
                                        <input type="text" id="spn_RoomType1" style="cursor: default;" disabled="disabled" class="form-control" value="Room 1" />
                                    </div>
                                    <div class="col-md-2">
                                        <span class="text-left a-white">Adults:</span>
                                        <select id="Select_Adults1" class="form-control">
                                            <option value="1">1</option>
                                            <option value="2" selected="selected">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="text-left a-white">Child:</span>
                                        <select id="Select_Children1" class="form-control">
                                            <option selected="selected" value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildFirst1" class="text-left a-white" style="display: none;">Age of Child 1:</span>
                                        <select id="Select_AgeChildFirst1" class="form-control" style="display: none">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildSecond1" class="text-left a-white" style="display: none;">Age of Child 2:</span>
                                        <select id="Select_AgeChildSecond1" class="form-control" style="display: none;">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_RoomType2" class="text-left a-white" style="display: none;">Room Type:</span>
                                        <input type="text" id="spn_RoomType2" class="form-control" disabled="disabled" value="Room 2" style="display: none; cursor: default;" />
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Adults2" class="text-left a-white" style="display: none;">Adults:</span>
                                        <select id="Select_Adults2" class="form-control" style="display: none">
                                            <option value="1">1</option>
                                            <option value="2" selected="selected">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Children2" class="text-left a-white" style="display: none;">Child:</span>
                                        <select id="Select_Children2" class="form-control" style="display: none">
                                            <option selected="selected" value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildFirst2" class="text-left a-white" style="display: none;">Age of Child 1:</span>
                                        <select id="Select_AgeChildFirst2" class="form-control" style="display: none">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildSecond2" class="text-left a-white" style="display: none;">Age of Child 2:</span>
                                        <select id="Select_AgeChildSecond2" class="form-control" style="display: none;">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_RoomType3" class="text-left a-white" style="display: none;">Room Type:</span>
                                        <input type="text" id="spn_RoomType3" class="form-control" disabled="disabled" value="Room 3" style="display: none; cursor: default;" />
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Adults3" class="text-left a-white" style="display: none;">Adults:</span>
                                        <select id="Select_Adults3" class="form-control" style="display: none">
                                            <option value="1">1</option>
                                            <option value="2" selected="selected">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Children3" class="text-left a-white" style="display: none;">Child:</span>
                                        <select id="Select_Children3" class="form-control" style="display: none">
                                            <option selected="selected" value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildFirst3" class="text-left a-white" style="display: none;">Age of Child 1:</span>
                                        <select id="Select_AgeChildFirst3" class="form-control" style="display: none">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildSecond3" class="text-left a-white" style="display: none;">Age of Child 2:</span>
                                        <select id="Select_AgeChildSecond3" class="form-control" style="display: none;">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_RoomType4" class="text-left a-white" style="display: none;">Room Type:</span>
                                        <input type="text" id="spn_RoomType4" class="form-control" value="Room 4" disabled="disabled" style="display: none; cursor: default;" />
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Adults4" class="text-left a-white" style="display: none;">Adults:</span>
                                        <select id="Select_Adults4" class="form-control" style="display: none">
                                            <option value="1">1</option>
                                            <option value="2" selected="selected">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_Children4" class="text-left a-white" style="display: none;">Child:</span>
                                        <select id="Select_Children4" class="form-control" style="display: none">
                                            <option selected="selected" value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildFirst4" class="text-left a-white" style="display: none;">Age of Child 1:</span>
                                        <select id="Select_AgeChildFirst4" class="form-control" style="display: none">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <span id="Span_AgeChildSecond4" class="text-left a-white" style="display: none;">Age of Child 2:</span>
                                        <select id="Select_AgeChildSecond4" class="form-control" style="display: none;">
                                            <option selected="selected" value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="float: right">
                                    <button type="button" class="button btn-medium sky-blue1" onclick="Redirect();" id="btn_Search">SEARCH</button>
                                </div>
                                <br />
                                <br />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        var tpj = jQuery;
        $(document).ready(function () {
            // GetPackageCity();
            //.....................................................................................................//
            tpj("#txtCity").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {

                    $('#hdnDCode').val(ui.item.id);
                }
            });
            tpj("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                }
            });
        });
    </script>
    <script type="text/javascript">
        var tpj = jQuery;
        var chkinDate;
        var chkinMonth;
        var chkoutDate;
        var chkoutMonth;
        tpj.noConflict();

        tpj(document).ready(function () {

            if (tpj.fn.cssOriginal != undefined)
                tpj.fn.css = tpj.fn.cssOriginal;
            tpj("#datepicker_Departure").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",


                minDate: "dateToday",

            });
            tpj("#datepicker_Return").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
            });
            tpj("#datepicker_HotelCheckin").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
            tpj("#datepicker_HotelCheckin").datepicker("setDate", new Date());
            var dateObject = tpj("#datepicker_HotelCheckin").datepicker('getDate', '+1d');
            dateObject.setDate(dateObject.getDate() + 1);
            tpj("#datepicker_HotelCheckout").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d-m-yy",
                beforeShow: insertDepartureDate,
                onSelect: insertDays,
                //dateFormat: "yy-m-d",
                minDate: "+1D",
                //maxDate: "+3M +20D"
            });
            tpj("#datepicker_HotelCheckout").datepicker("setDate", dateObject);
            Addyear();
            tpj('#datepicker_HotelCheckin').change(function () {
                var date2 = tpj('#datepicker_HotelCheckin').datepicker('getDate', '+1d');
                chkinDate = date2.getDate();
                //chkinMonth = date2.getMonth();
                date2.setDate(date2.getDate() + 1);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', date2);
                tpj('#Select_TotalNights').val("1");
            });
            tpj('#Select_TotalNights').change(function () {
                manage_date();
                //    tpj('#datepicker_HotelCheckout').datepicker('setDate', '+' + Number(Number(chkinDate) + Number(tpj('#Select_TotalNights').val())) + 'd')
            });
            //....................................................................................................//

            function insertDays() {
                var arrival_date = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var departure_date = new Date(tpj('#datepicker_HotelCheckout').datepicker("getDate"));
                days = (departure_date.getTime() - arrival_date.getTime()) / (1000 * 60 * 60 * 24);
                if (days > 30) {
                    alert("You are allowed to book for a maximum 30 Nights");
                    days = tpj('#Select_TotalNights').val();
                    days = parseInt(days);
                    var departure_date = new Date(arrival_date.getFullYear(), arrival_date.getMonth(), arrival_date.getDate() + days);
                    tpj('#datepicker_HotelCheckout').datepicker('setDate', departure_date);
                    tpj('#Select_TotalNights').val(days);
                    //document.getElementById("sel_days").value = days;
                    return false;
                }
                tpj('#Select_TotalNights').val(days);
                //document.getElementById("sel_days").value = days;
            }

            function manage_date() {
                days = tpj('#Select_TotalNights').val();
                days = parseInt(days);
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);
                insertDepartureDate('', '', '');

            }

            function insertDepartureDate(value, date, inst) {

                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                var dateAdjust = tpj('#Select_TotalNights').val();
                var current_date = new Date();

                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                if (days < 0) {
                    var add_day = 1;
                } else {
                    var add_day = 2;
                }
                days = parseInt(days);
                tpj('#datepicker_HotelCheckout').datepicker("option", "minDate", days + add_day);
                tpj('#datepicker_HotelCheckout').datepicker("option", "maxDate", days + 365);
                dateAdjust = parseInt(dateAdjust);
                var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
                tpj('#datepicker_HotelCheckout').datepicker('setDate', secondDate);

            }

            function Addyear() {
                var current_date = new Date();
                var firstDate = new Date(tpj('#datepicker_HotelCheckin').datepicker("getDate"));
                current_time = current_date.getTime();
                days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
                days = parseInt(days);
                var i = 0
                tpj('#datepicker_HotelCheckin').datepicker("option", "minDate", days + i);
                tpj('#datepicker_HotelCheckin').datepicker("option", "maxDate", days + 365);
            }
        });
    </script>
    <style>
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            overflow-x: hidden;
            cursor: pointer;
        }

        .ui-menu-item {
            border-bottom: 1px dotted;
            border-bottom-color: #333;
        }

        .ui-corner-all {
            font-size: 13px;
        }
        input.input-text, select, textarea, span.custom-select {
            background: #f5f5f5;
        }
    </style>
</asp:Content>
