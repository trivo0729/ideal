﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookingConfirm.aspx.cs" Inherits="CUTUK.BookingConfirm" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Cheap Air Tickets, Flight Ticket Booking of Domestic Flights at Lowest Airfare: Ideal Tour and Travels</title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="scripts/ConfirmationDetails.js"></script>
    <script src="scripts/Invoice.js"></script>
    <script src="scripts/CountryCityCode.js"></script>
    <script src="scripts/LoginAndRegister.js"></script>
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top style4">
            <div class="container">
                <h1 class="logo navbar-brand">
                    <a href="Default.aspx" title="Vacaay.com - home">
                        <img src="images/logo.png" alt="logo" style="width: 306px; height: 80px;" />
                    </a>
                </h1>
                <div class="pull-right hidden-mobile">
                    <ul class="social-icons clearfix pull-right hidden-mobile" style="margin-top: 30px; margin-left: 10px;">
                        <li class="twitter"><a title="" href="https://twitter.com/travels_ideal" target="_blank" data-toggle="tooltip" data-original-title="twitter"><i class="soap-icon-twitter"></i></a></li>
                        <li class="facebook"><a title="" href="https://www.facebook.com/IdealToursandTravels" target="_blank" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                        <li class="instagram"><a title="" href="https://www.instagram.com/ideal_jungle_safaris/" target="_blank" data-toggle="tooltip" data-original-title="instagram"><i class="soap-icon-instagram"></i></a></li>
                        <li class="instagram"><a title="" href="https://www.instagram.com/uniglobe_idealtours_nagpur/" target="_blank" data-toggle="tooltip" data-original-title="instagram"><i class="soap-icon-instagram"></i></a></li>
                        <li class="linkedin"><a title="" href="https://www.linkedin.com/company/uniglobe-ideal-tours-and-travels" target="_blank" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                    </ul>
                    <span class="user-logged">
                        <a href="#" class="a-white"><i class="fa fa-phone ieffect"></i>+91 992 375 0110</a>&nbsp;&nbsp;
                    <a href="#" class="a-white"><i class="fa fa-envelope ieffect"></i>idealtoursandtravels@gmail.com</a>
                    </span>
                </div>
            </div>
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
            </a>
            
            <div class="main-navigation">
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children active">
                                <a href="Default.aspx" class="a-white"><i class="soap-icon-address"></i>Home</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="AboutUs.aspx" class="a-white"><i class="soap-icon-notice"></i>About Us</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a style="cursor: pointer" class="a-white"><i class="soap-icon-suitcase"></i>Holidays</a>
                                <ul>
                                    <li><a href="Packages.aspx?Type=Domestic" class="a-white"><i class="soap-icon-suitcase"></i>Holidays in India</a></li>
                                    <li><a href="Packages.aspx?Type=International" class="a-white"><i class="soap-icon-suitcase"></i>International Holidays</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="FlightSearch.aspx" class="a-white"><i class="soap-icon-plane-right takeoff-effect"></i>Flight</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="HotelSearch.aspx" class="a-white"><i class="soap-icon-hotel-3"></i>Hotel</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="Contact.aspx" class="a-white"><i class="soap-icon-phone"></i>Contact Us</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="Payment.aspx" class="a-white"><i class="fa fa-money" aria-hidden="true"></i>Payment</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a style="cursor: pointer" class="a-white account"><i class="soap-icon-user"></i>My account</a>
                                <ul>
                                    <li><a style="cursor: pointer" class="a-white logReg" onclick="SignUpModal()">Login / Register</a></li>
                                    <li><a style="cursor: pointer" onclick="Mybookings()" class="a-white">My Bookings</a></li>
                                    <li><a style="cursor: pointer; display: none" onclick="Logout()" class="a-white Out">Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children a-white">
                        <a href="Default.aspx" class="a-white">Home</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#" class="a-white">About Us</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-1"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Packages.aspx?Type=Domestic" class="a-white"><i class="soap-icon-star"></i>Holidays in India</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Packages.aspx?Type=International" class="a-white"><i class="soap-icon-star"></i>International Holidays</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="FlightSearch.aspx" class="a-white">Flight</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-3"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="HotelSearch.aspx" class="a-white">Hotel</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-4"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#" class="a-white">Contact Us</a>
                        <button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-5"></button>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="Payment.aspx" class="a-white"><i class="fa fa-money" aria-hidden="true"></i>Payment</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a style="cursor: pointer" class="a-white account"><i class="soap-icon-user"></i>My account</a>
                        <ul>
                            <li><a style="cursor: pointer" class="a-white logReg" onclick="SignUpModal()">Login / Register</a></li>
                            <li><a style="cursor: pointer" onclick="Mybookings()" class="a-white">My Bookings</a></li>
                            <li><a style="cursor: pointer; display: none" onclick="Logout()" class="a-white Out">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </header>

        <section id="content" class="gray-area">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                        <div class="booking-information travelo-box">
                            <h2>Booking Confirmation</h2>
                            <hr />
                            <div class="booking-confirmation clearfix">
                                <i class="soap-icon-recommend icon circle"></i>
                                <div class="message">
                                    <h4 class="main-message" id="spComplete"></h4>
                                    <br />
                                    <h5 id="spAck"></h5>
                                    <p>A confirmation email has been sent to your provided email address.</p>
                                    <br />
                                </div>
                            </div>
                            <hr />
                            <h2>Booking Details</h2>
                            <dl class="term-description" id="bookingdetails">
                            </dl>
                            <hr />
                        </div>
                    </div>
                    <div class="sidebar col-sm-4 col-md-3">
                        <div class="travelo-box contact-box">
                            <h4>Need Ideal Travels Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i>+91 992 375 0110</span><br />
                                <span class="contact-phone"><i class="soap-icon-message"></i>info@uniglobeidealtours.in</span>
                                <br />
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>ABOUT US</h2>
                            <p class="p-white">
                                Ideal Tours and Travels had a modest start as a ticketing company in 2008 and gradually spread its wings to develop a huge client base of corporates and others dealing with everything related to tra...
                                <button type="button" class="full-width">READ MORE</button>
                            </p>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>POPULAR TOURS</h2>
                            <ul class="discover triangle hover row a-white">
                                <li class="col-xs-12"><a href="#">Romantic France</a></li>
                                <li class="col-xs-12"><a href="#">Wonderful Lodnon</a></li>
                                <li class="col-xs-12"><a href="#">Awesome Amsterdam</a></li>
                                <li class="col-xs-12"><a href="#">Wild Africa</a></li>
                                <li class="col-xs-12"><a href="#">Beach Goa</a></li>
                                <li class="col-xs-12"><a href="#">Casino Los Vages</a></li>
                                <li class="col-xs-12"><a href="#">Romantic France</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>OUR SERVICES</h2>
                            <ul class="discover triangle hover row a-white">
                                <li class="col-xs-12"><a href="#">Domestic Flights</a></li>
                                <li class="col-xs-12"><a href="#">International Flights</a></li>
                                <li class="col-xs-12"><a href="#">Tours &amp; Holidays</a></li>
                                <li class="col-xs-12"><a href="#">Domestic Hotels</a></li>
                                <li class="col-xs-12"><a href="#">International Hotels</a></li>
                                <li class="col-xs-12"><a href="#">Cruise Holidays</a></li>
                                <li class="col-xs-12"><a href="#">Car Rental</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>CONTACT US</h2>
                            <p class="p-white"><i class="soap-icon-address circle"></i>&nbsp;Golden Palace, SS8, W High Ct Rd, Near Sudhama Theatre, Dharampeth, Nagpur, Maharashtra 440010</p>
                            <p class="p-white"><i class="soap-icon-phone circle"></i><a href="#">+91 1800-00-0000</a></p>
                            <p class="p-white"><i class="soap-icon-message circle"></i><a href="mailto:info@uniglobeidealtours.in">info@uniglobeidealtours.in</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="bottom gray-area">
            <div class="container">
                <div class="copyright pull-left">
                    <p class="p-white">® All Rights Reserved © Copyright 2019 IDEAL TOUR & TRAVELS.</p>
                </div>
                <div class="pull-right">
                    <a id="back-to-top" href="#" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p class="p-white">Concept by Ideal Tours, Developed Trivo IT Solutions.</p>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade bs-example-modal-lg" id="PreviewModalRegisteration" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #fff">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="tab-container style1">
                                <ul class="tabs">
                                    <li class="active"><a href="#Login" data-toggle="tab" aria-expanded="true">Login</a></li>
                                    <li class=""><a href="#Register" data-toggle="tab" aria-expanded="false">Register</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Login">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="size13 dark">Username / Email : </span>
                                                <input type="text" id="txtUserName" data-trigger="focus" data-placement="top" placeholder="Username" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtUserName">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="size13 dark">Password : </span>
                                                <input type="password" id="txtPassword" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your B2C_Id" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassword">
                                                    <b>* This field is required</b></label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="block" style="float: right">
                                            <input type="button" onclick="LoginCustomer()" class="button btn-small sky-blue1" value="Login" />
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Register">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="size13 dark">User Name : </span>
                                                <input type="text" id="txt_UserName" data-trigger="focus" data-placement="top" placeholder="User Name" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your User Name" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_UserName">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Password: </span>
                                                <input type="password" id="txt_Password" data-trigger="focus" data-placement="top" placeholder="Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your Password" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Password">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Confirm Password: </span>
                                                <input type="password" id="txt_ConfirmPassword" data-trigger="focus" data-placement="top" placeholder="Confirm Password" class="form-control" data-content="This field is mandatory" data-original-title="Here you need to confirm your Password" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmPassword">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <br />
                                                <span class="size13 dark">Mobile : </span>
                                                <input type="text" id="txt_Mobile" data-trigger="focus" data-placement="top" placeholder="Mobile" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit your mobile number" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Mobile">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <br />
                                                <span class="size13 dark">Phone : </span>
                                                <input type="text" id="txt_Phone" data-trigger="focus" data-toggle="popover" data-placement="top" placeholder="Phone" class="form-control" data-content="This field is mandatory" data-original-title="Here you can create your Phone" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Phone">
                                                    <b>* This field is required</b></label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="size13 dark">Address : </span>
                                                <input type="text" id="txt_Address" data-trigger="focus" data-placement="top" placeholder="Address" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit address" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                                    <b>* This field is required</b></label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <span class="size13 dark">Country : </span>
                                                <select id="selCountry" class="form-control">
                                                    <option selected="selected" value="-">Select Any Country</option>
                                                </select>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">City : </span>
                                                <select id="selCity" class="form-control">
                                                    <option selected="selected" value="-">Select Any City</option>
                                                </select>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                                    <b>* This field is required</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="size13 dark">Pin Code : </span>
                                                <input type="text" id="txt_PinCode" data-trigger="focus" data-placement="top" placeholder="Pin Code" class="form-control" data-content="This field is mandatory" data-original-title="Here you can edit pin code" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                                    <b>* This field is required</b></label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="block" style="float: right">
                                            <input type="button" id="btn_RegiterAgent" value="Register" class="button btn-small sky-blue1" onclick="return ValidateRegistration()" />
                                            <input type="button" id="btn_Cancel" value="Cancel" class="button btn-small orange" onclick="ClearRegistration();" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script type="text/javascript" src="js/calendar.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">

    </script>
</body>
</html>
