﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="flight-booking.aspx.cs" Inherits="CUTUK.flight_booking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Travelo - Travel, Tour Booking HTML5 Template</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />

    <!-- Main Style -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->



</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">

            <div class="main-header">

                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="#" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>


                </div>
            </div>

            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="#">MY ACCOUNT</a></li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                    </ul>
                </div>
            </div>

            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">
                                    Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator">
                    <label>OR</label>
                </div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox">
                                Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Flight Booking</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">HOME</a></li>
                    <li class="active">Flight Booking</li>
                </ul>
            </div>
        </div>
        <section id="content" class="gray-area">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sms-6 col-sm-8 col-md-8">
                        <div class="booking-section travelo-box">

                            <form class="booking-form">
                                <div class="person-information">
                                    <h2>Your Personal Information</h2>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label>first name</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                        <div class="col-sm-6 col-md-5">
                                            <label>last name</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label>email address</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                        <div class="col-sm-6 col-md-5">
                                            <label>Verify E-mail Address</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label>Country code</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>United Kingdom (+44)</option>
                                                    <option>United States (+1)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-5">
                                            <label>Phone number</label>
                                            <input type="text" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label>Meal preference <small>(optional)</small></label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>United Kingdom (+44)</option>
                                                    <option>United States (+1)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <div class="card-information">
                                    <h2>Would you please give more details?</h2>


                                    <p>Please provide some additional information about you  </p>

                                    <br />
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label>first name</label>
                                            <input type="text" name="firstname" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Contact number</label>
                                            <input type="text" name="phone" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                        <div class="col-md-4">
                                            <label>email address</label>
                                            <input type="text" name="email" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Remark</label>
                                            <input type="text" name="card_number" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <br />
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">
                                            I accept the cancellation policy<a href="#"><span class="skin-color">Terms and Conditions</span></a>.
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="sidebar col-sms-6 col-sm-4 col-md-4">
                        <div class="booking-details travelo-box" id="Div_Trip">
                            <div class="tab-container style1">
                                <ul class="tabs full-width">
                                    <li class="active"><a href="#unlimited-layouts" data-toggle="tab" aria-expanded="true">Unlimited Layouts</a></li>
                                    <li class=""><a href="#design-inovation" data-toggle="tab" aria-expanded="false">Design Inovation</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="unlimited-layouts">
                                        <h4>Trip Summary</h4>
                                        <hr>
                                        <article class="flight-booking-details">
                                            <figure class="clearfix">
                                                <a title="" href="#" class="middle-block" style="position: relative;">
                                                    <img class="middle-item" alt="" src="images/AirlineLogo/SG.gif" style="position: absolute; top: 50%; margin-top: -37.5px; left: 50%; margin-left: -37.5px;">
                                                </a>
                                                <div class="travel-title">
                                                    <h5 class="box-title">Mumbai,India To Delhi,India<br>
                                                        <br>
                                                        <small>SpiceJet (SG)</small></h5>
                                                </div>
                                            </figure>
                                            <div class="details">
                                                <div class="constant-column-3 timing clearfix">
                                                    <div class="check-in">
                                                        <label>Departure</label>
                                                        <span>Thu, Aug 30, 2018 9:30 PM</span>
                                                    </div>
                                                    <div class="duration text-center"><i class="soap-icon-clock"></i><span>undefined min</span>                               </div>
                                                    <div class="check-out">
                                                        <label>Arrival </label>
                                                        <span>Thu, Aug 30, 2018 9:30 PM</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row" style="margin-bottom: 0px">
                                                            <div class="col-sm-8 col-md-8 toggle-container box" style="margin-bottom: 0px">
                                                                <div class="panel style1">
                                                                    <h6 class="panel-title"><a class="" href="#tgg_00" data-toggle="collapse" aria-expanded="true">Traveler 1  (1) Adults </a></h6>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-md-4" style="text-align: end">
                                                                <h5>
                                                                    <br>
                                                                    <span data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="PKR=16,662.50
USD=135.37
GBP=106.12
EUR=118.07
SAR=507.67
AED=497.12
"><i class="fa fa-inr"></i>9,331.00</span><br>
                                                                </h5>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row panel-collapse collapse in" id="tgg_00" aria-expanded="true" style="margin-bottom: 0px;">
                                                            <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">
                                                                Flight<br>
                                                            </div>
                                                            <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">
                                                                <span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="PKR=0.00
USD=0.00
GBP=0.00
EUR=0.00
SAR=0.00
AED=0.00
"><i class="fa fa-inr"></i>0.00</span><br>
                                                            </div>
                                                            <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">
                                                                Taxes &amp; Fees<br>
                                                            </div>
                                                            <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">
                                                                <span style="color: #337ab7" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="PKR=0.00
USD=0.00
GBP=0.00
EUR=0.00
SAR=0.00
AED=0.00
"><i class="fa fa-inr"></i>0.00</span><br>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group row">
                                                            <br>
                                                            <div class="col-sm-6 col-md-6"><span style="font-size: 15px"><b>Trip Total:</b></span>                                       </div>
                                                            <div class="col-sm-6 col-md-6">
                                                                <span class="price" data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="PKR=16,662.50
USD=135.37
GBP=106.12
EUR=118.07
SAR=507.67
AED=497.12
"><i class="fa fa-inr"></i>9,331.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br />

                        <div class="travelo-box contact-box">
                            <h4>Need Travelo Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i>+91 9910084810 </span>
                                <br>
                                <span class="contact-phone"><i class="soap-icon-phone"></i>0562 -22334810</span><br>
                                <span class="contact-phone"><i class="soap-icon-message"></i>info@Vacaaay.com</span>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer">

            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="#" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2018 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>

    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script type="text/javascript" src="js/calendar.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <script type="text/javascript">
    
    </script>
</body>
</html>
