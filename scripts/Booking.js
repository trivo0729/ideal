﻿//$(document).ready(function () {
//    RandomNo = getRandomInt(10000, 99999);
//    InvoiceNO();
//});

//function InvoiceNO() {
//    rnd = 'INVC-' + RandomNo;
//    $("#order_id").val(rnd);
//}

//function getRandomInt(min, max) {
//    return Math.floor(Math.random() * (max - min + 1)) + min;
//}

function BookingDetails() {
    var getCancellation1 = $("#SpnCanPolicy1").text();
    var getCancellation2 = $("#SpnCanPolicy2").text();
    //var Amount = PackageAmount.toFixed(2);
    //ServiceTax = (((parseFloat(Amount) + parseFloat(AgentMarkup)) * ServiceTaxPer) / 100).toFixed(2);
    //TotalAmount = ((parseFloat(Amount) + parseFloat(ServiceTax))).toFixed(2);
    $("#dspHotelName").text(HotelName);
    $("#dspCheckIn").text(HotelDetails.Checkin);
    $("#dspCheckOut").text(HotelDetails.CheckOut);
    if ($('#Supplier').val() != 'E') {
        Room = RoomsDetails.length;
        $("#dspCheckIn").text(HotelDetails.Checkin);
        $("#dspCheckOut").text(HotelDetails.CheckOut);
    }
    $("#dspRooms").text(Room);
    $("#dspNights").text(HotelDetails.NoDays);

    $("#dspHotelNameC").text(HotelName);
    $("#dspCheckInC").text(HotelDetails.Checkin);
    $("#dspCheckOutC").text(HotelDetails.CheckOut);
    $("#dspRoomsC").text(RoomsDetails.length);
    $("#dspNightsC").text(HotelDetails.NoDays);

    $("#dspHotelNameB").text(HotelName);
    $("#dspCheckInB").text(HotelDetails.Checkin);
    $("#dspCheckOutB").text(HotelDetails.CheckOut);
    $("#dspRoomsB").text(HotelDetails.length);
    $("#dspNightsB").text(HotelDetails.NoDays);

    //$("#dspAgencyRef").text();

    $("#tblPassengerDetails tr").remove();
    $("#tblPassengerDetailsC tr").remove();
    $("#tblPassengerDetailsB tr").remove();
    var tr = '<tr class="bold"><td>Sr. No.</td><td>Name</td><td>Gender</td><td>Room</td></tr>';
    var li = ''
    var code = $('#hdnHotelRoomID').val();
    var k = 0, SrNo = 1;
    var x = 0;
    Room = getParameterByName('id').split(',')
    var Html = ''
    for (var i = 0; i < Room.length; i++) {
        $('#roomPax_' + (i + 1)).empty();
        li = '';
        var c = 1;
        var Bed = $("#SelBedType_" + (i + 1)).val()
        var Smoking = $("#SelSmoking_" + (i + 1)).val()
        var Note = $("#Note_" + (i + 1)).val()
        var GuestID;
        k = 1;
        GuestID = Room[i];
        var j = 1;
        SrNo = (i + 1)
        $('#tblTravelers' + (i + 1) + ' div').each(function () {

            if ($(this).attr('name') == 'adult_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val();
                var FName = $(this).find("#txtFName_" + (i + 1) + "_" + j).val().replace(' ', '');
                var LName = $(this).find("#txtLName_" + (i + 1) + "_" + j).val();
                // var Room = i + 1;
                if (title == "Mrs" || title == "Miss")
                    li += '<li>' + title + ' ' + FName + ' ' + LName + '</li>';
                else if (title == "Mr")
                    li += '<li>' + title + ' ' + FName + ' ' + LName + '</li>';
                j = j + 1;
                SrNo = j + 1;
            }
            else if ($(this).attr('name') == 'child_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelCGender_" + (i + 1) + "_" + k).val();
                var Name = $(this).find("#txtCName_" + (i + 1) + "_" + k).val();
                if (title == "Miss")
                    li += '<li>' + title + ' ' + Name + '</li>';
                else if (title == "Master")
                    li += '<li>' + title + ' ' + Name + '</li>';
                j = j + 1;
                k++;
                SrNo = j + 1;
            }


        });
        $("#spn_Bedding" + (i + 1)).text(Bed)
        $("#spn_Smooking" + (i + 1)).text(Smoking)
        $("#spn_Remark" + (i + 1)).text(Note)
        $('#roomPax_' + (i + 1)).append(li);

    }
    $("#tblPassengerDetails").append(tr);
    $("#ConfirmModal").modal('show');
}


function ProceedConfirmBooking() {
    var Guest = new Array();
    var code = $('#hdnHotelRoomID').val();
    var HoldTime = $('#hdnholdtime').val();
    var Array_HotelID = code.split('|');
    var k = 0;
    var x = 0;
    Room = getParameterByName('id').split(',')
    var Html = ''
    for (var i = 0; i < Room.length; i++) {
        $('#roomPax_' + (i + 1)).empty();
        li = '';
        var c = 1;
        var Bed = $("#SelBedType_" + (i + 1)).val()
        var Smoking = $("#SelSmoking_" + (i + 1)).val()
        var Note = $("#Note_" + (i + 1)).val()
        var GuestID;
        k = 1;
        GuestID = Room[i];
        var Customer = new Array();
        var j = 1;
        SrNo = (i + 1)
        $('#tblTravelers' + (i + 1) + ' div').each(function () {

            if ($(this).attr('name') == 'adult_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val();
                var objCustomer = {
                    Age: 0,//$(this).find("input[name='txtAge']").val(),
                    type: "AD",
                    Name: title + ' ' + $(this).find("#txtFName_" + (i + 1) + "_" + j).val(),
                    LastName: $(this).find("#txtLName_" + (i + 1) + "_" + j).val()
                }
                
                Customer.push(objCustomer);
                j = j + 1;
                SrNo = j + 1;
            }
            else if ($(this).attr('name') == 'child_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelCGender_" + (i + 1) + "_" + c).val();
                var objCustomer = {
                    Age: ($(this).find("#txtCAge_" + (i + 1) + "_" + c).val() != "") ? $(this).find("#txtCAge_" + (i + 1) + "_" + c).val() : 0,
                    type: "CH",
                    Name: title + ' ' + $(this).find("#txtCName_" + (i + 1) + "_" + c).val(),
                    LastName: ""
                }
                Customer.push(objCustomer);
                c++;
                j = j + 1;
                k++;
                SrNo = j + 1;
            }


        });
        $("#spn_Bedding" + (i + 1)).text(Bed)
        $("#spn_Smooking" + (i + 1)).text(Smoking)
        $("#spn_Remark" + (i + 1)).text(Note)
        $('#roomPax_' + (i + 1)).append(li);
        var objGuest = {
            ID: GuestID,
            RoomNumber: (i + 1),
            sCustomer: Customer
        };
        Guest.push(objGuest);
    }
    var data;
    var Supplier = $("#Supplier").val();
    var HotelCode = getParameterByName('code')
    var RoomDescriptionId = getParameterByName('roomdescriptionId').split(',')
    var _Price = getParameterByName('RoomP')
    var CutID = getParameterByName('CutID')
    _Price = _Price.replace(/ /g, '+');
    var _noRooms = getParameterByName('_noRooms').split(',')
    data = {
        HotelCode: HotelCode,
        CutID: CutID,
        RoomID: Room,
        AgencyRefernce: $("#txt_AgentBookingreference").val(),
        MobileNo: $("#txt_CustomerMobileNumber").val(),
        Remark: $("#txt_BookingRemarks").val(),
        BookingStatus: "Vouchered",
        Email: $("#txt_AgentEmail").val(),
        List_Guest: Guest,
        ServiceTax: 0,
        Supplier: Supplier,
        RoomDescriptionId: RoomDescriptionId,
        HoldTime: HoldTime,
        _Price: _Price,
        _noRooms: _noRooms,
        ConfirmationId: $("#ConfirmationId").val(),

    }
    var param = JSON.stringify(data);

    var ConfId = $("#ConfirmationId").val();

    // window.location.href = 'waitforbooking.aspx?data=' + param;;

    if (($("#chkTermsAndCondition").is(":checked"))) {
        window.location.href = 'WaitBooking.aspx?Id=' + ConfId;
        //$("#Btn_CCAvenue").click();
    }
    //if ($("#chkTermsAndConditionCard").is(":checked")) {
    //    
    //    //WaitforbookingCard(param)
    //    window.location.href = 'waitforbooking.aspx?data=' + param;
    //}
    //if ($("#chkTermsAndConditionBank").is(":checked")) {
    //    window.location.href = 'waitforbooking.aspx?data=' + param;
    //}
}


function ConfirmationData() {
    var Guest = new Array();
    var code = $('#hdnHotelRoomID').val();
    var HoldTime = $('#hdnholdtime').val();
    var Array_HotelID = code.split('|');
    var k = 0;
    var x = 0;
    Room = getParameterByName('id').split(',')
    var Html = ''
    for (var i = 0; i < Room.length; i++) {
        var c = 1;
        $('#roomPax_' + (i + 1)).empty();
        li = '';
        var Bed = $("#SelBedType_" + (i + 1)).val()
        var Smoking = $("#SelSmoking_" + (i + 1)).val()
        var Note = $("#Note_" + (i + 1)).val()
        var GuestID;
        k = 1;
        GuestID = Room[i];
        var Customer = new Array();
        var j = 1;
        SrNo = (i + 1)
        $('#tblTravelers' + (i + 1) + ' div').each(function () {

            if ($(this).attr('name') == 'adult_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val();
                var objCustomer = {
                    Age: 0,//$(this).find("input[name='txtAge']").val(),
                    type: "AD",
                    Name: title + ' ' + $(this).find("#txtFName_" + (i + 1) + "_" + j).val(),
                    LastName: $(this).find("#txtLName_" + (i + 1) + "_" + j).val()
                }
                
                Customer.push(objCustomer);
                j = j + 1;
                SrNo = j + 1;
            }
            else if ($(this).attr('name') == 'child_' + GuestID + "_" + (i + 1)) {
                var title = $(this).find("#SelCGender_" + (i + 1) + "_" + c).val();
                var objCustomer = {
                    Age: ($(this).find("#txtCAge_" + (i + 1) + "_" + c).val() != "") ? $(this).find("#txtCAge_" + (i + 1) + "_" + c).val() : 0,
                    type: "CH",
                    Name: title + ' ' + $(this).find("#txtCName_" + (i + 1) + "_" + c).val(),
                    LastName: ""
                }
                Customer.push(objCustomer);
                k++;
                c++;
            }


        });
        $("#spn_Bedding" + (i + 1)).text(Bed)
        $("#spn_Smooking" + (i + 1)).text(Smoking)
        $("#spn_Remark" + (i + 1)).text(Note)
        $('#roomPax_' + (i + 1)).append(li);
        var objGuest = {
            ID: GuestID,
            RoomNumber: (i + 1),
            sCustomer: Customer
        };
        Guest.push(objGuest);
    }
    var data;
    var Supplier = $("#Supplier").val();
    var HotelCode = getParameterByName('code')
    var RoomDescriptionId = getParameterByName('roomdescriptionId').split(',')
    var _Price = getParameterByName('RoomP')
    var CutID = getParameterByName('CutID')
    _Price = _Price.replace(/ /g, '+');
    var _noRooms = getParameterByName('_noRooms').split(',')
    data = {
        HotelCode: HotelCode,
        CutID: CutID,
        RoomID: Room,
        AgencyRefernce: $("#txt_AgentBookingreference").val(),
        MobileNo: $("#txt_CustomerMobileNumber").val(),
        Remark: $("#txt_BookingRemarks").val(),
        BookingStatus: $("#hndBookingStatus").val(),
        Email: $("#txt_AgentEmail").val(),
        List_Guest: Guest,
        ServiceTax: 0,
        Supplier: Supplier,
        RoomDescriptionId: RoomDescriptionId,
        HoldTime: HoldTime,
        _Price: _Price,
        _noRooms: _noRooms,
        Price: ChkCurrent,
    }
    var param = JSON.stringify(data);
    return param;
}
