﻿var hiddensid;

function EditMarkupModal(sid) {
    //var buttonvalue = $('#btn_Markup').text();
    $('#lbl_ErrMarkup').css("display", "none");
    $('#lbl_ErrMarkup').html("* This field is required");
    hiddensid = sid;
    //if (buttonvalue == 'Update Markup') {
        $.ajax({
            type: "POST",
            url: "../DefaultHandler.asmx/GetAgentMarkup",
            data: '{"sid":"' + hiddensid + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session != 1) {
                    alert("Session Expired");
                }
                if (result.retCode == 1) {
                    var List_MarkupDetails = result.MarkupPercent;
                    $('#txt_Markup').val(List_MarkupDetails[0].MarkupPer);
                }
                if (result.retCode == 0) {
                    alert("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
            }
        });
    //}
    //else {
    //    $('#spn_ErrMarkupDefault').css("display", " ");
    //}
}

//function UpdateMarkup() {

//    $('#lbl_ErrMarkup').css("display", "none");
//    $('#lbl_ErrMarkup').html("* This field is required");
//    var reg = new RegExp('[1-9\.]+.*');
//    var regN = new RegExp('.*[a-zA-Z]+.*');
//    var bvalid = true;
//    var GroupName = $('#selGroup option:selected').text();
//    var GroupID = $('#selGroup option:selected').val();
//    var UserType = $('#hdn_Usertype').val();
//    var buttonvalue = $('#btn_Markup').text();
//    var MarkupPer = $('#txt_Markup').val();
//    if (MarkupPer == "") {
//        bvalid = false;
//        $('#lbl_ErrMarkup').css("display", "");
//    }
//    if (MarkupPer != "" && regN.test(MarkupPer)) {
//        bvalid = false;
//        $('#lbl_ErrMarkup').html("*Markup must be numeric");
//        $('#lbl_ErrMarkup').css("display", "");
//    }
//    if (bvalid == true) {
//        if (buttonvalue == 'Add Markup') {
//            if (confirm("Are you sure you want to Add Markup?") == true) {
//                $.ajax({
//                    type: "POST",
//                    url: "../DefaultHandler.asmx/AddUpdateAgentMarkup",
//                    data: '{"sid":"' + hiddensid + '","MarkupPer":"' + MarkupPer + '","GroupId":"' + GroupID + '","GroupName":"' + GroupName + '","Usertype":"'+UserType+'","sbuttonvalue":"' + buttonvalue + '"}',
//                    contentType: "application/json; charset=utf-8",
//                    datatype: "json",
//                    success: function (response) {
//                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                        if (result.Session != 1) {
//                            alert("Session Expired");

//                        }
//                        if (result.retCode == 1) {
//                            alert("Markup Percent updated successfully.");
//                            $("#MarkupModal").modal('hide');
//                            //$('#txt_Markup').val("");
//                        }
//                        if (result.retCode == 0) {
//                            alert("Something went wrong while processing your request! Please try again.");
//                        }
//                    },
//                    error: function () {
//                    }
//                });
//            }
//        }
//        else {
//            if (confirm("Are you sure you want to Update Markup?") == true) {
//                $.ajax({
//                    type: "POST",
//                    url: "../DefaultHandler.asmx/AddUpdateAgentMarkup",
//                    data: '{"sid":"' + hiddensid + '","MarkupPer":"' + MarkupPer + '","GroupId":"' + GroupID + '","GroupName":"' + GroupName + '","Usertype":"' + UserType + '","sbuttonvalue":"' + buttonvalue + '"}',
//                    contentType: "application/json; charset=utf-8",
//                    datatype: "json",
//                    success: function (response) {
//                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                        if (result.Session != 1) {
//                            alert("Session Expired");

//                        }
//                        if (result.retCode == 1) {
//                            alert("Markup Percent updated successfully.");
//                            $("#MarkupModal").modal('hide');
//                            //$('#txt_Markup').val("");
//                        }
//                        if (result.retCode == 0) {
//                            alert("Something went wrong while processing your request! Please try again.");
//                        }
//                    },
//                    error: function () {
//                    }
//                });
//            }
//        }
//    }
//}

function UpdateAgentMarkup() {
    $('#lbl_ErrMarkup').css("display", "none");
    $('#lbl_ErrMarkup').html("* This field is required");
    var reg = new RegExp('[1-9\.]+.*');
    var regN = new RegExp('.*[a-zA-Z]+.*');
    var bvalid = true;
    var MarkupPer = $('#txt_Markup').val();
    if (MarkupPer == "") {
        bvalid = false;
        $('#lbl_ErrMarkup').css("display", "");
    }
    if (MarkupPer != "" && regN.test(MarkupPer)) {
        bvalid = false;
        $('#lbl_ErrMarkup').html("*Markup must be numeric");
        $('#lbl_ErrMarkup').css("display", "");
    }
    if (bvalid == true) {
        if (confirm("Are you sure you want to Update Markup?") == true) {
            $.ajax({
                type: "POST",
                url: "../DefaultHandler.asmx/UpdateAgentMarkup",
                data: '{"sid":"' + hiddensid + '","MarkupPer":"' + MarkupPer + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session != 1) {
                        alert("Session Expired");

                    }
                    if (result.retCode == 1) {
                        alert("Markup Percent updated successfully.");
                        location.reload();
                        $("#MarkupModal").modal('hide');
                        $('#txt_Markup').val('');
                    }
                    if (result.retCode == 0) {
                        alert("Something went wrong while processing your request! Please try again.");
                    }
                },
                error: function () {
                }
            });
        }
    }
}