﻿var arrLocationCode = new Array();
var dLat, dLong;

function codeAddress(address) {
    geocoder = new google.maps.Geocoder();
    var AddNew = address;
    var address = AddNew.split(",");
    address = address[(address.length - 1)];
    address = address.trim();
    geocoder.geocode({ 'address': AddNew }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var Bounds = results[0].geometry.bounds;
            for (var i = 0; i < results[0].address_components.length; i++) {
                if (results[0].address_components[i].long_name == address) {
                    Restrict = { 'country': results[0].address_components[i].short_name };
                    AutoCompleteForPlace(Restrict, Bounds)
                }
            }
        }
    });
}

//function codeAddress(address) {
//    GetPlace();
//    var data = {
//        address: address.split(",")[0],
//    }
//    $.ajax({
//        type: "POST",
//        url: "../HotelHandler.asmx/GetPlace",
//        contentType: 'application/json',
//        data: JSON.stringify(data),
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//        },
//    });
//}

function AutoCompleteForPlace(Restrict, Bounds) {
    google.maps.event.addDomListener
    var places2 = new google.maps.places.Autocomplete(
        (document.getElementById('txtSource')),
        {
            icon:true,
            bounds: Bounds,
            strictBounds: true,
            componentRestrictions: Restrict
        });
    google.maps.event.addListener(places2, 'place_changed', function () {
        
        var place = places2.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var Placeid = place.place_id;
        $('#hdnlatitude').val(latitude);
        $('#hdnlongitude').val(longitude);
        $('#hdnnrbl').val(place.name);
        HotelFilter()
    });
}

function GetPlace() {
    var tpj = jQuery;
    tpj("#txt_Source").autocomplete({
        source: function (request, response) {
            tpj.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../HotelHandler.asmx/GetPlaceAuto",
                data: "{'Name':'" + document.getElementById('txt_Source').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            var Id = (ui.item.id).split("|");
            tpj('#hdnlatitude').val(Id[0]);
            tpj('#hdnlongitude').val(Id[1]);
            tpj('#hdnnrbl').val(ui.item.value);
            HotelFilter()
        }
    });
}

//','address':'" + address + "'
