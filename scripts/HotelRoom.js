﻿$(document).ready(function () {
    //$("#ConfirmModal").modal('show');
});


function numberWithCommas(x) {

    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;
}

function HotelRoom(objHotelCode, objRoomID, objSupplier, objRoomDescriptionId, objRoomPrice, _noRooms, CutCode) {
    var data = {
        HotelCode: objHotelCode,
        CutCode: CutCode,
        RoomID: objRoomID,
        Supplier: objSupplier,
        RoomDescriptionId: objRoomDescriptionId,
        RoomPrice: objRoomPrice,
        noRooms: _noRooms
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelRoom",
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $('.Hoteldetails').html(result.HotelDetails);
            $('#summary').html(result.HotelInfo);
            $('#roominfo').html(result.RoomInfo);
            $('#roomrates').html(result.roomrates);
            $('#maps').html(result.maps);
            $("#HotelDetailRight").html(result.BookedRate);
            $("#divhotelroom").attr("style", '');
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover({
                placement: 'top',
                trigger: 'hover'
            });
            $.getScript("../assets/js/initialize-carousel-detailspage.js", function () { });

        },
        error: function (xhr, error, statusCode) {
            $('#SpnMessege').text(xhr.responseText);
            $('#ModelMessege').modal('show')
            // alert(xhr.responseText);
        }
    });
}

function RedirectToHome() {

    window.location.href = "HotelSearch.aspx";
}

function ChangeRoomOption(HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier) {
    //if (Supplier!="DoTW") {

    //    window.location.href = 'HotelRoom.aspx?HotelCode=' + HotelCode + '&RoomCode=' + RoomID + '&Atr=' + Supplier;
    //    //window.location.href = 'hotelroom.aspx?hotelcode=' + HotelCode + '&roomcode=' + RoomID + '&Supplier=' + Supplier;
    //}
    if (Supplier == "C") {
        var data = {
            HotelCode: HotelCode,
            RoomID: RoomID,
            Supplier: Supplier,
            RoomDescriptionId: RoomDescriptionId,
            CUTRoomPrice: CUTRoomPrice
        }
        $.ajax({
            type: "POST",
            url: "../HotelHandler.asmx/CheckForRoomOccupancyChanged",
            contentType: 'application/json',
            data: JSON.stringify(data),
            datatype: "json",
            success: function (response) {
                var result = JSON.parse(response.d);
                if (result.retCode == 1) {
                    var Hotel_RoomType = result.Hotel_RoomType;
                    CheckForRoomOccupancyChanged(Hotel_RoomType, HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier);
                }
                else {
                    GetRoomDetails(HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier)
                }
            },
            error: function (xhr, error, statusCode) {
                alert(xhr.responseText);
            }
        });
    }
    else if (Supplier == "D") {
        GetRoomDetails(HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier)
    }
    else {
        window.location.href = 'hotelroom.aspx?hotelcode=' + HotelCode + '&roomcode=' + RoomID + '&roomdescriptionId=' + RoomDescriptionId + '&RoomP=' + CUTRoomPrice + '&Supplier=' + Supplier;
    }


}

function GetRoomDetails(HotelCode, CutCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier) {
    var data = {
        HotelCode: HotelCode,
        CutCode: CutCode,
        RoomID: RoomID,
        Supplier: Supplier,
        RoomDescriptionId: RoomDescriptionId,
        CUTRoomPrice: CUTRoomPrice
    }
    $.ajax({
        type: "POST",
        //url: "../HotelHandler.asmx/HotelFilter",
        url: "../HotelHandler.asmx/GetRoomDetails",
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d);
            if (result.retCode == 1) {
                if (result.CUTRoomPrice != undefined)
                    CUTRoomPrice = result.CUTRoomPrice;
                window.location.href = 'hotelroom.aspx?hotelcode=' + HotelCode + '&roomcode=' + RoomID + '&roomdescriptionId=' + RoomDescriptionId + '&RoomP=' + CUTRoomPrice + '&Supplier=' + Supplier;
            }
            else if (result.retCode == 2) {
                Success(result.ErrorMessage);
            }
                //else if (result.retCode == 0) {
                //    alert("Unable to block all rooms for this booking. Please retry!");
                //}
            else {
                ///alert("Unable to block all rooms for this booking. Please retry!");
                $("#AlertModal").modal("hide")
                //Success("This Room option currently Unavailable, Would you like to try another Room Option!");
                Ok("This Room option currently Unavailable, Would you like to try another Room Option!", "CheckOtherRooms", [HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, result.opt_RoomId, result.opt_RoomDesc, result.opt_Price]);

                //alert("This room cannot be booked. Please retry with another room option!");
            }
        },
        error: function (xhr, error, statusCode) {
            alert(xhr.responseText);
        }
    });
}

function CheckForRoomOccupancyChanged(Hotel_RoomType, HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier) {
    var tRow = 'Please note that during final booking, following occupancy change will take place..<br/><br/>';
    for (var i = 0; i < Hotel_RoomType.length; i++) {
        /////////////////////////
        if (Hotel_RoomType[i].validForOccupancy != null) {
            tRow += '<div class="row">';
            tRow += '<div class="col-md-6">';
            tRow += '<span class="text-left" style="color: #919191">Room ' + (i + 1) + '</span><br />';
            tRow += '<div class="input-group">';
            tRow += '<span class="text-left" style="color: #919191">Adult : ' + Hotel_RoomType[i].validForOccupancy.adults + '</span> ; ';
            tRow += '<span class="text-left" style="color: #919191">Children :' + Hotel_RoomType[i].validForOccupancy.children + '</span> ; ';
            tRow += '<span class="text-left" style="color: #919191">ExtraBed :' + Hotel_RoomType[i].validForOccupancy.extraBed + '</span> ; ';
            tRow += '<span class="text-left" style="color: #919191">ExtraBed Occupant :' + Hotel_RoomType[i].validForOccupancy.extraBedOccupant + '</span><br/>';
            tRow += '</div>';
            tRow += '</div>';
            tRow += '</div><br/>';
        }
        else {
            tRow += '<div class="row">';
            tRow += '<div class="col-md-6">';
            tRow += '<span class="text-left" style="color: #919191">Room ' + (i + 1) + '</span><br />';
            tRow += '<div class="input-group">';
            tRow += '<span class="text-left" style="color: #919191">No change in this room.</span><br />';
            tRow += '</div>';
            tRow += '</div>';
            tRow += '</div><br/>';
        }
        //////////////////////////
    }
    tRow += 'Kindly proceed if you agree, or choose other options.<br/>';

    Ok(tRow, 'GetRoomDetails', [HotelCode, RoomID, RoomDescriptionId, CUTRoomPrice, Supplier])
}

//function RedirectBooking(HCode, RoomID, Supplier) {
//    window.location.href = 'HotelBooking.aspx?HotelCode=' + HCode + '&RoomCode=' + RoomID + '&Atr=' + Supplier;
//}

function RedirectBooking(HCode, CutID, RoomID, RoomDescriptionId, RoomPrice, Supplier, noRooms) {
    
    if (Supplier == "HotelBeds") {
        Supplier = "A";
    }
    if (Supplier == "MGH") {
        Supplier = "B";
    }
    if (Supplier == "DoTW") {
        Supplier = "C";
    }

    window.location.href = 'HotelBooking.aspx?code=' + HCode + '&CutID=' + CutID + '&id=' + RoomID + '&roomdescriptionId=' + RoomDescriptionId + '&RoomP=' + RoomPrice + '&Supplier=' + Supplier + '&_noRooms=' + noRooms;
}

var DisplayRequest;
var PackageAmount;
var AgentMarkup;
var TotalAmount;
var ServiceTaxPer;
var HotelName;
var EanAvailRequest = null;
var EanRoomTypeCode = "";
var EanRoomId = "";
var ChkAvail = 0;
var ChkCurrent = 0;
var _sHotelDetails, _sConfirmation;
var RoomsDetails = new Array();
var HotelDetails = "";
var Occupancy = new Array();
var RateBrackupDetails = new Array();
var DisplayRequest = new Array();
var Currency;

function HotelBooking(objHotelCode, objRoomID, Supplier, RoomDescriptionId, RoomPrice, _noRooms, CutID) {
    
    var data = {
        HotelCode: objHotelCode,
        CutCode: CutID,
        RoomID: objRoomID,
        Supplier: Supplier,
        RoomDescriptionId: RoomDescriptionId,
        RoomPrice: RoomPrice,
        _noRooms: _noRooms
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelBooking",
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            RoomsDetails = result.RoomsDetails;
            HotelDetails = result.HotelDetails;
            DisplayRequest = result.DisplayRequest;
            Currency = DisplayRequest[0].CurrencyClass;
            ChkCurrent = DisplayRequest[0].TotalPayable.replace(",", "");
            $("#Total_Amount").val(DisplayRequest[0].TotalPayable);
            //$("#Amount").val("1");
            Occupancy = DisplayRequest[0].List_Occupancy;
            RateBrackupDetails = DisplayRequest[0].RateBrackupDetails;
            GenerateRooms();
            GetCurrentHotel();
            GenerateRigthRooms();
            TotalPayable();
            Confirmation();

            //var nAvailableCredit = result.AvailableCredit;
            //var nTotalAmount = result.TotalAmount;
            //nTotalAmount = parseFloat(nTotalAmount).toFixed(2);

            //ChkAvail = parseFloat(result.AvailableCredit)
            //var nBalance = result.AvailableBalance;
            //ServiceTaxPer = result.ServiceTaxPer;
            //$("#HotelDetailRight").append(result.Request);
            //$("#RoomDetails").append(result.RoomDetail);
            //$("#spAvailableCredit").text(numberWithCommas(nAvailableCredit));
            //TotalAmount = result.TotalPayable;
            //TotalAmount = parseFloat(TotalAmount).toFixed(2);
            //TotalAmount = numberWithCommas(TotalAmount);
            //$("#dspTotalPayable").text(TotalAmount);
            //if (result.AvailableBalance == "")
            //    $("#tdBalance").hide();
            //else
            //    $("#spAvailableBalance").text(numberWithCommas(nBalance));
            //$("#spCurrentBooking").text(numberWithCommas(nTotalAmount));
            //$("#spCurrentBookingC").text(numberWithCommas(nTotalAmount));
            //////
            //var paymentgatewaycharge = parseFloat(nTotalAmount * 2 / 100).toFixed(2);
            //$("#spAvailableCreditC").text(numberWithCommas(paymentgatewaycharge));
            //var paymentgatewaychargeTotal = parseFloat(parseFloat(paymentgatewaycharge) + parseFloat(nTotalAmount)).toFixed(2);
            //$("#spAvailableBalanceC").text(numberWithCommas(paymentgatewaychargeTotal));

            ////
            $("#spHoldingTime").append(result.HoldingPolicy);
            $("#spHoldingTimeCard").append(result.HoldingPolicy);
            $("#spHoldingTimeBank").append(result.HoldingPolicy);
            if (result.HoldingPolicy == "<input type=\"hidden\" id=\"hdnholdtime\" value=\"\" />" || result.HoldingPolicy == '<input type="hidden" id="hdnholdtime" value="" />') {
                $("#chktdHoldBooking").hide();
                $("#chktdHoldBookingCard").hide();
                $("#chktdHoldBookingBank").hide();
                $("#spHoldingTime").remove();
                $("#btn_Hold").remove();

                //$("#sptdHoldingTimeCard").hide();
                $("#sptdHoldingTimeBank").hide();
            }
            if (result.AlertMessage == "")
                $("#AlertMessage").hide();
            else
                $("#AlertMessage").append(result.AlertMessage);


            if (result.HotelDetails != undefined) {
                $(".Hoteldetails").css("display", "")
                $(".Hoteldetails").append(result.HotelDetails);

            }
            //if (result.ConfirmationDetails != null) {

            //    $(".ConfirmationDetails").append(result.ConfirmationDetails);

            //}
            $("#hdnCode").val(result.HCode); //Hotel Code
            $("#hdnRoomID").val(result.RCode); //RoomID (package id)
            $("#hdnHotelRoomID").val(result.HotelRoom); //all rooms SHRUI code seperated by |
            $("#hdnbBook").val(result.ProceedToBooking); // Booking confirmation True or false
            $("#hdnHotelName").val(HotelDetails.HotelName);
            HotelName = HotelDetails.HotelName;
            DisplayRequest = result.AvailRequest;
            PackageAmount = result.Amount;
            //PackageAmount = Math.round(PackageAmount)
            AgentMarkup = result.AgentRoomMarkup;
            //AgentMarkup = Math.round(AgentMarkup)
            TotalAmount = result.TotalAmount;
            var TotalCalAmount = PackageAmount + AgentMarkup;
            TotalCalAmount = PackageAmount + TotalCalAmount * ServiceTaxPer / 100.00;
            TotalCalAmount = (TotalCalAmount).toFixed(2);
            if (Supplier == "D") {
                $('[data-toggle="tooltip"]').tooltip()
                $("#Supplier").val(Supplier)
                EanAvailRequest = result.AvailRequest;
                EanRoomTypeCode = GetQueryStringParams('roomdescriptionId');
                EanRoomId = GetQueryStringParams('id');
                $('#hdnHotelRoomID').val(EanRoomId)
                EanTerms()
            }
            $('[data-toggle="tooltip"]').tooltip()


            //TotalCalAmount = Math.round(TotalCalAmount).toFixed(2);
            //$("#spCurrentBooking").text(TotalCalAmount);
            //parseFloat(Ammount).toFixed(2)
        },
        error: function (xhr, error, statusCode) {
            $('#SpnMessege').text(xhr.responseText);
            $('#ModelMessege').modal('show')
            //alert(xhr.responseText);
        }
    });
}
var IsValid;// validation related to credit and OTC and days
var Messege;
//function ProceedToHotelConfirmBooking() {
//    
//    $("#Amount1").addClass(Currency);
//    $("#Amount2").addClass(Currency);
//    $("#Amount3").addClass(Currency);
//    $("#Amount4").addClass(Currency);
//    $("#Amount5").addClass(Currency);
//    $("#Amount6").addClass(Currency);
//    // ValidationsForAmount();
//    
//    IsValid = true;
//    if (IsValid) {
//        var bValue = false;
//        $('.txtName').each(function () {
//            if ($(this).val() == "")
//                bValue = true;
//        });
//        $('.txtAge').each(function () {
//            if ($(this).val() == "")
//                bValue = true;
//            else if (!$.isNumeric($(this).val()))
//                bValue = true;
//            else if (parseInt($(this).val()) <= 0 || parseInt($(this).val()) >= 12)
//                bValue = true;
//        });
//        if (bValue) {
//            $('#SpnMessege').text("Please fill out the name of all passengers properly with valid information.");
//            $('#ModelMessege').modal('show')
//           // alert("Please fill out the name of all passengers properly with valid information.");
//        } else {
//            if ($("#hdnbBook").val() == "true" || ($("#chkTermsAndConditionCard").is(":checked"))) {
//                // if (($('#chktdHoldBooking').is(':hidden')) || ($('#chktdHoldBookingCard').is(':hidden')) || ($('#chktdHoldBookingBank').is(':hidden'))) {
//                if (($('#chktdHoldBooking').is(':hidden'))) {
//                    if (($("#chkTermsAndCondition").is(":checked")) || ($("#chkTermsAndConditionCard").is(":checked")) || ($("#chkTermsAndConditionBank").is(":checked"))) {
//                        OpenPriviewPopup('Vouchered');

//                    }
//                    else {
//                        $('#SpnMessege').text("Please accept the terms & conditions.");
//                        $('#ModelMessege').modal('show')
//                       // alert("Plesae accept the terms & conditions.");
//                    }

//                }
//                    //else if ((!$("#chkHoldBooking").is(":hidden")) || (!$("#chkHoldBookingCard").is(":hidden")) || (!$("#chkHoldBookingBank").is(":hidden"))) {
//                else if ((!$("#chkHoldBooking").is(":hidden"))) {
//                    //if (($("#chkHoldBooking").is(":checked")) || ($("#chkHoldBookingCard").is(":checked")) || ($("#chkHoldBookingBank").is(":checked"))) {
//                    //    if (($("#chkTermsAndCondition").is(":checked")) || ($("#chkTermsAndConditionCard").is(":checked")) || ($("#chkTermsAndConditionBank").is(":checked"))) {
//                    if (($("#chkHoldBooking").is(":checked")) || ($("#chkHoldBookingBank").is(":checked"))) {
//                        if (($("#chkTermsAndCondition").is(":checked")) || ($("#chkTermsAndConditionCard").is(":checked")) || ($("#chkTermsAndConditionBank").is(":checked"))) {
//                            OpenPriviewPopup('Booking');

//                        }
//                        else {
//                            $('#SpnMessege').text("Please accept the terms & conditions.");
//                            $('#ModelMessege').modal('show')
//                           // alert("Plesae accept the terms & conditions.");
//                        }

//                    }
//                    else {
//                        if (!$("#tdBalance").is(":hidden")) {
//                            if (($("#chkTermsAndCondition").is(":checked")) || ($("#chkTermsAndConditionCard").is(":checked")) || ($("#chkTermsAndConditionBank").is(":checked"))) {
//                                OpenPriviewPopup('Vouchered');

//                            }
//                            else {
//                                $('#SpnMessege').text("Please accept the terms & conditions.");
//                                $('#ModelMessege').modal('show')
//                               // alert("Please accept the terms & conditions.");
//                            }

//                        }
//                        else {
//                            $('#SpnMessege').text("Your balance is insufficient! Please hold the booking.")
//                            $('#ModelMessege').modal('show')
//                           // alert("Your balance is insufficient! Please hold the booking.")
//                        }

//                    }
//                }
//            }
//            else {
//                $('#SpnMessege').text("Your balance is insufficient! Please credit your account for making this booking");
//                $('#ModelMessege').modal('show')
//               // alert("Your balance is insufficient! Please credit your account for making this booking");
//            }

//        }
//    }
//    else {
//        //  alert(Messege);
//    }


//}

function ChkCondition() {
    $("#chkTermsAndConditionCard").removeAttr('checked');
}

function ChkConditionCard() {
    $("#chkTermsAndCondition").removeAttr('checked');
}


function ProceedToHotelConfirmBooking() {
    
    $("#Amount1").addClass(Currency);
    $("#Amount2").addClass(Currency);
    $("#Amount3").addClass(Currency);
    $("#Amount4").addClass(Currency);
    $("#Amount5").addClass(Currency);
    $("#Amount6").addClass(Currency);
    // ValidationsForAmount();
    
    IsValid = true;
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    if (IsValid) {
        var bValue = false;
        var cValue = false;
        var dValue = false;
        var eValue = false;
        var fValue = false;
        //var gValue = false;
        $('.txtName').each(function () {
            if ($(this).val() == "")
                bValue = true;
            if (/\d/.test($(this).val()))
                cValue = true;
            if (/\s/.test($(this).val()))
                dValue = true;
            if (!regex.test($(this).val()))
                eValue = true;
            if ($(this).val().length < 2 || $(this).val().length > 25)
                fValue = true;
            //if ($(this).val() == "")
            //    gValue = true;
        });
        $('.txtAge').each(function () {
            if ($(this).val() == "")
                bValue = true;
            else if (!$.isNumeric($(this).val()))
                bValue = true;
            else if (parseInt($(this).val()) <= 0 || parseInt($(this).val()) >= 12)
                bValue = true;
        });
        if (bValue) {
            alert("Please fill out the name of all passengers properly with valid information.");
        }
        else if (cValue) {
            alert("Name must not be numeric.");
        }
        else if (dValue) {
            alert("No white spaces allowed.");
        }
        else if (eValue) {
            alert("No special characters allowed.");
        }
        else if (fValue) {
            alert("Name should contain minimum 2 characters and maximum 25.");
        }
            //else if (gValue) {
            //    alert("Please fill out the name of all passengers properly with valid information.");
            //}
        else {
            if ($("#hdnbBook").val() == "true") {
                // if (($('#chktdHoldBooking').is(':hidden')) || ($('#chktdHoldBookingCard').is(':hidden')) || ($('#chktdHoldBookingBank').is(':hidden'))) {
                if (($('#chktdHoldBooking').is(':hidden'))) {
                    if (($("#chkTermsAndCondition").is(":checked")) || ($("#chkTermsAndConditionCard").is(":checked")) || ($("#chkTermsAndConditionBank").is(":checked"))) {
                        OpenPriviewPopup('Vouchered');

                    }
                    else
                        alert("Plesae accept the terms & conditions.");
                }
                //else if ((!$("#chkHoldBooking").is(":hidden")) || (!$("#chkHoldBookingCard").is(":hidden")) || (!$("#chkHoldBookingBank").is(":hidden"))) {

            }
            else
                OpenPriviewPopup('Vouchered');
        }
    }
    else {
        //  alert(Messege);
    }


}



var ServiceTax;
var Room;
function OpenPriviewPopup(status) {

    //for (var i = 0; i < DisplayRequest.room.length; i++) {
    if (EanAvailRequest == null) {
        var getCancellation1 = $("#SpnCanPolicy0").text();
        var getCancellation2 = $("#SpnCanPolicy2").text();
        //var Amount = PackageAmount * DisplayRequest.Night;
        //var Amount = Math.round(PackageAmount);
        var Amount = PackageAmount.toFixed(2);
        ServiceTax = (((parseFloat(Amount) + parseFloat(AgentMarkup)) * ServiceTaxPer) / 100).toFixed(2);
        //ServiceTax = Math.round((((Amount + AgentMarkup) * ServiceTaxPer) / 100).toFixed(4));
        //var ServiceTax = (((Amount + AgentMarkup) * ServiceTaxPer) / 100);
        //Amount = Math.round(Amount.toFixed(4));
        //TotalAmount = ((parseFloat(Amount))).toFixed(2);
        TotalAmount = ((parseFloat(Amount) + parseFloat(ServiceTax))).toFixed(2);
        $("#dspHotelName").text(HotelName);
        $("#dspCheckIn").text(DisplayRequest.fDate);
        $("#dspCheckOut").text(DisplayRequest.tDate);
        if ($('#Supplier').val() == 'C') {
            Room = DisplayRequest.room.length;
            //$("#dspRooms").text(DisplayRequest.room.length);
        }
        else if ($('#Supplier').val() == 'B') {
            Room = DisplayRequest.Room;
        }
        else {
            Room = DisplayRequest.Room;

        }
        $("#dspRooms").text(Room);
        $("#dspNights").text(DisplayRequest.Night);

        $("#dspHotelNameC").text(HotelName);
        $("#dspCheckInC").text(DisplayRequest.CheckIn);
        $("#dspCheckOutC").text(DisplayRequest.CheckOut);
        $("#dspRoomsC").text(DisplayRequest.Room);
        $("#dspNightsC").text(DisplayRequest.Night);

        $("#dspHotelNameB").text(HotelName);
        $("#dspCheckInB").text(DisplayRequest.CheckIn);
        $("#dspCheckOutB").text(DisplayRequest.CheckOut);
        $("#dspRoomsB").text(DisplayRequest.Room);
        $("#dspNightsB").text(DisplayRequest.Night);

        //$("#dspAgencyRef").text();

        $("#tblPassengerDetails tr").remove();
        $("#tblPassengerDetailsC tr").remove();
        $("#tblPassengerDetailsB tr").remove();
        var tr = '<tr class="bold"><td>Sr. No.</td><td>Name</td><td>Gender</td><td>Room</td></tr>';
        var code = $('#hdnHotelRoomID').val();
        var Array_HotelID = code.split('|');
        var k = 0;
        var x = 0;
        for (var i = 0; i < Room; i++) {
            var GuestID;
            if (x == 0)
                GuestID = Array_HotelID[i];
            else if (x > 0)
                GuestID = Array_HotelID[i - x];
            if ($('#tblTravelers' + (i + 1) + ' tr').attr('name') == $('#tblTravelers' + (i) + ' tr').attr('name')) {
                GuestID = Array_HotelID[i - 1];
                x = x + 1;
            }
            var j = 1;
            $('#tblTravelers' + (i + 1) + ' tr').each(function () {
                if ($(this).attr('name') == 'adult_' + GuestID) {
                    k = k + 1;
                    var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val();
                    var FName = $(this).find("#txtFName_" + (i + 1) + "_" + j).val();
                    var LName = $(this).find("#txtLName_" + (i + 1) + "_" + j).val();
                    var Room = i + 1;
                    if (title == "Mrs" || title == "Miss")
                        tr += '<tr><td>' + k + '</td><td>' + FName + ' ' + LName + '</td><td>Female</td><td>Room ' + (i + 1) + '</td></tr>';
                    else if (title == "Mr")
                        tr += '<tr><td>' + k + '</td><td>' + FName + ' ' + LName + '</td><td>Male</td><td>Room ' + (i + 1) + '</td></tr>';
                    j = j + 1;
                }
                else if ($(this).attr('name') == 'child_' + GuestID) {
                    k = k + 1;
                    var title = $(this).find("#SelCGender_" + (i + 1) + "_" + j).val();
                    var Name = $(this).find("#txtCName_" + (i + 1) + "_" + j).val();
                    //var Age = $(this).find("#txtCAge_" + (i + 1) + "_" + j).val();
                    var Room = i + 1;
                    if (title == "Miss")
                        tr += '<tr><td>' + k + '</td><td>' + Name + '</td><td>Female</td><td>Room ' + (i + 1) + '</td></tr>';
                    else if (title == "Master")
                        tr += '<tr><td>' + k + '</td><td>' + Name + '</td><td>Male</td><td>Room ' + (i + 1) + '</td></tr>';
                    j = j + 1;
                }
            });
        }
    }
    else {
        var getCancellation1 = $("#SpnCanPolicy0").text();
        var getCancellation2 = $("#SpnCanPolicy2").text();
        //var Amount = PackageAmount * DisplayRequest.Night;
        //var Amount = Math.round(PackageAmount);
        var Amount = PackageAmount.toFixed(2);
        ServiceTax = (((parseFloat(Amount) + parseFloat(AgentMarkup)) * ServiceTaxPer) / 100).toFixed(2);
        //ServiceTax = Math.round((((Amount + AgentMarkup) * ServiceTaxPer) / 100).toFixed(4));
        //var ServiceTax = (((Amount + AgentMarkup) * ServiceTaxPer) / 100);
        //Amount = Math.round(Amount.toFixed(4));
        //TotalAmount = ((parseFloat(Amount))).toFixed(2);
        TotalAmount = ((parseFloat(Amount) + parseFloat(ServiceTax))).toFixed(2);
        $("#dspHotelName").text(EanAvailRequest.hotelName);
        $("#dspCheckIn").text(EanAvailRequest.arrivalDate);
        $("#dspCheckOut").text(EanAvailRequest.departureDate);
        $("#dspRooms").text(EanAvailRequest.numberOfRoomsRequested);
        $("#dspNights").text(EanAvailRequest.noNight);
        Room = EanAvailRequest.numberOfRoomsRequested;
        $("#dspHotelNameC").text(EanAvailRequest.hotelName);
        $("#dspCheckInC").text(EanAvailRequest.arrivalDate);
        $("#dspCheckOutC").text(EanAvailRequest.departureDate);
        $("#dspRoomsC").text(EanAvailRequest.HotelRoomResponse);
        $("#dspNightsC").text(DisplayRequest.Night);

        $("#dspHotelNameB").text(HotelName);
        $("#dspCheckInB").text(EanAvailRequest.arrivalDate);
        $("#dspCheckOutB").text(EanAvailRequest.departureDate);
        $("#dspRoomsB").text(EanAvailRequest.HotelRoomResponse);
        $("#dspNightsB").text(EanAvailRequest.noNight);

        //$("#dspAgencyRef").text();

        $("#tblPassengerDetails tr").remove();
        $("#tblPassengerDetailsC tr").remove();
        $("#tblPassengerDetailsB tr").remove();
        var tr = '<tr class="bold"><td>Sr. No.</td><td>Name</td><td>Gender</td><td>Room</td></tr>';
        var code = $('#hdnHotelRoomID').val();
        //alert(code)
        var Array_HotelID = EanRoomId.split('|');
        var k = 0;
        var x = 0;
        for (var i = 0; i < EanAvailRequest.numberOfRoomsRequested; i++) {
            var GuestID;
            if (x == 0)
                GuestID = Array_HotelID[i];
            else if (x > 0)
                GuestID = Array_HotelID[i - x];
            if ($('#tblTravelers' + (i + 1) + ' tr').attr('name') == $('#tblTravelers' + (i) + ' tr').attr('name')) {
                GuestID = Array_HotelID[i - 1];
                x = x + 1;
            }
            var j = 1;
            $('#tblTravelers' + (i + 1) + ' tr').each(function () {
                if ($(this).attr('name') == 'adult_' + GuestID) {
                    k = k + 1;
                    var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val();
                    var FName = $(this).find("#txtFName_" + (i + 1) + "_" + j).val();
                    var LName = $(this).find("#txtLName_" + (i + 1) + "_" + j).val();
                    var Room = i + 1;
                    if (title == "Mrs" || title == "Miss")
                        tr += '<tr><td>' + k + '</td><td>' + FName + ' ' + LName + '</td><td>Female</td><td>Room ' + (i + 1) + '</td></tr>';
                    else if (title == "Mr")
                        tr += '<tr><td>' + k + '</td><td>' + FName + ' ' + LName + '</td><td>Male</td><td>Room ' + (i + 1) + '</td></tr>';
                    j = j + 1;
                }
                else if ($(this).attr('name') == 'child_' + GuestID) {
                    k = k + 1;
                    var title = $(this).find("#SelCGender_" + (i + 1) + "_" + j).val();
                    var Name = $(this).find("#txtCName_" + (i + 1) + "_" + j).val();
                    //var Age = $(this).find("#txtCAge_" + (i + 1) + "_" + j).val();
                    var Room = i + 1;
                    if (title == "Miss")
                        tr += '<tr><td>' + k + '</td><td>' + Name + '</td><td>Female</td><td>Room ' + (i + 1) + '</td></tr>';
                    else if (title == "Master")
                        tr += '<tr><td>' + k + '</td><td>' + Name + '</td><td>Male</td><td>Room ' + (i + 1) + '</td></tr>';
                    j = j + 1;
                }
            });
        }
    }
    $("#tblPassengerDetails").append(tr);
    $("#tblPassengerDetailsC").append(tr);
    $("#tblPassengerDetailsB").append(tr);

    $("#dspTotalAmount").text(" " + numberWithCommas(Amount));
    $("#dspServiceTax").text(" " + numberWithCommas(ServiceTax));
    //$("#dspTotalPayable").text(" " + numberWithCommas(TotalAmount));

    $("#dspTotalAmountC").text(" " + numberWithCommas(Amount));
    $("#dspServiceTaxC").text(" " + numberWithCommas(ServiceTax));
    $("#dspTotalPayableC").text(" " + numberWithCommas(TotalAmount));

    $("#dspTotalAmountB").text(" " + numberWithCommas(Amount));
    $("#dspServiceTaxB").text(" " + numberWithCommas(ServiceTax));
    $("#dspTotalPayableB").text(" " + numberWithCommas(TotalAmount));

    //$("#dspTotalAmount").text(" " + numberWithCommas(Amount));
    //$("#dspServiceTax").text(" " + numberWithCommas(ServiceTax));
    //$("#dspTotalPayable").text(" " + numberWithCommas(TotalAmount));

    //$("#dspTotalAmountC").text(" " + numberWithCommas(Amount));
    //$("#dspServiceTaxC").text(" " + numberWithCommas(ServiceTax));
    //$("#dspTotalPayableC").text(" " + numberWithCommas(TotalAmount));

    //$("#dspTotalAmountB").text(" " + numberWithCommas(Amount));
    //$("#dspServiceTaxB").text(" " + numberWithCommas(ServiceTax));
    //$("#dspTotalPayableB").text(" " + numberWithCommas(TotalAmount));

    if (status == "Vouchered") {
        $("#hndBookingStatus").val("Vouchered");
        $("#btnConfirmBooking").val("Book Now");
        $("#dspBookingStatus").text("To be vouchered");
        $("#dspAlertMessage").text("Your booking is about to confirm. Please note that leading guest should be min 18 year or older & should carry valid passport / photo ID.");
        $("#dspCan1").text(getCancellation1);

        $("#hndBookingStatusC").val("Vouchered");
        $("#btnConfirmBookingC").val("Book Now");
        $("#dspBookingStatusC").text("To be vouchered");
        $("#dspAlertMessageC").text("Your booking is about to confirm. Please note that leading guest should be min 18 year or older & should carry valid passport / photo ID.");
        $("#dspCan1C").text(getCancellation1);

        $("#hndBookingStatusB").val("Vouchered");
        $("#btnConfirmBookingB").val("Book Now");
        $("#dspBookingStatusB").text("To be vouchered");
        $("#dspAlertMessageB").text("Your booking is about to confirm. Please note that leading guest should be min 18 year or older & should carry valid passport / photo ID.");
        $("#dspCan1B").text(getCancellation1);

    } else if (status == "Booking") {
        $("#hndBookingStatus").val("Booking");
        $("#btnConfirmBooking").val("Hold Now");
        $("#dspBookingStatus").text("To be on hold");
        $("#dspAlertMessage").text("Your booking is going to be on hold. Please confirm it as soon as possible.");
        $("#dspCan1").text(getCancellation1);

        $("#hndBookingStatusC").val("Booking");
        $("#btnConfirmBookingC").val("Hold Now");
        $("#dspBookingStatusC").text("To be on hold");
        $("#dspAlertMessageC").text("Your booking is going to be on hold. Please confirm it as soon as possible.");
        $("#dspCan1C").text(getCancellation1);

        $("#hndBookingStatusB").val("Booking");
        $("#btnConfirmBookingB").val("Hold Now");
        $("#dspBookingStatusB").text("To be on hold");
        $("#dspAlertMessageB").text("Your booking is going to be on hold. Please confirm it as soon as possible.");
        $("#dspCan1B").text(getCancellation1);

        //if (getCancellation1 != "") {
        //    $("#dspCan2").text(getCancellation1);
        //    //$("#dspCan2").css("display", "");
        //    document.getElementById('#dspCan2').style.display = 'show';
        //}

    }
    if (($("#chkTermsAndCondition").is(":checked"))) {
        //$("#PreviewModal").modal('show');                 
        $("#PreviewModalRegisteration").modal('show');
    }
    if ($("#chkTermsAndConditionCard").is(":checked")) {

        $("#PreviewModalRegisteration").modal('show');
        //HotelConfirmBooking();
        //WaitforbookingCard(param);

        //$("#PreviewModalCard").modal('show');
    }
    if ($("#chkTermsAndConditionBank").is(":checked")) {
        $("#PreviewModalBank").modal('show');
    }

    //$("#PreviewModal").modal('show');
}

function HotelConfirmBooking() {
    var Guest = new Array();
    var code = $('#hdnHotelRoomID').val();
    var Array_HotelID = code.split('|');
    var k = 0;
    var x = 0;
    //for (var i = 0; i < DisplayRequest.Room; i++) {
    for (var i = 0; i < Room; i++) {
        var GuestID;
        if (x == 0)
            GuestID = Array_HotelID[i];
        else if (x > 0)
            GuestID = Array_HotelID[i - x];
        if ($('#tblTravelers' + (i + 1) + ' tr').attr('name') == $('#tblTravelers' + (i) + ' tr').attr('name')) {
            GuestID = Array_HotelID[i - 1];
            x = x + 1;
        }
        var Customer = new Array();
        var j = 1;
        var k = 1;
        
        $('#tblTravelers' + (i + 1) + ' tr').each(function () {
            if ($(this).attr('name') == 'adult_' + GuestID) {
                //var j = 1;
                var title = $(this).find("#SelAGender_" + (i + 1) + "_" + j).val(); //$(this).find("select[name='Select_Gender'] option:selected").val();
                var objCustomer = {
                    Age: 0,//$(this).find("input[name='txtAge']").val(),
                    type: "AD",
                    Name: title + ' ' + $(this).find("#txtFName_" + (i + 1) + "_" + j).val(),
                    LastName: $(this).find("#txtLName_" + (i + 1) + "_" + j).val()
                }
                
                Customer.push(objCustomer);
                j = j + 1;
            }
            else if ($(this).attr('name') == 'child_' + GuestID) {
                //var k = 1;
                //if ($(this).find("#txtCName_" + (i + 1) + "_" + j).val() != "") {
                var title = $(this).find("#SelCGender_" + (i + 1) + "_" + k).val();
                var objCustomer = {
                    Age: ($(this).find("#txtCAge_" + (i + 1) + "_" + k).val() != "") ? $(this).find("#txtCAge_" + (i + 1) + "_" + k).val() : 0,
                    type: "CH",
                    Name: title + ' ' + $(this).find("#txtCName_" + (i + 1) + "_" + k).val(),
                    LastName: ""
                }
                Customer.push(objCustomer);
                k = k + 1;
                //}
            }
            //j = j + 1;
        });
        //$('#tblTravelers' + (i + 1) + 'tr[name="child_' + GuestID + '"]').each(function () {
        //    var title = $(this).find("#SelCGender_" + (i + 1) + "_" + j).val();
        //    var objCustomer = {
        //        Age: $(this).find("#txtCAge_" + (i + 1) + "_" + j).val(),
        //        type: "CH",
        //        Name: title + ' ' + $(this).find("#txtCName_" + (i + 1) + "_" + j).val(),
        //        LastName: ""
        //    }
        //    Customer.push(objCustomer);
        //});
        var objGuest = {
            ID: GuestID,
            RoomNumber: (i + 1),
            sCustomer: Customer
        };
        Guest.push(objGuest);
    }


    var data;
    var Supplier = $("#Supplier").val();
    var RoomDescriptionId = $("#RoomDescriptionId").val();
    if (Supplier == 'A') {
        data = {
            HotelCode: $("#hdnCode").val(),
            RoomID: $("#hdnRoomID").val(),
            AgencyRefernce: $("#txt_AgentBookingreference").val(),
            MobileNo: $("#txt_CustomerMobileNumber").val(),
            Remark: $("#txt_BookingRemarks").val(),
            BookingStatus: $("#hndBookingStatus").val(),
            Email: $("#txt_AgentEmail").val(),
            List_Guest: Guest,
            Guest_listMGH: null,
            Guest_listDoTW: null,
            Guest_listEan: null,
            ServiceTax: ServiceTax,
            Supplier: Supplier,
            RoomDescriptionId: RoomDescriptionId
        }
    }
    else if (Supplier == 'B') {
        data = {
            HotelCode: $("#hdnCode").val(),
            RoomID: $("#hdnRoomID").val(),
            AgencyRefernce: $("#txt_AgentBookingreference").val(),
            MobileNo: $("#txt_CustomerMobileNumber").val(),
            Remark: $("#txt_BookingRemarks").val(),
            BookingStatus: $("#hndBookingStatus").val(),
            Email: $("#txt_AgentEmail").val(),
            List_Guest: null,
            Guest_listMGH: Guest,
            Guest_listDoTW: null,
            Guest_listEan: null,
            ServiceTax: ServiceTax,
            Supplier: Supplier,
            RoomDescriptionId: RoomDescriptionId
        }
    }
    else if (Supplier == 'D') {
        data = {
            HotelCode: $("#hdnCode").val(),
            RoomID: $("#hdnRoomID").val(),
            AgencyRefernce: $("#txt_AgentBookingreference").val(),
            MobileNo: $("#txt_CustomerMobileNumber").val(),
            Remark: $("#txt_BookingRemarks").val(),
            BookingStatus: $("#hndBookingStatus").val(),
            Email: $("#txt_AgentEmail").val(),
            List_Guest: null,
            Guest_listMGH: null,
            Guest_listDoTW: null,
            Guest_listEan: Guest,
            ServiceTax: ServiceTax,
            Supplier: Supplier,
            RoomDescriptionId: RoomDescriptionId
        }
    }
    else {
        data = {
            HotelCode: $("#hdnCode").val(),
            RoomID: $("#hdnRoomID").val(),
            AgencyRefernce: $("#txt_AgentBookingreference").val(),
            MobileNo: $("#txt_CustomerMobileNumber").val(),
            Remark: $("#txt_BookingRemarks").val(),
            BookingStatus: $("#hndBookingStatus").val(),
            Email: $("#txt_AgentEmail").val(),
            List_Guest: null,
            Guest_listMGH: null,
            Guest_listDoTW: Guest,
            Guest_listEan: null,
            ServiceTax: ServiceTax,
            Supplier: Supplier,
            RoomDescriptionId: RoomDescriptionId
        }
    }
    //data = {
    //    HotelCode: $("#hdnCode").val(),
    //    RoomID: $("#hdnRoomID").val(),
    //    AgencyRefernce: $("#txt_AgentBookingreference").val(),
    //    MobileNo: $("#txt_CustomerMobileNumber").val(),
    //    Remark: $("#txt_BookingRemarks").val(),
    //    BookingStatus: $("#hndBookingStatus").val(),
    //    Email: $("#txt_AgentEmail").val(),
    //    List_Guest: Guest,
    //    Guest_listMGH: Guest,
    //    Guest_listDoTW: Guest,
    //    ServiceTax: ServiceTax,
    //    Supplier: $("#Supplier").val()
    //}
    var param = JSON.stringify(data);

    // window.location.href = 'waitforbooking.aspx?data=' + param;;

    if (($("#chkTermsAndCondition").is(":checked"))) {
        window.location.href = 'WaitBooking.aspx?data=' + param;
    }
    if ($("#chkTermsAndConditionCard").is(":checked")) {
        

        WaitforbookingCard(param)
    }
    if ($("#chkTermsAndConditionBank").is(":checked")) {

    }
}

function WaitforbookingCard(data) {
    
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelConfirmSession",
        contentType: 'application/json',
        data: data,
        datatype: "json",
        success: function (response) {
            
            $("#PreviewModalCard").modal('show');

        },
        error: function (xhr, error, statusCode) {
            $('#SpnMessege').text('Something Went Wrong');
            $('#ModelMessege').modal('show')
            // alert('Something Went Wrong');
        }
    });
}

function Waitforbooking(ConfirmId) {
    
    var data = { ConfirmId: ConfirmId };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/ConfirmBooking",
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1)
                window.location.href = 'BookingConfirm.aspx?ResID=' + result.ReservationID + '&Status=Vouchered';
            else {

                Ok(result.Message + ".<br/> Please contact administrator regarding this booking.<br/>On clicking OK button you'll be redirected to search page.", 'RedirectToHome', null)
            }

        },
        error: function (xhr, error, statusCode) {

            Ok("Something went wrong! <br/> Please contact administrator regarding this booking.<br/>On clicking Ok button you'll be redirected to search page.", 'RedirectToHome', null)
        }
    });
}

function OnlinePayment() {
    
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/OnlinePayment",
        contentType: 'application/json',
        data: {},
        datatype: "json",
        success: function (response) {
            
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                window.location.href = 'BookingConfirm.aspx?ResID=' + result.ReservationID + '&Status=' + result.Status;
            }
            if (result.retCode != 1) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert('Something Went Wrong');
            }
        },
        error: function (xhr, error, statusCode) {
            $('#SpnMessege').text('Something Went Wrong');
            $('#ModelMessege').modal('show')
            // alert('Something Went Wrong');
        }
    });
}
function CopyPassengerName(RoomCount, AdultCount) {
    $('#txtFName_' + RoomCount + '_' + (AdultCount + 1)).val($('#txtFName_' + RoomCount + '_' + AdultCount).val());
    $('#txtLName_' + RoomCount + '_' + (AdultCount + 1)).val($('#txtLName_' + RoomCount + '_' + AdultCount).val());
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

