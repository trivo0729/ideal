﻿var arrPax = new Array();
var arrCountry = new Array();
var Country = "India";
var CountryC = "IN";
var City = "Nagpur";
var Address = "Golden Palace, SS8, W High Ct Rd, Near Sudhama Theatre, Dharampeth, Nagpur, Maharashtra 440010";
var Nationality = "";
var Currency = "INR";
var CurrencyClass = '<i class="fa fa-inr" aria-hidden="true"></i>'
var MobileNo = "9923750110";
var email = "idealtoursandtravels@gmail.com";
function GenratePaxDetails() {
    var html = '';
    var PaxType = "";
    html += '<div class="person-information">'
    html += '<h3>Passenger Details</h3>'
    html += '<hr />'
    $("#div_Pax").append(html)
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        html = '';
        html += '<div class="toggle-container box">                                                                     '
        html += '<div class="panel style2">                                                                             '
        html += '<h4 class="panel-title">                                                                               '
        html += '<a class="" href="#Passenger_' + (i) + '" data-toggle="collapse" aria-expanded="true">Passenger ' + (i + 1) + ': (' + arrPax.PaxDetails[i].PaxType.replace("AD", "Adult ").replace("CH", "Child ") + ' )</a>'
        html += '</h4>                                                                                                  '
        html += '<div class="panel-collapse collapse in" id="Passenger_' + (i) + '" aria-expanded="true">'
        html += '<div class="panel-content">                                                                            '
        html += '<div class="form-group row">                                                                           '

        html += '<div class="col-sm-4 col-md-4">                                                                        '
        html += '<span style="font-size: 13px">First Name</span>' + RequiredFild(arrPax.PaxDetails[i].FirstName.IsRequired)
        html += '<input type="text" class="form-control " placeholder="" id="txt_Name' + i + '"><span  style="color:red" id="lbl_Name' + i + '"></span>'
        html += '<input type="hidden" id-"' + arrPax.PaxDetails[i].Type + '"  id-"txtPaxType' + i + '">'
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-4 col-md-3">                                                                        '
        html += '<span style="font-size: 13px">Last Name</span>' + RequiredFild(arrPax.PaxDetails[i].LastName.IsRequired)
        html += '<input type="text" id="txt_LastName' + i + '" class="form-control " placeholder=""><span style="color:red" id="lbl_LastName' + i + '"></span>'
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-2 col-md-3">                                                                        '
        //html += '<span style="font-size: 13px">Date of birth</span>' + RequiredFild(arrPax.PaxDetails[i].DOB.IsRequired)
        html += '<span style="font-size: 13px">Date of birth</span><span style="color:red">*</span>'
        html += '<div class="datepicker-wrap">                                                                          '
        html += '<input type="text" name="date_from" class="form-control" id="txt_DOB' + i + '" placeholder="dd-MM-yyyy"/ >'
        html += '</div>                                                                                                 '
        html += '<span  style="color:red" id="lbl_DOB' + i + '"></span>'
        html += '</div>                                                                                                 '
        html += '<div class="col-sm-2 col-md-2">                                                    '
        html += '<br />                                                                         '
        html += '<span>                                                                         '
        html += '<input type="radio" checked="" name="' + i + '" id="rdb_Male' + i + '" value="M">Male'
        html += '</span>                                                                        '
        html += '<br />                                                                         '
        html += '<span>                                                                         '
        html += '<input type="radio" name="' + i + '"  id="rdb_Female' + i + '" value="F">Female'
        html += '</span>                                                                        '
        html += '</div>                                                                             '
        html += '</div>                                                                                 '
        if (i == 0) {
            //html += '<div class="form-group row">                                                           '
            //html += '<div class="col-sm-4 col-md-4">                                                    '
            //html += '<span style="font-size: 13px">Mobile :</span><span style="color:red">*</span>'
            //html += '<input type="text" class="form-control " placeholder="" id="txt_MobileNo' + i + '"> <span  style="color:red" id="lbl_MobileNo' + i + '"></span>'
            //html += '</div>                                                                             '
            //html += '<div class="col-sm-4 col-md-4">                                                    '
            //html += '<span style="font-size: 13px">Email:</span><span style="color:red">*</span>'
            //html += '<input type="text" class="form-control " placeholder="" id="txt_Email' + i + '"> <span  style="color:red" id="lbl_Email' + i + '"></span>'
            //html += '</div>'
            //html += '</div>'
            html += '<div class="form-group row" style="display:none">'
            html += '<div class="col-sm-8 col-md-8">'
            html += '<span style="font-size: 13px">Address : </span><span style="color:red">*</span>'
            //html += '<textarea id="txt_Address" rows="4" class="form-control"></textarea>'
            html += '<input id="txt_Address" value="' + Address + '" type="text" class="form-control"/>'
            html += '<span  style="color:red" id="lbl_Address' + i + '"></span>'
            html += '                    </div>'
            html += '                    <div class="col-sm-4 col-md-4">'
            html += '                        <span style="font-size: 13px">Country </span></span><span style="color:red">*</span>'
            html += '                        <select class="form-control" id="sel_Country">'
            html += '<option value="' + CountryC + '" selected="selected">' + Country + '</option>'
            //html += GetCountry()
            html += '                        </select><span  style="color:red" id="lbl_Country' + i + '"></span>'
            html += '                    </div>'
            html += '                    <div class="col-sm-4 col-md-4">'
            html += '                        <span style="font-size: 13px">City:</span><span style="color:red">*</span>'
            html += '<input type="text" class="form-control " value="' + City + '" placeholder="" id="txt_City">'
            html += '<span  style="color:red" id="lbl_City' + i + '"></span></div> '
            html += '                    </div>'
        }
        html += '                <div class="form-group row">                                                           '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Passport No:</span>' + RequiredFild(arrPax.PaxDetails[i].Passport.IsRequired)
        html += '<input type="text"  class="form-control" placeholder="" id="txt_PassportNo' + i + '">'
        html += '<span  style="color:red" id="lbl_PassportNo' + i + '"></span>'
        html += '                    </div>                                                                             '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Expiry date (if specified)</span>                '
        html += '                        <div class="datepicker-wrap">                                                  '
        html += '                            <input id="txt_PassportExp' + i + '" type="text" name="date_from" class="form-control" />'
        html += '                        </div>                                                                         '
        html += '                    </div>                                                                             '
        html += '                    <div class="col-sm-4 col-md-4">                                                    '
        html += '                        <span style="font-size: 13px">Nationality </span><span style="color:red">*</span>'
        html += '                        <select class="form-control" id="sel_Nationality' + i + '">                                 '
        html += GetCountryOrNationality()
        html += '                        </select>                                                                      '
        html += '<span  style="color:red" id="lbl_Nationality' + i + '"></span></div> '
        html += '                    </div>                                                                             '

        html += '                <br />                                                                                 '
        html += '                <h4>Special Service Request</h4>                                                       '
        html += '                <hr />                                                                                 '
        html += '                <br />                                                                                 '

        html += GetSSR(i)
        html += '                </div>                                                                                 '
        html += '                </div>                                                                                 '
        html += '            </div>                                                                                     '
        html += '        </div>                                                                                         '
        html += '    </div>'
        html += '</div>'
        html += '<hr />'
        $("#div_Pax").append(html);
        var tpj = jQuery;
        tpj("#txt_DOB" + i).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: "1920:dateToday",
            maxDate: "dateToday",
        });
        tpj("#txt_PassportExp" + i).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
        });
    }
    html = '';
    //html += '<h3>Contact Details</h3>'
    //html += '<hr />'
    //html += '<div class="form-group row">                                                           '
    //html += '<div class="col-sm-4 col-md-4">                                                    '
    //html += '<span style="font-size: 13px">Mobile :</span><span style="color:red">*</span>'
    //html += '<input type="tel" class="form-control " placeholder="" id="txt_MobileNo0"> <span  style="color:red" id="lbl_MobileNo0"></span>'
    //html += '</div>                                                                             '
    //html += '<div class="col-sm-4 col-md-4">                                                    '
    //html += '<span style="font-size: 13px">Email:</span><span style="color:red">*</span>'
    //html += '<input type="text" class="form-control " placeholder="" id="txt_Email0"> <span  style="color:red" id="lbl_Email0"></span>'
    //html += '</div>'
    //html += '</div>'
    html += '<div class="form-group row">'
    html += '    <div class="col-sm-8 col-md-18">'
    html += '    </div>'
    html += '    <div class="col-sm-4 col-md-4">'
    html += '        <a class="button btn-medium sky-blue1" onclick="CheckAvailcredit()">Proceed for booking</a>'
    html += '    </div>'
    html += '</div>'
    //html += '<br />'
    //html += '</div>'
    $("#div_Pax").append(html);
    $("#txt_MobileNo0").intlTelInput({
    });
}

function RequiredFild(Validate) {
    var htmlreq = '';
    if (Validate)
        htmlreq += '<span style="color:red">*</span>'
    return htmlreq;
}
function GetCountryOrNationality() {
    var html = '';
    for (var i = 0; i < arrCountry.length; i++) {
        html += '<option value="' + arrCountry[i].Country_Code + '">' + arrCountry[i].Country_name + '</option>'
    }
    return html;
} /*Pax Input Design*/

var arrPaxDetails = new Array();
function Book() {
    arrPaxDetails = new Array()
    if (Validate()) {
        for (var i = 0; i < arrPax.PaxDetails.length; i++) {
            var Title = "Mr", AddressLine = "", City = "", CountryCode = "", CountryName = "", email = "";
            var isLeading = false;
            var Gender = "1"
            var Type = "1"
            if ($("#rdb_Female" + i).prop("checked") && arrPax.PaxDetails[i].PaxType != "CH" && arrPax.PaxDetails[i].PaxType != "In") {
                Title = "Mrs";
                Gender = 2;
                Type = "1";
            }
            else if (arrPax.PaxDetails[i].PaxType == "CH") {
                Title = "MASTER";
                Gender = 1;
                Type = "2"
            }
            else if (arrPax.PaxDetails[i].PaxType == "In") {
                Title = "MASTER";
                Gender = 1;
                Type = "3"
            }
            if (i == 0) {
                isLeading = true;
                AddressLine = $("#txt_Address").val();
                City = $("#txt_City").val();
                CountryCode = $("#sel_Country").val();
                CountryName = $("#sel_Country option:selected").text();
                email = email;
                //var XYZ = $("#txt_MobileNo0").val();
                //var XYZ1 = $(".selected-flag");
                //XYZ1 = XYZ1[0].title;
                //XYZ1 = XYZ1.split(":");
                //XYZ1 = XYZ1[1];
                MobileNo = MobileNo;
                //if (XYZ.startsWith("+")) {
                //    MobileNo = XYZ;
                //}
                //else {
                //    MobileNo = XYZ;
                //}
            }
            var Baggage = [];
            for (var b = 0; b < $(".sel_Baggage" + i).length; b++) {
                Baggage.push(GetBaggageDetails($(".sel_Baggage" + i)[b].value, b));
            }
            var MealDynamic = [];
            for (var b = 0; b < $(".sel_DMeal" + i).length; b++) {
                MealDynamic.push(GetDMeal($(".sel_DMeal" + i)[b].value, b));
            }
            var Meal = [];
            for (var b = 0; b < $(".sel_Meal" + i).length; b++) {
                //Meal.push(GetMeal($(".sel_Meal" + i)[b].value, b));
                Meal = GetMeal($(".sel_Meal" + i)[b].value, b);
            }
            if ($(".sel_Meal" + i).length == 0)
                Meal = { Code: '', Description: '' }

            var Passenger = {
                Title: Title,
                FirstName: $("#txt_Name" + i).val(),
                LastName: $("#txt_LastName" + i).val(),
                PaxType: Type,
                DateOfBirth: $("#txt_DOB" + i).val(),
                Gender: Gender,
                PassportNo: $("#txt_PassportNo" + i).val(),
                PassportExpiry: $("#txt_PassportExp" + i).val(),
                AddressLine1: AddressLine,
                AddressLine2: "",
                City: City,
                CountryCode: CountryCode,
                CountryName: CountryName,
                Nationality: $("#sel_Nationality" + i + " option:selected").text(),
                ContactNo: MobileNo,
                Email: email,
                IsLeadPax: isLeading,
                Baggage: Baggage,
                MealDynamic: MealDynamic,
                Meal: Meal
            }
            arrPaxDetails.push(Passenger)
        }
        var arrBooking = new Array();
        Booking = {
            Passengers: arrPaxDetails,
            ResultIndexes: FlightId,
            EndUserIp: "",
        };
        $.ajax({
            type: "POST",
            url: "../handler/FlightHandler.asmx/Booking",
            data: JSON.stringify({ objFlightBooking: Booking }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var arrYourBooking = result.PNR;
                    var tr = '';
                    for (var i = 0; i < arrYourBooking.length; i++) {
                        tr += '<tr>'
                        if (arrYourBooking[i].Success)
                            tr += '<td class="green">Flight PNR: <td>' + arrYourBooking[i].PNR;
                        else if (arrYourBooking[i].OnHold)
                            tr += '<td class="red">Flight PNR: <td>' + arrYourBooking[i].PNR + ".Please Contact Customer support regarding this Booking.";
                        tr += '</tr>'
                    }
                    $('#SpnMessege').append(tr)
                    $('#ModelMessege').modal('show');
                    window.location.href = "flightConfirmation.aspx?BookingID=" + JSON.stringify(arrYourBooking);
                }
                else if (result.retCode == 0) {
                    $('#SpnMessege').text(result.errormsg);
                    $('#ModelMessege').modal('show')
                    if (result.errormsg == "Your session (TraceId) is expired.") {
                        alert("Flight fare has been changed kindly research to get new fare")
                        setTimeout(function () {
                            window.location.href = "FlightBooking.aspx";
                        }, 2000)
                    }
                    else {
                        alert("" + result.errormsg + "")
                    }
                }
            },
            error: function () {
                alert("something went wrong")
            },
            complete: function () {
                $("#dlgLoader").css("display", "none");
            }
        });
    }
} /*Booking*/


function GetMeal(Code, nSegment) {
    var Meal = [];
    try {
        var arrMeal = $.grep(arrFlightDetails.SSR.Meal, function (p) { return p.Code == Code; })
                    .map(function (p) { return p; })[0];
        Meal = {
            Code: Code,
            Description: arrMeal.Description,
        };

    } catch (e) {

    }

    return Meal
}

function Validate() {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    var bValid = true;
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        $('#lbl_LastName' + i).hide()
        if ($("#txt_LastName" + i).val() == "" && arrPax.PaxDetails[i].LastName.IsRequired) {
            $('#lbl_LastName' + i).text("* Please add Last Name")
            $('#lbl_LastName' + i).show()
            bValid = false;
        }
        $('#lbl_Name' + i).hide()
        if ($("#txt_Name" + i).val() == "" && arrPax.PaxDetails[i].FirstName.IsRequired) {
            $('#lbl_Name' + i).text("* Please add First Name")
            $('#lbl_Name' + i).show()
            bValid = false;
        }
        $('#lbl_DOB' + i).hide()
        if ($("#txt_DOB" + i).val() == "" && arrPax.PaxDetails[i].DOB.IsRequired) {
            if ($("#txt_DOB" + i).val() == "") {
                $('lbl_DOB' + i).text("* Please add DOB")
                $('lbl_DOB' + i).show()
                bValid = false;
            }
        }
        if ($("#txt_DOB" + i).val() != "") {
            bValid = CheckAge(i, arrPax.PaxDetails[i].PaxType);
        }

        $('#lbl_MobileNo0').hide()
        if (arrPax.PaxDetails[i].IsLeading) {
            if (!(pattern.test(email))) {
                bValid = false;
                $("#lbl_Email0").text("* Wrong email format.");
                $("#lbl_Email0").show();
            }

            $('lbl_City0').hide()
            if ($("#txt_City").val() == "") {
                $('#lbl_City0').text("* Please add City")
                $('#lbl_City0').show()
                bValid = false;
            }
            $('#lbl_Address0').hide()
            if ($("#txt_Address").val() == "") {
                $('#lbl_Address0').text("* Please add Address here")
                $('#lbl_Address0').show()
                bValid = false;

            }
        }

        $('#lbl_PassportNo' + i).hide()
        if ($("#txt_PassportNo" + i).val() == "" && arrPax.PaxDetails[i].Passport.IsRequired) {
            $('#lbl_PassportNo' + i).text("* Please add Passport No")
            $('#lbl_PassportNo' + i).show()
            bValid = false;
        }
    }
    return bValid;
}/*Booking Validation*/

var arrFlightDetails = new Array();
var FlightModalDetails = "";
var AmountForPayment = 0;
/* Flight Details*/ var r = 0;
FlightCount = 0;
function GenrateFlightDetails() {
    var html = '';
    var Origin = '';
    var Destination = '';
    var Date = '';
    var OriginR = '';
    var DestinationR = '';
    var DateR = '';
    html += '<div class="all-searchbox">'
    html += '<div class="all-box-multiwaydata">'
    html += '<div class="col-lg-12">'
    html += '<div class="row row-box-multiwaydata">'
    html += '<div class="col-md-12 col-sm-12 col-xs-12 product-multi-way inherit-tabs">'
    html += '    <a class="flight-panelmultiway take-off"><div class="icon"><i class="soap-icon-plane-right" style="font-size: 30px;"></i></div>&nbsp;Flight Details </a>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
        if (arrFlightDetails.JourneyType == "1") {
            if (i == 0) {
                Origin = arrFlightDetails.FlightDetails[i].Origin.Airport.CityName;
                Date = moment(arrFlightDetails.FlightDetails[i].Origin.DepTime).format("llll");
            }
            Destination = arrFlightDetails.FlightDetails[arrFlightDetails.FlightDetails.length - 1].Destination.Airport.CityName;
            if (i == 0) {
                html += '<div class="col-lg-12">'
                html += '    <div class="row mb10" style="padding: 0px 0px 3px 10px;">'
                html += '<div class="take-off col-lg-12" style="font-size: 14px;">'
                html += '    <div class="icon"><i class="soap-icon-plane-right" style="font-size: 30px;"></i></div>'
                html += '    <span>' + Origin + ' - ' + Destination + '&nbsp;&nbsp;<b>| ' + Date + ' |</b></span>'
                html += '</div>'
                html += '    </div>'
                html += '</div>'
            }
        }

        if (arrFlightDetails.JourneyType == "2" || arrFlightDetails.JourneyType == "3") {
            if (i == 0) {
                Origin = arrFlightDetails.FlightDetails[i].Origin.Airport.CityName;
                Date = moment(arrFlightDetails.FlightDetails[i].Origin.DepTime).format("llll");
            }
            DestinationR = arrFlightDetails.FlightDetails[arrFlightDetails.FlightDetails.length - 1].Destination.Airport.CityName;
            for (var j = i; j < arrFlightDetails.FlightDetails.length; j++) {
                if (arrFlightDetails.FlightDetails[j].Mile == 2) {
                    Destination = arrFlightDetails.FlightDetails[j - 1].Destination.Airport.CityName;
                    break;
                }
            }
            if (i == 0) {
                html += '<div class="col-lg-12">'
                html += '    <div class="row mb10" style="padding: 0px 0px 3px 10px;">'
                html += '<div class="take-off col-lg-12" style="font-size: 14px;">'
                html += '    <div class="icon"><i class="soap-icon-plane-right" style="font-size: 30px;"></i></div>'
                html += '    <span>' + Origin + ' - ' + Destination + '&nbsp;&nbsp;<b>| ' + Date + ' |</b></span>'
                html += '</div>'
                html += '    </div>'
                html += '</div>'
            }
            if (arrFlightDetails.FlightDetails[i].Mile == 2) {
                if (FlightCount == 0) {
                    OriginR = arrFlightDetails.FlightDetails[i].Origin.Airport.CityName;
                    DateR = moment(arrFlightDetails.FlightDetails[i].Origin.DepTime).format("llll");
                    html += '<div class="col-lg-12">'
                    html += '    <div class="row mb10" style="padding: 0px 0px 3px 10px;">'
                    if (arrFlightDetails.JourneyType == "3")
                        html += '<div class="take-off col-lg-12" style="font-size: 14px;">'
                    else
                        html += '<div class="landing col-lg-12" style="font-size: 14px;">'
                    if (arrFlightDetails.JourneyType == "3")
                        html += '    <div class="icon"><i class="soap-icon-plane-right" style="font-size: 30px;"></i></div>'
                    else
                        html += '    <div class="icon"><i class="soap-icon-plane-left" style="font-size: 30px;"></i></div>'
                    html += '    <span>' + OriginR + ' - ' + DestinationR + '&nbsp;&nbsp;<b>| ' + DateR + ' |</b></span>'
                    html += '</div>'
                    html += '    </div>'
                    html += '</div>'
                    FlightCount++;
                }
            }
        }

        html += '<div class="col-lg-12">'
        html += '    <div class="row rowfs">'
        html += '        <div class="col-md-12 col-lg-12 col-xs-12 multiway-leftblock">'
        html += '<div class="row">'
        html += '    <div class="col-md-2 col-lg-2 col-xs-4 tour-multi-waydays">'
        html += '' + GetLogo(arrFlightDetails.FlightDetails[i].Airline.AirlineCode) + ''
        html += '        &nbsp;<br>'
        html += '        <span>' + arrFlightDetails.FlightDetails[i].FlightName + '</span><br>'
        html += '        <span>(' + arrFlightDetails.FlightDetails[i].Airline.AirlineCode + ')</span>'
        html += '    </div>'
        html += '    <div class="col-md-4 col-lg-4 col-xs-2 product-multi-way">'
        html += '        <span>' + arrFlightDetails.FlightDetails[i].Origin.Airport.AirportName + '</span><br>'
        html += '        <span>' + moment(arrFlightDetails.FlightDetails[i].Origin.DepTime).format("llll") + '</span><br>'
        html += '        <span>Terminal - ' + arrFlightDetails.FlightDetails[i].Origin.Airport.Terminal + '</span><br>'
        html += '        <span>' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '</span>'
        html += '    </div>'

        html += '    <div class="col-md-2 col-lg-2 col-xs-4 product-multi-way take-off" style="text-align: center;">'
        var minutes = parseInt(arrFlightDetails.FlightDetails[i].Duration);
        var hours = "";
        hours = Math.floor(minutes / 60);
        minutes = minutes % 60;
        html += '        <div class="icon"><i class="soap-icon-clock" style="font-size: 30px;"></i></div>'
        html += '        <br>'
        html += '        <span>' + hours + 'h ' + minutes + 'm </span>'
        html += '    </div>'
        html += '    <div class="col-md-4 col-lg-4 col-xs-2 product-multi-way">'
        html += '        <span>' + arrFlightDetails.FlightDetails[i].Destination.Airport.AirportName + '</span><br>'
        html += '        <span>' + moment(arrFlightDetails.FlightDetails[i].Destination.ArrTime).format("llll") + '</span><br>'
        html += '        <span>Terminal - ' + arrFlightDetails.FlightDetails[i].Destination.Airport.Terminal + '</span><br>'
        html += '        <span>' + arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + '</span>'
        html += '    </div>'
        html += '</div>'
        html += '<div class="mb20"></div>'
        html += '        </div>'
        html += '    </div>'
        html += '</div>'
    }

    html += '</div>'
    html += '</div>'
    $("#Div_Details").append(html)
    FlightModalDetails = html;
    ModalDesign();
}

function GetLogo(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img  src="images/AirlineLogo/' + AirlinCode + ext + '">'
    return img;
}/*Logo*/

function GetSSR(s) {
    var html = "";
    if (arrFlightDetails.SSR != null) {
        var Baggage = arrFlightDetails.SSR.Baggage;
        if (Baggage != undefined) {
            html += '                <div class="form-group row">'
            html += '                    <div class="col-sm-2 col-md-2"> '
            html += '                        <br />'
            html += '                        <span style="font-size: 13px">Baggage:</span><span class="red"></span>'
            html += '                    </div>'
            for (var i = 0; i < arrFlightDetails.SSR.Baggage.length; i++) {
                html += '                    <div class="col-sm-4 col-md-4">'
                html += '                        <span style="font-size: 13px"></span>(' + Baggage[i][0].Origin + '-' + Baggage[i][0].Destination + ')<span class="red">*</span>'
                html += '                        <select class="form-control sel_Baggage' + s + '" id="sel_Baggage' + s + '" onchange="ChangeAmount(' + s + ',' + i + ')">'
                for (var j = 0; j < Baggage[i].length; j++) {
                    var Price = Baggage[i][j].Price;
                    html += '<option value="' + Baggage[i][j].Code + '">' + Baggage[i][j].Weight + 'Kg (' + Currency + ' ' + Price + ')</option>'
                }
                html += '                        </select>'
                html += '                    </div>'
            }
            html += '                </div>'
        }
        var Meal = arrFlightDetails.SSR.MealDynamic;
        if (Meal != undefined) {
            html += '                <div class="form-group row">'
            html += '                    <div class="col-sm-2 col-md-2"> '
            html += '                        <br />'
            html += '                        <span style="font-size: 13px">Meal Preferences:</span><span class="red"></span>'
            html += '                    </div>'
            for (var i = 0; i < Meal.length; i++) {
                html += '                    <div class="col-sm-4 col-md-4">'
                html += '                        <span style="font-size: 13px"></span>(' + Meal[i][0].Origin + '-' + Meal[i][0].Destination + ')</span><span class="red">*</span>'
                html += '                        <select class="form-control sel_DMeal' + s + '" id="sel_DMeal' + s + '" onchange="ChangeAmount(' + s + ',' + i + ')">'
                for (var j = 0; j < Meal[i].length; j++) {
                    var Price = Meal[i][j].Price;
                    html += '<option value="' + Meal[i][j].Code + '">' + Meal[i][j].AirlineDescription + '(' + Currency + ' ' + Price + ')</option>'
                }
                html += '                        </select>'
                html += '                    </div>'
            }
            html += '                </div>'
        }

        var Meal = arrFlightDetails.SSR.Meal;
        if (Meal != undefined) {
            if (Meal.length != 0) {
                html += '                <div class="form-group row">'
                html += '                    <div class="col-sm-2 col-md-2"> '
                html += '                        <br />'
                html += '                        <span style="font-size: 13px">Meals:</span><span class="red"></span>'
                html += '                    </div>'
                html += '                    <div class="col-sm-4 col-md-4">'
                html += '                        <span style="font-size: 13px"></span><span class="red"></span>'
                html += '                        <select class="form-control sel_Meal' + s + '">'
                for (var i = 0; i < Meal.length; i++) {
                    html += '<option value="' + Meal[i].Code + '">' + Meal[i].Description + '</option>'
                }
                html += '                        </select>'
                html += '                    </div>'
                html += '                </div>'
            }
        }
    }
    return html;
    // $("#div_ssr").append(html);
}  /*SSR Design*/

function GetBaggageDetails(BaggageID) {
    var Baggage = new Array();
    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[0], function (p) { return p.Code == BaggageID; })
        .map(function (p) { return p; })[0];
    Baggage = {
        WayType: arrBaggage.WayType,
        Price: arrBaggage.Price,
        Code: BaggageID,
        Currency: arrBaggage.Currency,
        WayType: arrBaggage.WayType,
        Description: arrBaggage.Description,
        Weight: arrBaggage.Weight,
        Origin: arrBaggage.Origin,
        Destination: arrBaggage.Destination
    };
    return Baggage
}

function GetBreckDown() {
    var html = '';
    html += '<h4>Trip Summary</h4>'
    html += '<hr />'
    for (var i = 0; i < arrFlightDetails.Breackdowns.length; i++) {
        if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic) {
            html += '<div style="background-color: rgb(217, 217, 217);">'
            if (i == 0)
                html += '<p style="color:white;text-align: center">Departure</p>'
            else
                html += '<p style="color:white;text-align: center">Return</p>'
            html += '</div>'
        }
        for (var j = 0; j < arrFlightDetails.Breackdowns[i].length; j++) {
            html += '<div class="form-group row" style="margin-bottom: 0px">'
            html += '    <div class="col-sm-8 col-md-8 toggle-container box" style="margin-bottom: 0px">'
            html += '        <div class="panel style1">'
            html += '            <h6 class="panel-title"><a class="" href="#tgg_' + j + '" data-toggle="collapse" aria-expanded="true">Traveler ' + (j + 1) + '  (' + arrFlightDetails.Breackdowns[i][j].PassengerCount + ') ' + arrFlightDetails.Breackdowns[i][j].PassengerType + ' </a></h6>'
            html += '        </div>'
            html += '    </div>'
            html += '    <div class="col-sm-4 col-md-4" style="text-align: end">'
            html += '        <h5>'
            html += '            <br />'
            html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="' + arrFlightDetails.Breackdowns[i][j].FareTip + '" rel="popover" data-placement="left">' + CurrencyClass + ' ' + arrFlightDetails.Breackdowns[i][j].Fare + '</a><br>'
            html += '        </h5>'
            html += '    </div>'
            html += '</div>'
            html += '<div class="form-group row panel-collapse collapse in" id="tgg_' + j + '" aria-expanded="true" style="margin-bottom: 0px">'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Taxes & Fees<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' ' + arrFlightDetails.Breackdowns[i][j].Taxes + '</a><br>'
            html += '    </div>'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Excess Baggage<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic)
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="BaggageAmount' + (j) + '' + (i) + '"> 0.00</label></a><br>'
            else
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="BaggageAmount' + (j) + '"> 0.00</label></a><br>'
            html += '    </div>'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Meal Preferences<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic)
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MealAmount' + (j) + '' + (i) + '"> 0.00</label></a><br>'
            else
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MealAmount' + (j) + '"> 0.00</label></a><br>'
            html += '    </div>'
            html += '</div>'
        }
    }
    html += '<hr />'
    html += '<div class="form-group row">'
    html += '<br />'
    html += '<div class="col-sm-6 col-md-6">'
    html += '    <span style="font-size: 15px"><b>Trip Total:</b></span>'
    html += '</div>'
    html += '<div class="col-sm-6 col-md-6 Total">'
    html += '<a class="Price" href="#" id="TotalAmount"  data-container="body" data-html="true" data-content="' + arrFlightDetails.TotalPriceTip + '" rel="popover" data-placement="left" style="float: right"><input type="hidden" id="TotalAmountOriginal" />' + CurrencyClass + ' ' + arrFlightDetails.TotalPrice + '</a>'
    html += '</div>'
    html += '</div>'
    $("#Div_Trip").append(html);
    $('.Price').popover({ trigger: "hover" });
    OriginalAmount = arrFlightDetails.TotalPrice
    $('#TotalAmountOriginal').val(arrFlightDetails.TotalPrice);
}/*Price Breackups*/

function LogoForModal(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img src="images/AirlineLogo/' + AirlinCode + ext + '" style="height:50px;">'
    return img;
}/*Logo*/

function GenerateModal() {
    var html = ""
    var ul = '';
    $("#Div_Modal").empty();
    if (arrFlightDetails.JourneyType == "1") {
        html += ModalDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
            arrFlightDetails.FlightDetails[0].FlightName,
            arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
            arrFlightDetails.FlightDetails[0].Origin.DepTime,
            arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
            arrFlightDetails.FlightDetails[0].Destination.ArrTime,
            arrFlightDetails.FlightDetails[0].Duration, 0
        )
    }
    else if (arrFlightDetails.JourneyType == "2") {
        html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        html += '<li class="active"><a href="#departModal" data-toggle="tab" aria-expanded="true">Out Bound Ticket</a></li>'
        html += '<li class=""><a href="#ArrivalModal" data-toggle="tab" aria-expanded="false">Inbound Ticket</a></li>'
        html += ' </ul>'
        html += '<div class="tab-content">'
        html += '<!-- Tab 1 -->'
        html += '<div class="tab-pane fade active in" id="departModal" style="padding: 5px;">'
        html += ModalDesign(arrFlightDetails.FlightDetails[0].AirlineCode,
            arrFlightDetails.FlightDetails[0].FlightName,
            arrFlightDetails.FlightDetails[0].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
            arrFlightDetails.FlightDetails[0].Origin.DepTime,
            arrFlightDetails.FlightDetails[0].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
            arrFlightDetails.FlightDetails[0].Destination.ArrTime,
            arrFlightDetails.FlightDetails[0].Duration, 0

        )
        html += '</div>'
        html += '<!-- End of Tab 1 -->'
        html += '<!-- Tab 2 -->'
        html += '<div class="tab-pane fade" id="ArrivalModal" style="padding: 5px;">'
        html += ModalDesign(arrFlightDetails.FlightDetails[1].AirlineCode,
            arrFlightDetails.FlightDetails[1].FlightName,
            arrFlightDetails.FlightDetails[1].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Origin.Airport.CountryName,
            arrFlightDetails.FlightDetails[1].Origin.DepTime,
            arrFlightDetails.FlightDetails[1].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[0].Destination.Airport.CountryName,
            arrFlightDetails.FlightDetails[1].Destination.ArrTime,
            arrFlightDetails.FlightDetails[1].Duration, 1
        )
        html += '</div>'
        html += '<!-- End of Tab 2 -->'
        html += '</div>'
        html += '</div>'

    }
    else if (arrFlightDetails.JourneyType == "3") {
        html += '<div class="tab-container style1">'
        html += '<ul class="tabs full-width">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            var expanded = "false";
            if (i == 0) {
                Class = "active";
                expanded = "true";
            }
            html += '<li class="' + Class + '"><a href="#tabModal' + i + '" data-toggle="tab" aria-expanded="' + expanded + '">' + arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + '-'
            html += arrFlightDetails.FlightDetails[i].Destination.Airport.CityName
            html += '</a></li>'
        }
        ul += ' </ul>'
        html += '<div class="tab-content">'
        for (var i = 0; i < arrFlightDetails.FlightDetails.length; i++) {
            var Class = "";
            if (i == 0)
                Class = "active in";
            html += '<!-- Tab ' + i + ' -->'
            html += '<div class="tab-pane fade ' + Class + '" id="tabModal' + i + '" style="padding: 5px;">'
            html += ModalDesign(arrFlightDetails.FlightDetails[i].AirlineCode,
                arrFlightDetails.FlightDetails[i].FlightName,
                arrFlightDetails.FlightDetails[i].Origin.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Origin.Airport.CountryName,
                arrFlightDetails.FlightDetails[i].Origin.DepTime,
                arrFlightDetails.FlightDetails[i].Destination.Airport.CityName + ',' + arrFlightDetails.FlightDetails[i].Destination.Airport.CountryName,
                arrFlightDetails.FlightDetails[i].Destination.ArrTime,
                arrFlightDetails.FlightDetails[i].Duration, 0
            )
            html += '</div>'
        }
        html += '</div>'
        html += '</div>'
    }
    $("#Div_Modal").append(html)
    $('[data-toggle="tooltip"]').tooltip()
}/* Flight Details*/ var n = 0;

//function ModalDesign(AirlineCode, FlightName, Origin, DepTime, Destination, ArrTime, Duration, Id) {
function ModalDesign() {
    var html = '';
    html += FlightModalDetails;
    html += '<hr />'

    html += '<div class="row" style="margin-bottom: 0px">'
    html += Accordions()
    html += '<div class="col-sm-5" id="Pax_Confirmation">'
    html += '</div>'
    html += '</div>'
    n++;
    $("#Div_Modal").append(html)
    $('.Price').popover({ trigger: "hover" });
}

function Accordions() {
    var html = '';
    html += '<div class="col-sm-7">'
    for (var i = 0; i < arrFlightDetails.Breackdowns.length; i++) {
        if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic) {
            html += '<div style="background-color: rgb(217, 217, 217);">'
            if (i == 0)
                html += '<p style="color:white;text-align: center">Departure</p>'
            else
                html += '<p style="color:white;text-align: center">Return</p>'
            html += '</div>'
        }
        for (var j = 0; j < arrFlightDetails.Breackdowns[i].length; j++) {
            html += '<div class="form-group row" style="margin-bottom: 0px">'
            html += '    <div class="col-sm-8 col-md-8 toggle-container box" style="margin-bottom: 0px">'
            html += '        <div class="panel style1">'
            html += '            <h6 class="panel-title"><a class="" href="#tgg_' + j + '" data-toggle="collapse" aria-expanded="true">Traveler ' + (j + 1) + '  (' + arrFlightDetails.Breackdowns[i][j].PassengerCount + ') ' + arrFlightDetails.Breackdowns[i][j].PassengerType + ' </a></h6>'
            html += '        </div>'
            html += '    </div>'
            html += '    <div class="col-sm-4 col-md-4" style="text-align: end">'
            html += '        <h5>'
            html += '            <br />'
            html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="' + arrFlightDetails.Breackdowns[i][j].FareTip + '" rel="popover" data-placement="left">' + CurrencyClass + ' ' + arrFlightDetails.Breackdowns[i][j].Fare + '</a><br>'
            html += '        </h5>'
            html += '    </div>'
            html += '</div>'
            html += '<div class="form-group row panel-collapse collapse in" id="tgg_' + j + '" aria-expanded="true" style="margin-bottom: 0px">'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Taxes & Fees<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' ' + arrFlightDetails.Breackdowns[i][j].Taxes + '</a><br>'
            html += '    </div>'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Excess Baggage<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic)
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MBaggageAmount' + (j) + '' + (i) + '"> 0.00</label></a><br>'
            else
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MBaggageAmount' + (j) + '"> 0.00</label></a><br>'
            html += '    </div>'
            html += '    <div class="col-sm-7 col-md-7" style="color: #337ab7; text-align: end">'
            html += '        Meal Preferences<br>'
            html += '    </div>'
            html += '    <div class="col-sm-5 col-md-5" style="color: #337ab7; text-align: end">'
            if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic)
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MMealAmount' + (j) + '' + (i) + '"> 0.00</label></a><br>'
            else
                html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="" rel="popover" data-placement="left">' + CurrencyClass + ' <label id="MMealAmount' + (j) + '"> 0.00</label></a><br>'
            html += '    </div>'
            html += '</div>'
        }
    }
    html += '<div class="col-sm-12 col-md-12">'
    html += '<span style="font-size: 15px"><b>Trip Total:</b></span>&nbsp;&nbsp;&nbsp;<label class="TotalForModal">'
    html += '<a class="Price" style="color: #337ab7" href="#" data-container="body" data-html="true" data-content="' + arrFlightDetails.TotalPriceTip + '" rel="popover" data-placement="left">' + CurrencyClass + ' ' + arrFlightDetails.TotalPrice + '</a>'
    html += '</label>'
    html += '</div>'
    html += '</div>'
    $("#Total_Amount").val(arrFlightDetails.TotalPrice);
    AmountForPayment = arrFlightDetails.TotalPrice;
    return html;
}


function GetBaggageDetails(BaggageID, nSegment) {
    var Baggage = new Array();
    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[nSegment], function (p) { return p.Code == BaggageID; })
        .map(function (p) { return p; })[0];
    Baggage = {
        WayType: arrBaggage.WayType,
        Price: arrBaggage.Price,
        Code: BaggageID,
        Currency: arrBaggage.Currency,
        Description: arrBaggage.Description,
        Weight: arrBaggage.Weight,
        Origin: arrBaggage.Origin,
        Destination: arrBaggage.Destination
    };
    return Baggage
}

function GetDMeal(Code, nSegment) {
    var Meal = new Array();
    var arrMeal = $.grep(arrFlightDetails.SSR.MealDynamic[nSegment], function (p) { return p.Code == Code; })
        .map(function (p) { return p; })[0];
    Meal = {
        WayType: arrMeal.WayType,
        Code: Code,
        Description: arrMeal.Description,
        AirlineDescription: arrMeal.AirlineDescription,
        Quantity: arrMeal.Quantity,
        Price: arrMeal.Price,
        Currency: arrMeal.Currency,
        Origin: arrMeal.Origin,
        Destination: arrMeal.Destination
    };
    return Meal
}

function CheckAvailcredit() {
    var FlightId = GetQueryStringParams("FlightID").split(',');
    if (Validate()) {
        $("#Pax_Confirmation").empty()
        $.ajax({
            type: "POST",
            url: "../handler/FlightHandler.asmx/CheckAvailCredit",
            data: JSON.stringify({ arrIndex: FlightId, TotalBaggAmount: TotalBaggAmount, TotalMealAmount: TotalMealAmount }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var html = '';
                    html += '<span><b>Pax Details:</b></span>'
                    html += '<ul class="circle">'
                    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
                        var Title = "Mr", AddressLine = "", City = "", CountryCode = "", CountryName = "";
                        var isLeading = false;
                        var Gender = "1"
                        var Type = "1"
                        if ($("#rdb_Female" + i).prop("checked") && arrPax.PaxDetails[i].PaxType != "CH") {
                            Title = "Mrs";
                            Gender = 2;
                            Type = "1";
                        }
                        else if (arrPax.PaxDetails[i].PaxType == "CH") {
                            Title = "MASTER";
                            Gender = 1;
                            Type = "2"
                        }
                        html += '<li>' + Title + ' ' + $("#txt_Name" + i).val() + ' ' + $("#txt_LastName" + i).val(), +'</li>'
                    }
                    $("#Pax_Confirmation").append(html)
                    html += '</ul>'
                    //$('#ConfirmModal').modal('show')
                    CheckUserLogin();
                }
                else if (result.retCode == 0) {
                    $('#AgencyBookingCancelModal').modal('hide')
                    var Massage = "Unable To Process Your Request At This Time";
                    alert("Unable To Process Your Request At This Time");
                    //$('#SpnMessege').text(Massage);
                    //$('#ModelMessege').modal('show')
                }
            },
            error: function () {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text("something went wrong");
                $('#ModelMessege').modal('show')
                // alert("something went wrong");
            },
            complete: function () {
                $("#dlgLoader").css("display", "none");
            }
        });
    }
}/*Check Avail credit*/

function CheckUserLogin() {
    $.ajax({
        url: "DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                $('#ConfirmModal').modal('show');
            }
            else if (result.retCode == 0) {
                $("#RegisterationModal").modal('show');
            }
        },
    });
}

function LoginForFlightBooking() {
    var sUserName = document.getElementById("txtUserNameF").value;
    var sPassword = document.getElementById("txtPasswordF").value;
    var bValid = true;

    if (sUserName == "") {
        bValid = false;
        alert("User Name/email cannot be blank");
        document.getElementById('txtUserNameF').focus();
        return false;
    }
    else if (!emailReg.test(sUserName)) {
        bValid = false;
        alert("The email address you have entered is invalid");
        document.getElementById('txtUserNameF').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        alert("Password cannot be blank");
        document.getElementById('txtPasswordF').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    return false;
                }
                else if (result.retCode == 1) {
                    $("#RegisterationModal").modal('hide');
                    $('#ConfirmModal').modal('show');
                    CheckloginUser();
                }
                else {
                    alert('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
}
//................Validation.......................//
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function ClearAll() {
    $("#txt_UserIdF").value('');
    $("#txt_UserPasswordF").value('');
}

function ValidateRegistrationForFlight() {
    $("#lbl_UserNameF").css("display", "none");
    $("#lbl_PasswordF").css("display", "none");
    $("#lbl_ConfirmPasswordF").css("display", "none");
    $("#lbl_MobileF").css("display", "none");
    $("#lbl_PhoneF").css("display", "none");
    $("#lbl_AddressF").css("display", "none");
    $("#lbl_CityF").css("display", "none");
    $("#lbl_PinCodeF").css("display", "none");

    $("#lbl_UserNameF").html("* This field is required.");
    $("#lbl_PasswordF").html("* This field is required.");
    $("#lbl_ConfirmPasswordF").html("* This field is required.");
    $("#lbl_MobileF").html("* This field is required.");
    $("#lbl_PhoneF").html("* This field is required.");
    $("#lbl_AddressF").html("* This field is required.");
    $("#lbl_CityF").html("* This field is required.");
    $("#lbl_PinCodeF").html("* This field is required.");

    var reg = new RegExp('[0-9]$');

    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;

    var UserName = $("#txt_UserNameF").val();

    var Address = $("#txt_AddressF").val();
    var CityId = $("#selCityF").val();
    var Email = $("#txt_EmailF").val();
    var Password = $("#txt_PasswordF").val();
    var ConfirmPassword = $("#txt_ConfirmPasswordF").val();
    var Contact2 = $("#txt_PhoneF").val();
    var Contact1 = $("#txt_MobileF").val();
    if (UserName == "") {
        bValid = false;
        $("#lbl_UserNameF").css("display", "");
    }
    else {
        if (!(pattern.test(UserName))) {
            bValid = false;
            $("#lbl_UserNameF").html("* Wrong email format.");
            $("#lbl_UserNameF").css("display", "");
        }
    }
    if (Password == "") {
        bValid = false;
        $("#lbl_PasswordF").css("display", "");
    }
    if (ConfirmPassword == "") {
        bValid = false;
        $("#lbl_ConfirmPasswordF").html("* This field is required.");
        $("#lbl_ConfirmPasswordF").css("display", "");
    }
    else if (Password != ConfirmPassword) {
        bValid = false;
        $("#lbl_ConfirmPasswordF").html("Confirm Password did not matched!");
        $("#lbl_ConfirmPasswordF").css("display", "");
    }
    if (Address == "") {
        bValid = false;
        $("#lbl_AddressF").css("display", "");
    }
    if (Address != "" && reg.test(Address)) {
        bValid = false;
        $("#lbl_AddressF").html("* Address must not be numeric at end.");
        $("#lbl_AddressF").css("display", "");
    }
    if (CityId == "-") {
        bValid = false;
        $("#lbl_CityF").css("display", "");
    }
    if (Contact1 == "") {
        bValid = false;
        $("#lbl_MobileF").css("display", "");
    }

    else {
        if (!(reg.test(Contact1))) {
            bValid = false;
            $("#lbl_MobileF").html("* Mobile no must be numeric.");
            $("#lbl_MobileF").css("display", "");
        }
    }
    if (Contact2 == "") {
        Contact2 = '0';
    }
    else {
        if (!(reg.test(Contact2))) {
            bValid = false;
            $("#lbl_PhoneF").html("* Phone no must be numeric.");
            $("#lbl_PhoneF").css("display", "");
        }
    }
    if (bValid == true) {

        var data = {
            UserName: UserName,
            Password: Password,
            Contact1: Contact1,
            Contact2: Contact2,
            Address: Address,
            CityId: CityId,
        };
        $.ajax({
            type: "POST",
            url: "B2CCustomerLoginHandler.asmx/UserRegistration",
            //data: '{"sB2C_Id":"' + sB2C_Id + '","sUserName":"' + sUserName + '","sEmail":"' + sEmail + '","nMobile":"' + nMobile + '","nPhone":"' + nPhone + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '"}',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    alert("Registration Successfully");
                    $("#RegisterationModal").modal('hide');
                    ClearRegistrationForFlight();
                    $('#ConfirmModal').modal('show');
                    CheckloginUser();
                }
                else if (result.retCode == 2) {
                    alert("Username already registered. Please register with other Username.");
                }
                else if (result.retCode == -1) {
                    alert("Registeration UnSuccessfull.");
                }

                else {
                    alert("Something Went Wrong");
                    window.location.reload();
                }
            }
        });
    }
}


function ClearRegistrationForFlight() {
    $("#PreviewModalRegisteration").modal('hide');
    $('#txt_UserNameF').val("");
    $('#txt_PasswordF').val("");
    $('#txt_ConfirmPasswordF').val("");
    $('#txt_MobileF').val("");
    $('#txt_PhoneF').val("");
    $('#txt_AddressF').val("");
    $('#selCityF option').val("-");
    $('#txt_PinCodeF').val("");

    $("#lbl_UserNameF").css("display", "none");
    $("#lbl_PasswordF").css("display", "none");
    $("#lbl_ConfirmPasswordF").css("display", "none");
    $("#lbl_MobileF").css("display", "none");
    $("#lbl_PhoneF").css("display", "none");
    $("#lbl_AddressF").css("display", "none");
    $("#lbl_CityF").css("display", "none");
    $("#lbl_PinCodeF").css("display", "none");

    $("#lbl_UserNameF").html("* This field is required.");
    $("#lbl_PasswordF").html("* This field is required.");
    $("#lbl_ConfirmPasswordF").html("* This field is required.");
    $("#lbl_MobileF").html("* This field is required.");
    $("#lbl_PhoneF").html("* This field is required.");
    $("#lbl_AddressF").html("* This field is required.");
    $("#lbl_CityF").html("* This field is required.");
    $("#lbl_PinCodeF").html("* This field is required.");


    //for login clearing

    $('#txtUserNameF').val("");
    $('#txtPasswordF').val("");

    $("#lbl_txtUserNameF").css("display", "none");
    $("#lbl_txtPasswordF").css("display", "none");

    $("#lbl_txtUserNameF").html("* This field is required.");
    $("#lbl_txtPasswordF").html("* This field is required.");
    //Clear();
}


var TotalAmountBagMeal = 0; var TotalBaggAmount = 0; var TotalMealAmount = 0; var OriginalAmount;
function ChangeAmount(ID, DD) {
    var AmntBaggage = 0; var AmntMeals = 0; var PriceBaggageA = 0; var PriceBaggageC = 0; var PriceBaggageI = 0; var PriceMealA = 0; var PriceMealC = 0; var PriceMealI = 0;
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        for (var b = 0; b < $(".sel_Baggage" + i).length; b++) {
            if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic) {
                var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[b], function (p) { return p.Code == $(".sel_Baggage" + i)[b].value; })
                                .map(function (p) { return p; })[0];
                if (arrPax.PaxDetails[i].PaxType == "AD") {
                    if (arrBaggage.Price != 0) {
                        $('#BaggageAmount0' + b).text(arrBaggage.Price);
                        $('#MBaggageAmount0' + b).text(arrBaggage.Price);
                        AmntBaggage += parseFloat(arrBaggage.Price);
                    }
                }
                if (arrPax.PaxDetails[i].PaxType == "CH") {
                    if (arrBaggage.Price != 0) {
                        $('#BaggageAmount1' + b).text(arrBaggage.Price);
                        $('#MBaggageAmount1' + b).text(arrBaggage.Price);
                        AmntBaggage += parseFloat(arrBaggage.Price);
                    }
                }
                if (arrPax.PaxDetails[i].PaxType == "In") {
                    if (arrBaggage.Price != 0) {
                        $('#BaggageAmount2' + b).text(arrBaggage.Price);
                        $('#MBaggageAmount2' + b).text(arrBaggage.Price);
                        AmntBaggage += parseFloat(arrBaggage.Price);
                    }
                }
            }
            else {

                if (arrFlightDetails.JourneyType == "2" && arrFlightDetails.IsDomestic) {
                    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[b], function (p) { return p.Code == $(".sel_Baggage" + i)[b].value; })
                                                    .map(function (p) { return p; })[0];
                    if (arrPax.PaxDetails[i].PaxType == "AD") {
                        if (arrBaggage.Price != 0) {
                            $('#BaggageAmount0' + b).text(arrBaggage.Price);
                            $('#MBaggageAmount0' + b).text(arrBaggage.Price);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                    if (arrPax.PaxDetails[i].PaxType == "CH") {
                        if (arrBaggage.Price != 0) {
                            $('#BaggageAmount1' + b).text(arrBaggage.Price);
                            $('#MBaggageAmount1' + b).text(arrBaggage.Price);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                    if (arrPax.PaxDetails[i].PaxType == "In") {
                        if (arrBaggage.Price != 0) {
                            $('#BaggageAmount2' + b).text(arrBaggage.Price);
                            $('#MBaggageAmount2' + b).text(arrBaggage.Price);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                }
                else {
                    var arrBaggage = $.grep(arrFlightDetails.SSR.Baggage[b], function (p) { return p.Code == $(".sel_Baggage" + i)[b].value; })
                                                    .map(function (p) { return p; })[0];
                    if (arrPax.PaxDetails[i].PaxType == "AD") {
                        if (arrBaggage.Price != 0) {
                            PriceBaggageA += parseFloat(arrBaggage.Price);
                            $('#BaggageAmount0').text(PriceBaggageA);
                            $('#MBaggageAmount0').text(PriceBaggageA);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                    if (arrPax.PaxDetails[i].PaxType == "CH") {
                        if (arrBaggage.Price != 0) {
                            PriceBaggageC += parseFloat(arrBaggage.Price);
                            $('#BaggageAmount1').text(PriceBaggageC);
                            $('#MBaggageAmount1').text(PriceBaggageC);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                    if (arrPax.PaxDetails[i].PaxType == "In") {
                        if (arrBaggage.Price != 0) {
                            PriceBaggageI += parseFloat(arrBaggage.Price);
                            $('#BaggageAmount2').text(PriceBaggageI);
                            $('#MBaggageAmount2').text(PriceBaggageI);
                            AmntBaggage += parseFloat(arrBaggage.Price);
                        }
                    }
                }

            }
        }
    }
    for (var i = 0; i < arrPax.PaxDetails.length; i++) {
        for (var b = 0; b < $(".sel_DMeal" + i).length; b++) {
            var arrMeal = $.grep(arrFlightDetails.SSR.MealDynamic[b], function (p) { return p.Code == $(".sel_DMeal" + i)[b].value; })
                .map(function (p) { return p; })[0];
            if (arrPax.PaxDetails[i].PaxType == "AD") {
                if (arrMeal.Price != 0) {
                    PriceMealA += parseFloat(arrMeal.Price);
                    $('#MealAmount0').text(PriceMealA);
                    $('#MMealAmount0').text(PriceMealA);
                    AmntMeals += parseFloat(arrMeal.Price);
                }
            }
            if (arrPax.PaxDetails[i].PaxType == "CH") {
                if (arrMeal.Price != 0) {
                    PriceMealC += parseFloat(arrMeal.Price);
                    $('#MealAmount1').text(PriceMealC);
                    $('#MMealAmount1').text(PriceMealC);
                    AmntMeals += parseFloat(arrMeal.Price);
                }
            }
            if (arrPax.PaxDetails[i].PaxType == "In") {
                if (arrMeal.Price != 0) {
                    PriceMealI += parseFloat(arrMeal.Price);
                    $('#MealAmount2').text(PriceMealI);
                    $('#MMealAmount2').text(PriceMealI);
                    AmntMeals += parseFloat(arrMeal.Price);
                }
            }
        }
    }
    TotalBaggAmount = AmntBaggage;
    TotalMealAmount = AmntMeals;
    TotalAmountBagMeal = parseFloat(AmntBaggage) + parseFloat(AmntMeals);
    var TotalAmount = parseFloat(OriginalAmount.replace(",", "")) + parseFloat(TotalAmountBagMeal);
    TotalAmount = TotalAmount.toFixed(2);
    $('#TotalAmount').text(TotalAmount);
    $("#Total_Amount").val(TotalAmount);
    AmountForPayment = TotalAmount;
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetToolTips",
        data: '{"TotalAmount":"' + TotalAmount + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Tips = result.Tips;
                var html = '';
                var htmlM = '';
                htmlM += '<a class="Price"  href="#" data-container="body" data-html="true" data-content="' + Tips + '" rel="popover" data-placement="left">' + CurrencyClass + ' ' + TotalAmount + '</a>'
                html += '<a class="Price" id="TotalAmount" href="#" data-container="body" data-html="true" data-content="' + Tips + '" rel="popover" data-placement="left" style="float: right"><input type="hidden" id="TotalAmountOriginal" />' + CurrencyClass + ' ' + TotalAmount + '</a>'
                $(".Total").empty();
                $(".Total").append(html);
                $(".TotalForModal").empty();
                $(".TotalForModal").append(htmlM);
                $('.Price').popover({ trigger: "hover" });
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.Message);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });

}

function CheckAge(Id, Type) {
    var bValid = true;
    var today = new Date();
    if ($("#txt_DOB" + Id).val() != "") {
        var DOB_ = $("#txt_DOB" + Id).val().split("-");
        DOB_ = DOB_[1] + "-" + DOB_[0] + "-" + DOB_[2]
        var birthDate = new Date(DOB_);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        if (Type == "AD" && age < 12) {
            $("#lbl_DOB" + Id).text("Min Age Of Adult Must Be 12 Years")
            $('#lbl_DOB' + Id).show()
            bValid = false;
        }
        if (Type == "CH") {
            if (age > 11) {
                $("#lbl_DOB" + Id).text("Max Age Of Child Must Be 11 Years")
                $('#lbl_DOB' + Id).show()
                bValid = false;
            }
            if (age < 2) {
                $("#lbl_DOB" + Id).text("Min Age Of Child Must Be 2 Years")
                $('#lbl_DOB' + Id).show()
                bValid = false;
            }
        }
        if (Type == "In") {
            if (age > 2) {
                $("#lbl_DOB" + Id).text("Max Age Of Child Must Be 2 Years")
                $('#lbl_DOB' + Id).show()
                bValid = false;
            }
        }
    }
    return bValid;
}

