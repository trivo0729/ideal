﻿var globe_urlParamDecoded = 0;
var global_CategoryCount = [];
var global_HotelItinerary = [];
$(document).ready(function () {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var urlParameter = atob(url[0]).split('=');
    globe_urlParamDecoded = urlParameter[1];
    GetPackageDetail(globe_urlParamDecoded);
    RandomNo = getRandomInt(10000, 99999);
    InvoiceNO();
});

function InvoiceNO() {
    rnd = 'INVC-' + RandomNo;
    $("#lblInvoiceNo").val(rnd);
    $("#order_id").val(rnd);
}

function GetCategory(id) {
    if (id == 1) {
        return "Standard";
    }
    else if (id == 2) {
        return "Deluxe";
    }
    else if (id == 3) {
        return "Premium";
    }
    else if (id == 4) {
        return "Luxury";
    }
    else {
        return "No Category";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}

function GetInclusion(inclusion) {
    var html = "";
    for (var i = 0; i < inclusion.length - 1; i++) {

        if (inclusion[i] == 1) {

            html += '<li>Airfare with Airport transfer</li>'
        }

        else if (inclusion[i] == 2) {

            html += '<li>Sight Seeing</li>'
        }
        else if (inclusion[i] == 3) {

            html += '<li>Breakfast</li>'
        }
        else if (inclusion[i] == 4) {

            html += '<li>Tour Guide</li>'
        }
        else if (inclusion[i] == 5) {

            html += '<li>Lunch</li>'
        }
        else if (inclusion[i] == 6) {

            html += '<li>Dinner</li>'
        }
    }
    return html;
}
function GetItineraryVal(Itinerary, tabindex, index) {
    if (index == 1) {
        return Itinerary[tabindex].sItinerary_1.split('^-^');
    }
    else if (index == 2) {
        return Itinerary[tabindex].sItinerary_2.split('^-^');
    }
    else if (index == 3) {
        return Itinerary[tabindex].sItinerary_3.split('^-^');
    }
    else if (index == 4) {
        return Itinerary[tabindex].sItinerary_4.split('^-^');
    }
    else if (index == 5) {
        return Itinerary[tabindex].sItinerary_5.split('^-^');
    }
    else if (index == 6) {
        return Itinerary[tabindex].sItinerary_6.split('^-^');
    }
    else if (index == 7) {
        return Itinerary[tabindex].sItinerary_7.split('^-^');
    }
    else if (index == 8) {
        return Itinerary[tabindex].sItinerary_8.split('^-^');
    }
    else if (index == 9) {
        return Itinerary[tabindex].sItinerary_9.split('^-^');
    }
    else if (index == 10) {
        return Itinerary[tabindex].sItinerary_10.split('^-^');
    }
    else if (index == 11) {
        return Itinerary[tabindex].sItinerary_11.split('^-^');
    }
    else if (index == 12) {
        return Itinerary[tabindex].sItinerary_12.split('^-^');
    }
    else if (index == 13) {
        return Itinerary[tabindex].sItinerary_13.split('^-^');
    }
    else if (index == 14) {
        return Itinerary[tabindex].sItinerary_14.split('^-^');
    }
    else if (index == 15) {
        return Itinerary[tabindex].sItinerary_15.split('^-^');
    }
}

var List_PackageDetail = new Array();
var PackageImage = new Array();
var Itinerary = new Array();
var PackagePricing = new Array();
var Hotels = new Array();

function GetPackageDetail(nID) {
    $("#packagedloader").show();
    $.ajax({
        url: "PackageHandler.asmx/GetPackageDetail",
        type: "post",
        data: '{"Id":"' + nID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {

                alert("No packages found");
            }
            else if (result.retCode == 1) {
                List_PackageDetail = result.Arr;
                PackageImage = result.PackImg;
                Itinerary = result.Itn;
                PackagePricing = result.Pric;
                Hotels = result.ItnHotels;
                HotelImgs = result.HotelImgs;
                GetDetails();
                GetSlidImg();
                GetTab();
                GetPackgDescription();
                var from = List_PackageDetail[0].dValidityFrom.split(' ');
                var To = List_PackageDetail[0].dValidityTo.split(' ');
                $("#dspPackageName").text(List_PackageDetail[0].sPackageName);
                var City = List_PackageDetail[0].sPackageDestination.split("|")
                $("#dspCitys").text(City[0]);
                $("#dspNDays").text(List_PackageDetail[0].nDuration);
                $("#dspCheckIn").text(from[0]);
                $("#dspCheckOut").text(To[0]);
                $("#Amount").val(PackagePricing[0].dSingleAdult);
                $("#packagedloader").hide();
                $("#sdb").show();
            }
        },
        error: function () {
            $("#packagedloader").hide();
            alert('Errror in getting Product details, Please try again!');
        }
    });
}


function GetHotelName(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return Hotels[k].sHotelName_1;
    }
    else if (itineraryIndex == 2) {
        return Hotels[k].sHotelName_2;
    }
    else if (itineraryIndex == 3) {
        return Hotels[k].sHotelName_3;
    }
    else if (itineraryIndex == 4) {
        return Hotels[k].sHotelName_4;
    }
    else if (itineraryIndex == 5) {
        return Hotels[k].sHotelName_5;
    }
    else if (itineraryIndex == 6) {
        return Hotels[k].sHotelName_6;
    }
    else if (itineraryIndex == 7) {
        return Hotels[k].sHotelName_7;
    }
    else if (itineraryIndex == 8) {
        return Hotels[k].sHotelName_8;
    }
    else if (itineraryIndex == 9) {
        return Hotels[k].sHotelName_9;
    }
    else if (itineraryIndex == 10) {
        return Hotels[k].sHotelName_10;
    }
    else if (itineraryIndex == 11) {
        return Hotels[k].sHotelName_11;
    }
    else if (itineraryIndex == 12) {
        return Hotels[k].sHotelName_12;
    }
    else if (itineraryIndex == 13) {
        return Hotels[k].sHotelName_13;
    }
    else if (itineraryIndex == 14) {
        return Hotels[k].sHotelName_14;
    }
}
function GetHotelDescription(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return Hotels[k].sHotelDescrption_1;
    }
    else if (itineraryIndex == 2) {
        return Hotels[k].sHotelDescrption_2;
    }
    else if (itineraryIndex == 3) {
        return Hotels[k].sHotelDescrption_3;
    }
    else if (itineraryIndex == 4) {
        return Hotels[k].sHotelDescrption_4;
    }
    else if (itineraryIndex == 5) {
        return Hotels[k].sHotelDescrption_5;
    }
    else if (itineraryIndex == 6) {
        return Hotels[k].sHotelDescrption_6;
    }
    else if (itineraryIndex == 7) {
        return Hotels[k].sHotelDescrption_7;
    }
    else if (itineraryIndex == 8) {
        return Hotels[k].sHotelDescrption_8;
    }
    else if (itineraryIndex == 9) {
        return Hotels[k].sHotelDescrption_9;
    }
    else if (itineraryIndex == 10) {
        return Hotels[k].sHotelDescrption_10;
    }
    else if (itineraryIndex == 11) {
        return Hotels[k].sHotelDescrption_11;
    }
    else if (itineraryIndex == 12) {
        return Hotels[k].sHotelDescrption_12;
    }
    else if (itineraryIndex == 13) {
        return Hotels[k].sHotelDescrption_13;
    }
    else if (itineraryIndex == 14) {
        return Hotels[k].sHotelDescrption_14;
    }
}

function GetHotelImages(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return HotelImgs[k].sHotelImage1;
    }
    else if (itineraryIndex == 2) {
        return HotelImgs[k].sHotelImage2;
    }
    else if (itineraryIndex == 3) {
        return HotelImgs[k].sHotelImage3;
    }
    else if (itineraryIndex == 4) {
        return HotelImgs[k].sHotelImage4;
    }
    else if (itineraryIndex == 5) {
        return HotelImgs[k].sHotelImage5;
    }
    else if (itineraryIndex == 6) {
        return HotelImgs[k].sHotelImage6;
    }
    else if (itineraryIndex == 7) {
        return HotelImgs[k].sHotelImage7;
    }
    else if (itineraryIndex == 8) {
        return HotelImgs[k].sHotelImage8;
    }
    else if (itineraryIndex == 9) {
        return HotelImgs[k].sHotelImage9;
    }
    else if (itineraryIndex == 10) {
        return HotelImgs[k].sHotelImage10;
    }
    else if (itineraryIndex == 11) {
        return HotelImgs[k].sHotelImage11;
    }
    else if (itineraryIndex == 12) {
        return HotelImgs[k].sHotelImage12;
    }
    else if (itineraryIndex == 13) {
        return HotelImgs[k].sHotelImage13;
    }
    else if (itineraryIndex == 14) {
        return HotelImgs[k].sHotelImage14;
    }
}

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}


function GetDetails() {
    try {
        var html = "";
        $("#PackageRight").empty();
        var sInventory = List_PackageDetail[0].sInventory;
        if (sInventory == "0") {
            sInventory = "On Request";
        }
        else {
            sInventory = "Fixed no of Packages: &nbsp;" + sInventory;
        }
        var Vfrom = List_PackageDetail[0].dValidityFrom.split(' ');
        var VTo = List_PackageDetail[0].dValidityTo.split(' ');
        var City = List_PackageDetail[0].sPackageDestination.split("|");
        html += '<ul>                                                               '
        html += '<li>                                                               '
        html += '<i class="soap-icon-bag circle"></i>                       '
        html += ' <p> ' + List_PackageDetail[0].sPackageName + '</p>            '
        html += '</li>                                                              '
        html += '<li>                                                               '
        html += '<i class="soap-icon-departure circle"></i>                       '
        html += ' <p> ' + City[0] + '</td></p>            '
        html += '</li>                                                              '
        html += '<li>                                                               '
        html += '<i class="soap-icon-clock circle"></i>                       '
        html += ' <p> ' + List_PackageDetail[0].nDuration + ' Days</p>            '
        html += '</li>                                                              '
        html += '<li>                                                               '
        html += '<i class="soap-icon-calendar-1 circle"></i>                       '
        html += ' <p> ' + Vfrom[0] + '</p>            '
        html += '</li>                                                              '
        html += '<li>                                                               '
        html += '<i class="soap-icon-calendar-1 circle"></i>                       '
        html += ' <p> ' + VTo[0] + '</p>            '
        html += '</li>                                                              '
        html += '</ul>                                                              '
        $("#PackageRight").html(html);
    }
    catch (e) {

    }

}

function GetSlidImg() {
    try {
        var html = "";
        $("#PackgImg").empty();

        var Images = PackageImage[0].ImageArray.split("^_^");
        var PackageId = PackageImage[0].nPackageID;

        html += '<div class="photo-gallery style1" data-animation="slide" data-sync="#PackgImg .image-carousel">'
        html += '<ul class="slides">'
        for (var i = 0; i < Images.length - 1; i++) {
            var Url = 'http://admin.Vacaaay.com/ImagesFolder/' + PackageId + '/' + Images[i]
            html += '    <li><img style="height: 350px;" src="' + Url + '" alt="" /></li>'
        }
        html += '</ul>'
        html += '</div>'

        html += '<div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#PackgImg .photo-gallery">'
        html += '<ul class="slides">'
        for (var j = 0; j < Images.length - 1; j++) {
            var Url = 'http://admin.Vacaaay.com/ImagesFolder/' + PackageId + '/' + Images[j]
            html += '<li><img src="' + Url + '" alt="" /></li>'
        }
        html += '</ul>'
        html += '</div>'
        $("#PackgImg").html(html);
        var tpj = jQuery;
        tjq('.image-carousel').each(function () {
            displayImageCarousel(tjq(this));
        });
        tjq('.photo-gallery').each(function () {
            displayPhotoGallery(tjq(this));
        });
    }

    catch (e) {

    }
}

function GetPackgDescription() {
    try {
        var html = "";
        $("#PackgDescription").empty();
        html += '<div class="tab-content">'
        for (var i = 0; i < PackagePricing.length; i++) {
            if (i == 0) {
                html += '                <div class="tab-pane fade in active" id="' + PackagePricing[i].sCategoryName + '">'
                html += '                    <ul class="tabs full-width">'
                html += '                        <li class="active"><a href="#Pricing_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Pricing</a></li>'
                html += '                        <li><a href="#Itinerary_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Itinerary</a></li>'
                html += '                        <li><a href="#HotelDetails_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Hotel Details</a></li>'
                html += '                    </ul>'
                html += '                    <div class="tab-content">'
                html += '                        <div class="tab-pane fade in active" id="Pricing_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-sm-12 col-lg-12 table-cell cruise-itinerary">'
                html += '        <table>'
                html += '            <thead>'
                html += '                <tr>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>DOUBLE SHARING<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>EXTRA ADULT<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>KID W/O BED<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>KID WITH BED<b/></th>'
                html += '                </tr>'
                html += '            </thead>'
                html += '            <tbody>'
                html += '                <tr>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dCouple + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dExtraAdult + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dKidWOBed + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dKidWBed + '</td>'
                html += '                </tr>'
                html += '            </tbody>'
                html += '        </table>'

                html += '<div class="row">'
                html += '<div class="col-sm-6">'
                html += '<div class="toggle-container box" id="accordion_' + i + '">'
                html += '                <div class="panel style2">'
                html += '                    <h4 class="panel-title">'
                html += '                        <a href="#acc1_' + i + '" data-toggle="collapse" data-parent="#accordion_' + i + '">Package inclusion</a>'
                html += '                    </h4>'
                html += '                    <div class="panel-collapse collapse in" id="acc1_' + i + '">'
                html += '                        <div class="panel-content">'
                html += '<ul class="features check">'
                if (PackagePricing[i].sStaticInclusions == null) {

                }
                else {

                    var Inclusion = PackagePricing[i].sStaticInclusions.split(',');
                }
                html += GetInclusion(Inclusion);

                if (PackagePricing[i].sDynamicInclusion != "" || PackagePricing[i].sDynamicInclusion != null || PackagePricing[i].sDynamicInclusion != undefined) {
                    var DInclusion = PackagePricing[i].sDynamicInclusion.split(',');
                    for (var d = 0; d < DInclusion.length - 1; d++) {
                        html += '<li>' + DInclusion[d] + '</li>'
                    }
                }

                html += '</ul>'
                html += '                        </div>'
                html += '                    </div>'
                html += '                </div>'
                html += '            </div>'
                html += '</div>'

                html += '<div class="col-sm-6">'
                html += '<div class="toggle-container box" id="accordions_' + i + '">'

                html += '                <div class="panel style2">'
                html += '                    <h4 class="panel-title">'
                html += '                        <a href="#acc2_' + i + '" data-toggle="collapse" data-parent="#accordions_' + i + '">Package exclusion </a>'
                html += '                    </h4>'
                html += '                    <div class="panel-collapse collapse in" id="acc2_' + i + '">'
                html += '                        <div class="panel-content">'
                html += '<ul class="features check">'
                if (PackagePricing[i].sStaticExclusion == null) {

                }
                else {

                    var Exclusion = PackagePricing[i].sStaticExclusion.split(',');
                }
                html += GetInclusion(Exclusion);

                if (PackagePricing[i].sDynamicExclusion != "" || PackagePricing[i].sDynamicExclusion != null || PackagePricing[i].sDynamicExclusion != undefined) {
                    var DExclusion = PackagePricing[i].sDynamicExclusion.split(',');
                    for (var e = 0; e < DExclusion.length - 1; e++) {
                        html += '<li>' + DExclusion[e] + '</li>'
                    }
                }

                html += '</ul>'
                html += '                        </div>'
                html += '                    </div>'
                html += '                </div>'
                html += '            </div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'

                html += '</div>'

                html += '<div class="tab-pane fade" id="Itinerary_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-md-12">'
                html += '<div class="notification-area">'
                var sTineraryData = '';
                var sHotelData = '';
                for (var itineraryCount = 0; itineraryCount < List_PackageDetail[0].nDuration ; itineraryCount++) {
                    var sItineraryArray = GetItineraryVal(Itinerary, Itinerary.length - 1, itineraryCount + 1);
                    if (sItineraryArray[0] == undefined) {
                        sItineraryArray[0] = "";
                    }
                    if (sItineraryArray[1] == undefined) {
                        sItineraryArray[1] = "";
                    }
                    if (sItineraryArray[2] == undefined) {
                        sItineraryArray[2] = "";
                    }
                    html += '<div class="info-box block" id="Days">'
                    html += '<div class="date"><label class="month">Day</label><label class="date">' + (itineraryCount + 1) + '</label></div>'
                    html += '<p>' + sItineraryArray[2] + '</p>'
                    html += '</div><br/>'
                }
                html += '</div>'

                html += '</div>'

                html += '</div>'
                html += '<div class="tab-pane fade" id="HotelDetails_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-md-12">'

                for (var hotelCount = 0; hotelCount < List_PackageDetail[0].nDuration - 1; hotelCount++) {
                    var sHotelName = GetHotelName(i, hotelCount);
                    var sHotelDescription = GetHotelDescription(i, hotelCount);
                    if (sHotelName == null || sHotelName == undefined) {
                        sHotelName = "";
                    }
                    if (sHotelDescription == null || sHotelDescription == undefined) {
                        sHotelDescription = "";
                    }
                    var productfolder = List_PackageDetail[0].nID;
                    var sHotelImagename = GetHotelImages(i, hotelCount);
                    var url = 'http://admin.Vacaaay.com/HotelImagesFolder/' + productfolder + '/' + PackagePricing[i].sCategoryName + '/' + sHotelImagename;

                    html += '<div class="clearfix main-iti-divs" style="padding-top:5px;">'
                    html += '     <p class="itinere-head" style="color:#000"><span class="dayss">Day ' + (hotelCount + 1) + '</span> ' + sHotelName + '</p>'
                    html += '     <div class="row">'
                    html += '         <div class="col-sm-3 text-center">'
                    html += '                  <img class="img-responsive img-thumbnail" src="' + url + '" alt=""></div>'
                    html += '         <div class="col-sm-9 text-center-res">'
                    html += '             <p class="content-tans-p">' + sHotelDescription + '</p>'
                    html += '         </div>'
                    html += '     </div>'
                    html += '</div>'
                }
                html += '</div>'
                html += '                        </div>'
                html += '                    </div>'
                html += '                    </div>'
            }

            else {

                html += '                <div class="tab-pane fade" id="' + PackagePricing[i].sCategoryName + '">'
                html += '                    <ul class="tabs full-width">'
                html += '                        <li class="active"><a href="#Pricing_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Pricing</a></li>'
                html += '                        <li><a href="#Itinerary_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Itinerary</a></li>'
                html += '                        <li><a href="#HotelDetails_' + PackagePricing[i].sCategoryName + '" data-toggle="tab">Hotel Details</a></li>'
                html += '                    </ul>'
                html += '                    <div class="tab-content">'
                html += '                        <div class="tab-pane fade in active" id="Pricing_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-sm-12 col-lg-12 table-cell cruise-itinerary">'
                html += '        <table>'
                html += '            <thead>'
                html += '                <tr>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>DOUBLE SHARING<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>EXTRA ADULT<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>KID W/O BED<b/></th>'
                html += '                    <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>KID WITH BED<b/></th>'
                html += '                </tr>'
                html += '            </thead>'
                html += '            <tbody>'
                html += '                <tr>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dCouple + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dExtraAdult + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dKidWOBed + '</td>'
                html += '                    <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + PackagePricing[i].dKidWBed + '</td>'
                html += '                </tr>'
                html += '            </tbody>'
                html += '        </table>'

                html += '<div class="row">'
                html += '<div class="col-sm-6">'
                html += '<div class="toggle-container box" id="accordion_' + i + '">'
                html += '                <div class="panel style2">'
                html += '                    <h4 class="panel-title">'
                html += '                        <a href="#acc1_' + i + '" data-toggle="collapse" data-parent="#accordion_' + i + '">Package inclusion</a>'
                html += '                    </h4>'
                html += '                    <div class="panel-collapse collapse in" id="acc1_' + i + '">'
                html += '                        <div class="panel-content">'
                html += '<ul class="features check">'
                if (PackagePricing[i].sStaticInclusions == null) {

                }
                else {

                    var Inclusion = PackagePricing[i].sStaticInclusions.split(',');
                }
                html += GetInclusion(Inclusion);

                if (PackagePricing[i].sDynamicInclusion != "" || PackagePricing[i].sDynamicInclusion != null || PackagePricing[i].sDynamicInclusion != undefined) {
                    var DInclusion = PackagePricing[i].sDynamicInclusion.split(',');
                    for (var d = 0; d < DInclusion.length - 1; d++) {
                        html += '<li>' + DInclusion[d] + '</li>'
                    }
                }

                html += '</ul>'
                html += '                        </div>'
                html += '                    </div>'
                html += '                </div>'
                html += '            </div>'
                html += '</div>'

                html += '<div class="col-sm-6">'
                html += '<div class="toggle-container box" id="accordions_' + i + '">'
                html += '                <div class="panel style2">'
                html += '                    <h4 class="panel-title">'
                html += '                        <a href="#acc2_' + i + '" data-toggle="collapse" data-parent="#accordions_' + i + '">Package exclusion </a>'
                html += '                    </h4>'
                html += '                    <div class="panel-collapse collapse in" id="acc2_' + i + '">'
                html += '                        <div class="panel-content">'
                html += '<ul class="features check">'
                if (PackagePricing[i].sStaticExclusion == null) {

                }
                else {

                    var Exclusion = PackagePricing[i].sStaticExclusion.split(',');
                }
                html += GetInclusion(Exclusion);

                if (PackagePricing[i].sDynamicExclusion != "" || PackagePricing[i].sDynamicExclusion != null || PackagePricing[i].sDynamicExclusion != undefined) {
                    var DExclusion = PackagePricing[i].sDynamicExclusion.split(',');
                    for (var e = 0; e < DExclusion.length - 1; e++) {
                        html += '<li>' + DExclusion[e] + '</li>'
                    }
                }

                html += '</ul>'
                html += '                        </div>'
                html += '                    </div>'
                html += '                </div>'
                html += '            </div>'
                html += '</div>'
                html += '</div>'

                html += '</div>'
                html += '</div>'

                html += '<div class="tab-pane fade" id="Itinerary_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-md-12">'
                html += '<div class="notification-area">'
                var sTineraryData = '';
                var sHotelData = '';
                for (var itineraryCount = 0; itineraryCount < List_PackageDetail[0].nDuration ; itineraryCount++) {
                    var sItineraryArray = GetItineraryVal(Itinerary, Itinerary.length - 1, itineraryCount + 1);
                    if (sItineraryArray[0] == undefined) {
                        sItineraryArray[0] = "";
                    }
                    if (sItineraryArray[1] == undefined) {
                        sItineraryArray[1] = "";
                    }
                    if (sItineraryArray[2] == undefined) {
                        sItineraryArray[2] = "";
                    }
                    html += '<div class="info-box block" id="Days">'
                    html += '<div class="date"><label class="month">Day</label><label class="date">' + (itineraryCount + 1) + '</label></div>'
                    html += '<p>' + sItineraryArray[2] + '</p>'
                    html += '</div><br/>'
                }
                html += '</div>'
                html += '</div>'

                html += '</div>'
                html += '<div class="tab-pane fade" id="HotelDetails_' + PackagePricing[i].sCategoryName + '">'

                html += '<div class="col-md-12">'
                for (var hotelCount = 0; hotelCount < List_PackageDetail[0].nDuration - 1; hotelCount++) {
                    var sHotelName = GetHotelName(i, hotelCount);
                    var sHotelDescription = GetHotelDescription(i, hotelCount);
                    if (sHotelName == null || sHotelName == undefined) {
                        sHotelName = "";
                    }
                    if (sHotelDescription == null || sHotelDescription == undefined) {
                        sHotelDescription = "";
                    }
                    var productfolder = List_PackageDetail[0].nID;
                    var sHotelImagename = GetHotelImages(i, hotelCount);
                    var url = 'http://admin.Vacaaay.com/HotelImagesFolder/' + productfolder + '/' + PackagePricing[i].sCategoryName + '/' + sHotelImagename;

                    html += '<div class="clearfix main-iti-divs" style="padding-top:5px;">'
                    html += '     <p class="itinere-head" style="color:#000"><span class="dayss">Day ' + (hotelCount + 1) + '</span> ' + sHotelName + '</p>'
                    html += '     <div class="row">'
                    html += '         <div class="col-sm-3 text-center">'
                    html += '                  <img class="img-responsive img-thumbnail" src="' + url + '" alt=""></div>'
                    html += '         <div class="col-sm-9 text-center-res">'
                    html += '             <p class="content-tans-p">' + sHotelDescription + '</p>'
                    html += '         </div>'
                    html += '     </div>'
                    html += '</div>'
                }
                html += '</div>'

                html += '                        </div>'
                html += '                    </div>'
                html += '                    </div>'

            }

        }

        html += '<hr />'
        html += '<div class="toggle-container box">'
        html += '<div class="panel style2">'
        html += '       <h4 class="panel-title">'
        html += '           <a class="collapsed" href="#tgg4" data-toggle="collapse" aria-expanded="false">Description</a>'
        html += '       </h4>'
        html += '       <div class="panel-collapse collapse" id="tgg4" aria-expanded="false" style="height: 0px;">'
        html += '           <div class="panel-content">'
        html += '               <p>' + List_PackageDetail[0].sPackageDescription + '</p>'
        html += '           </div>'
        html += '       </div>'
        html += '   </div>'
        html += '   <div class="panel style2">'
        html += '       <h4 class="panel-title">'
        html += '           <a href="#tgg5" data-toggle="collapse" aria-expanded="false" class="collapsed">Cancellation Policy</a>'
        html += '       </h4>'
        html += '       <div class="panel-collapse collapse" id="tgg5" aria-expanded="false" style="height: 0px;">'
        html += '           <div class="panel-content">'
        html += '<div class=""><b>' + List_PackageDetail[0].dCancelCharge + ' % Cancellation charge will be applied before ' + List_PackageDetail[0].nCancelDays + ' days of arriving date </b>'
        html += '</div>'
        html += '           </div>'
        html += '       </div>'
        html += '   </div>'
        html += '   <div class="panel style2">'
        html += '       <h4 class="panel-title">'
        html += '           <a href="#tgg6" data-toggle="collapse" aria-expanded="false" class="collapsed">Terms & Condition</a>'
        html += '       </h4>'
        html += '       <div class="panel-collapse collapse" id="tgg6" aria-expanded="false" style="height: 0px;">'
        html += '           <div class="panel-content">'
        html += '<div class="" id="terms">' + List_PackageDetail[0].sTermsCondition + ''
        html += '</div>'
        html += '           </div>'
        html += '       </div>'
        html += '   </div>'
        html += '   </div>'
        html += '</div>'
        html += '                    </div>'
        $("#PackgDescription").append(html);
        $("#terms ul").addClass("features check");

    } catch (e) {

    }


}

function BookMyTrip() {
    $("#BookingModal").modal("show")
}
function NoChild(Val) {
    if (Val == 0) {
        $("#div_FirstChild").css("display", "none")
        $("#div_SecondChild").css("display", "none")
        $("#Childs").atrr("class", "col-md-12")
    }
    else if (Val == 1) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "none")
    }
    else if (Val == 2) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "")
    }
}

var Name, MobileNo, Email, noAdults, noChild, TarvelDate, AddOnReq, ChildAges = "0";
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function SendMail() {
    var from = List_PackageDetail[0].dValidityFrom.split(' ');
    var To = List_PackageDetail[0].dValidityTo.split(' ');
    var Amt = PackagePricing[0].dSingleAdult;
    var bValid = true;
    Name = $("#txt_name").val()
    MobileNo = $("#txt_phone").val()
    Email = $("#txt_email").val()
    noAdults = $("#Select_Adults").val()
    noChild = $("#Select_CHILD").val()
    TarvelDate = $("#txt_date").val()
    AddOnReq = $("#txt_Remark").val()
    bValid = ValidateMail();
    if (bValid == true) {
        var data = {
            nID: globe_urlParamDecoded,
            Name: Name,
            MobileNo: MobileNo,
            Email: Email,
            noAdults: noAdults,
            noChild: noChild,
            ChildAges: ChildAges,
            TarvelDate: TarvelDate,
            AddOnReq: AddOnReq,
            PackageName: $("#dspPackageName").text(),
            Amt: Amt,
            StartDate: from[0],
            TillDate: To[0],
        }
        var Jason = JSON.stringify(data)
        $.ajax({
            type: "POST",
            url: "PackageHandler.asmx/BookPackage",
            data: Jason,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result == 1) {
                    $("#BookingModal").modal("hide")
                    alert("Thanks For Booking, We Will Update you when it is Conform by Administrator..", null)

                }
            },
            error: function () {
                alert("An error occured", null)
            }
        });
    }
}
function ValidateMail() {
    if (Name == "") {
        $("#txt_name").focus();
        alert("Please Insert  Passenger Name", "txt_name")
        return false;
    }
    if (MobileNo == "") {
        $("#txt_name").focus();
        alert("Please Insert  Mobile No", "txt_phone")
        return false;
    }
    if (Email == "") {
        $("#txt_email").focus();
        alert("Please Insert  MailId", "txt_email")
        return false;
    }
    if (!emailReg.test(Email)) {
        $("#txt_email").focus();
        alert("Please Insert Valid MailId", "txt_email")
        return false;
    }
    if (noChild != "0") {
        if (noChild == "1") {
            ChildAges = $("#Select_AgeChildSecond1").val()
        }
        else if (noChild == "2") {
            ChildAges = $("#Select_AgeChildSecond1").val() + "," + $("#Select_AgeChildSecond2").val()

        }
        return true
    }
    if (TarvelDate == "") {
        $("#txt_date").focus();
        alert("Please Insert Tarvel Date", "txt_date")
        return false;
    }
    if (AddOnReq == "") {
        $("#txt_Remark").focus();
        alert("Please Insert Add Request", "txt_Remark")
        return false;
    }
    return true
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var count = "";
function GetTab() {

    try {
        $("#PkgTab").empty();
        var html = "";
        var Class = "";
        for (var count = 0; count < PackagePricing.length; count++) {
            if (count == 0) {
                Class = "PkgType active"
                html += '<li class="' + Class + '"><a href="#' + PackagePricing[count].sCategoryName + '" id="clickTab_' + count + ' "  data-toggle="tab">' + PackagePricing[count].sCategoryName + '</a></li>';
            }
            else {
                Class = "PkgType"
                html += '<li class="' + Class + '"><a href="#' + PackagePricing[count].sCategoryName + '"  data-toggle="tab">' + PackagePricing[count].sCategoryName + '</a></li>';
            }
            $("#PkgTab").html(html)
        }
    } catch (e) {

    }
}

function Booking() {
    for (var i = 0; i < $(".PkgType").length ; i++) {
        $($(".PkgType")[i]).hasClass(".active")
        if ($($(".PkgType")[i])[0].className == "PkgType active") {
            window.location.href = "PackageBooking.aspx?CatName=" + $($(".PkgType")[i])[0].innerText + "&Id=" + globe_urlParamDecoded;
        }
    }
}


function PackageMailModal() {
    $("#PackageMailModal").modal('show');
}

function SendPackageMail() {
    $("#lbl_Email").hide();
    var Email = $("#Email").val();
    var Name = $("#Name").val();
    var Remark = $("#Remark").val();

    if (Email == "") {
        $("#lbl_Email").show();
        return false;
    }
    var data = {
        ID: globe_urlParamDecoded,
        Name: Name,
        Email: Email,
        Remark: Remark
    }
    var Jason = JSON.stringify(data)
    $.ajax({
        type: "POST",
        url: "PackageHandler.asmx/SendPackagesDetails",
        data: Jason,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result == true) {
                alert("Package Details Sent On Your Email Successfully  ..")
                $("#PackageMailModal").modal('hide');
            }
        },
        error: function () {
            alert("An error occured")
        }
    });
}