﻿$(document).ready(function () {
    GetMerchantDetails()
});


function GetMerchantDetails() {
    $.ajax({
        type: "POST",
        url: "B2CCustomerLoginHandler.asmx/GetMerchantDetails",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
                return false;
            }
            else if (result.retCode == 1) {
               // $('#instId').val(result.Installation_ID);
               // $('#amount').val(result.Amount);
            }
            else {
                alert('Something went wrong.');
            }
        },
        error: function () {
        }
    });
}