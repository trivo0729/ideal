﻿$(function () {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var url = url[0].split('=')[1].replace(/%20/g, " ").split(',')[0];
    if (url == "Domestic" || url == "International")
        GetAllPackages(url);
    else
        GetPackagesByCity(url);
    //var Type = url[1].split('=')[1];
    //if (CityName == "") {
    //    GetAllPackages(Type);
    //}
    //else {
    //    GetPackagesByCity(CityName, Type);
    //}
});


function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function GetCategory(id) {
    if (id == 1) {
        return "Standard";
    }
    else if (id == 2) {
        return "Deluxe";
    }
    else if (id == 3) {
        return "Premium";
    }
    else if (id == 4) {
        return "Luxury";
    }
    else {
        return "No Category";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}

function GetAllPackages(Type) {
    $("#packageloader").show();
    $.ajax({
        url: "PackageHandler.asmx/GetAllPackages",
        type: "post",
        data: '{"Type":"' + Type + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {
                var html = '';
                html += '<center><div class="col-sn-12 text-center" style="padding:65px 0px;">'
                html += '<h1 style="margin-bottom:0px;"> <i class="fa fa-exclamation-triangle" style="font-size: 71px;color: #4cb9f1;"></i></h1>'
                html += '<h2 style=" font-size:58px;">SORRY!!</h2>'
                html += '<h2 style="font-size:21px; color: #737373;">PACKAGE NOT FOUND</h2>'
                html += '</div></center>';
                $("#div_Nopackage").html(html);
                $("#Nopackage").show();
                $("#packageloader").hide();
            }
            else if (result.retCode == 1) {
                $("#div_package").empty();
                List_Packages = result.Arr;
                var html = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryRow = '';
                    for (var sCate = 0; sCate < sCategory.length; sCate++) {
                        if (sCategory[sCate] != "") {
                            sCategoryRow += '<input type=button class="button btn-mini sky-blue2" value="' + GetCategory(sCategory[sCate]) + '" />&nbsp;';
                        }
                    }
                    var sTheme = List_Packages[i].sPackageThemes.split(',');
                    var sThemeRow = '';
                    for (var sThem = 0; sThem < sTheme.length; sThem++) {
                        if (sTheme[sThem] != "") {

                            sThemeRow += '<input type=button class="button btn-mini dull-blue" value="' + GetPackageType(sTheme[sThem]) + '"/>&nbsp;';
                        }
                    }
                    var Img = List_Packages[i].ImageArray.split("^_^")[0];
                    var PackageId = List_Packages[i].nID;
                    var City = List_Packages[i].sPackageDestination.split('|')
                    var Url = 'http://admin.Vacaaay.com/ImagesFolder/' + PackageId + '/' + Img

                    html += ' <article class="box">'
                    html += '<figure class="col-sm-4">'
                    html += '    <a title="" class="hover-effect">'
                    html += '        <img width="270" height="160" style="width: 100%;height: 200px;" alt="" src="' + Url + '"></a>'
                    html += '</figure>'
                    html += '<div class="details col-sm-8">'
                    html += '    <div class="clearfix">'
                    html += '        <h4 class="box-title pull-left">' + List_Packages[i].sPackageName + '<small>' + List_Packages[i].nDuration + ' Days</small></h4>'
                    GetPrice(List_Packages[i].nID);
                    html += '        <span class="price pull-right"><i class="fa fa-inr"></i> <span id="' + List_Packages[i].nID + '"></span>'
                    html += '    </div>'
                    html += '    <div class="character clearfix">'
                    html += '        <div class="col-xs-3 date">'
                    html += '            <i class="soap-icon-clock yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Valid From</span><br>'
                    html += '                ' + List_Packages[i].dValidityFrom.split(" ")[0] + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '        <div class="col-xs-3 date">'
                    html += '            <i class="soap-icon-clock yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Valid Upto</span><br>'
                    html += '                ' + List_Packages[i].dValidityTo.split(" ")[0] + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '        <div class="col-xs-6 departure">'
                    html += '            <i class="soap-icon-departure yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Destination</span><br>'
                    html += '                ' + City + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '    </div>'
                    html += '    <div class="clearfix">'
                    html += '        <div class="review pull-left">'
                    html += '            <span><h4 class="box-title pull-left">Category :</h4> ' + sCategoryRow + '</span></br></br>'
                    html += '           <span><h4 class="box-title pull-left">Theme :</h4> ' + sThemeRow + '</span>'
                    html += '        </div>'
                    html += '        <button type="button" class="button btn-small pull-right" onclick="PackageImages(' + List_Packages[i].nID + ')">Details</button>'
                    html += '    </div>'
                    html += '</div>'
                    html += '</article>'
                }
                $("#div_package").html(html);
                $("#packageloader").hide();
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}


function GetPackagesByCity(CityName) {
    $("#packageloader").show();
    $.ajax({
        url: "PackageHandler.asmx/GetPackagesByCity",
        type: "post",
        data: '{"City":"' + CityName + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {
                var html = '';
                html += '<center><div class="col-sn-12 text-center" style="padding:65px 0px;">'
                html += '<h1 style="margin-bottom:0px;"> <i class="fa fa-exclamation-triangle" style="font-size: 71px;color: #4cb9f1;"></i></h1>'
                html += '<h2 style=" font-size:58px;">SORRY!!</h2>'
                html += '<h2 style="font-size:21px; color: #737373;">PACKAGE NOT FOUND</h2>'
                html += '</div></center>';
                $("#div_Nopackage").html(html);
                $("#Nopackage").show();
                $("#packageloader").hide();
            }
            else if (result.retCode == 1) {
                $("#div_package").empty();
                List_Packages = result.Arr;
                var html = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryRow = '';
                    for (var sCate = 0; sCate < sCategory.length; sCate++) {
                        if (sCategory[sCate] != "") {
                            sCategoryRow += '<input type=button class="button btn-mini sky-blue2" value="' + GetCategory(sCategory[sCate]) + '" />&nbsp;';
                        }
                    }
                    var sTheme = List_Packages[i].sPackageThemes.split(',');
                    var sThemeRow = '';
                    for (var sThem = 0; sThem < sTheme.length; sThem++) {
                        if (sTheme[sThem] != "") {

                            sThemeRow += '<input type=button class="button btn-mini dull-blue" value="' + GetPackageType(sTheme[sThem]) + '"/>&nbsp;';
                        }
                    }
                    var Img = List_Packages[i].ImageArray.split("^_^")[0];
                    var PackageId = List_Packages[i].nID;
                    var City = List_Packages[i].sPackageDestination.split('|')

                    var Url = 'http://admin.Vacaaay.com/ImagesFolder/' + PackageId + '/' + Img

                    html += ' <article class="box">'
                    html += '<figure class="col-sm-4">'
                    html += '    <a title="" class="hover-effect">'
                    html += '        <img width="270" height="160" style="width: 100%;height: 200px;" alt="" src="' + Url + '"></a>'
                    html += '</figure>'
                    html += '<div class="details col-sm-8">'
                    html += '    <div class="clearfix">'
                    html += '        <h4 class="box-title pull-left">' + List_Packages[i].sPackageName + '<small>' + List_Packages[i].nDuration + ' Days</small></h4>'
                    GetPrice(List_Packages[i].nID);
                    html += '        <span class="price pull-right"><i class="fa fa-inr"></i> <span id="' + List_Packages[i].nID + '"></span>'
                    html += '    </div>'
                    html += '    <div class="character clearfix">'
                    html += '        <div class="col-xs-3 date">'
                    html += '            <i class="soap-icon-clock yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Valid From</span><br>'
                    html += '                ' + List_Packages[i].dValidityFrom.split(" ")[0] + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '        <div class="col-xs-3 date">'
                    html += '            <i class="soap-icon-clock yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Valid Upto</span><br>'
                    html += '                ' + List_Packages[i].dValidityTo.split(" ")[0] + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '        <div class="col-xs-6 departure">'
                    html += '            <i class="soap-icon-departure yellow-color"></i>'
                    html += '            <div>'
                    html += '                <span class="skin-color">Destination</span><br>'
                    html += '                ' + City + ''
                    html += '            </div>'
                    html += '        </div>'
                    html += '    </div>'
                    html += '    <div class="clearfix">'
                    html += '        <div class="review pull-left">'
                    html += '            <span><h4 class="box-title pull-left">Category :</h4> ' + sCategoryRow + '</span></br></br>'
                    html += '           <span><h4 class="box-title pull-left">Theme :</h4> ' + sThemeRow + '</span>'
                    html += '        </div>'
                    html += '        <button type="button" class="button btn-small pull-right" onclick="PackageImages(' + List_Packages[i].nID + ')">Details</button>'
                    html += '    </div>'
                    html += '</div>'
                    html += '</article>'
                }
                $("#div_package").html(html);
                $("#packageloader").hide();
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}



function PackageImages(nID) {
    $(location).attr('href', '/PackageDetails.aspx?' + btoa('nID=' + nID));
}



function GetPrice(Id) {
    var data = { Id: Id };
    $.ajax({
        type: "POST",
        url: "PackageHandler.asmx/GetPrice",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Arr = result.Arr;
                $("#" + Id + "").text(Arr[0].dCouple)
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });


}