﻿$(document).ready(function () {
    RandomNo = getRandomInt(10000, 99999);
    InvoiceNO();

});

function InvoiceNO() {
    rnd = 'INVC-' + RandomNo;
    $("#order_id").val(rnd);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function PayOnline() {
    $("#PaymentModal").modal("show")
}

//function Pay() {

//    var FullName = $("#Name").val();
//    var CustAddress = $("#Address").val();
//    var EmailId = $("#Email").val();
//    var CustMobile = $("#Mobile").val();
//    var TotalAmount = $("#txt_Amount").val();
//    $("#Amount").val(TotalAmount);
//    if (FullName == "") {
//        alert("Plase Enter Your Name");
//        return false;
//    }
//    if (CustAddress == "") {
//        alert("Plase Enter Your Address");
//        return false;
//    }
//    if (EmailId == "") {
//        alert("Plase Enter Your Email");
//        return false;
//    }
//    if (CustMobile == "") {
//        alert("Plase Enter Your Mobile No");
//        return false;
//    }

//    if (TotalAmount == "") {
//        alert("Plase Enter Your Amount");
//        return false;
//    }

//    else {
//        $("#Btn_CCAvenue").click();
//    }
//}
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function Pay() {
    var bValid = true;
    var FullName = $("#Name").val();
    var Email = $("#Email").val();
    var Mobile = $("#Mobile").val();
    var Address = $("#Address").val();
    var total = $("#txt_Amount").val();
    if (FullName == "") {
        alert("Please Enter Full Name")
        bValid = false;
        return false;
    }
    if (Email == "") {
        alert("Please Enter Email")
        bValid = false;
        return false;
    }
    if (!emailReg.test(Email)) {
        alert("Please Insert Valid MailId")
        return false;
    }
    if (Mobile == "") {
        alert("Please Enter Mobile No")
        bValid = false;
        return false;
    }
    if (Address == "") {
        alert("Please Enter Address")
        bValid = false;
        return false;
    }
    if (total == "") {
        alert("Please Enter Amount");
        bValid = false;
        return false;
    }
    if (bValid == true) {
        var TotalFare = parseFloat(total.replace(",", ""));
        TotalFare = (TotalFare * 100).toFixed(2);
        var options = {
            "key": "rzp_live_M18B25XfgvEoqf",
            "amount": TotalFare, // 2000 paise = INR 20
            "name": "IdealTours",
            "description": "Payment",

            "image": "http://idealtours.in/images/logo.png",
            "handler": function (response) {
                window.alert('Payment Success');
                window.location.href = "http://idealtours.in"
            },
            "prefill": {
                "name": $("#Name").val(),
                "email": $("#Email").val(),
            },
            "notes": {
                "address": $("#Address").val(),
            },
            "theme": {
                "color": "#f58634"
            }
        };
        var rzp1 = new Razorpay(options);
        document.getElementById('rzp-button1').onclick = function (e) {
            rzp1.open();
            e.preventDefault();
        }
        document.getElementById("rzp-button1").click();
    }
}