﻿$(function () {
    GetStaticPackage()
});

var arrPackages = new Array();
function GetStaticPackage() {
    $.ajax({
        type: "POST",
        url: "../PackageHandler.asmx/GetStaticPackage",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrPackages = result.Packages;
                StaticPackagehtml();
            }
            else if (result.retCode == 0) {

            }
        },
        error: function () {
            alert("something went wrong");
        },
        complete: function () {
            $("#Loader").css("display", "none");
        }
    });
}

function StaticPackagehtml() {
    var html = '';
    $("#Static").empty();
    for (var i = 0; i < arrPackages.length; i++) {
        html += '<li class="box post">'
        html += '<figure>'
        html += '<a href="Contact.aspx" class="hover-effect">'
        if (arrPackages[i].image != "" || arrPackages[i].image != "") {
            html += '<img src="http://clickurtrip.net/StaticImage/' + arrPackages[i].image + '" onerror="ErrorImage(this)" alt="" /></a>'
        }
        else {
            html += '<img src="images/news/1.png" alt="" /></a>'
        }

        html += '<figcaption class="entry-date">'
        html += '<label class="date">' + arrPackages[i].StarRating + '</label>'
        html += '<label class="month">Star</label>'
        html += '</figcaption>'
        html += '</figure>'
        html += '<div class="details">'
        html += '<a href="Contact.aspx" class="button btn-small yellow-bg white-color">Book</a>'
        html += '<h4 class="post-title entry-title">' + arrPackages[i].Package_Name.toUpperCase() + '</h4>'
        html += '<div class="post-meta single-line-meta vcard">'
        html += '<span class="fn"><a rel="author" class="author">Valid Upto ' + arrPackages[i].ValidUpto + '</a></span>'
        html += '<span class="sep">|</span>'
        var Currency = arrPackages[i].Currency.toLowerCase();
        html += '<a href="#" class="comment"><i class="fa fa-' + Currency + '" aria-hidden="true"></i> ' + arrPackages[i].Pakages_Price + '</a>'
        html += '</div>'
        html += '</div>'
        html += '</li>'
    }
    $("#Static").html(html);
    var tpj = jQuery;
    tjq('.image-carousel').each(function () {
        displayImageCarousel(tjq(this));
    });
}

function ErrorImage(Image) {
    Image.src = "../images/news/1.png"
    Image.onerror = "";
}