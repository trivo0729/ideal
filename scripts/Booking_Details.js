﻿var arrFlightsName = new Array();
arrFlightsName.push({ ID: "SG", Value: "Spice Jet", Type: ["N", "L"] },
                { ID: "6E", Value: "Indigo", Type: ["N", "L"] },
                { ID: "G8", Value: "Go Air", Type: ["N", "L"] },
                { ID: "G9", Value: "Air Arabia", Type: ["N"] },
                { ID: "FZ", Value: "Fly Dubai", Type: ["N"] },
                { ID: "IX", Value: "Air India Express", Type: ["N"] },
                { ID: "AK", Value: "Air Asia", Type: ["N"] },
                { ID: "LB", Value: "Air Costa", Type: ["N"] },
                { ID: "UK", Value: "Air Vistara", Type: ["N", "G"] },
                { ID: "AI", Value: "Air India", Type: ["G"] },
                { ID: "9W", Value: "Jet Airways", Type: ["G"] },
                { ID: "S2", Value: "JetLite", Type: ["G"] }
);

function CancelBooking(BookingID, Type) {
    Ok("Are u sure want to cancel this booking.The cancellation charge may be applied.", "CancelFlight", [BookingID, Type])
}

function CancelFlight(BookingID, Type) {
    var data = {
        BookingID: BookingID,
        Type: Type
    }

    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $('#SpnMessege').text("Your flight ticket cancel sucessfully.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while cancel booking")
            $('#ModelMessege').modal('show')
        }
    });
}

function GetPrintInvoice(ReservationID, Uid, Status) {
    debugger;
    if (Status == 'Tiketed' || Status == 'Cancelled') {

        //var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        var win = window.open('ViewFlightInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
        //window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else {
        $('#SpnMessege').text('Please Confirm Your Booking to get Invoice !!')
        $('#ModelMessege').modal('show')
        //  alert('Please Confirm Your Booking!')
    }
}

function GetPrintTicket(ReservationID, Uid, Status) {
    debugger;
    if (Status == 'Tiketed') {

        var win = window.open('ViewFlightTicket.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
    }
    else {
        $('#SpnMessege').text('Cancelled  !!')
        $('#ModelMessege').modal('show')
    }
}

