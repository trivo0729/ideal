﻿function ValidateRegistration() {
    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");
    
    var reg = new RegExp('[0-9]$');

    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;

    var UserName = $("#txt_UserName").val();

    var Address = $("#txt_Address").val();
    var CityId = $("#selCity").val();
    var Email = $("#txt_Email").val();
    var Password = $("#txt_Password").val();
    var ConfirmPassword = $("#txt_ConfirmPassword").val();
    var Contact2 = $("#txt_Phone").val();
    var Contact1 = $("#txt_Mobile").val();
    if (UserName == "") {
        bValid = false;
        $("#lbl_UserName").css("display", "");
    }
    else {
        if (!(pattern.test(UserName))) {
            bValid = false;
            $("#lbl_UserName").html("* Wrong email format.");
            $("#lbl_UserName").css("display", "");
        }
    }
    if (Password == "") {
        bValid = false;
        $("#lbl_Password").css("display", "");
    }
    if (ConfirmPassword == "") {
        bValid = false;
        $("#lbl_ConfirmPassword").html("* This field is required.");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    else if (Password != ConfirmPassword) {
        bValid = false;
        $("#lbl_ConfirmPassword").html("Confirm Password did not matched!");
        $("#lbl_ConfirmPassword").css("display", "");
    }
    if (Address == "") {
        bValid = false;
        $("#lbl_Address").css("display", "");
    }
    if (Address != "" && reg.test(Address)) {
        bValid = false;
        $("#lbl_Address").html("* Address must not be numeric at end.");
        $("#lbl_Address").css("display", "");
    }
    if (CityId == "-") {
        bValid = false;
        $("#lbl_City").css("display", "");
    }
    if (Contact1 == "") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }

    else {
        if (!(reg.test(Contact1))) {
            bValid = false;
            $("#lbl_Mobile").html("* Mobile no must be numeric.");
            $("#lbl_Mobile").css("display", "");
        }
    }
    if (Contact2 == "") {
        Contact2 = '0';
    }
    else {
        if (!(reg.test(Contact2))) {
            bValid = false;
            $("#lbl_Phone").html("* Phone no must be numeric.");
            $("#lbl_Phone").css("display", "");
        }
    }
    if (bValid == true) {
        
        var data = {
            UserName: UserName,
            Password: Password,
            Contact1: Contact1,
            Contact2: Contact2,
            Address: Address,
            CityId: CityId,
        };
        $.ajax({
            type: "POST",
            url: "B2CCustomerLoginHandler.asmx/UserRegistration",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    alert("Registration Successfully");
                    $("#PreviewModalRegisteration").modal('hide');
                    ClearRegistration();
                    CheckloginUser();
                    if (($("#chkTermsAndCondition").is(":checked"))) {
                        setTimeout(function () {
                            Sholess()
                            BookingDetails()
                        }, 1500);
                    }
                }
                else if (result.retCode == 2) {
                    alert("Username already registered. Please register with other Username.");
                }
                else if (result.retCode == -1) {
                    alert("Registeration UnSuccessfull.");
                }

                else {
                    alert("Something Went Wrong");
                    window.location.reload();
                }
            }
        });
    }
}
function Clear() {
    $("#PreviewModalRegisteration").modal('hide');

    $('#txtUserName').val("");
    $('#txtPassword').val("");

    $("#lbl_txtUserName").css("display", "none");
    $("#lbl_txtPassword").css("display", "none");

    $("#lbl_txtUserName").html("* This field is required.");
    $("#lbl_txtPassword").html("* This field is required.");

    //for registration clearing
    $('#txt_UserName').val("");
    $('#txt_Password').val("");
    $('#txt_ConfirmPassword').val("");
    $('#txt_Mobile').val("");
    $('#txt_Phone').val("");
    $('#txt_Address').val("");
    $('#selCity option').val("-");
    $('#txt_PinCode').val("");

    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");
    //ClearRegistration()
}
function ClearRegistration() {
    $("#PreviewModalRegisteration").modal('hide');

    $('#txt_UserName').val("");
    $('#txt_Password').val("");
    $('#txt_ConfirmPassword').val("");
    $('#txt_Mobile').val("");
    $('#txt_Phone').val("");
    $('#txt_Address').val("");
    $('#selCity option').val("-");
    $('#txt_PinCode').val("");

    $("#lbl_UserName").css("display", "none");
    $("#lbl_Password").css("display", "none");
    $("#lbl_ConfirmPassword").css("display", "none");
    $("#lbl_Mobile").css("display", "none");
    $("#lbl_Phone").css("display", "none");
    $("#lbl_Address").css("display", "none");
    $("#lbl_City").css("display", "none");
    $("#lbl_PinCode").css("display", "none");

    $("#lbl_UserName").html("* This field is required.");
    $("#lbl_Password").html("* This field is required.");
    $("#lbl_ConfirmPassword").html("* This field is required.");
    $("#lbl_Mobile").html("* This field is required.");
    $("#lbl_Phone").html("* This field is required.");
    $("#lbl_Address").html("* This field is required.");
    $("#lbl_City").html("* This field is required.");
    $("#lbl_PinCode").html("* This field is required.");


    //for login clearing

    $('#txtUserName').val("");
    $('#txtPassword').val("");

    $("#lbl_txtUserName").css("display", "none");
    $("#lbl_txtPassword").css("display", "none");

    $("#lbl_txtUserName").html("* This field is required.");
    $("#lbl_txtPassword").html("* This field is required.");
    //Clear();
}
function LoginCustomer() {
    $("#lbl_txtUserName").css("display", "none");
    $("#lbl_txtPassword").css("display", "none");
    $("#lbl_txtUserName").html("* This field is required.");
    $("#lbl_txtPassword").html("* This field is required.");

    var UserName = $("#txtUserName").val();
    var Password = $("#txtPassword").val();
    var bValid = true;
    if (UserName == "") {
        bValid = false;
        $("#lbl_txtUserName").html("* This field is required.");
        $("#lbl_txtUserName").css("display", "");
    }
    if (Password == "") {
        bValid = false;
        $("#lbl_txtPassword").css("display", "");
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/UserLogin",
            data: '{"sUserName":"' + UserName + '","sPassword":"' + Password + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    return false;
                }
                else if (result.retCode == 1) {
                    CheckloginUser();
                    $("#PreviewModalRegisteration").modal('hide');
                    Clear();
                    if (($("#chkTermsAndCondition").is(":checked"))) {
                        setTimeout(function () {
                            LoginUser();
                            Sholess();
                            BookingDetails();
                        }, 1500);
                    }
                    if ($("#chkTermsAndConditionCard").is(":checked")) {
                        setTimeout(function () {
                            Sholess()
                            BookingDetails()
                        }, 1500);
                    }
                }
                else if (result.retCode == 2) {
                    alert("Username is not registered. Please register first.");
                }
                else {
                    alert('Username/Password did not match.');
                }
            },
            error: function () {
            }
        });
    }
}



