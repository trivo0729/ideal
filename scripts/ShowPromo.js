﻿function ShowPromo()
{
    //Slider()
   
    $.ajax({
        type: "POST",
        url: "../Admin/GenralHandler.asmx/GetPromoImageDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#ul_Promo li").remove()
                var List_PromoImageDetails = result.tbl_PromoImage;
                var li = "";
                for (var i = 0; i < List_PromoImageDetails.length; i++) {
                    li += ' <li data-transition="fade" data-slotamount="1" data-masterspeed="300">'
                    li += '<img src="' + List_PromoImageDetails[i].Path.replace("~","..") + '" alt="" />'
                    li += '<div class="tp-caption scrolleffect sft" data-x="center" data-y="100" data-speed="1000" data-start="800" data-easing="easeOutExpo">'
                    li += ' <div class="sboxpurple textcenter">'
                    li += '<span class="lato size48 slim caps white">' + List_PromoImageDetails[i].ImageName + '</span><br /><br />  <br />'
                    li += '<span class="lato size100 slim caps white">' + List_PromoImageDetails[i].facility + '</span><br />'
                    li += '<span class="lato size20 normal uppercase white">' + List_PromoImageDetails[i].service + '</span><br />'
                    li += '<br /> <span class="lato size48 slim uppercase yellow">'+List_PromoImageDetails[i].start+'</span><br />'
                    li += '</div>'
                    li += '</div>'
                    li += '<div class="tp-caption sfb" data-x="left" data-y="371" data-speed="1000" data-start="800" data-easing="easeOutExpo">'
                    li += '<div class="blacklable">'
                    li += '<h4 class="lato bold white"><span class="orange"></span<span class="orange">' + List_PromoImageDetails[i].details + '</span></h4>'
                    if (List_PromoImageDetails[i].Notes != "")
                    {
                        li += '<h5 class="lato grey mt-10">' + List_PromoImageDetails[i].Notes + '</h5>'
                    }
                         
                    li += '</div>'
                    li += '</div>'
                    li += '</li>';
                }
                $("#ul_Promo").html(li);
                Slider()

                //$("#tbl_PromoImageDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
        }
    });

}
function Slider()
{
    var tpj = jQuery;
    if (tpj.fn.cssOriginal != undefined)
        tpj.fn.css = tpj.fn.cssOriginal;

    var api = tpj('.fullwidthbanner').revolution(
    {
        delay: 9000,
        startwidth: 960,
        startheight: 446,

        onHoverStop: "on", 					// Stop Banner Timet at Hover on Slide on/off

        thumbWidth: 100, 						// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
        thumbHeight: 50,
        thumbAmount: 3,

        hideThumbs: 0,
        navigationType: "bullet", 			// bullet, thumb, none
        navigationArrows: "solo", 			// nexttobullets, solo (old name verticalcentered), none

        navigationStyle: "round", 			// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


        navigationHAlign: "right", 			// Vertical Align top,center,bottom
        navigationVAlign: "bottom", 				// Horizontal Align left,center,right
        navigationHOffset: 30,
        navigationVOffset: 30,

        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,

        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,

        touchenabled: "on", 					// Enable Swipe Function : on/off


        stopAtSlide: -1, 						// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
        stopAfterLoops: -1, 					// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

        hideCaptionAtLimit: 0, 				// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
        hideAllCaptionAtLilmit: 0, 			// Hide all The Captions if Width of Browser is less then this value
        hideSliderAtLimit: 0, 				// Hide the whole slider, and stop also functions if Width of Browser is less than this value


        fullWidth: "on",

        shadow: 1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

    });
    // TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
    // YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
    api.bind("revolution.slide.onloaded", function (e) {


        jQuery('.tparrows').each(function () {
            var arrows = jQuery(this);

            var timer = setInterval(function () {

                if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
                    arrows.fadeOut(300);
            }, 3000);
        })

        jQuery('.tp-simpleresponsive, .tparrows').hover(function () {
            jQuery('.tp-simpleresponsive').addClass("mouseisover");
            jQuery('body').find('.tparrows').each(function () {
                jQuery(this).fadeIn(300);
            });
        }, function () {
            if (!jQuery(this).hasClass("tparrows"))
                jQuery('.tp-simpleresponsive').removeClass("mouseisover");
        })
    });
    // END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS


}