﻿
var validate;
function SendMail() {
    bValid = Validate();
    if (bValid == true) {
        var Data = {
            name: name,
            email: email,
            subject: subject,
            message: message
        }
        var Jason = JSON.stringify(Data);
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/SendContactMail",
            data: Jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result == true) {
                        alert("Your request sent.We will get in touch with you soon.");
                        window.location.reload();
                    }
                    else {
                        alert("request Not sent");
                    }

                } catch (e) { }
            },
            error: function () {
                alert("error");
            }
        });

    }

}

function Validate() {

    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    var bValid = true;

    name = $("#name").val();
    email = $("#email").val();
    subject = $("#subject").val();
    message = $("#message").val();

    if (name == "") {
        alert("Please Enter Your Name");
        return false;
        bValid = false;
    }
    if (email == "") {
        alert("Please Enter Email");
        return false;
        bValid = false;
    }
    if (subject == "") {
        alert("Please Enter subject");
        return false;
        bValid = false;
    }
    if (message == "") {
        alert("Please Enter Your Message ");
        return false;
        bValid = false;
    }
    return bValid;
}

