﻿$(document).ready(function () {
    GetResult();
    GetSession();
    //$("#Slider").modal("show")
});


var CurrencyForDetail = "usd";
var Arr = new Array();
var FilteTip = new Array();
var ListFacility = new Array();
var ListHotel = new Array();
var MapList = new Array();
var NearByLocation = new Array();
var Ratings = new Array();

function initializeAll() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(MapList[0].Maplatitude, MapList[0].MapLangitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var marker;
    var location = {};
    var Result = [];
    $("#gridSystemModalLabel3").text("Map View")
    for (var i = 0; i < MapList.length; i++) {
        try {
            location = {
                id: MapList[i].HotelId,
                name: MapList[i].MapName,
                Image: MapList[i].MapImageurl,
                pointlat: parseFloat(MapList[i].Maplatitude),
                pointlng: parseFloat(MapList[i].MapLangitude),
                Rating: MapList[i].Rating,
                Description: MapList[i].Decription,
                Price: MapList[i].PriceList,
                Booking: MapList[i].BookingList

            };
            console.log(location);

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(location.pointlat, location.pointlng),
                map: map,

            });

            google.maps.event.addListener(marker, 'click', (function (marker, location) {
                return function () {
                    infowindow.setContent('<h4>' + location.name + ' <img src="' + location.Rating + '" style="    float: right;"/> </h4><span  style="    float: right;" class="opensans orange size12"><i class="fa fa-inr" aria-hidden="true"></i> ' + location.Price + '</i> </span><br /><table><tr> <td><img src="' + location.Image + '" width="200" height="100"/></td><td><p>' + location.Description + '</p><input type="button" class="selectbtn mt1" id="btn_Hotel' + location.id + '"  value="Details" onclick="GetMAPHotel(\'' + location.id + '\',\'' + location.name + '\');" style="float: right;"></td></tr></tabel>');
                    infowindow.open(map, marker);
                };
            })(marker, location));
        }
        catch (ex) {
            alert(ex)
        }
    }
}

function initialize(HotelId) {
    var objHotel = $.grep(MapList, function (p) { return p.HotelId == HotelId; })
        .map(function (p) { return p; });
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: new google.maps.LatLng(objHotel[0].Maplatitude, objHotel[0].MapLangitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker;
    var location = {};
    var Result = []
    $("#gridSystemModalLabel3").text(objHotel[0].MapName)
    location = {
        name: objHotel[0].MapName,
        Image: objHotel[0].MapImageurl,
        pointlat: parseFloat(objHotel[0].Maplatitude),
        pointlng: parseFloat(objHotel[0].MapLangitude),
        Rating: objHotel[0].Rating,
        Description: objHotel[0].Decription,
        Price: objHotel[0].PriceList,
        Booking: objHotel[0].BookingList
    };
    console.log(location);
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.pointlat, location.pointlng),
        map: map,
        zoom: 15,
    });
}

function GetResult() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetResult",
        contentType: 'application/json',
        data: '',
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.retCode == 1) {

                Arr = obj.arrHotel;
                FilteTip = Arr.FilteTip;
                ListFacility = Arr.ListFacility;
                ListHotel = Arr.ListHotel;
                MapList = Arr.MapList;
                NearByLocation = Arr.NearByLocation;
                Ratings = Arr.Ratings;
                FilterTip();
                GenerateHTML();
                //StarRatings();
                RatingCheck();
                var arrPrice = Arr.m_Price;
                PriceRage(arrPrice.MinPrice, arrPrice.MaxPrice, '<i class="fa fa-inr" aria-hidden="true"></i>');
            }
            else {
                Msg = obj.Content;
                //alert(Msg);
                $("#Design").empty();
                var html = '';
                html += '' + Msg + '';
                $("#Design").append(html);
            }
        }
    });
}

//function StarRatings() {

//    var html = "";
//    $("#accomodation-type-filter").empty();
//    html += '<div class="panel-content">'
//    html += '<div class="form-group">'
//    for (var r = 0; r < Ratings.length; r++) {
//        html += ' <label class="checkbox checked">'
//        html += ' <input type="checkbox" data-Rating="' + Ratings[r].Rating + '" onclick="HotelFilter();"  name="chkRating" value="" /><img src="HotelRating/' + r + '.png" />  (' + Ratings[r].Count + ')'
//        html += ' </label>'
//    }
//    html += '</div>'
//    html += '</div>'
//    $("#accomodation-type-filter").append(html);
//}

var list_Occupancy = new Array();

function GetSession() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetSession",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            list_Occupancy = obj.list_Occupancy;
            var DestinationCode = obj.DestinationCode;
            var Location = obj.Location;
            var CheckIn = obj.CheckIn;
            var CheckOut = obj.CheckOut;
            var Night = obj.Night;
            var HotelCode = obj.HotelCode;
            var HotelName = obj.HotelName;
            var StarRating = obj.StarRating;
            var NationalityCode = obj.NationalityCode
            if (obj.retCode == 1) {
                codeAddress(Location);
                $("#txtCity").val(Location);
                $("#hdnDCode").val(DestinationCode);
                $("#datepicker_HotelCheckin").val(CheckIn);
                $("#datepicker_HotelCheckout").val(CheckOut);
                $("#Select_TotalNights ").val(Night);
                $("#txt_HotelName").val(HotelName);
                $("#hdnHCode").val(HotelCode);
                $("#Select_Nationality").val(NationalityCode);
                if (NationalityCode != "") {
                    $("#Select_Nationality option:selected").val(NationalityCode);
                }
                $("#Select_Rooms").val(list_Occupancy);
                var DisplayRequest = list_Occupancy;
                SetRooms(DisplayRequest);
            }
            else {
            }
        }
    });
}

function FilterTip() {
    var html = "";
    $("#FilterTrip").empty();
    html += '<i class="soap-icon-search"></i> '
    html += '<b>' + FilteTip.TotalCount + ' '
    html += '</b>hotel out of <b> ' + FilteTip.Displayed + ''
    html += '</b> ' + FilteTip.Location + ''
    $("#FilterTrip").append(html);
}

function GenerateHTML() {

    try {
        $("#Design").empty();
        $("#loders").show();
        for (var i = 0; i < ListHotel.length; i++) {
            var html = '';
            html += '<article class="box">'
            html += '    <figure class="col-sm-5 col-md-4">'
            html += '        <a class="hover-effect" style="cursor:pointer" onclick="GetHotelImages(\'' + ListHotel[i].HotelId + '\' ,\'' + ListHotel[i].HotelName + '\')">'
            if (ListHotel[i].Image.length != 0) {
                html += ' <img width="270" height="160" alt="" src="' + ListHotel[i].Image[0].Url + '" onerror="ErrorImage(this)"></a>'
            }
            else {
                html += ' <img width="270" height="160" alt="" src="../images/no-image.png"></a>'
            }
            html += '    </figure>'
            html += '    <div class="details col-sm-7 col-md-8">'
            html += '        <div>'
            html += '            <div>'
            if (ListHotel[i].Address != null) {
                html += '<h5 class="box-title"><a onclick="GetHotelInfo(\'' + ListHotel[i].HotelId + '\')" style="cursor:pointer">' + ListHotel[i].HotelName + '</a><small ><a onclick="GetMapView(\'' + ListHotel[i].HotelId + '\')" style="cursor:pointer"><i class="soap-icon-departure yellow-color"></a></i>&nbsp' + tran(ListHotel[i].Address) + '</small>'
                if ($("#txtSource").val() != "") {
                    html += '<b><span style="font-size: 12px;">' + ListHotel[i].Distance.toFixed(2); + ' km From ' + $("hdnnrbl").val() + '</span></b>'
                }
                html += '</h5>'
            }
            else {
                html += '<h4 class="box-title">' + ListHotel[i].HotelName + '<small><i class="soap-icon-departure yellow-color"></i> Not Available </small></h4>'
            }
            html += '                <div class="amenities">'
            html += GetHotelFacility(ListHotel[i].Facility);
            html += '                </div>'
            html += '            </div>'
            html += '            <div>'
            html += '                <div class="five-stars-container">'
            html += '                    <span class="five-stars" style="' + GetRating(ListHotel[i].Category) + ';"></span>'
            html += '                </div>'
            html += '            </div>'
            html += '        </div>'
            html += '        <div>'
            //html += '            <p title="\'' + ListHotel[i].Description + '\'">' + trancate_per(ListHotel[i].Description) + '</p>'
            html += '            <p>' + ListHotel[i].Description.replace("<p>", ""); + '</p>'
            html += '            <div>'
            html += '                <span style="font-size: 13px;" data-placement="left"  data-toggle="tooltip" class="price" data-container="body"  data-original-title="' + ListHotel[i].MinToolTip + '"><small>Start From</small><i class="fa fa-inr" aria-hidden="true"></i> ' + ListHotel[i].MinPrice + '</span>'

            html += '                <a data-toggle="collapse" href="#Details_' + ListHotel[i].HotelId + '" onclick="DetailsShow(\'' + ListHotel[i].HotelId + '\' ,\'' + ListHotel[i].Supplier + '\' )" class="button btn-small sky-blue1 collapsed">Details</a>'
            html += '            </div>'
            html += '        </div>'
            html += '    </div>'
            html += '<div class="toggle-container col-sm-12 col-md-12 ">'
            html += '    <div id="Details_' + ListHotel[i].HotelId + '"  class="col-md-12 panel-collapse " role="tablist" aria-multiselectable="true">'
            html += '    </div>'
            html += '</article>'

            $("#Design").append(html);
            $('[data-toggle="tooltip"]').tooltip();
        }
        Pager(FilteTip.Displayed)
        $("#loders").hide();
    }


    catch (ex) {
        alert(ex.message)
    }
}

function DetailsShow(HotelCode, Supplier) {
    $("#Details_" + HotelCode).empty();
    var data = { HotelCode: HotelCode, Supplier: Supplier };
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GenerateRoomDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var RoomDetails = result.ListRateGroup;

                var html = "";
                html += '<div class="toggle-container" id="Hotel_' + HotelCode + '">'
                var Rate = 1;

                for (var i = 0; i < RoomDetails.length; i++) {
                    Supplier = RoomDetails[i].Name;
                    var TotalRooms = 0;
                    var tab = "collapsed";
                    var tapDiv = "collapse";
                    if (i == 0) {
                        tab = "";
                        tapDiv = "collapse in"
                    }
                    html += '<div class="panel style2" style="padding-right: 14px">'

                    html += '<h4 class="panel-title">'
                    html += '<a data-toggle="collapse" data-parent="#Hotel_' + HotelCode + '"  href="#tgg_' + i + '_' + HotelCode + '" class="' + tab + '">' + RoomDetails[i].RateGroupNo + ' </a>'
                    html += '</h4>'

                    var r = 1;
                    html += '<div id="tgg_' + i + '_' + HotelCode + '" class="panel-collapse ' + tapDiv + '" >'
                    for (var j = 0; j < RoomDetails[i].RoomOccupancy.length; j++) {
                        html += '<div>'
                        html += '<header id="header" class="navbar-static-top">'
                        html += '<div class="topnav hidden-xs">'
                        html += '<div class="container">'
                        html += '<ul class="quick-menu pull-left">'
                        html += '<li><a href="#" style="font-size: 15px">&nbsp Room No. ' + RoomDetails[i].RoomOccupancy[j].OccupancyDetails + '</a></li>'
                        html += '</ul>'
                        html += '</div>'
                        html += '</div>'
                        html += '</header><br/>'
                        html += '<div class="panel-content">'
                        var DefaultSelected = "selected='selected'";
                        for (var k = 0; k < RoomDetails[i].RoomOccupancy[j].Rooms.length; k++) {

                            html += '<div class="row RateGroup RefundGroup_' + RoomDetails[i].RoomOccupancy[j].ClassRefund + '" >'
                            //Refunplolicy(this)
                            html += '<div class="col-lg-3">'
                            html += '<small><b><a style="cursor:pointer" onclick="GetRoomFacility(\'' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomTypeId + '\' ,\'' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomDescriptionId + '\',\'' + Supplier + '\',\'' + RoomDetails[i].AvailToken + '\',\'' + HotelCode + '\')">' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomTypeName + '</b></small>'
                            html += '</div>'
                            html += '<div class="col-lg-2">'
                            html += '<small><b>' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomDescription + '</b></small>'
                            html += '</div>'
                            html += '<div class="col-lg-3">'
                            var nonRefundable = "";
                            var Cancel = "<ul>";
                            for (var c = 0; c < RoomDetails[i].RoomOccupancy[j].Rooms[k].Cancelation.length; c++) {
                                Cancel += '<li>' + RoomDetails[i].RoomOccupancy[j].Rooms[k].Cancelation[c].replace(/['"*]/g, '') + '</li>';
                            }
                            Cancel += "</ul>";
                            if (RoomDetails[i].RoomOccupancy[j].Rooms[k].Refundable == "nonRefundable") {

                                html += '<span class="Cancellation" data-toggle="popover" data-container="body" data-html="true" data-original-title="Cancellation Policy" data-content="' + RoomDetails[i].RoomOccupancy[j].Rooms[k].Cancelation[0] + '"><small style="color:#ff3300"><b> Non Refundable </b></small></span>'

                            }
                            else {
                                html += '<b style="color:#7db921" data-toggle="popover" data-container="body" data-html="true" data-original-title="Cancellation Policy" data-content="' + Cancel + '">' + RoomDetails[i].RoomOccupancy[j].Rooms[k].Cancelation[0] + '</b>'
                            }
                            html += '</div>'
                            html += '<div class="col-lg-2">'
                            html += '<b style="font-size: 13px; color:#ff6000" data-placement="left"  data-toggle="tooltip" data-container="body"  data-original-title="' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomAmtToolTip + '"><i class="fa fa-inr" aria-hidden="true"></i> ' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomAmt + '</b>'
                            html += '</div>'
                            html += '<div class="col-lg-2">'
                            html += '<div class="form-group" style="margin-bottom: 0px">'
                            html += '<div class="full-width">'
                            html += '<input type="hidden" value="' + RoomDetails[i].RoomOccupancy[j].RoomCount + '" class="RoomDetails_' + RoomDetails[i].AvailToken + '_' + r + '_' + Supplier + '">'
                            if (RoomDetails[i].RoomOccupancy[j].noRooms != 1) {
                                html += '<select id=' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomTypeId + "^" + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomDescriptionId + "^" + RoomDetails[i].RoomOccupancy[j].Rooms[k].Total + "^" + r + ' class="form-control Room_' + RoomDetails[i].AvailToken.replace("!", "_") + '_' + r + '_' + Supplier + " HotelRoom_" + RoomDetails[i].AvailToken.replace("!", "_") + '_' + Rate + '_' + Supplier + '"  onchange="SetRoomoccupancy(this ,\'' + RoomDetails[i].AvailToken + '_' + r + '_' + Supplier + '\' ,\'' + Supplier + '\' , \'' + 'HotelRoom_' + RoomDetails[i].AvailToken + '_' + Rate + '_' + Supplier + '\' ,\'' + RoomDetails[i].AvailToken + '\' ,\'' + HotelCode + '\' )" >'
                                for (var no = 0; no <= RoomDetails[i].RoomOccupancy[j].TotalRoomCount; no++) {
                                    html += '<option ' + DefaultSelected + ' value=' + no + '>' + no + '</option>'
                                }
                                html += '</select>'
                            }
                            else {

                                html += '<a class="button btn-small sky-blue1" title="" href="#." onclick="GetSingleBookingRates(\'' + RoomDetails[i].AvailToken.replace('!', '_') + '\',\'' + Supplier + '\',\'' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomTypeId + '\' ,\'' + RoomDetails[i].RoomOccupancy[j].Rooms[k].RoomDescriptionId + '\',\'' + RoomDetails[i].RoomOccupancy[j].Rooms[k].Total + '\',\'' + HotelCode + '\')\">Book</a>'
                            }
                            html += '</div>'
                            html += '</div>'
                            html += '</div>'
                            html += '</div>'
                            html += '<hr /> '
                            DefaultSelected = "";
                        }
                        TotalRooms += RoomDetails[i].RoomOccupancy[j].TotalRoomCount;
                        html += '</div>'
                        html += '</div>'
                        r++;
                    }
                    if (TotalRooms != 1) {
                        html += '<hr />'
                        html += '<div class="row">'
                        html += '<input type="hidden" value="' + TotalRooms + '" class="TotalRomms_' + RoomDetails[i].AvailToken + '_' + Rate + '_' + Supplier + '">'
                        html += '<div class="col-md-2 col-md-offset-10"><span> Total : </span><lable data-toggle="tooltip" data-placement="left" data-container="body" data-original-title="' + RoomDetails[i].netDefaultRateTip + '" class="Total_' + RoomDetails[i].AvailToken + Supplier + '"><i class="fa fa-inr" aria-hidden="true"></i> ' + RoomDetails[i].DefaultRate + '</lable>'
                        html += '<a class="button btn-small full-width text-center" title="" href="#." onclick="GetBookingRates(\'' + RoomDetails[i].AvailToken.replace('!', '_') + '\',\'' + Supplier + '\',\'' + TotalRooms + '\',\'' + 'HotelRoom_' + RoomDetails[i].AvailToken.replace('!', '_') + '_' + Rate + '_' + Supplier + '\',\'' + HotelCode + '\')\">Book</a></div>'
                        html += '</div>'
                    }
                    html += '</div><br/>'
                    html += '</div>'
                    Rate++;
                }

                html += '</div>'
                $("#Details_" + HotelCode).append(html)
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover({
                    placement: 'top',
                    trigger: 'hover',
                });

            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}

function trancate_per(title) {
    var length = 275;
    if (title.length > length) {
        title = title.substring(0, length) + '...';
    }
    return title;
}

function tran(title) {
    var length = 35;
    if (title.length > length) {
        title = title.substring(0, length) + '...';
    }
    return title;
}

function GetRating(Category) {
    var Ratings = "";
    if (Category == 1 || Category == "1.png")
        Ratings = "width: 20%";
    if (Category == 2 || Category == "2.png")
        Ratings = "width: 40%";
    if (Category == 3 || Category == "3.png")
        Ratings = "width: 60%";
    if (Category == 4 || Category == "4.png")
        Ratings = "width: 80%";
    if (Category == 5 || Category == "5.png")
        Ratings = "width: 100%";
    if (Category == 0)
        Ratings = "width: 0%";
    return Ratings;
}

function GetHotelFacility(arrFacility) {
    var arrClass = "";
    var html = '';
    for (var f = 0; f < arrFacility.length; f++) {
        if (arrFacility[f].Name.includes("High Speed Internet"))
            html += '<i class="soap-icon-wifi circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("Gym"))
            html += '<i class="soap-icon-fitnessfacility circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("Breakfast"))
            html += '<i class="soap-icon-food circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("tv"))
            html += '<i class="soap-icon-television circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("bar"))
            html += '<i class="soap-icon-winebar circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("fridege"))
            html += '<i class="soap-icon-fridge circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("swimming"))
            html += '<i class="soap-icon-swimming circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("pat"))
            html += '<i class="soap-icon-dog circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '"></i>'
        else if (arrFacility[f].Name.includes("Car"))
            html += '<i class="soap-icon-pickanddrop circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '" ></i>'
        else if (arrFacility[f].Name.includes("Room Service"))
            html += '<i class="soap-icon-phone circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '" ></i>'
        else if (arrFacility[f].Name.includes("Business Centre"))
            html += '<i class="soap-icon-businessbag circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '" ></i>'
        else if (arrFacility[f].Name.includes("Air Conditioning"))
            html += '<i class="soap-icon-aircon circle" data-toggle="tooltip" title="' + arrFacility[f].Name + '" ></i>'
    }
    return html;
}

function PriceRage(MinPrice, MaxPrice) {
    $("#price-range").slider({
        range: true,
        min: MinPrice,
        max: MaxPrice,
        values: [MinPrice, MaxPrice],
        slide: function (event, ui) {
            $(".min-price-label").html("<i class='fa fa-inr' aria-hidden='true'></i> " + " " + ui.values[0]);
            $(".max-price-label").html("<i class='fa fa-inr' aria-hidden='true'></i> " + " " + ui.values[1]);
            $('.ui-slider-handle').click(function () {
                HotelFilter();
            })
        }
    });
    $(".min-price-label").html("<i class='fa fa-inr' aria-hidden='true'></i> " + $("#price-range").slider("values", 0));
    $(".max-price-label").html("<i class='fa fa-inr' aria-hidden='true'></i> " + $("#price-range").slider("values", 1));

    $("#rating").slider({
        range: "min",
        value: MinPrice,
        min: 0,
        max: MaxPrice,
        slide: function (event, ui) {

        }
    });
}

function Pager(Count) {
    PageSize = 50;
    isLastPage = true;
    PageNo = 1;
    var html = '';
    var noPager = Count / PageSize;
    var PagerReminder = Count % PageSize;
    if (PagerReminder > 0)
        noPager = noPager;
    $("#divpagingbottom").empty();
    html += '<ul class="pagination right paddingbtm20"><input type="hidden" id="hdnCurrentPage" value="1">'
    if (PageNo == 1)
        html += "<li class=\"disabled\"><a>&laquo;</a></li>";
    else
        html += "<li><a onclick=\"HotelPager(" + (PageNo - 1) + ");>&laquo;</a></li>";

    for (var i = 0; i < noPager; i++) {
        var m_pageno = (PageSize * (PageNo - 1)) + i + 1;
        html += "<li><a onclick=\"HotelPager(" + m_pageno + ");\">" + m_pageno + "</a></li>";
    }
    if (!isLastPage && noPager > 1)
        html += "<li><a onclick=\"HotelPager(" + (PageSize * PageNo) + ");>&raquo;</a></li>";
    else
        html += "<li class=\"disabled\"><a>&raquo;</a></li>";
    html += "</ul>";
    $("#divpagingbottom").append(html)
}

function ErrorImage(Image) {
    Image.src = "../images/no-image.png"
    Image.onerror = "";
}

function RatingCheck() {

    for (var i = 0; i < Ratings.length; i++) {
        if (Ratings[i].Rating == "1") {
            if (Ratings[i].Rating != 1) {
                $("#Star_1").text(0);
            }
            else {
                $("#Star_1").text(Ratings[i].Count);
            }

        }
        else if (Ratings[i].Rating == "2") {

            if (Ratings[i].Rating != 2) {
                $("#Star_2").text(0);
            }
            else {
                $("#Star_2").text(Ratings[i].Count);
            }
        }

        else if (Ratings[i].Rating == "3") {

            if (Ratings[i].Rating != 3) {
                $("#Star_3").text(0);
            }
            else {
                $("#Star_3").text(Ratings[i].Count);
            }
        }
        else if (Ratings[i].Rating == "4") {

            if (Ratings[i].Rating != 4) {
                $("#Star_4").text(0);
            }
            else {
                $("#Star_4").text(Ratings[i].Count);
            }
        }
        else if (Ratings[i].Rating == "5") {

            if (Ratings[i].Rating != 5) {
                $("#Star_5").text(0);
            }
            else {
                $("#Star_5").text(Ratings[i].Count);
            }
        }
        else if (Ratings[i].Rating == "0") {

            if (Ratings[i].Rating != "0") {
                $("#Star_0").text(0);
            }
            else {
                $("#Star_0").text(Ratings[i].Count);
            }
        }
    }

}

function Show() {
    $("#SearchDive").show();
    $("#btn_hide").show();
    $("#btn_Show").hide();

}

function Hide() {
    $("#SearchDive").hide();
    $("#btn_hide").hide();
    $("#btn_Show").show();

}

function Slide() {
    var totalItems = $('#carousel .item').length;
    var thumbs = 3;
    var currentThumbs = 0;
    var to = 0;
    var thumbActive = 1;

    function toggleThumbActive(i) {
        $('#carousel-thumbs .item>div').removeClass('active');
        $('#carousel-thumbs .item.active>div:nth-child(' + i + ')').addClass('active');
    }

    $('#carousel').on('slide.bs.carousel', function (e) {

        var from = $('#carousel .item.active').index() + 1;
        var next = $(e.relatedTarget);
        to = next.index() + 1;
        var nextThumbs = Math.ceil(to / thumbs) - 1;
        if (nextThumbs != currentThumbs) {
            $('#carousel-thumbs').carousel(nextThumbs);
            currentThumbs = nextThumbs;
        }
        thumbActive = +to - (currentThumbs * thumbs);
    });
    $('#carousel').on('slid.bs.carousel', function (e) {
        toggleThumbActive(thumbActive);
    });
    $('#carousel-thumbs').on('slid.bs.carousel', function (e) {
        toggleThumbActive(thumbActive);
    });
    $("#carousel").on("swiperight", function () {
        $('#carousel').carousel('prev');
    });
    $("#carousel").on("swipeleft", function () {
        $('#carousel').carousel('next');
    });
    $("#carousel-thumbs").on("swiperight", function () {
        $('#carousel-thumbs').carousel('prev');
    });
    $("#carousel-thumbs").on("swipeleft", function () {
        $('#carousel-thumbs').carousel('next');
    });
}

function GetMapView(HotelId) {
    setTimeout('initialize(\'' + HotelId + '\')', 300);
    $('#MapView').modal('show');
}

function GetMapAll() {
    setTimeout('initializeAll()', 300);
    $('#MapView').modal('show');
}

function SortBy(Type) {
    if (Type == "Hotel") {
        $("#txt_Preferred").val(0)
        if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "3") {
            $("#lblHotelName").text("3");
            HotelFilter();
        }
        else {
            $("#lblHotelName").text("4");
            HotelFilter();
        }
    }
    else if (Type == "Price") {
        $("#txt_Preferred").val(0)
        if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "1") {
            $("#lblHotelName").text("1")
            HotelFilter();
        }
        else {
            $("#lblHotelName").text("2");
            HotelFilter();
        }
    }
    else if (Type == "Category") {
        $("#txt_Preferred").val(0)
        var StarRating = document.getElementsByName('chkRating'), i;
        var Acending = document.getElementsByName("chkRating")[0].getAttribute("data-rating");
        var Decending = document.getElementsByName("chkRating")[StarRating.length - 1].getAttribute("data-rating");
        if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "5") {
            $("#lblHotelName").text("5")
            HotelFilter();
        }
        else {
            $("#lblHotelName").text("6");
            HotelFilter();
        }
    }
    else if (Type == "Preferred") {
        $("#txt_Preferred").val(1)
        $("#lblHotelName").text(0)
        HotelFilter();
    }
}

function SortByHotel() {
    $("#txt_Preferred").val(0)
    if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "3") {
        $("#lblHotelName").text("3");
        HotelFilter();
    }
    else {
        $("#lblHotelName").text("4");
        HotelFilter();
    }
}

function StarRating() {
    $("#txt_Preferred").val(0)
    var StarRating = document.getElementsByName('chkRating'), i;
    var Acending = document.getElementsByName("chkRating")[0].getAttribute("data-rating");
    var Decending = document.getElementsByName("chkRating")[StarRating.length - 1].getAttribute("data-rating");
    if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "5") {
        $("#lblHotelName").text("5")
        HotelFilter();
    }
    else {
        $("#lblHotelName").text("6");
        HotelFilter();
    }
}

function SortByPrice() {
    $("#txt_Preferred").val(0)
    if ($("#lblHotelName").text() == "0" || $("#lblHotelName").text() != "1") {
        $("#lblHotelName").text("1")
        HotelFilter();
    }
    else {
        $("#lblHotelName").text("2");
        HotelFilter();
    }
}

function GetPreferred() {
    $("#txt_Preferred").val(1)
    $("#lblHotelName").text(0)
    HotelFilter();
}

function ResetName() {
    $("#txtSearchHotel").val("");
    HotelName();
}

var Images = new Array();

function GetHotelImages(HotelCode, Name) {
    $("#Slide").empty();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHotelImages",
        data: '{"HotelCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#SlideHotelName").text(Name)
                $("#Slide").append(result.Slider);
                Images = result.Slider;
                SetImages();
            }
        },
        error: function () {
        }
    });
}

var HotelInfo = "";
function GetHotelInfo(HotelID) {
    $("#div_Hotel").empty();
    $("#lbl_HotelName").empty();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHotelInfo",
        data: '{"HotelID":"' + HotelID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //HotelInfo = result.HotelInfo;
            //DesignHotelInfo();
            $("#div_Hotel").append(result.HotelInfo)
            $("#lbl_HotelName").append(result.objHotelDetails.HotelName + "<br/>" + "<label style='color:grey'> " + result.objHotelDetails.Address + "</label>");
            $("#ModalHotel").modal("show")
        },
        error: function () {
        }
    });
}

function GetRoomFacility(RoomTypeId, RoomDescriptionId, Name, HotelID, CutCode) {
    var objHotel = $.grep(MapList, function (p) { return p.HotelId == CutCode; })
        .map(function (p) { return p; });
    $("#RoomFacility").empty();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetRoomFacilit",
        data: '{"CutCode":"' + CutCode + '","HotelCode":"' + HotelID + '","RoomTypeId":"' + RoomTypeId + '","RoomDescriptionId":"' + RoomDescriptionId + '","Supplier":"' + Name + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#lblRoomFacility").append(objHotel.HotelName)
            $("#RoomFacility").append(result.Details);
            $("#ModalRoomFacility").modal("show")
        },
        error: function () {
        }
    });
}

function SetImages() {
    var html = "";
    $("#Img").empty();
    html += '<div id="myCarousel" class="carousel slide" data-ride="carousel">'
    html += '  <div class="carousel-inner">'
    for (var i = 0; i < Images.length; i++) {
        if (i == 0) {
            html += '      <div class="item active">'
            html += '          <img src="' + Images[i].Url + '" alt="Los Angeles" style="width: 100%; height:400px">'
            html += '          <div class="carousel-caption">'
            html += '          </div>'
            html += '      </div>'
        }
        else {
            html += '      <div class="item">'
            html += '          <img src="' + Images[i].Url + '" alt="Chicago" style="width: 100%; height:400px">'
            html += '          <div class="carousel-caption">'
            html += '          </div>'
            html += '      </div>'
        }
    }
    html += '  </div>'
    html += '  <a class="left carousel-control" href="#myCarousel" data-slide="prev">'
    html += '      <span class="glyphicon glyphicon-chevron-left"></span>'
    html += '      <span class="sr-only">Previous</span>'
    html += '  </a>'
    html += '  <a class="right carousel-control" href="#myCarousel" data-slide="next">'
    html += '      <span class="glyphicon glyphicon-chevron-right"></span>'
    html += '      <span class="sr-only">Next</span>'
    html += '  </a>'
    html += '  </div>'
    $("#Img").append(html)
    $("#Slider").modal("show")
}