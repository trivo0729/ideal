﻿$(document).ready(function () {
    GetSearch();

    Datee = getParameterByName('Date');

    var d = Datee.split('-')[0];
    var m = Datee.split('-')[1];
    var y = Datee.split('-')[2];
    Datee = m + "-" + d + "-" + y;

});

var Datee;
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function GetSearch() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/Get_CalendarSerchFlightevents",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            
            $('#fullCalendar').fullCalendar('destroy')
            var arrEvents = new Array();
            var SearchResult = result.CalendarList.SearchResults;
            for (var i = 0; i < SearchResult.length; i++) {
                //if (SearchResult[i].IsLowestFareMonth == true)
                //    Color = "#FF0000";
                //else
                //    Color = "#3584ff";

                // var html = "";
                //html += '<img src="../images/AirlineLogo/' + SearchResult[i].AirlineCode + '.gif" alt="" width="50%" height="20%">'
                //html += '</br>'
                //html += '<a>' + SearchResult[i].Fare + '</a>'
                //html += '</br>'
                //html += '<button value="Book" style="Background-color:red">Book</button>'
                //html += '</br>'

                var Event = {
                    id: i,
                    title: "<p> <img src='../images/AirlineLogo/" + SearchResult[i].AirlineCode + ".gif' width='50px' height='30px'> </br> <a>Airline : " + SearchResult[i].AirlineName + "</a> </br> <a>Time : " + SearchResult[i].DepartureDate.split('T')[1] + "</a> </br> <a>Fare : " + SearchResult[i].Fare + "</a> </br> <a class='button btn-mini silver' style='float:right' id='Update" + i + "' onclick='UpdateRequest(this.id,\"" + SearchResult[i].DepartureDate.split('T')[0] + "\",\"" + result.CalendarList.Origin + "\",\"" + result.CalendarList.Destination + "\")'>Update</a></p>",
                    start: SearchResult[i].DepartureDate,
                    //url: SearchResult.url,
                    end: SearchResult[i].DepartureDate,
                    //Type: result.CalendarList[i].Type,
                    html: true,
                    color: '#FFF',
                    textEscape: false,
                    // textColor:black
                }
                arrEvents.push(Event)
            }
            var date = new Date(Datee);
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#fullCalendar').fullCalendar({
                editable: true,
                defaultDate: moment(date),

                //header: {
                //    left: 'prev,next today',
                //    center: 'title',
                //    right: 'month,agendaWeek,agendaDay'
                //},
                events: arrEvents,
                // Convert the allDay from string to boolean
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                    event.editable = false;
                    //element.find('.fc-title, .fc-list-item-title').html("<b>" + event.title + "</b>");

                    element.find('.fc-title').html("<b>" + event.title + "</b>");

                },
                selectable: true,
                selectHelper: true,

                editable: true,
                eventAfterAllRender: function (view) {
                    //$(".fc-event-container").append("<span class='closon'>X</span>");

                    //$(".fc-event-container").append("<span class='closon'><img src='../images/AirlineLogo/" + SearchResult[i].AirlineCode + ".gif' width='50%' height='20%'> </br> <a>" + SearchResult[i].Fare + "</a> </br> <button value='Book' style='Background-color:red'>Book</button> </br></span>");
                    // $(".fc-event-container").append("<span class='closon'><img src='../images/AirlineLogo/6E.gif' width='50%' height='20%'> </br> <a>500</a> </br> <button value='Book' style='Background-color:red'>Book</button> </br></span>");

                },
                eventDrop: function (event, delta) {

                },
                eventResize: function (event) {


                },
                eventClick: function (event) {

                },
            });

        }
    });
}

function Check() {
    $("#ModalBook").modal('show');
}

function UpdateRequest(btn, Date, Origin, Destination) {
    var JourneyType = "1";
    // var Class = $("#sel_CalendarClass").val();

    var Sources = new Array();
    var Segment = new Array();
    var spurce = {
        Destination: Destination,
        FlightCabinClass: "",
        Origin: Origin,
        PreferredDepartureTime: Date,
    };
    Segment.push(spurce)

    var PreferredAirlines = new Array();
    PreferredAirlines = null;

    Sources = null;



    Search = {
        Origin: Origin,
        Destination: Destination,
        PreferredAirlines: PreferredAirlines,
        Segments: Segment,
        Sources: Sources,
        TokenId: "",
        JourneyType: JourneyType,
    }

    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/UpdateCalendarSerchFlight",
        data: JSON.stringify({ ObjCalendarSearch: Search }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //window.location.href = "b2bCalendarSearch.aspx";
                //  $("#Update").html('Book');
                GetSearch();
                setTimeout(function () {
                    $("#" + btn)[0].textContent = "Book";
                    document.getElementById(btn).setAttribute("onclick", "Check()")

                }, 3000)
            }
            if (result.retCode == 0) {
                alert(result.error);
            }
        },
        error: function () {
            alert("Somethings went wrong")
        }
    });
}


function Book() {
    var Adult = $("#sel_Adults").val();
    var Child = $("#sel_Child").val();
    var Infant = $("#sel_Infant").val();
    var data = {
        Adult: Adult,
        Child: Child,
        Infant: Infant
    }

    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/BookCalendarFlight",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Search = new Array();
                Search = result.Search;
                window.location.href = "waitforFlights.aspx?Search=" + JSON.stringify(Search);
            }
            if (result.retCode == 0) {
                alert(result.error);
            }
        },
        error: function () {
            alert("Somethings went wrong")
        }
    });
}


