﻿var PriviousSelected, nextSelected;
function SetRoomoccupancy(Selected, RoomCode, Supplier, RateGroup, HotelCode, CutCode) {
    var sRooms = $(Selected).val();
    RoomNo = $(".HotelDetails_" + RoomCode + Supplier).val();
    var otherRooms = $(".Room_" + RoomCode);
    var noRooms = $(".RoomDetails_" + RoomCode).val();
    var remainingToBook = noRooms
    remainingToBook = noRooms - sRooms
    for (var i = 0; i < otherRooms.length; i++) {
        if (remainingToBook != $(otherRooms)[i].value) {
            if ($(otherRooms)[i].value > 0 && $(otherRooms)[i] != Selected) {
                if ($(otherRooms)[i].value - remainingToBook >= 0) {
                    $(otherRooms)[i].value = remainingToBook
                    remainingToBook = $(otherRooms)[i].value - remainingToBook;
                }
                else {
                    remainingToBook = noRooms - 1
                    continue
                }
            }
            else if ($(otherRooms)[i] != Selected) {
                $(otherRooms)[i].selectedIndex = 0;
            }
        }
        else {
            if ($(otherRooms)[i].selectedIndex != 0 && remainingToBook != sRooms) {
                $(otherRooms)[i].value = remainingToBook - 1
            }
            else {
                if (PriviousSelected != null && PriviousSelected == $(otherRooms)[i]) {
                    if (remainingToBook != 0) {
                        $(otherRooms)[i].value = remainingToBook - 1;
                    }

                }
            }

        }

    }
    PriviousSelected = Selected;
    $(Selected).val(sRooms)

    var otherRooms = $("." + RateGroup)


    var RoomId = [], RoomDesc = [], _CutPrice = [], _RoomNo = [], _noRooms = [];
    var selRate = 0;
    for (var i = 0; i < otherRooms.length; i++) {
        if ($(otherRooms)[i].selectedIndex != 0) {

            var rData = $(otherRooms)[i].id.split('^');
            if (rData.length > 1) {
                _noRooms.push($(otherRooms)[i].value);
                for (var j = 0; j < $(otherRooms)[i].value; j++) {
                    selRate += 1;
                    RoomId.push(rData[0]);
                    RoomDesc.push(rData[1]);
                    _CutPrice.push(rData[2]);
                    _RoomNo.push(rData[3]);
                }
            }
        }
    }
    var RoomCount = $(".TotalRomms_" + RateGroup.replace("HotelRoom_", "")).val();
    if (selRate == RoomCount) {
        $("#btn_" + RateGroup.replace("HotelRoom_", "")).removeClass("disabled")
        $("#btn_" + RateGroup.replace("HotelRoom_", "")).attr("disabled", false);
    }
    else {
        $("#btn_" + RateGroup.replace("HotelRoom_", "")).addClass("btn btn-info disabled")
        $("#btn_" + RateGroup.replace("HotelRoom_", "")).attr("disabled", true);
    }
    var data = {
        Supplier: Supplier,
        HotelId: HotelCode,
        CutCode: CutCode,
        RoomId: RoomId,
        RoomDesc: RoomDesc,
        RoomCount: RoomCount,
        _RoomNo: _RoomNo,
        _CutPrice: _CutPrice,
        _noRooms: _noRooms
    }

    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetRoomPrice",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Total = result.Total;
                $(".Total_" + HotelCode + Supplier).empty();
                $(".Total_" + HotelCode + Supplier).append(Total);
                $('[data-toggle="tooltip"]').tooltip();
            }
        },
        error: function () {
        }
    });
}

function GetBookingRates(HotelCode, Supplier, RoomCount, RateGroup, CutCode) {
    var otherRooms;
    var otherRooms = $("." + RateGroup)
    var RoomId = [], RoomDesc = [], _CutPrice = [], _RoomNo = [], _noRooms = [];
    var selRate = 0;
    for (var i = 0; i < otherRooms.length; i++) {
        if ($(otherRooms)[i].selectedIndex != 0) {

            var rData = $(otherRooms)[i].id.split('^');
            if (rData.length > 1) {
                _noRooms.push($(otherRooms)[i].value);
                for (var j = 0; j < $(otherRooms)[i].value; j++) {
                    selRate += 1;
                    RoomId.push(rData[0]);
                    RoomDesc.push(rData[1]);
                    _CutPrice.push(rData[2]);
                    _RoomNo.push(rData[3]);
                }
            }
        }
    }
    if (selRate == RoomCount)
        RedirectHotelRoom(HotelCode, RoomId, RoomDesc, _CutPrice, Supplier, _noRooms, CutCode)
    else {
        Success("Please select Room Before Book")
    }
}
function GetSingleBookingRates(HotelCode, Supplier, RoomTypeId, RoomDescriptionId, Total, CutCode) {
    var RoomId = [], RoomDesc = [], _CutPrice = [], _noRooms = [];
    _noRooms.push(1);
    RoomId.push(RoomTypeId);
    RoomDesc.push(RoomDescriptionId);
    _CutPrice.push(Total);
    RedirectHotelRoom(HotelCode, RoomId, RoomDesc, _CutPrice, Supplier, _noRooms, CutCode)

}

/* Show hide Rate plolices*/
function Refunplolicy(val) {

    var RateGroups = $('.RateGroup');
    if (Refundable.checked) {
        //$(".Refundable").css("display", "")
        $(".RefundGroup_False").hide();
    }
    else {
        //$(".Refundable").css("display", "none")
        $(".RefundGroup_False").hide();
    }
    if (NonRefundable.checked) {
        //$(".nonRefundable").css("display", "")
        $(".RefundGroup_True").hide();
    }
    else {
        //$(".nonRefundable").css("display", "none")
        $(".RefundGroup_True").hide();
    }
}