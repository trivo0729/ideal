﻿var globe_urlParamDecoded = 0;
var global_CategoryCount = [];
var global_HotelItinerary = [];
$(document).ready(function () {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var urlParameter = atob(url[0]).split('=');
    globe_urlParamDecoded = urlParameter[1];
    GetPackageDetail(globe_urlParamDecoded);
});

var List_PackageDetail = [];
var Images = [];

function GetPackageDetail(nID) {
    $("#packagedloader").show();
    var AdminID = 42744;
    var parm = { ID: nID, AdminId: AdminID };
    $.ajax({
        url: "https://trivo.org/Webservices/Packages/PackagesHandler.asmx/PackageDetail",
        type: "post",
        data: JSON.stringify(parm),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = response
            if (result.length == 0) {
                alert("No packages found");
            }
            else if (result.length != 0) {
                List_PackageDetail = result[0];
                Images = List_PackageDetail.Images;
                GetSlidImg();
                GetDetails();
                GetPackgDescription();
                //  console.log(result);
                $("#packagedloader").hide();
                $("#sdb").show();
            }
        },
        error: function () {
            $("#packagedloader").hide();
            alert('Errror in getting Product details, Please try again!');
        }
    });
}

function GetDetails() {
    try {
        var html = "";
        $("#PackageRight").empty();
        html += '<article class="tour-detail">'
        html += '<h6> <b>' + List_PackageDetail.PackageName + '</h6><b>'
        html += '<hr /><br />'
        html += '<h6><b>Category : </b>'
        for (var i = 0; i < List_PackageDetail.Category.length; i++) {
            html += ' ' + List_PackageDetail.Category[i].Categoryname + ''
            if (i != List_PackageDetail.Category.length - 1)
                html += ','
        }
        html += '</h6>'
        html += '                <hr /><br />'
        html += '                   <h6><b>Themes : </b>' + List_PackageDetail.Themes + ''
        html += '</h6>'
        html += '                    <div class="details">'
        html += '                        <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-departure"></i>'
        html += '                            </div>'
        html += '                            <dl class="details" style="text-transform:none">'
        html += '                                <dt class="skin-color">Cities</dt>'
        html += '                                <dd>' + List_PackageDetail.City + '</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                         <div class="icon-box style11 full-width">'
        html += '                            <div class="icon-wrapper">'
        html += '                                <i class="soap-icon-clock"></i>'
        html += '                            </div>'
        html += '                            <dl class="details" style="text-transform:none">'
        html += '                                <dt class="skin-color">Duration</dt>'
        html += '                                <dd>' + ((List_PackageDetail.noDays) - 1) + ' Nights  /' + List_PackageDetail.noDays + ' Days</dd>'
        html += '                            </dl>'
        html += '                        </div>'
        html += '                    </div>'
        html += '                </article>'
        $("#PackageRight").html(html);
    }
    catch (e) {

    }

}

function GetSlidImg() {
    try {
        var html = "";
        $("#PackgImg").empty();

        html += '<div class="photo-gallery style1" data-animation="slide" data-sync="#PackgImg .image-carousel">'
        html += '<ul class="slides">'
        for (var i = 0; i < Images.length; i++) {
            html += '    <li><img style="height: 500px;" src="' + Images[i].Url + '" alt="" /></li>'
        }
        html += '</ul>'
        html += '</div>'

        html += '<div class="image-carousel style1" style="margin-left: 340px; margin-right: 340px;" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#PackgImg .photo-gallery">'
        html += '<ul class="slides">'
        for (var j = 0; j < Images.length; j++) {
            html += '<li><img src="' + Images[j].Url + '" alt="" /></li>'
        }
        html += '</ul>'
        html += '</div>'
        $("#PackgImg").html(html);
        var tpj = jQuery;
        tjq('.image-carousel').each(function () {
            displayImageCarousel(tjq(this));
        });
        tjq('.photo-gallery').each(function () {
            displayPhotoGallery(tjq(this));
        });
    }

    catch (e) {

    }
}

function GetPackgDescription() {
    try {
        var html = "";
        $("#PackgDescription").empty();
        var Category = List_PackageDetail.Category;
        if (List_PackageDetail.Description != "") {
            html += '<div class="row">'
            html += '<h3> Description </h3> ';
            html += '<div class="day_duration" style="text-align:justify">' + List_PackageDetail.Description + ''
            html += '</div>'
            html += '  </br>'
        }
        var Inclusion = List_PackageDetail.arrInclusions;
        var Exclusion = List_PackageDetail.arrExclusions;
        if (Inclusion.length != 0) {
            html += '<br/>'
            html += '<div class="day_duration">'
            html += '<div class="row">'
            html += '<div class="col-sm-6">'
            html += '<h3> Inclusions </h3>'
            html += '<ul class="features check">'
            for (var i = 0; i < Inclusion.length; i++) {

                html += '<li>' + Inclusion[i] + '</li>';
            }
            html += '</ul>'
            html += '</div>'
            if (Exclusion.length != 0) {
                html += '<div class="col-sm-6">'
                html += '<h3> Exclusions </h3>'
                html += '<ul class="cross">'
                for (var i = 0; i < Exclusion.length; i++) {
                    html += '<li>' + Exclusion[i] + '</li>';
                }
                html += '</div>'
            }
            html += '</div>'
            html += '</div>'
            html += '</br>'
        }
        var itineraryarr = List_PackageDetail.Itinerary;
        if (itineraryarr.length != 0) {
            html += '<div class="panel-headin">'
            html += '<ul class="nav nav-tabs nav-justified">'
            html += '<div class="panel-body mng_panel left_sec">'
            html += '<div class="tab-content">'
            html += '<div class="boxi">'
            html += '<ul class="nav nav-tabs nav-justified">'
            html += '    <li class="nav-item active"><a class="nav-link" href="#Itinerary_" data-toggle="tab">Itinerary</a></li>'
            html += '</ul>'
            html += '<div class="tab-content">'
            html += Itinerary(c);
            html += '</div>'
            html += '</div>'
        }

        ////
        html += '<div class="panel-headin">'
        html += '<ul class="nav nav-tabs nav-justified">'
        for (var i = 0; i < Category.length; i++) {
            Class = "";
            if (i == 0) {
                Class = "active"
            }
            html += '<li class="PkgType ' + Class + ' nav-item">'
            html += '<input type="hidden" id=Id_' + Category[i].Categoryname + ' name="name" value="' + Category[i].CategoryID + '" />'
            html += '<a class="nav-link" href="#' + Category[i].Categoryname + '"  data-toggle="tab">' + Category[i].Categoryname + '</a>'
            html += '</li>';
        }
        html += '</ul>'
        html += '</div>'

        html += '<div class="panel-body mng_panel left_sec">'
        html += '<div class="tab-content">'

        for (var c = 0; c < Category.length; c++) {
            var Class = "";
            if (c == 0) {
                Class = "in active";
            }
            html += '<div class="tab-pane fade ' + Class + '" id="' + Category[c].Categoryname + '">'
            html += '<div class="boxi">'
            html += '<ul class="nav nav-tabs nav-justified">'
            // html += '    <li class="nav-item active"><a class="nav-link" href="#Itinerary_' + Category[c].Categoryname + '" data-toggle="tab">Itinerary</a></li>'
            html += '    <li class="nav-item active"><a class="nav-link" href="#Pricing_' + Category[c].Categoryname + '" data-toggle="tab">Price</a></li>'
            //  html += '    <li class="nav-item"><a class="nav-link" href="#Policy_' + Category[c].Categoryname + '" data-toggle="tab">Policy</a></li>'
            html += '    <li class="nav-item"><a class="nav-link" href="#HotelDetails_' + Category[c].Categoryname + '" data-toggle="tab">Hotel</a></li>'
            html += '</ul>'
            html += '<div class="tab-content">'
            html += TabContains(c);
            html += '</div>'
            html += '</div>'
            html += '</div>'
        }

        html += '</div>'
        html += '</div>'

        //////
        if (List_PackageDetail.TermsCondition != "") {
            html += '<div class="day_duration">'
            html += '<div class="row">'
            html += '<div class="col-md-12">'
            html += '<p>'
            html += '<h4>Terms & Conditions </h4>'
            html += '<div class="" id="terms">' + List_PackageDetail.TermsCondition + ''
            html += '</div>'
            html += '</p>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
        }


        $("#PackgDescription").append(html);
        $("#terms ul").addClass("features check");

    } catch (e) {

    }
}

//function GetPackgDescription() {
//    try {
//        var html = "";
//        $("#PackgDescription").empty();
//        var Category = List_PackageDetail.Category;
//        html += '<ul class="tabs">'
//        for (var i = 0; i < Category.length; i++) {
//            Class = "";
//            if (i == 0) {
//                Class = "active"
//            }
//            html += '<li class="PkgType ' + Class + '">'
//            html += '<input type="hidden" id=Id_' + Category[i].Categoryname + ' name="name" value="' + Category[i].CategoryID + '" />'
//            html += '<a style="text-transform: none;" href="#' + Category[i].Categoryname + '"  data-toggle="tab">' + Category[i].Categoryname + '</a>'
//            html += '</li>';
//        }
//        html += '</ul>'
//        html += '<div class="tab-container box" style="border: 2px solid #ffb300;">'
//        html += '<div class="tab-content">'

//        for (var c = 0; c < Category.length; c++) {
//            var Class = "";
//            if (c == 0) {
//                Class = "in active";
//            }
//            html += '<div class="tab-pane fade ' + Class + '" style="padding:8px" id="' + Category[c].Categoryname + '">'
//            html += '<ul class="tabs">'
//            html += '    <li class="active"><a style="text-transform: none;" href="#Itinerary_' + Category[c].Categoryname + '" data-toggle="tab">Itinerary</a></li>'
//            html += '    <li><a style="text-transform: none;" href="#Pricing_' + Category[c].Categoryname + '" data-toggle="tab">Price</a></li>'
//            html += '    <li><a style="text-transform: none;" href="#Policy_' + Category[c].Categoryname + '" data-toggle="tab">Policy</a></li>'
//            html += '    <li><a style="text-transform: none;" href="#HotelDetails_' + Category[c].Categoryname + '" data-toggle="tab">Hotel</a></li>'
//            html += '</ul>'
//            html += '<div class="tab-content">'
//            html += TabContains(c);
//            html += '</div>'
//            html += '</div>'
//        }

//        html += '</div>'
//        html += '</div>'

//        $("#PackgDescription").append(html);
//        $("#terms ul").addClass("features check");

//    } catch (e) {

//    }
//}

function TabContains(c) {
    var html = "";
    //  html += Itinerary(c);
    html += Price(c);
    //   html += Policy(c);
    html += Hotel(c);
    return html;
}

//function Itinerary(c) {
//    var html = "";
//    html += '<div class="tab-pane fade in active" style="padding:8px" id="Itinerary_' + List_PackageDetail.Category[c].Categoryname + '">'
//    html += '<div class="col-md-12">'
//    html += '<div class="notification-area">'
//    var Itinerary = List_PackageDetail.Category[c].Itinerary;
//    for (var i = 0; i < Itinerary.length ; i++) {
//        html += '<div class="info-box block" id="Days">'
//        html += '<div class="date"><label class="month">Day</label><label class="date">' + Itinerary[i].noDay + '</label></div>'
//        html += '<p>' + Itinerary[i].Description + '</p>'
//        html += '</div><br/>'
//    }
//    html += '</div>'
//    html += '</div>'
//    html += '</div>'
//    return html;
//}

function Itinerary(c) {
    var html = "";
    //   html += '<div class="tab-pane text-align form-new active" id="Itinerary_' + List_PackageDetail.Itinerary[c].Categoryname + '">'
    html += '<section class="Material-contact-section section-padding section-dark">'
    html += '<div class="">'
    html += '<div class="">'
    html += '<h3>Day Wise Itinerary</h3>'
    html += '</div>'
    html += '<ul class="timeline">'
    var Itinerary = List_PackageDetail.Itinerary;
    for (var i = 0; i < Itinerary.length ; i++) {
        var number = parseInt(i);
        if (number % 2 == 0)
            html += '<li>'
        else
            html += '<li class="timeline-inverted">'
        html += '<div class="timeline-badge"><i class="soap-icon-departure"></i></div>'
        html += '<div class="timeline-panel">'
        html += '<div class="timeline-heading">'
        html += '<h4 class="timeline-title">Day ' + Itinerary[i].noDay + '</h4>'
        html += '</div>'
        html += '<div class="timeline-body">' + Itinerary[i].Description + ''
        html += '</div>'
        html += '</div>'
        html += '</li>'
    }
    html += '</ul>'
    html += '</div>'
    html += '</section>'
    //   html += '</div>'
    return html;
}
//function Itinerary(c) {
//    var html = "";
//    html += '<div class="tab-pane text-align form-new active" id="Itinerary_' + List_PackageDetail.Category[c].Categoryname + '">'
//    html += '<section class="Material-contact-section section-padding section-dark">'
//    html += '<div class="">'
//    html += '<div class="">'
//    html += '<h3>Day Wise Itinerary</h3>'
//    html += '</div>'
//    html += '<ul class="timeline">'
//    var Itinerary = List_PackageDetail.Category[c].Itinerary;
//    for (var i = 0; i < Itinerary.length ; i++) {
//        var number = parseInt(i);
//        if (number % 2 == 0)
//            html += '<li>'
//        else
//            html += '<li class="timeline-inverted">'
//        html += '<div class="timeline-badge"><i class="soap-icon-departure"></i></div>'
//        html += '<div class="timeline-panel">'
//        html += '<div class="timeline-heading">'
//        html += '<h4 class="timeline-title">Day ' + Itinerary[i].noDay + '</h4>'
//        html += '</div>'
//        html += '<div class="timeline-body">' + Itinerary[i].Description + ''
//        html += '</div>'
//        html += '</div>'
//        html += '</li>'
//    }
//    html += '</ul>'
//    html += '</div>'
//    html += '</section>'
//    html += '</div>'
//    return html;
//}

function Price(c) {

    var html = "";
    html += ' <div class="tab-pane text-align form-new active" id="Pricing_' + List_PackageDetail.Category[c].Categoryname + '">'
    html += ' <div class="col-sm-12 col-lg-12 table-cell cruise-itinerary">'
    html += '<br/>'
    html += '<div class="">'
    html += '<h3>Price Detail</h3>'
    html += '</div>'
    html += '  <table>'
    html += '    <thead>'
    html += '      <tr>'
    html += '        <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Start Date<b/></th>'
    html += '        <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>End Date<b/></th>'
    if (List_PackageDetail.Category[c].Single != 0)
        html += '<th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Single<b/></th>'

    if (List_PackageDetail.Category[c].Double != 0)
        html += '<th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Double Sharing<b/></th>'

    if (List_PackageDetail.Category[c].Triple != 0)
        html += '<th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Triple Sharing<b/></th>'

    if (List_PackageDetail.Category[c].Quad != 0)
        html += '<th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Quad Sharing<b/></th>'

    if (List_PackageDetail.Category[c].Quint != 0)
        html += '<th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Quint Sharing<b/></th>'

    if (List_PackageDetail.Category[c].ExtraAdult != 0) {
        html += '        <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Extra Adult<b/></th>'
    }

    if (List_PackageDetail.Category[c].ChildNoBedPrice) {
        html += '        <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Kid W/O Bed<b/></th>'
    }
    if (List_PackageDetail.Category[c].ChildWithBedPrice) {
        html += '        <th style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><b>Kid With Bed<b/></th>'
    }
    html += '      </tr>'
    html += '    </thead>'
    html += '    <tbody>'
    html += '      <tr>'
    html += '        <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"> ' + List_PackageDetail.Category[c].ValidFrom + '</td>'
    html += '        <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"> ' + List_PackageDetail.Category[c].ValidUpto + '</td>'
    if (List_PackageDetail.Category[c].Single != 0)
        html += '<td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].Single + '</td>'

    if (List_PackageDetail.Category[c].Double != 0)
        html += '<td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].Double + '</td>'

    if (List_PackageDetail.Category[c].Triple != 0)
        html += '<td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].Triple + '</td>'

    if (List_PackageDetail.Category[c].Quad != 0)
        html += '<td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].Quad + '</td>'

    if (List_PackageDetail.Category[c].Quint != 0)
        html += '<td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].Quint + '</td>'

    if (List_PackageDetail.Category[c].ExtraAdult != 0) {
        html += '        <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].ExtraAdult + '</td>'
    }

    if (List_PackageDetail.Category[c].ChildNoBedPrice != 0) {
        html += '        <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].ChildNoBedPrice + '</td>'
    }
    if (List_PackageDetail.Category[c].ChildWithBedPrice != 0) {
        html += '        <td style="border: 1px solid #dddddd;text-align: center;padding: 8px;"><i class="fa fa-inr"></i> ' + List_PackageDetail.Category[c].ChildWithBedPrice + '</td>'
    }
    html += '      </tr>'
    html += '    </tbody>'
    html += '  </table><br/>'
    html += '  <blockquote class="style1">'
    html += '  <span class="triangle"></span>' + List_PackageDetail.Category[c].PriceNote + ''
    html += '  </blockquote>'
    html += '</div>'
    html += '</div>'

    return html;
}

function Policy(c) {

    var html = "";

    html += ' <div class="tab-pane fade text-align form-new" id="Policy_' + List_PackageDetail.Category[c].Categoryname + '">'
    html += '<section class="Material-contact-section section-padding section-dark">'

    html += '<div class="">'
    html += '<h3> Description </h3> ';
    html += '<div class="day_duration">' + List_PackageDetail.Description + ''
    html += '</div>'
    html += '<br/>'
    html += '<div class="day_duration">'
    html += '<div class="row">'
    html += '<div class="col-sm-6">'
    if (List_PackageDetail.Category[c].Inclusions.length > 0 || List_PackageDetail.Category[c].PackageInclusion.PackageMeal.length > 0 || List_PackageDetail.Category[c].PackageInclusion.PackageActivity.length > 0 || List_PackageDetail.Category[c].PackageInclusion.PackageVehicle > 0 || List_PackageDetail.Category[c].PackageInclusion.PackageTax.length > 0 || List_PackageDetail.Category[c].PackageInclusion.PackageOther.length > 0) {
        html += '<h5> Inclusions </h5>'
    }
    html += '<ul class="features check">'
    var Inclusions = List_PackageDetail.Category[c].Inclusions;
    for (var i = 0; i < Inclusions.length; i++) {
        html += '<li>' + Inclusions[i].Name + '</li>';
    }
    html += '</ul>'

    // For Meals Inclusion 
    var Meals = List_PackageDetail.Category[c].PackageInclusion.PackageMeal;
    if (Meals.length > 0) {
        html += '<h5> Meal </h5>';
        html += '<ul class="features check">'
        for (var i = 0; i < Meals.length; i++) {

            html += '<li>' + Meals[i].MealName + '</li>';
        }
        html += '</ul>'
    }
    //For Activity Inclusion
    var PackageActivity = List_PackageDetail.Category[c].PackageInclusion.PackageActivity;

    if (PackageActivity.length > 0) {
        html += '<h5> Activity </h5>';
        html += '<ul class="features check">'
        for (var i = 0; i < PackageActivity.length; i++) {

            html += '<li>' + PackageActivity[i].PackageActivityName + '</li>';

        }
        html += '</ul>'

    }
    // For Vehicle inclusion
    var PackageVehicle = List_PackageDetail.Category[c].PackageInclusion.PackageVehicle;
    if (PackageVehicle.length > 0) {
        html += '<h5> Transfers </h5>';
        html += '<ul class="features check">'
        for (var i = 0; i < PackageVehicle.length; i++) {

            html += '<li>' + PackageVehicle[i].VehicleName + '</li>';

        }
        html += '</ul>'
    }
    // For Package Tax inclusion
    var PackageTax = List_PackageDetail.Category[c].PackageInclusion.PackageTax;
    if (PackageTax.length > 0) {
        html += '<h5> Tax </h5>'
        html += '<ul class="features check">'

        for (var i = 0; i < PackageTax.length; i++) {

            html += '<li>' + PackageTax.TaxValue + "%" + PackageTax.Name + '</li>';

        }
        html += '</ul>';
    }
    // For Others Inclusion 
    var PackageOther = List_PackageDetail.Category[c].PackageInclusion.PackageOther;
    if (PackageOther.length > 0) {
        html += '<h5> Other </h5>';
        html += '<ul class="features check">'
        for (var i = 0; i < PackageOther.length; i++) {

            html += '<li>' + PackageOther.Name + '</li>';

        }
        html += '</ul>';
    }

    html += '</div>'

    html += '<div class="col-sm-6">'
    if (List_PackageDetail.Category[c].Exclusion.length > 0 || List_PackageDetail.Category[c].PackageExclusion.PackageMeal.length > 0 || List_PackageDetail.Category[c].PackageExclusion.PackageActivity.length > 0 || List_PackageDetail.Category[c].PackageExclusion.PackageVehicle.length > 0 || List_PackageDetail.Category[c].PackageExclusion.PackageTax.length > 0 || List_PackageDetail.Category[c].PackageExclusion.PackageOther.length > 0) {
        html += '<h5> Exclusions </h5>'
    }
    html += '<ul class="features check">'
    var Exclusion = List_PackageDetail.Category[c].Exclusion;
    for (var i = 0; i < Exclusion.length; i++) {
        html += '<li>' + Exclusion[i].Name + '</li>'
    }
    html += '</ul>'

    // For Package Meal Exclusion
    var PackageMeals = List_PackageDetail.Category[c].PackageExclusion.PackageMeal;
    if (PackageMeals.length > 0) {
        html += '<h5>Meal</h5>'
        html += '<ul class="features check">'
        for (var i = 0; i < PackageMeals.length; i++) {

            html += '<li>' + PackageMeals[i].MealName + '</li>';
        }
        html += '</ul>'
    }
    // For Package Activity Exclusion
    var PackageActivities = List_PackageDetail.Category[c].PackageExclusion.PackageActivity;
    if (PackageActivities.length > 0) {
        html += '<h5>Activity</h5>'
        html += '<ul class="features check">'
        for (var i = 0; i < PackageActivities.length; i++) {
            html += '<li>' + PackageActivities[i].PackageActivityName + '</li>';
        }
        html += '</ul>'
    }

    // For Package Vehicle Exclusion
    var PackageVehicles = List_PackageDetail.Category[c].PackageExclusion.PackageVehicle;
    if (PackageVehicles.length > 0) {
        html += '<h5>Transfers</h5>'
        html += '<ul class="features check">'
        for (var i = 0; i < PackageVehicles.length; i++) {
            html += '<li>' + PackageVehicles[i].VehicleName + '</li>';
        }
        html += '</ul>'
    }
    // For Package  Tax Exclusion
    var PackageTaxes = List_PackageDetail.Category[c].PackageExclusion.PackageTax;
    if (PackageTaxes.length > 0) {
        html += '<h5>Tax</h5>'
        html += '<ul class="features check">'
        for (var i = 0; i < PackageTaxes.length ; i++) {
            html += '<li>' + PackageTaxes[i].TaxValue + '</li>';
        }
        html += '</ul>'
    }
    // For Package Other Details
    var PackageOthers = List_PackageDetail.Category[c].PackageExclusion.PackageOther;
    if (PackageOthers.length > 0) {
        html += '<h5>Other</h5>'
        html += '<ul class="features check">'
        for (var i = 0; i < PackageOthers.length ; i++) {
            html += '<li>' + PackageOthers[i].Name + '</li>';
        }
        html += '</ul>'
    }

    html += '</div>'
    html += '</div>'
    html += '</div>'

    html += '<br />'
    html += '<div class="day_duration">'
    html += '<div class="row">'
    html += '<div class="col-md-12">'
    html += '<p>'
    html += '<h4>Terms & Conditions </h4>'
    html += '<div class="" id="terms">' + List_PackageDetail.TermsCondition + ''
    html += '</div>'
    html += '</p>'
    html += '</div>'
    html += '</div>'
    html += '</div>'

    html += '</div>'
    html += '</section>'
    html += '</div>'


    return html;
}

function Hotel(c) {

    debugger;
    var html = "";
    html += '<div class="tab-pane fade" id="HotelDetails_' + List_PackageDetail.Category[c].Categoryname + '">'
    html += '<br/>'
    html += '<div class="col-md-12">'
    var Hotel = List_PackageDetail.Category[c].PackageHotels;
    for (var i = 0; i < Hotel.length; i++) {
        html += '<div class="clearfix main-iti-divs">'
        html += '     <p class="itinere-head" style="color:#000"><span class="dayss"> ' + Hotel[i].Nights + ' Nights</span> ' + Hotel[i].HotelName + '</p>'
        html += '     <div class="row">'
        html += '         <div class="col-sm-3 text-center">'
        if (Hotel[i].PackHotelImage.length != 0) {
            html += '                  <img class="img-responsive img-thumbnail" src="http://clickurtrip.net/HotelImages/' + Hotel[i].HotelId + '/' + Hotel[i].PackHotelImage[0].Url + '" alt="">'
        }
        html += '</div>'
        html += '         <div class="col-sm-9 text-center-res">'
        html += '             <p class="content-tans-p">' + Hotel[i].Description + '</p>'
        html += '         </div>'
        html += '     </div>'
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'

    return html;
}

function BookMyTrip() {
    $("#BookingModal").modal("show")
}
function NoChild(Val) {
    if (Val == 0) {
        $("#div_FirstChild").css("display", "none")
        $("#div_SecondChild").css("display", "none")
        $("#Childs").atrr("class", "col-md-12")
    }
    else if (Val == 1) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "none")
    }
    else if (Val == 2) {
        $("#div_FirstChild").css("display", "")
        $("#div_SecondChild").css("display", "")
    }
}

var Name, MobileNo, Email, noAdults, noChild, TarvelDate, AddOnReq, ChildAges = "0";
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function SendMail() {
    var from = List_PackageDetail[0].dValidityFrom.split(' ');
    var To = List_PackageDetail[0].dValidityTo.split(' ');
    var Amt = PackagePricing[0].dSingleAdult;
    var bValid = true;
    Name = $("#txt_name").val()
    MobileNo = $("#txt_phone").val()
    Email = $("#txt_email").val()
    noAdults = $("#Select_Adults").val()
    noChild = $("#Select_CHILD").val()
    TarvelDate = $("#txt_date").val()
    AddOnReq = $("#txt_Remark").val()
    bValid = ValidateMail();
    if (bValid == true) {
        var data = {
            nID: globe_urlParamDecoded,
            Name: Name,
            MobileNo: MobileNo,
            Email: Email,
            noAdults: noAdults,
            noChild: noChild,
            ChildAges: ChildAges,
            TarvelDate: TarvelDate,
            AddOnReq: AddOnReq,
            PackageName: $("#dspPackageName").text(),
            Amt: Amt,
            StartDate: from[0],
            TillDate: To[0],
        }
        var Jason = JSON.stringify(data)
        $.ajax({
            type: "POST",
            url: "PackageHandler.asmx/BookPackage",
            data: Jason,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result == 1) {
                    $("#BookingModal").modal("hide")
                    alert("Thanks For Booking, We Will Update you when it is Conform by Administrator..", null)

                }
            },
            error: function () {
                alert("An error occured", null)
            }
        });
    }
}
function ValidateMail() {
    if (Name == "") {
        $("#txt_name").focus();
        alert("Please Insert  Passenger Name", "txt_name")
        return false;
    }
    if (MobileNo == "") {
        $("#txt_name").focus();
        alert("Please Insert  Mobile No", "txt_phone")
        return false;
    }
    if (Email == "") {
        $("#txt_email").focus();
        alert("Please Insert  MailId", "txt_email")
        return false;
    }
    if (!emailReg.test(Email)) {
        $("#txt_email").focus();
        alert("Please Insert Valid MailId", "txt_email")
        return false;
    }
    if (noChild != "0") {
        if (noChild == "1") {
            ChildAges = $("#Select_AgeChildSecond1").val()
        }
        else if (noChild == "2") {
            ChildAges = $("#Select_AgeChildSecond1").val() + "," + $("#Select_AgeChildSecond2").val()

        }
        return true
    }
    if (TarvelDate == "") {
        $("#txt_date").focus();
        alert("Please Insert Tarvel Date", "txt_date")
        return false;
    }
    if (AddOnReq == "") {
        $("#txt_Remark").focus();
        alert("Please Insert Add Request", "txt_Remark")
        return false;
    }
    return true
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var count = "";

function Booking() {
    for (var i = 0; i < $(".PkgType").length ; i++) {
        $($(".PkgType")[i]).hasClass(".active")
        if ($($(".PkgType")[i])[0].className == "PkgType active nav-item") {
            var ID = $($(".PkgType")[i])[0].innerHTML;
            ID = $("#Id_" + $($(".PkgType")[i])[0].innerText).val();
            $(location).attr('href', '/PackageBooking.aspx?' + btoa('CatId=' + ID + "&Id=" + globe_urlParamDecoded));
        }
    }
}


function PackageMailModal() {
    $("#PackageMailModal").modal('show');
}

function SendPackageMail() {
    $("#lbl_Email").hide();
    var Email = $("#Email").val();
    var Name = $("#Name").val();
    var Remark = $("#Remark").val();
    var AdminID = 42744;
    var PackageName = List_PackageDetail.PackageName;
    if (Email == "") {
        $("#lbl_Email").show();
        return false;
    }
    var data = {
        ID: globe_urlParamDecoded,
        PackageName: PackageName,
        Email: Email,
        Name: Name,
        Remark: Remark,
        ParentID: AdminID
    }
    var Jason = JSON.stringify(data)
    $.ajax({
        type: "POST",
        url: "https://trivo.org/Webservices/Packages/PackagesHandler.asmx/SendPackageDetails",
        data: Jason,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = response;
            //  var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (obj.retCode == 1) {
                alert("Package Details Sent On Your Email Successfully  ..");
                $("#PackageMailModal").modal('hide');
            }
        },
        error: function () {
            alert("An error occured")
        }
    });
}