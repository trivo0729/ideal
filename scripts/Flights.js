﻿$(function () {
    GetSearch()
});
var arrFlights = new Array();
var arrSearch = new Array();
function GetSearch() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/Getflight",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSearch = result.arrFligts;
                arrFlights = result.arrFligts.FlightList;
                GenrateFilter();
                GenrateHtml();
                tpj("#datepicker_Return").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: "dateToday",
                });

                tpj("#dt_CalendarMonthYear").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: "dateToday",
                });
                $('#sel_Airlines').change(function () {
                    console.log($(this).val());
                }).multipleSelect({
                    width: '100%'
                });
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.Message);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function GenrateHtml() {
    var html = '';
    $("#div_Airline").empty();
    html += '<div class="row">'
    if ((arrSearch.JourneyType == "1") || ((arrSearch.JourneyType == "5") && arrSearch.IsDomastic == false)) {
        html += '<div class="col-sm-12">'
        html += OrderByFilter('Out');
        html += '<div class="flight-list listing-style3 flight">'
        for (var i = 0; i < arrFlights[0].length; i++) {
            debugger
            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += htmlbuilder(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].FareTip.replace('↵', '\n'), arrFlights[0][i].Fare, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
        }
        html += '<div>'
        html += '<div>'

    }
    else if (arrSearch.JourneyType == "2" && arrSearch.IsDomastic == false) {
        html += '<div class="col-sm-12">'
        html += OrderByFilter('Out');
        html += '<div class="flight-list listing-style3 flight">'
        for (var i = 0; i < arrFlights[0].length; i++) {

            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += GenrateSegmentsNonDom(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].FareTip.replace('↵', '\n'), arrFlights[0][i].Fare, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
        }
        html += '</div>                                                                                                        '
        html += '</div>                                                                                                        '
    }
    else if (arrSearch.JourneyType == "3") {
        html += '<div class="col-sm-12">'
        html += OrderByFilter('Out');
        html += '<div class="flight-list listing-style3 flight">'
        for (var i = 0; i < arrFlights[0].length; i++) {

            var Class = "1";
            if (i % 2 === 0) { Class = "1" }
            else { Class = "2" }
            html += GenrateSegmentsMulti(arrFlights[0][i].Segment, Class, arrFlights[0][i].Logo, arrFlights[0][i].FareTip.replace('↵', '\n'), arrFlights[0][i].Fare, arrFlights[0][i].Remark, arrFlights[0][i].ResultIndex, arrFlights[0][i].Refunadble)
        }
        html += '</div>                                                                                                        '
        html += '</div>                         '
    }

    else if ((arrSearch.JourneyType == "2" || arrSearch.JourneyType == "5") && arrSearch.IsDomastic) {
        try {
            html += '<div class="all-searchbox">'
            /*Departure*/
            html += '<div class="col-md-6 col-xs-6 pad-rightn" id="div_Out">'
            html += OrderByFilterRound('Out');
            html += '<div class="row">'
            var arrDeparture = arrSearch.FlightList[0];
            for (var i = 0; i < arrDeparture.length; i++) {
                var Class = "1";
                if (i % 2 === 0) { Class = "1" }
                else { Class = "2" }
                html += Genrate2WSegments(arrDeparture[i].Segment, Class + ' roubdbk1', arrDeparture[i].Logo, arrDeparture[i].FareTip.replace('↵', '\n'), arrDeparture[i].Fare, arrDeparture[i].Remark, arrDeparture[i].ResultIndex, arrDeparture[i].Refunadble)
            }
            html += '</div>'
            html += '</div>'
            /*Departure*/
            html += '<div class="col-md-6 col-xs-6 pad-rightn" id="div_In">'
            html += OrderByFilterRound('In');
            html += '<div class="row">'
            var arrArrival = arrSearch.FlightList[1]

            for (var j = 0; j < arrArrival.length; j++) {
                var Class = "2";
                if (j % 2 === 0) { Class = "2" }
                else { Class = "1" }
                html += Genrate2WSegments(arrArrival[j].Segment, Class + ' roubdbk2', arrArrival[j].Logo, arrArrival[j].FareTip.replace('↵', '\n'), arrArrival[j].Fare, arrArrival[j].Remark, arrArrival[j].ResultIndex, arrArrival[j].Refunadble)
            }
            html += '</div>'
            html += '</div>'

            html += '</div>'
        }
        catch (ex) {
            alert(ex.message)
        }

    }
    html += '<div>'
    $("#div_Airline").append(html);
    $('.FareType').popover({ trigger: "hover" });
    $('.FareRule').popover({ trigger: "hover" });
    $('.Bge').popover({ trigger: "hover" });
    $('.stp').popover({ trigger: "hover" });
    $('.Price').popover({ trigger: "hover" });
    $('[data-toggle="tooltip"]').tooltip();
    GetSelection();
    GetCityForModify();
}

function htmlbuilder(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';
    var Bg = '';
    var calhours = 0;
    var calminutes = 0;
    html += '<div class="all-searchbox">                                                                                                                                     '
    html += '<div class="all-box-multiwaydata">                                                                                                                          '
    html += '<div class="col-lg-12">                                                                                                                                 '
    html += '<div class="row mb10" style="padding: 0px 0px 3px 10px;">                                                                                           '
    html += ' <div class="col-lg-3" style="font-size: 14px;">                                                                                                 '
    html += '</div>                                                                                                               '
    html += '<div class="col-lg-9 content_onewayprice" style="text-align: right;font-size: 14px;">                                                           '
    var Baggage = '';
    var Operate = '';
    Bg += " <table>"
    Bg += "<thead>                  "
    Bg += "    <tr align='center'>  "
    Bg += "    <th>Sector</th>      "
    Bg += "    <th>Baggage</th>     "
    Bg += "    <th>OperatedBy</th>  "
    Bg += "    </tr>                "
    Bg += "</thead>                 "
    Bg += "<tbody>                  "
    for (var b = 0; b < arrSegment.length; b++) {
        for (var g = 0; g < arrSegment[b].length; g++) {
            Baggage = arrSegment[b][g].Baggage
            Operate = arrSegment[b][g].Airline.AirlineCode;
            var cn = arrSegment[b].length - 1
            var deptdest = arrSegment[b][g].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[b][g].DepartureAt).format("LT");
            var arrdest = arrSegment[b][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[b][cn].ArrivalFrom.CityName;
            if (g == 0) {
                Bg += "<tr align='center'>"
                Bg += "<td>" + deptdest + ' - ' + arrdest + ""
                Bg += " </td>"
                Bg += " <td>"
                Bg += "   " + Baggage + ""
                Bg += " </td>"
                Bg += " <td>" + Operate + ""
                Bg += " </td>"
                Bg += "</tr>"
            }
        }
    }
    Bg += "</tbody>"
    Bg += "</table>"
    html += '<a class="Bge" href="#" data-container="body" data-html="true" data-content="' + Bg + '" rel="popover" data-placement="top">'
    html += '<i class="soap-icon-businessbag circle" style="font-size: 12px;cursor:pointer;color:#838383">'
    html += '</i></a>&nbsp;'
    html += '<a class="FareRule" href="#" data-container="body" data-content="Fare Rule" onclick="GetFareRule(\'' + ResultIndex + '\')" rel="popover" data-placement="top"><i class="soap-icon-status circle" style="font-size: 12px;cursor:pointer;color:#838383"></i>'
    html += '</a>&nbsp;'
    if (Refunadble) {
        html += '<a class="FareType" href="#" data-container="body" data-content="Refundable" rel="popover" data-placement="top"><i class="soap-icon-stories circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    else {
        html += '<a class="FareType" href="#" data-container="body" data-content="Non-Refundable" rel="popover" data-placement="top"><i class="soap-icon-liability circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    html += '</div>                                                                                                               '
    html += '</div>                                                                                                                                          '
    html += '</div>                                                                                                                                              '
    html += '<div class="col-lg-12">                                                                                                                             '
    html += '<div class="row rowfs">                                                                                                                         '
    html += '<div class="col-md-10 col-lg-10 col-xs-10 multiway-leftblock">'
    for (var s = 0; s < arrSegment.length; s++) {
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage
            Operate = arrSegment[s][i].Airline.AirlineCode;
            var htmlc = "";
            for (var k = 0; k < arrSegment[s].length; k++) {

                var cnminutes = parseInt(arrSegment[s][k].Duration);
                var cnhours = "";
                cnhours = Math.floor(cnminutes / 60);
                cnminutes = cnminutes % 60;

                calhours = calhours + cnhours;
                calminutes = calminutes + cnminutes;
                htmlc += "<tr align='center'>                                                      "
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].DepartureAt).format('LT') + "<br>" + arrSegment[s][k].DepartureFrom.CityName + "(" + arrSegment[s][k].DepartureFrom.AirportCode + ") "
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].ArrivalTime).format('LT') + "<br>" + arrSegment[s][k].ArrivalFrom.CityName + "(" + arrSegment[s][k].ArrivalFrom.AirportCode + ")"
                htmlc += "</td>"
                htmlc += "</tr>"
            }
            var Stop = arrSegment[s].length - 1;
            if (Stop == 0) {
                Stop = "Non"
            }
            var cn = arrSegment[s].length - 1
            var deptdest = arrSegment[s][i].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[s][i].DepartureAt).format("LT");
            var arrdest = arrSegment[s][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[s][cn].ArrivalFrom.CityName;
            var arrtime = moment(arrSegment[s][cn].ArrivalTime).format("LT");
            var arrtimeFull = moment(arrSegment[s][cn].ArrivalTime).format("ll");
            // get value
            var minutes = parseInt(arrSegment[s][cn].Duration);
            // calculate
            var hours = "";
            hours = Math.floor(minutes / 60);
            minutes = minutes % 60;
            if (i == 0) {
                html += ' <div class="row mb10">                                                                                                                  '
                html += '<div class="col-md-4 col-lg-3 col-xs-4 tour-multi-waydays">                                                                         '
                html += '<div class="row">                                                                                                               '
                html += '<div class="col-md-3 col-lg-2 col-xs-12">                                                                                   '
                html += GetLogo(Logo)
                html += '&nbsp;</div>                                                                                               '
                html += '<div class="col-md-9 col-lg-10 col-xs-12">                                                                                  '
                html += '        <span>' + arrSegment[s][i].Airline.AirlineName + '</span><br><span>' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + '</span>'
                html += '</div>                                                                                               '
                html += '</div>                                                                                                                      '
                html += '</div>                                                                                                                          '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                                      '
                html += '    <span>' + arrSegment[s][i].DepartureFrom.AirportCode + '(' + arrSegment[s][i].DepartureFrom.CityName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + moment(arrSegment[s][i].DepartureAt).format("LT") + '</span><br><span>' + moment(arrSegment[s][i].DepartureAt).format("ll") + '</span>'
                html += '</div>'
                html += '<div class="col-md-4 col-lg-5 col-xs-4 product-multi-way" style="text-align: center;">                                  '
                if (Stop == "Non") {
                    html += '    <span>' + hours + 'h ' + minutes + 'm | ' + Stop + ' Stop</span><br><img class="oneway-imgarrow" src="images/arow_main.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                else {
                    html += '<span>' + hours + 'h ' + minutes + 'm | <a class="stp" href="#" data-container="body" data-html="true" data-content="' + htmlc + '" rel="popover" data-placement="top">' + Stop + ' Stop</a></span><br><img class="oneway-imgarrow" src="images/arow_main.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                html += '</div>                                                                                                       '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                      '
                html += '    <span>' + arrdest + '(' + arrdestName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + arrtime + '</span><br><span>' + arrtimeFull + '</span>'
                html += '</div>                                                                                                       '
                html += '</div>'
            }
        }
    }
    html += '</div>                                                                                                          '
    html += '<div class="col-md-2 col-lg-2 col-xs-2 productmultiway-bookbtn">                                                '
    html += '<span class="">                                                                                             '
    html += '<span class="product-multi-wayprice">                                                                   '
    //html += '<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;' + TotalFare + '                                                     '
    html += '<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<a class="Price" href="#" data-container="body" data-html="true" data-content="' + FareTip + '" rel="popover" data-placement="top">' + TotalFare + '</a>'
    html += '</span>                                                                                                      '
    html += '</span><br>                                                                                                 '
    html += '<br><a href="#" class="btn btn-primary" onclick="BookTicket(\'' + ResultIndex + '\')">Book Now</a><br>'
    html += '</div>                                                                                                               '
    html += '</div>                                                                                                                   '
    html += '<div class="clearfix"></div>                                                                                '
    html += '</div>                                                                                                          '
    html += '		</div>                                                                                                                           '
    html += '</div>                                                                                                                                   '
    return html;
}

function GenrateSegmentsNonDom(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';
    var Bg = '';
    var calhours = 0;
    var calminutes = 0;
    html += '   <div class="all-searchbox">                                                                                        '
    html += '	<div class="all-box-multiwaydata">                                                                      '
    html += '	<div class="col-lg-12">                                                                             '
    html += '		<div class="row mb10" style="padding: 0px 0px 3px 10px;">                                       '
    html += '			<div class="col-lg-3" style="font-size: 14px;">                                             '
    html += '			</div>                                                                                      '
    html += '			<div class="col-lg-9" style="text-align: right;font-size: 14px;">                           '
    var Baggage = '';
    var Operate = '';
    Bg += " <table>"
    Bg += "<thead>                  "
    Bg += "    <tr align='center'>  "
    Bg += "    <th>Sector</th>      "
    Bg += "    <th>Baggage</th>     "
    Bg += "    <th>OperatedBy</th>  "
    Bg += "    </tr>                "
    Bg += "</thead>                 "
    Bg += "<tbody>                  "
    for (var b = 0; b < arrSegment.length; b++) {
        for (var g = 0; g < arrSegment[b].length; g++) {
            Baggage = arrSegment[b][g].Baggage
            Operate = arrSegment[b][g].Airline.AirlineCode;
            var cn = arrSegment[b].length - 1
            var deptdest = arrSegment[b][g].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[b][g].DepartureAt).format("LT");
            var arrdest = arrSegment[b][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[b][cn].ArrivalFrom.CityName;
            if (g == 0) {
                Bg += "<tr align='center'>"
                Bg += "<td>" + deptdest + ' - ' + arrdest + ""
                Bg += " </td>"
                Bg += " <td>"
                Bg += "   " + Baggage + ""
                Bg += " </td>"
                Bg += " <td>" + Operate + ""
                Bg += " </td>"
                Bg += "</tr>"
            }
        }
    }
    Bg += "</tbody>"
    Bg += "</table>"
    html += '<a class="Bge" href="#" data-container="body" data-html="true" data-content="' + Bg + '" rel="popover" data-placement="top">'
    html += '<i class="soap-icon-businessbag circle" style="font-size: 12px;cursor:pointer;color:#838383">'
    html += '</i></a>&nbsp;'
    html += '<a class="FareRule" href="#" data-container="body" data-content="Fare Rule" onclick="GetFareRule(\'' + ResultIndex + '\')" rel="popover" data-placement="top"><i class="soap-icon-status circle" style="font-size: 12px;cursor:pointer;color:#838383"></i>'
    html += '</a>&nbsp;'
    if (Refunadble) {
        html += '<a class="FareType" href="#" data-container="body" data-content="Refundable" rel="popover" data-placement="top"><i class="soap-icon-stories circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    else {
        html += '<a class="FareType" href="#" data-container="body" data-content="Non-Refundable" rel="popover" data-placement="top"><i class="soap-icon-liability circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    html += '			</div>                                                                                      '
    html += '		</div>                                                                                          '
    html += '	</div>                                                                                              '
    html += '					<div class="col-lg-12">                                                                             '
    html += '						<div class="row rowfs">                                                                         '
    html += '							<div class="col-md-10 col-lg-10 col-xs-10 multiway-leftblock">                              '
    for (var s = 0; s < arrSegment.length; s++) {
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage
            Operate = arrSegment[s][i].Airline.AirlineCode;
            var htmlc = "";
            for (var k = 0; k < arrSegment[s].length; k++) {

                var cnminutes = parseInt(arrSegment[s][k].Duration);
                var cnhours = "";
                cnhours = Math.floor(cnminutes / 60);
                cnminutes = cnminutes % 60;

                calhours = calhours + cnhours;
                calminutes = calminutes + cnminutes;
                htmlc += "<tr align='center'>                                                      "
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].DepartureAt).format('LT') + "<br>" + arrSegment[s][k].DepartureFrom.CityName + "(" + arrSegment[s][k].DepartureFrom.AirportCode + ") "
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].ArrivalTime).format('LT') + "<br>" + arrSegment[s][k].ArrivalFrom.CityName + "(" + arrSegment[s][k].ArrivalFrom.AirportCode + ")"
                htmlc += "</td>"
                htmlc += "</tr>"
            }
            var Stop = arrSegment[s].length - 1;
            if (Stop == 0) {
                Stop = "Non"
            }

            if (s == 1 && i == 0) {
                html += '<hr class="border-multiway">'
            }

            var cn = arrSegment[s].length - 1
            var deptdest = arrSegment[s][i].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[s][i].DepartureAt).format("LT");
            var arrdest = arrSegment[s][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[s][cn].ArrivalFrom.CityName;
            var arrtime = moment(arrSegment[s][cn].ArrivalTime).format("LT");
            var arrtimeFull = moment(arrSegment[s][cn].ArrivalTime).format("ll");
            // get value
            var minutes = parseInt(arrSegment[s][cn].Duration);
            // calculate
            var hours = "";
            hours = Math.floor(minutes / 60);
            minutes = minutes % 60;

            if (i == 0) {
                html += '<div class="row mb10">                                                                  '
                html += '	<div class="col-md-4 col-lg-3 col-xs-4 tour-multi-waydays">                         '
                html += '		<div class="row">                                                               '
                html += '			<div class="col-md-3 col-lg-3 col-xs-12">                                   '
                html += GetLogo(Logo)
                html += '&nbsp;</div>                                                                                                             '
                html += '    <div class="col-md-9 col-lg-9 col-xs-12">                                                                         '
                html += '        <span>' + arrSegment[s][i].Airline.AirlineName + '</span><br><span>' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + '</span>                                                                '
                html += '    </div>                                                                                                             '
                html += '</div>                                                                                                                 '
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                             '
                html += '    <span>' + arrSegment[s][i].DepartureFrom.AirportCode + '(' + arrSegment[s][i].DepartureFrom.CityName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + moment(arrSegment[s][i].DepartureAt).format("LT") + '</span><br><span>' + moment(arrSegment[s][i].DepartureAt).format("ll") + '</span>'
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-4 col-lg-5 col-xs-4 product-multi-way" style="text-align: center;">                                 '
                if (Stop == "Non") {
                    html += '    <span>' + hours + 'h ' + minutes + 'm | ' + Stop + ' Stop</span><br><img class="oneway-imgarrow" src="images/arow_main.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                else {
                    html += '<span>' + hours + 'h ' + minutes + 'm | <a class="stp" href="#" data-container="body" data-html="true" data-content="' + htmlc + '" rel="popover" data-placement="top">' + Stop + ' Stop</a></span><br><img class="oneway-imgarrow" src="images/main-img1.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                             '
                html += '    <span>' + arrdest + '(' + arrdestName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + arrtime + '</span><br><span>' + arrtimeFull + '</span>'
                html += '</div>                                                                                                                 '
                html += '</div>                                                                                                                 '
            }
        }
    }
    html += '</div>                                                                                                                 '
    html += '<div class="col-md-2 col-lg-2 col-xs-2 productmultiway-bookbtn">                                                       '
    html += '    <span class="">                                                                                                    '
    html += '        <span class="product-multi-wayprice">                                                                          '
    html += '<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<a class="Price" href="#" data-container="body" data-html="true" data-content="' + FareTip + '" rel="popover" data-placement="top">' + TotalFare + '</a>'
    html += '        </span>                                                                                                        '
    html += '    </span><br>                                                                                                        '
    html += '<br><a href="#" class="btn btn-primary" onclick="BookTicket(\'' + ResultIndex + '\')">Book Now</a><br>'
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    html += '<div class="clearfix"></div>                                                                                           '
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    return html;
} // 
function GenrateSegmentsMulti(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';
    var Bg = '';
    var calhours = 0;
    var calminutes = 0;
    html += '   <div class="all-searchbox">                                                                                        '
    html += '	<div class="all-box-multiwaydata">                                                                      '
    html += '	<div class="col-lg-12">                                                                             '
    html += '		<div class="row mb10" style="padding: 0px 0px 3px 10px;">                                       '
    html += '			<div class="col-lg-3" style="font-size: 14px;">                                             '
    html += '			</div>                                                                                      '
    html += '			<div class="col-lg-9" style="text-align: right;font-size: 14px;">                           '
    var Baggage = '';
    var Operate = '';
    Bg += " <table>"
    Bg += "<thead>                  "
    Bg += "    <tr align='center'>  "
    Bg += "    <th>Sector</th>      "
    Bg += "    <th>Baggage</th>     "
    Bg += "    <th>OperatedBy</th>  "
    Bg += "    </tr>                "
    Bg += "</thead>                 "
    Bg += "<tbody>                  "
    for (var b = 0; b < arrSegment.length; b++) {
        for (var g = 0; g < arrSegment[b].length; g++) {
            Baggage = arrSegment[b][g].Baggage
            Operate = arrSegment[b][g].Airline.AirlineCode;
            var cn = arrSegment[b].length - 1
            var deptdest = arrSegment[b][g].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[b][g].DepartureAt).format("LT");
            var arrdest = arrSegment[b][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[b][cn].ArrivalFrom.CityName;
            if (g == 0) {
                Bg += "<tr align='center'>"
                Bg += "<td>" + deptdest + ' - ' + arrdest + ""
                Bg += " </td>"
                Bg += " <td>"
                Bg += "   " + Baggage + ""
                Bg += " </td>"
                Bg += " <td>" + Operate + ""
                Bg += " </td>"
                Bg += "</tr>"
            }
        }
    }
    Bg += "</tbody>"
    Bg += "</table>"
    html += '<a class="Bge" href="#" data-container="body" data-html="true" data-content="' + Bg + '" rel="popover" data-placement="top">'
    html += '<i class="soap-icon-businessbag circle" style="font-size: 12px;cursor:pointer;color:#838383">'
    html += '</i></a>&nbsp;'
    html += '<a class="FareRule" href="#" data-container="body" data-content="Fare Rule" onclick="GetFareRule(\'' + ResultIndex + '\')" rel="popover" data-placement="top"><i class="soap-icon-status circle" style="font-size: 12px;cursor:pointer;color:#838383"></i>'
    html += '</a>&nbsp;'
    if (Refunadble) {
        html += '<a class="FareType" href="#" data-container="body" data-content="Refundable" rel="popover" data-placement="top"><i class="soap-icon-stories circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    else {
        html += '<a class="FareType" href="#" data-container="body" data-content="Non-Refundable" rel="popover" data-placement="top"><i class="soap-icon-liability circle" style="font-size: 12px;cursor:pointer;color:#838383"></i></a>       '
    }
    html += '			</div>                                                                                      '
    html += '		</div>                                                                                          '
    html += '	</div>                                                                                              '
    html += '					<div class="col-lg-12">                                                                             '
    html += '						<div class="row rowfs">                                                                         '
    html += '							<div class="col-md-10 col-lg-10 col-xs-10 multiway-leftblock">                              '
    for (var s = 0; s < arrSegment.length; s++) {
        for (var i = 0; i < arrSegment[s].length; i++) {
            Baggage = arrSegment[s][i].Baggage
            Operate = arrSegment[s][i].Airline.AirlineCode;
            var htmlc = "";
            for (var k = 0; k < arrSegment[s].length; k++) {

                var cnminutes = parseInt(arrSegment[s][k].Duration);
                var cnhours = "";
                cnhours = Math.floor(cnminutes / 60);
                cnminutes = cnminutes % 60;

                calhours = calhours + cnhours;
                calminutes = calminutes + cnminutes;
                htmlc += "<tr align='center'>                                                      "
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].DepartureAt).format('LT') + "<br>" + arrSegment[s][k].DepartureFrom.CityName + "(" + arrSegment[s][k].DepartureFrom.AirportCode + ") "
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].ArrivalTime).format('LT') + "<br>" + arrSegment[s][k].ArrivalFrom.CityName + "(" + arrSegment[s][k].ArrivalFrom.AirportCode + ")"
                htmlc += "</td>"
                htmlc += "</tr>"
            }
            var Stop = arrSegment[s].length - 1;
            if (Stop == 0) {
                Stop = "Non"
            }

            if (s != 0 && i == 0) {
                html += '<hr class="border-multiway">'
            }

            var cn = arrSegment[s].length - 1
            var deptdest = arrSegment[s][i].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[s][i].DepartureAt).format("LT");
            var arrdest = arrSegment[s][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[s][cn].ArrivalFrom.CityName;
            var arrtime = moment(arrSegment[s][cn].ArrivalTime).format("LT");
            var arrtimeFull = moment(arrSegment[s][cn].ArrivalTime).format("ll");
            // get value
            var minutes = parseInt(arrSegment[s][cn].Duration);
            // calculate
            var hours = "";
            hours = Math.floor(minutes / 60);
            minutes = minutes % 60;

            if (i == 0) {
                html += '<div class="row mb10">                                                                  '
                html += '	<div class="col-md-4 col-lg-3 col-xs-4 tour-multi-waydays">                         '
                html += '		<div class="row">                                                               '
                html += '			<div class="col-md-3 col-lg-3 col-xs-12">                                   '
                html += GetLogo(Logo)
                html += '&nbsp;</div>                                                                                                             '
                html += '    <div class="col-md-9 col-lg-9 col-xs-12">                                                                         '
                html += '        <span>' + arrSegment[s][i].Airline.AirlineName + '</span><br><span>' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + '</span>                                                                '
                html += '    </div>                                                                                                             '
                html += '</div>                                                                                                                 '
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                             '
                html += '    <span>' + arrSegment[s][i].DepartureFrom.AirportCode + '(' + arrSegment[s][i].DepartureFrom.CityName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + moment(arrSegment[s][i].DepartureAt).format("LT") + '</span><br><span>' + moment(arrSegment[s][i].DepartureAt).format("ll") + '</span>'
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-4 col-lg-5 col-xs-4 product-multi-way" style="text-align: center;">                                 '
                if (Stop == "Non") {
                    html += '    <span>' + hours + 'h ' + minutes + 'm | ' + Stop + ' Stop</span><br><img class="oneway-imgarrow" src="images/arow_main.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                else {
                    html += '<span>' + hours + 'h ' + minutes + 'm | <a class="stp" href="#" data-container="body" data-html="true" data-content="' + htmlc + '" rel="popover" data-placement="top">' + Stop + ' Stop</a></span><br><img class="oneway-imgarrow" src="images/main-img1.png" style="margin-left: 20%;" ><br><span><b>' + arrSegment[s][i].noSeatAvailable + ' seat(s) left</b></span>'
                }
                html += '</div>                                                                                                                 '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-multi-way">                                                             '
                html += '    <span>' + arrdest + '(' + arrdestName + ')</span><br>                                                                                        '
                html += '    <span class="single-multi-waytime">' + arrtime + '</span><br><span>' + arrtimeFull + '</span>'
                html += '</div>                                                                                                                 '
                html += '</div>                                                                                                                 '
            }
        }
    }
    html += '</div>                                                                                                                 '
    html += '<div class="col-md-2 col-lg-2 col-xs-2 productmultiway-bookbtn">                                                       '
    html += '    <span class="">                                                                                                    '
    html += '        <span class="product-multi-wayprice">                                                                          '
    html += '<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<a class="Price" href="#" data-container="body" data-html="true" data-content="' + FareTip + '" rel="popover" data-placement="top">' + TotalFare + '</a>'
    html += '        </span>                                                                                                        '
    html += '    </span><br>                                                                                                        '
    html += '<br><a href="#" class="btn btn-primary" onclick="BookTicket(\'' + ResultIndex + '\')">Book Now</a><br>'
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    html += '<div class="clearfix"></div>                                                                                           '
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    html += '</div>                                                                                                                 '
    return html;
} // 
function Genrate2WSegments(arrSegment, Class, Logo, FareTip, TotalFare, Remark, ResultIndex, Refunadble) {
    var html = '';
    var calhours = 0;
    var calminutes = 0;
    for (var s = 0; s < arrSegment.length; s++) {
        for (var i = 0; i < arrSegment[s].length; i++) {

            var htmlc = "";
            for (var k = 0; k < arrSegment[s].length; k++) {
                var cnminutes = parseInt(arrSegment[s][k].Duration);
                var cnhours = "";
                cnhours = Math.floor(cnminutes / 60);
                cnminutes = cnminutes % 60;

                calhours = calhours + cnhours;
                calminutes = calminutes + cnminutes;

                htmlc += "<tr>                                                      "
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].DepartureAt).format('LT') + "<br>" + arrSegment[s][k].DepartureFrom.CityName + "(" + arrSegment[s][k].DepartureFrom.AirportCode + ") "
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"
                htmlc += "</td>"
                htmlc += "<td>"
                htmlc += "" + moment(arrSegment[s][k].ArrivalTime).format('LT') + "<br>" + arrSegment[s][k].ArrivalFrom.CityName + "(" + arrSegment[s][k].ArrivalFrom.AirportCode + ")"
                htmlc += "</td>"
                htmlc += "</tr>"
            }

            var cn = arrSegment[s].length - 1
            var deptdest = arrSegment[s][i].DepartureFrom.AirportCode;
            var depttime = moment(arrSegment[s][i].DepartureAt).format("LT");
            var arrdest = arrSegment[s][cn].ArrivalFrom.AirportCode;
            var arrdestName = arrSegment[s][cn].ArrivalFrom.CityName;
            var arrtime = moment(arrSegment[s][cn].ArrivalTime).format("LT");
            var arrtimeFull = moment(arrSegment[s][cn].ArrivalTime).format("ll");
            // get value
            var minutes = parseInt(arrSegment[s][cn].Duration);
            // calculate
            var hours = "";
            hours = Math.floor(minutes / 60);
            minutes = minutes % 60;

            var Stop = arrSegment[s].length - 1;
            if (Stop == 0) {
                Stop = "Non"
            }

            if (i == 0) {
                html += '<div class="col-md-12 col-xs-12">'
                html += '<div class="all-box-roundtripwaydata ' + Class + '" id="' + ResultIndex + '">                                                    '
                html += '<div class="row rowRfs">                                                          '
                html += '<div class="col-md-3 col-lg-3 col-xs-3 tour-roundtripwaydays">                '
                html += GetLogo(Logo);
                html += '<br><span>' + arrSegment[s][i].Airline.AirlineName + '</span><br><span>(' + arrSegment[s][i].Airline.AirlineCode + '-' + arrSegment[s][i].Airline.FlightNumber + '' + arrSegment[s][i].Airline.FareClass + ')</span>                                                          '
                html += '</div>                                                                                                       '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-roundtripway fr-timing">                                      '
                html += '    <span class="roundtrip-waytime" style="margin-left: -30px;">' + moment(arrSegment[s][i].DepartureAt).format("LT") + '</span><br><span style="margin-left: -30px;">' + arrSegment[s][i].DepartureFrom.CityName + '</span>                                      '
                html += '</div>                                                                                                       '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-roundtripway stop-roundtrip">                                 '
                html += '    <span style="margin-left: -20px;">' + hours + 'h ' + minutes + 'm</span><br><img class="oneway-imgarrow" src="images/arrow-f.png" style="margin-left: 4px;"><br>'
                if (Stop == "Non") {
                    html += '<span style="margin-left: -20px;">' + Stop + ' Stop</span>'
                }
                else {
                    html += '<span style="margin-left: -20px;"><a class="stp" href="#" data-container="body" data-html="true" data-content="' + htmlc + '" rel="popover" data-placement="top">' + Stop + ' Stop</a></span>'
                }

                html += '</div>                                                                                                       '
                html += '<div class="col-md-2 col-lg-2 col-xs-2 product-roundtripway fr-timing">                                      '
                html += '    <span class="roundtrip-waytime" style="margin-left: -30px;">' + arrtime + '</span><br><span style="margin-left: -30px;">' + arrdestName + '</span>                                      '
                html += '</div>                                                                                                       '
                html += '<div class="col-md-3 col-lg-3 col-xs-3 content_roundtripwayprice">		                                      '
                html += '    <span class="pull-right">                                                                                '
                html += '        <span class="product-roundtripwayprice">                                                             '
                //html += '            <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;' + TotalFare + '                                           '
                html += '            <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;' + TotalFare + '                                           '
                html += '        </span>                                                                                              '
                html += '    </span>                                                                                                  '
                html += '</div>                                                                                                       '
                html += '</div>                                                                                                       '
                html += '</div>                                                                                                       '
                html += '</div>                                                                                                       '
                html += '<div class="clearfix mb10"></div>'
            }
        }
    }
    return html;
} // 

function GetLogo(AirlinCode) {
    var ext = ".gif";
    if (AirlinCode == "UK")
        ext = ".png";
    var img = '<img src="../images/AirlineLogo/' + AirlinCode + ext + '" alt="">';
    return img;
}/*Logo*/

function LogoForModal(Logo) {
    var ext = ".gif";
    if (Logo == "UK")
        ext = ".png";
    var img = '<img src="images/AirlineLogo/' + Logo + ext + '" style="height:51px ; width:65px" />';
    return img;
}

function GetFareRule(ResultIndex) {
    $("#AlertMessage").empty();
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetFareRule",
        data: '{"ResultIndex":"' + ResultIndex + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrRules = result.arrRules;
                $("#AlertModal .modal-dialog").css("width", "80%")
                Success(arrRules[0].FareRuleDetail);
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.Message);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function OrderByFilter(Type) {
    var html = '';
    html += '<div class="sort-by-section clearfix box">'
    html += '<ul class="sort-bar clearfix block-sm">'
    html += '<li class="sort-by-price"><a class="sort-by-container" href="#"onclick="SortBy(\'' + Type + '\',4,this)" title="Sort By Price"><span>Price </span></a></li>'
    html += '<label id="lblCategory" style="display: none">0</label>'
    html += '<li class="sort-by-Depart"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',1,this)" title="Sort By Departure Time"><span>Depart </span></a></li>'
    html += '<input type="hidden" id="txt_FilterBy' + Type + '" value="0">'
    html += '<input type="hidden" id="txt_Orderby' + Type + '" value="0">'
    html += '<li class="sort-by-Arrival"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',2,this)" title="Sort By Arrival Time"><span>Arrival </span></a></li>'
    html += '<li class="sort-by-Time"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',3,this)" title="Sort By Duration"><span>Time</span></a></li>'
    html += '</ul>'
    html += '<div class="col-md-2" style="margin-top: 15px;">'
    html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e" class="full-width" id="btn_Show" onclick="Show();">Modify Search</button>'
    html += '<button type="button" style="background-color: #f5f5f5; color: #9e9e9e; display: none" class="full-width" id="btn_hide" onclick="Hide();">Modify Search</button>'
    html += '</div>'
    html += '</div>'
    return html;
}

function OrderByFilterRound(Type) {
    var html = '';
    html += '<div class="sort-by-section clearfix box" style="padding: 1px 8px;">'
    html += '<ul class="sort-bar clearfix block-sm">'
    html += '<li class="sort-by-price" style="padding: 15px 2px;"><a class="sort-by-container" href="#"onclick="SortBy(\'' + Type + '\',4,this)" title="Sort By Price"><span>Price </span></a></li>'
    html += '<label id="lblCategory" style="display: none">0</label>'
    html += '<li class="sort-by-Depart"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',1,this)" title="Sort By Departure Time"><span>Depart </span></a></li>'
    html += '<input type="hidden" id="txt_FilterBy' + Type + '" value="0">'
    html += '<li class="sort-by-Arrival"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',2,this)" title="Sort By Arrival Time"><span>Arrival </span></a></li>'
    html += '<li class="sort-by-Time"><a class="sort-by-container" href="#" onclick="SortBy(\'' + Type + '\',3,this)" title="Sort By Duration"><span>Time</span></a></li>'
    html += '</ul>'
    html += '</div>'
    return html;
}



var ResultIndex = new Array();
function GetFareQuote() {
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetFarQuote",
        data: JSON.stringify({ ReultIndex: ResultIndex }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Booking(ResultIndex)
            }
            else if (result.retCode == 0) {
                $("#exampleModal2").modal("hide");
                if (result.errormsg != "Your Fare not Found.Please select another Fare.")
                    Ok(result.errormsg, "Booking", [ResultIndex])
                else
                    Success(result.errormsg);
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
            // alert("something went wrong");
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

function Booking(FlightID) {
    window.location.href = "FlightBooking.aspx?FlightID=" + FlightID;
}

var DepartID, ArrivalID;
function GetSelection() {
    $('.roubdbk2').on('click', function () {
        ArrivalID = $(this)[0].id;
        GetDomesticPrice()
        $('.roubdbk2').removeClass('activeroundblock');
        $(this).addClass('activeroundblock');
    });
    /*for right section*/
    $('.roubdbk1').on('click', function () {
        DepartID = $(this)[0].id;
        GetDomesticPrice()
        $('.roubdbk1').removeClass('activeroundblock2');
        $(this).addClass('activeroundblock2');
    });
}

var trip = false;
var CityDeaprtt = "";
var CityRDpartt = "";
var DeaprtFare = "";
var ArrivlFare = "";
var DeaprtLogo = "";
var ArrivlLogo = "";
function GetDomesticPrice() {

    ResultIndex = [];
    if (DepartID != undefined && ArrivalID != undefined) {

        var objDSegment = $.grep(arrSearch.FlightList[0], function (p) { return p.ResultIndex == DepartID })
            .map(function (p) { return p });
        var objASegment = $.grep(arrSearch.FlightList[1], function (p) { return p.ResultIndex == ArrivalID })
            .map(function (p) { return p });
        CityDeaprtt = objDSegment[0].Segment[0][0].DepartureFrom.CityName + ' ' + objDSegment[0].Segment[0][0].DepartureFrom.CountryName;
        CityRDpartt = (objASegment[0].Segment[0][0].DepartureFrom.CityName + ' ' + objASegment[0].Segment[0][0].DepartureFrom.CountryName)
        DeaprtFare = objDSegment[0].Fare;
        ArrivlFare = objASegment[0].Fare;
        DeaprtLogo = objDSegment[0].Logo;
        ArrivlLogo = objASegment[0].Logo;
        $("#Div_domestic").empty();
        $("#Div_domestic").show();
        var html = '';
        html += ' 	<div class="container">                                                                                              '
        html += ' 			<div class="row mb10">                                                                                       '
        var cnd = objDSegment[0].Segment[0].length - 1
        for (var i = 0; i < objDSegment[0].Segment[0].length; i++) {
            var CityDeaprt = objDSegment[0].Segment[0][i].DepartureFrom.CityName;

            var CityRDpart = (objDSegment[0].Segment[0][cnd].ArrivalFrom.CityName)
            if (i == 0) {
                html += GetDomsticReturn(CityDeaprt,
                    CityRDpart,
                    moment(objDSegment[0].Segment[0][i].DepartureAt).format("LT"),
                    moment(objDSegment[0].Segment[0][cnd].ArrivalTime).format("LT"), objDSegment[0].Segment[0][i].Duration, DeaprtFare, DeaprtLogo, objDSegment[0].Segment[0][i].Airline.AirlineName, (objDSegment[0].Segment[0][i].Airline.AirlineCode + '-' + objDSegment[0].Segment[0][i].Airline.FlightNumber + '' + objDSegment[0].Segment[0][i].Airline.FareClass)) /*One Way Ticket*/
            }
        }

        if (i == objDSegment[0].Segment[0].length) {
            trip = true;

        }
        var cna = objASegment[0].Segment[0].length - 1
        for (var i = 0; i < objASegment[0].Segment[0].length; i++) {
            var CityRDpart = objASegment[0].Segment[0][i].DepartureFrom.CityName;
            var CityDeaprt = (objASegment[0].Segment[0][cna].ArrivalFrom.CityName)
            if (i == 0) {
                html += GetDomsticReturn(CityRDpart,
                    CityDeaprt,
                    moment(objASegment[0].Segment[0][i].DepartureAt).format("LT"),
                    moment(objASegment[0].Segment[0][cna].ArrivalTime).format("LT"), objASegment[0].Segment[0][i].Duration, ArrivlFare, ArrivlLogo, objASegment[0].Segment[0][i].Airline.AirlineName, (objASegment[0].Segment[0][i].Airline.AirlineCode + '-' + objASegment[0].Segment[0][i].Airline.FlightNumber + '' + objASegment[0].Segment[0][i].Airline.FareClass))/*Return Way Ticket*/
            }
        }
        html += ' 				<div class="col-md-4 col-lg-4 col-xs-3">                                                                 '
        html += ' 					<div class="row rowfs">								                                                 '
        html += ' 						<div class="col-md-4 col-lg-4 col-xs-6 product-round-way" style="font-size: 13px;">              '
        html += ' 							<span class="single-waytime">Grand Total</span>                                              '
        html += ' 						</div>                                                                                           '
        html += ' 						<div class="col-md-3 col-lg-3 col-xs-4 content_round-waysprice" style="font-size: 14px;">        '
        html += ' 							<span class="pull-right">                                                                    '
        html += ' 								<span class="product-round-waysprice">                                                   '
        html += ' 									<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;' + (parseFloat(objDSegment[0].Fare.replace(",", "")) + parseFloat(objASegment[0].Fare.replace(",", ""))).toFixed(2); + '                               '
        html += ' 								</span>                                                                                  '
        html += ' 							</span>                                                                                      '
        html += ' 						</div>                                                                                           '
        html += ' 						<div class="col-md-4 col-lg-4 productroundways-btn">                                             '
        html += ' 							<a href="#" class="btn btn-primary" onclick ="GetFareQuote()">Book Now</a>                                             '
        html += ' 						</div>                                                                                           '
        html += ' 						                                                                                                 '
        html += ' 					</div>                                                                                               '
        html += ' 				</div>                                                                                                   '
        html += ' 			</div>                                                                                                       '
        html += ' 	</div>                                                                                                               '
        //html += ' </div>'
        ResultIndex.push(DepartID)
        ResultIndex.push(ArrivalID)
        $("#Div_domestic").append(html)
    }
}

function GetDomsticReturn(Origin, Destination, DepartTime, Arrival, Duration, Fare, Logo, Name, Code) {
    var html = '';
    html += '<div class="col-md-4 col-lg-4 col-xs-4 border-rightbar">                                                 '
    html += '	<div class="row rowfs">                                                                              '
    html += '		<div class="col-md-3 col-sm-2 col-xs-4 tour-round-days air-flight">                              '
    html += GetLogo(Logo);
    html += '			&nbsp;<br><span>' + Name + '</span><br><span>(' + Code + ')</span>                                              '
    html += '		</div>                                                                                           '
    html += '		<div class="col-md-3 col-sm-2 col-xs-3 product-round-way">                                       '
    html += '			<span class="single-waytime">' + DepartTime + '</span><br><span class="city-title">' + Origin + '</span>             '
    html += '		</div>                                                                                           '
    html += '		<div class="col-md-1 col-sm-2 col-xs-2 right-arrownew" style="font-size: 20px;">                 '
    html += '			<i class="fa fa-long-arrow-right" aria-hidden="true" style="margin-left: -25px;"></i>                                    '
    html += '		</div>                                                                                           '
    html += '		<div class="col-md-2 col-sm-2 col-xs-3 product-round-way">                                       '
    html += '			<span class="single-waytime" style="margin-left: -25px;">' + Arrival + '</span><br><span class="city-title" style="margin-left: -22px;">' + Destination + '</span>             '
    html += '		</div>                                                                                           '
    html += '		<div class="col-md-3 col-sm-2 col-xs-3 content_round-waysprice" style="font-size: 14px;">		 '
    html += '			<span class="pull-right pullnone">                                                           '
    html += '				<span class="product-round-waysprice">                                                   '
    html += '					<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;' + Fare + '                               '
    html += '				</span>                                                                                  '
    html += '			</span>                                                                                      '
    html += '		</div>                                                                                           '
    html += '	</div>                                                                                               '
    html += '</div>                                                                                                   '
    return html;
}

function BookTicket(ID) {
    debugger
    ResultIndex = [];
    ResultIndex.push(ID)
    GetFareQuote()
}

function SortBy(Type, By, FilterBy) {
    $("#txt_FilterBy" + Type).val(By);
    GenrateFilters(Type);
}

var arrFares = []
function GetPriceRBD(ResultIndex) {
    var Data = {
        ResultIndex: ResultIndex,
        FareClass: arrFares
    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetSegments",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#spn_Total" + ResultIndex).append(result.sTotal)
                $('[data-toggle="tooltip"]').tooltip();
            }
            else if (result.retCode == 0) {
                $('#AgencyBookingCancelModal').modal('hide')
                $('#SpnMessege').text(result.errorMsg);
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("something went wrong");
            $('#ModelMessege').modal('show')
        },
        complete: function () {
            $("#dlgLoader").css("display", "none");
        }
    });
}

var ListFares = new Array();
function SetClass(ResultIndex, arrFare, TotalSegment, selected, ClassItem) {
    arrFares[selected] = arrFare;
    $(".Fare_" + ResultIndex + selected).removeClass("red")
    $(ClassItem).addClass("red")
    if (parseInt(TotalSegment) == arrFares.length) {
        $("#btn_" + ResultIndex).show()
    }
}

var arrCity = new Array();
function GetCityForModify() {
    var sndcountry = "";
    $.ajax({
        type: "POST",
        url: "../Handler/FlightHandler.asmx/GetselCityList",
        data: "{'sndcountry':'" + sndcountry + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var ddlRequest = "";
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.Arr;
            }
            BindSearch();
        },
        error: function () {
        }
    });
}

function BindSearch() {

    if (arrSearch.JourneyType != "3") {
        var DepDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[0].Origin; })
            .map(function (p) { return p.City_Name; });
        var ArrivDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[0].Destination; })
            .map(function (p) { return p.City_Name; });
        $("#origin0").val(DepDest);
        $("#hdoriginCode0").val(arrSearch.ObjSearch.Segments[0].Origin);
        $("#destination0").val(ArrivDest);
        $("#hddestCode0").val(arrSearch.ObjSearch.Segments[0].Destination);
    }

    var Dat = arrSearch.ObjSearch.Segments[0].PreferredArrivalTime;
    var Date = Dat.split('T');
    var tdate = moment(Date[0]).format('DD-MM-YYYY');


    $("#sel_Adults").val(arrSearch.ObjSearch.AdultCount);
    $("#sel_Child").val(arrSearch.ObjSearch.ChildCount);
    $("#sel_Infant").val(arrSearch.ObjSearch.InfantCount);
    $("#sel_Class").val(arrSearch.ObjSearch.Segments[0].FlightCabinClass);
    if (arrSearch.ObjSearch.OneStopFlight == "true") {
        $("#chkNonStop").prop("checked");
    }

    if (arrSearch.JourneyType == "1") {
        $("#radio01").attr('checked', true);
        OneWayRadioFunc()
        $("#dte_OnwardDateId0").val(tdate);
    }
    if (arrSearch.JourneyType == "2") {
        $("#radio02").attr('checked', true);
        RoundWayRadioFunc();
        var TDat = arrSearch.ObjSearch.Segments[0].PreferredArrivalTime;
        var TDate = TDat.split('T');
        var RDat = arrSearch.ObjSearch.Segments[1].PreferredArrivalTime;
        var RDate = RDat.split('T');
        var RDatee = moment(RDate[0]).format('DD-MM-YYYY');
        var TDatee = moment(TDate[0]).format('DD-MM-YYYY');
        $("#dte_OnwardDateId0").val(TDatee);
        $("#datepicker_Return").val(RDatee);
    }
    if (arrSearch.JourneyType == "3") {
        $("#radio03").attr('checked', true);
        MultiWayRadioFunc()
        for (var i = 0; i < arrSearch.ObjSearch.Segments.length; i++) {
            var TDat = arrSearch.ObjSearch.Segments[i].PreferredArrivalTime;
            var TDate = TDat.split('T');
            var TDatee = moment(TDate[0]).format('DD-MM-YYYY');
            $("#dte_OnwardDateId" + i + "").val(TDatee);
            var DepDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[i].Origin; })
                .map(function (p) { return p.City_Name; });
            var ArrivDest = $.grep(arrCity, function (p) { return p.City_Code == arrSearch.ObjSearch.Segments[i].Destination; })
                .map(function (p) { return p.City_Name; });
            $("#origin" + i + "").val(DepDest);
            $("#hdoriginCode" + i + "").val(arrSearch.ObjSearch.Segments[0].Origin);
            $("#destination" + i + "").val(ArrivDest);
            $("#hddestCode" + i + "").val(arrSearch.ObjSearch.Segments[0].Destination);
        }
    }
}

function Show() {
    $("#SearchDive").show();
    $("#btn_hide").show();
    $("#btn_Show").hide();

}

function Hide() {
    $("#SearchDive").hide();
    $("#btn_hide").hide();
    $("#btn_Show").show();
}

function ShowR() {
    $("#SearchDive").show();
    $("#btn_hider").show();
    $("#btn_Showr").hide();

}

function HideR() {
    $("#SearchDive").hide();
    $("#btn_hider").hide();
    $("#btn_Showr").show();
}
