﻿using CUTUK.DataLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TboFlightLib.Request.Search;

namespace CUTUK
{
    public partial class waitforFlights : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DBHandlerDataContext db = new DBHandlerDataContext();
            CUT.BL.DBHelper.DBReturnCode retcode = MarkupTaxManager.AssignMarkupTax();
            var jSonSearch = Request.QueryString["Search"];
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            Search ObjSearch = JsonConvert.DeserializeObject<Search>(jSonSearch);
            lbl_AdultCount.Text = ObjSearch.AdultCount;
            lbl_Childs.Text = ObjSearch.ChildCount;
            lbl_Infants.Text = ObjSearch.InfantCount;
            lbl_Departure.Text = (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Origin select obj).FirstOrDefault().City_Name + ", " + (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Origin select obj).FirstOrDefault().Country_name + '\n' + " - ";
            lbl_Departure.Text += (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Destination select obj).FirstOrDefault().City_Name + ", " + (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Destination select obj).FirstOrDefault().Country_name + "<br/>";
            lbl_DepartDate.Text = ObjSearch.Segments[0].PreferredDepartureTime;
            if (ObjSearch.JourneyType == "1")
            {
                //lbl_Departure.Text= (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Origin select obj).FirstOrDefault().City_Name + ", " + (from obj in db.tbl_TBOCityCodes where obj.City_Code == ObjSearch.Segments[0].Origin select obj).FirstOrDefault().Country_name;
                //lbl_DepartDate.Text = ObjSearch.Segments[0].PreferredDepartureTime;
                lbl_Type.Text = "One way";
            }
            else if (ObjSearch.JourneyType == "2")
            {
                lbl_Type.Text = "Round way";
            }
            else if (ObjSearch.JourneyType == "3")
                lbl_Type.Text = "Multiway way";
        }
    }
}