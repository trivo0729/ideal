﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewVoucher.aspx.cs" Inherits="CUTUK.ViewVoucher" %>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>View Voucher</title>
    <style type="text/css">
        @page {
            size: A3 portrait;
            margin: 0.5cm;
        }

        @media print {
            .page {
                page-break-after: avoid;
            }
        }
    </style>

    <script src="scripts/Cancellation.js"></script>
    <script type="text/javascript" src="scripts/Invoice.js"></script>


    <meta charset="utf-8" />
    <meta name="keywords" content=" " />
    <meta name="description" content="" />
    <meta name="author" content="SoapTheme" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/responsive.css" />

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/modal.js"></script>


    <script>
        var Latitude;
        var Longitutude;
        var HotelCode;
        $(document).ready(function () {
            
            var ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
            var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid');
            var Ststs = GetQueryStringParamsForAgentRegistrationUpdate('Status');
            HotelCode = GetQueryStringParamsForAgentRegistrationUpdate('HotelCode');

            // VoucherPrint(ReservationID, Uid);

            // document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationPopup('" + ReservationID + "', '" + Ststs + "')");
            document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFVoucher('" + ReservationID + "', '" + Uid + "','" + Ststs + "')");

            $("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));



        });


        function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }



    </script>

</head>


<body style="background-color: #fff">

    <div style="margin-left: 43%;" id="BtnDiv">
        <input type="button" value="Mail" class="button btn-small orange" style="cursor: pointer" title="Voucher" data-toggle="modal" data-target="#VoucherModal" />
        <input type="button" class="button btn-small orange" value="DownLoad" id="btn_VoucherPDF" />
        <%--   <input type="button" value="Cancel" id="btn_Cancel" />--%>
    </div>
    <br />
    <div id="maincontainer" runat="server" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray">

        <%--<img  src="https://maps.googleapis.com/maps/api/staticmap?zoom=14&size=400x250&sensor=false&maptype=roadmap&markers=color:red|39.926586,116.405640" style="width:auto; height:auto" >"--%>
        <%--        <div style="background-color: #00AEEF; height: 13px">
            &nbsp;
        </div>


        <table style="height: 150px; width: 100%;">
            <tbody>
                <tr style="color: #57585A;">

                    <td id="AgentLog" style="width: 33%; padding-left: 15px"></td>
                    <td style="width: 66%; padding-right: 15px" align="right">
                        <span style="margin-right: 15px">
                            <span style="margin-right: 15px" id="spnAdd">Address line 1,</span><br />
                            <span style="margin-right: 15px" id="spnCity">City, Pin/Zip Code</span><br />
                            <span style="margin-right: 15px" id="spnCounty">COUNTRY<br />
                            </span>
                            <br />
                            <span style="margin-right: 15px"><b>Tel:</b><span id="spnAgenPhone">  +91-712-6660666</span></span>
                            <br />
                            <span style="margin-right: 15px"><b>Email:</b><span id="spnAgentEmail"> agent@agentname.com</span></span><br>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>

        <br />

        <table border="1" style="width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">
            <tr>
                <td rowspan="3" style="width: 35%; color: #00CCFF; font-size: 30px; text-align: center; border: none; padding-right: 35px; padding-left: 10px">
                    <b>HOTEL VOUCHER</b>

                </td>
                <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 10px; margin-bottom: 0px; color: #57585A">
                    <b>Booking Date &nbsp;    : &nbsp;</b><span id="spnDate" style="font-weight: 500"> &nbsp; </span>
                </td>
                <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 3px 12px; margin-bottom: 0px; color: #57585A;">
                    <b>Voucher No &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;    : </b>&nbsp;<span id="spnVoucher" style="font-weight: 500"> </span>
                </td>
            </tr>
            <tr>
                <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 10px; color: #57585A">
                    <b>Agent Ref No.&nbsp; :&nbsp; </b><span id="spnAgentRef" style="font-weight: 500">&nbsp; </span>
                </td>
                <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 3px 10px; color: #57585A">
                    <b>Supplier Ref No&nbsp; : &nbsp;</b><span id="spnSupplierRef" style="font-weight: 500"> </span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 25px; border: none"></td>
            </tr>
        </table>


        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A;"><b>BOOKING DETAILS</b></span>
        </div>


        <table border="1" style="margin-top: 3px; width: 100%; height: 250px; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">
            <tr style="border: none">
                <td rowspan="4" style="width: 20%; border: none" id="Map">
                    
                </td>


                <td style="height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none;">
                    <span style="margin-left: 15px"><b>Check In</span>
                </td>
                <td colspan="2" style="height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none;">: <span id="spnChkIn" style="margin-left: 15px"><b></b></span>
                </td>
            </tr>
            <tr style="border: none">
                <td style="width: 15%; height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none">
                    <span style="margin-left: 15px"><b>Check Out</b></span>
                </td>
                <td colspan="2" style="height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none">: <span id="spnChkOut" style="margin-left: 15px"><b></b></span>
                </td>
            </tr>
            <tr style="border: none">
                <td style="width: 15%; height: 45px; background-color: #35C2F1; font-size: 24px; color: white; border: none">

                    <span style="margin-left: 15px"><b>Hotel Name</b></span>

                </td>
                <td style="height: 45px; background-color: #35C2F1; font-size: 22px; color: white; border: none">: 
                    <div style="margin-left: 30px; margin-top: -28px;">
                        <span id="spnHotelName"><b></b></span>
                    </div>
                </td>
                <td rowspan="2" align="center" style="background-color: #35C2F1; font-size: 22px; color: white; border: none; width: 10%">
                    <br>
                    <br>
                    <b>Nights</b>
                    <br>
                    <span id="spnNights" style="font-size: 40px; font-weight: 700">05</span>
                </td>
            </tr>

            <tr style="border: none; color: #57585A;">

                <td colspan="2" style="border: none; color: #57585A;">

                    <span style="margin-left: 15px"><b>City</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> <span id="spnCity1" style="margin-left: 15px; font-weight: 600"></span>
                    <br />
                    <span style="margin-left: 15px"><b>Country</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> <span id="spnCountry1" style="margin-left: 15px; font-weight: 600"></span>
                    <br />
                    <span style="margin-left: 15px"><b>Phone</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</span> <span id="spnPhone1" style="margin-left: 15px; font-weight: 600"></span>
                    <br />
                    <span style="margin-left: 15px"><b>Address</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span id="spnAdd1" style="margin-left: 15px; font-weight: 600"></span>
                    <br />
                    <span style="margin-left: 15px"><b>Postal Code</b> &nbsp;: </span><span id="spnPostalCode" style="margin-left: 15px; font-weight: 600"></span>
                    <br />
                </td>

            </tr>

        </table>



        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A"><b>GUEST & ROOM DETAILS</b></span>
        </div>

        <div>
            <table id="tbl_RoomDetails" border="1" style="margin-top: 3px; width: 100%; border-spacing: 0px; border-bottom-width: 3px; border-bottom-color: white; border-left: none; border-right: none">
                <tr style="border: none;">
                    <td style="background-color: #00AEEF; color: white; font-size: 18px; border: none; padding-left: 10px; font-weight: 700">Room No.1
                    </td>
                    <td colspan="7" style="height: 35px; background-color: #35C2F1; color: white; font-size: 18px; border: none; padding-left: 10px; font-weight: 700;">Guest Name :&nbsp;&nbsp;&nbsp; <span>Muhammed Imran Muhammed Zakaria</span>
                    </td>
                </tr>

            </table>
        </div>


        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A">
                <b>Terms & Conditions</b>
            </span>
        </div>
        <hr style="border-top-width: 3px" />
        <div style="height: auto; color: #57585A; font-size: small">
            <ul style="list-style-type: disc">
                <li>Please collect all extras directly from clients prior to departure</li>
                <li>It is mandatory to present valid Passport at the time of check-in for International
                hotel booking & any valid photo ID for domestic hotel booking
                </li>
                <li>Hotel holds right to reject check in or charge supplement due to wrongly selection
                of Nationality or Residency at the time of booking
                </li>
                <li>Early check-in & late check-out will be subject to availability and approval by
                respective hotel only
                </li>
                <li>Kindly inform hotel in advance for any late check-in, as hotel may automatically
                cancel the room in not check-in by 18:00Hrs local time
                </li>
                <li>You may need to deposit security amount at some destination as per the local rule
                and regulations at the time of check-in
                </li>
                <li>Tourism fees may apply at the time of check-in at some destinations like Dubai,
                Paris, etc...
                </li>
                <li>General check-in time is 14:00Hrs & check-out time is 12:00Hrs</li>
            </ul>
            <br>
        </div>

        <div style="background-color: #00AEEF; text-align: center; font-size: 20px; color: white; padding-bottom: 5px">
            <span>Check your booking details carefully and inform us immediately before it’s too
            late...
            </span>
        </div>--%>
    </div>


    <div class="modal fade bs-example-modal-lg" id="AgencyBookingCancelModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 700px; left: -10%">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                    <div class="frow2">
                        <table>
                            <tr>
                                <td><span class="dark bold">Hotel: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspHotelName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckin"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckout"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Passenger: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspPasssengerName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspLocation"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspNights"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Booking ID: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingID"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingDate"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingAmount"></span></td>
                            </tr>
                        </table>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Cancellation Policy</b></div>
                    <div class="frow2">
                        <div id="dspCancellationPolicy">
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    </div>

                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;" id="divRemark1"><b>Remark</b></div>
                    <div class="frow2" id="divRemark2">
                        <textarea rows="4" cols="140" id="txtRemark" class="form-control"></textarea>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div class="alert alert-warning" id="dspAlertMessage" style="display: none"></div>


                    <div class="frow2" style="text-align: center;">
                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br />
                        <input type="button" value="Cancel Booking" onclick="ProceedToCancellation(); return false;" id="btnCancelBooking" class="btn-search4 margtop20" style="float: none;" />
                        <input type="hidden" id="hndReferenceCode" />
                        <input type="hidden" id="hndCancellationAmount" />
                        <input type="hidden" id="hndReservatonID" />
                        <input type="hidden" id="hndIsCancelable" />
                        <input type="hidden" id="hndStatus" />
                        <input type="hidden" id="hndIsConfirmable" />
                        <input type="hidden" id="hdn_TotalFare" />
                        <input type="hidden" id="hdn_ServiceCharge" />
                        <input type="hidden" id="hdn_Total" />
                    </div>

                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="VoucherModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 60%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Voucher</h4>
                </div>
                <div class="modal-body">

                    <%--<table class="table table-hover table-responsive" id="tbl_SendVoucher" style="width: 100%; border-top: none">
                        <tr>
                            <td style="border: none;">
                                <span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;
                                       
                                <input type="text" id="txt_sendVoucher" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 70%;" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_sendVoucher"><b>* This field is required</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none;">
                                <input id="btn_sendVoucher" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailVoucher();" />
                            </td>
                        </tr>
                    </table>--%>

                    <form class="cruise-booking-form">
                        <div class="person-information">
                            <div class="row">
                                <div class="form-group col-sm-8 col-md-8">
                                    <input type="text" id="txt_sendVoucher" class="input-text full-width" value="" placeholder="Enter Your Email" />
                                </div>
                                <div class="form-group col-sm-4 col-md-4">
                                    <button type="button" class="button btn-small orange" id="btn_sendVoucher" onclick="MailVoucher()">Send Invoice</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <script src="../assets/js/js-index.js"></script>

    <!-- CarouFredSel -->
    <script src="../assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="../assets/js/helper-plugins/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.transit.min.js"></script>
    <script type="text/javascript" src="../assets/js/helper-plugins/jquery.ba-throttle-debounce.min.js"></script>

    <!-- Easing -->
    <script src="../assets/js/jquery.easing.js"></script>
    <!-- jQuery KenBurn Slider  -->
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="../assets/js/js-about.js"></script>
    <!-- Easy Pie Chart  -->
    <script src="../assets/js/jquery.easy-pie-chart.js"></script>
    <!-- Nicescroll  -->
    <script src="../assets/js/jquery.nicescroll.min.js"></script>
    <!-- Custom functions -->
    <script src="../assets/js/functions.js"></script>
    <!-- Custom Select -->
    <script type='text/javascript' src='../assets/js/jquery.customSelect.js'></script>
    <!-- Load Animo -->
    <script src="../plugins/animo/animo.js"></script>
    <!-- Picker -->
    <script src="../assets/js/jquery-ui.js"></script>
    <!-- Picker -->
    <script src="../assets/js/jquery.easing.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../dist/js/bootstrap.min.js"></script>

</body>

</html>

