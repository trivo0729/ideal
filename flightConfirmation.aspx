﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="flightConfirmation.aspx.cs" Inherits="CUTUK.flightConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/Booking_Details.js"></script>
    <script>
         function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        var arrRervation;
        $(function () {
            var BookingID = getParameterByName("BookingID");
            var Status = getParameterByName("Success");
             var ErrorStatus =  getParameterByName("ErrorStatus");
            $.ajax({
                type: "POST",
                url: "../handler/FlightHandler.asmx/GetTicketDetails",
                data: '{"BookingID":"'+BookingID.replace(/ /g, '+')+'"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1)
                    {
                        arrRervation = result.arrResrvation;  
                        $("#reservationId").text(arrRervation.PNR);
                        var Res = "";
                        if (Status.replace(/ /g, '+') == true)
                        {
                            Res = "Booking is Confirmed";
                            $("#spComplete").text(Res);
                        }
                        else
                        {
                             Res = "Booking is OnHold.";
                             $("#spComplete").text(Res);
                             var  Msg =  " Please contact administrator regarding this booking. (" + ErrorStatus.replace(/ /g, '+')+ " )" ;
                            $("#spAck").text(Msg);
                        }
                    }
                    else if (result.retCode == 0) {
                        $('#AgencyBookingCancelModal').modal('hide')
                        $('#SpnMessege').text(result.ex);
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("something went wrong");
                    $('#ModelMessege').modal('show')
                    // alert("something went wrong");
                },
                complete: function () {
                    $("#dlgLoader").css("display", "none");
                }
            });
        })
        function ConfirmationInvoice() {
            GetPrintTicket(arrRervation.BookingID, arrRervation.Uid, arrRervation.TicketStatus)
        }
        function ConfirmationTicket() {
            GetPrintTicket(arrRervation.BookingID, arrRervation.Uid, arrRervation.TicketStatus)
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content" class="gray-area">
        <div class="container">
            <div class="row">
                <div id="main" class="col-sm-8 col-md-8">
                    <div class="booking-information travelo-box">
                        <h2>Booking Confirmation</h2>
                        <hr />
                        <div class="booking-confirmation clearfix">
                            <i class="soap-icon-recommend icon circle"></i>
                            <div class="message">
                                <h4 class="main-message" id="spComplete"></h4>
                                <br />
                                <h5 id="spAck"></h5>
                                <p>A confirmation email has been sent to your provided email address.</p>
                                <br />
                                <p> Your PNR number is :<span><b id="reservationId"></b></span></p>
                                <br />
                            </div>
                        </div>
                        <input type="button" class="button btn-medium sky-blue1" value="Get Invoice" onclick="ConfirmationInvoice()" />
                        <input type="button" class="button btn-medium sky-blue1" value="Get Ticket" onclick="ConfirmationTicket()" />
                        <input type="button" class="button btn-medium sky-blue1" value="Book Another Flight" onclick="window.location.href = 'Default.aspx'" />
                        <hr />
                    </div>
                </div>
                <div class="sidebar col-sm-4 col-md-4">
                    <div class="travelo-box contact-box">
                        <h4>Need Ideal Travels Help?</h4>
                        <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                        <address class="contact-details">
                            <span class="contact-phone"><i class="soap-icon-phone"></i>+91 992 375 0110</span><br />
                            <span class="contact-phone"><i class="soap-icon-message"></i>info@uniglobeidealtours.in</span>
                            <br />
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
