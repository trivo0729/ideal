﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="CUTUK.AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content">
        <div class="container">
            <div class="image-style style1 large-block">
                <h2 class="title">INTRODUCTION OF IDEAL TOUR AND TRAVELS</h2>
                <p>
                    Ideal Tours and Travels had a modest start as a ticketing company in 2008 and gradually spread its wings to develop a huge client base of corporates and others dealing with everything related to travel. Specialising in domestic and international tour packages.
                    <br />
                    <br />
                    After having firmly established its footing as the fastest growing tours and travel company in city, Ideal Tours and Travels turned a new leaf in Sept 2011, by venturing into money exchange. IDEAL MONEY EXCHANGE PVT. LTD (RBI approved money exchanger) is now a well known brand amongst its client base and the entire travel fraternity.
                <br />
                    <br />
                    In another leap towards providing complete travel solutions, we bring to you IDEAL JUNGLE SAFARIS, the perfect way to explore wildlife. Placed in a city like Nagpur, which has put itself on the map of wildlife tourism in a big way, it seemed like the need of the hour to have a separate wildlife tourism segment to provide better travel solutions. Owing to its geographical placement, Nagpur has gained a status of The TIGER CAPITAL of the country, with Five major Tiger reserves, PENCH, TADOBA, NAGZIRA, KANHA and BANDHAVGARH all within 4-5 hours driving distance. Nagpur is drawing wildlife enthusiasts from all over the world.
                <br />
                    <br />
                    We, at Ideal, believe in paying attention to details and carefully monitoring every booking made with us to make sure your experience is enjoyable and memorable. Providing you with the best discounted rates, tie ups with luxury hotels will ensure that your holiday is hassle free and full of adventure.
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="large-block">
                <h2>OUR TEAM</h2>
                <h4>Meet The Team Behind Ideal Tours & Travels</h4>
                <div class="row image-box style1 team">
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Tickets/Shama.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Shama<small>Head of TICKETING</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                   <%-- <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Harsha<small>DOMESTIC TICKETING CONTACT PERSON</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>--%>
                <%--    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Rajshri<small>DOMESTIC TICKETING CONTACT PERSON</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>--%>
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Tickets/Saurabh.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Mr Saurabh<small>DOMESTIC TICKETING CONTACT PERSON</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div> 
                      <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/PackagesandVisa/Takshak.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Mr Takshak Dharamkumar <small>Head of VISAS/ HOTEL BOOKING / PACKAGES</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 07030361288  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649415</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:takshak@uniglobeidealtours.in"></a>takshak@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/PackagesandVisa/Philo.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Philo <small>HOTEL BOOKING Person</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 07030361288  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649415</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:vacations@uniglobeidealtours.in"></a>vacations@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div> 
                    </div>
                 <div class="row image-box style1 team">
                   <%-- <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Shama Sheikh<small>INTERNATIONAL TICKETING CONTACT PERSON</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>--%>
                  <%--  <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Harsha Dhoke<small>INTERNATIONAL TICKETING CONTACT PERSON</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030367199  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712- 6649585</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:ticketing@uniglobeidealtours.in"></a>ticketing@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>--%>
                  
                     </div>
                 <div class="row image-box style1 team">
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/PackagesandVisa/Sneha.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Sneha <small>HOTEL BOOKING Person</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 07030361288  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649415</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:vacations@uniglobeidealtours.in"></a>vacations@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                <%--    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">MR Rajan <small>HOTEL BOOKING Person</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 07030361288  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649415</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:vacations@uniglobeidealtours.in"></a>vacations@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>--%>
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Forex/Wahid.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Mr Wahid Ali <small>Head of FOREIGN EXCHANGE MONEY CHANGER</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 9823139573  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712-6620108/107</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:idealmoneyexchange@gmail.com"></a>idealmoneyexchange@gmail.com </p>
                            </div>
                        </article>
                    </div>
                  <%--  <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/shortcodes/team/team.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Mr Twinkle David <small>Head of IDEAL JUNGLE SAFARIS</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 7030167865  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0712-6649413/414</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:idealjunglesafaris@gmail.com"></a>idealjunglesafaris@gmail.com </p>
                            </div>
                        </article>
                    </div> --%>
                       <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Account/Roshan.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Mr Roshan Borate  <small>Head of ACCOUNTS DEPARTMENT</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 9922046955  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649588</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:accounts@uniglobeidealtours.in"></a>accounts@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Account/Priya.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">Ms Priya  <small>ACCOUNTS Contact Person</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 9922046955  ,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649588</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:accounts@uniglobeidealtours.in"></a>accounts@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                     </div>
                 <div class="row image-box style1 team">
                  
                    <div class="col-sm-3 col-md-3">
                        <article class="box">
                            <figure>
                                <a href="#">
                                    <img src="images/Staff/Account/Parvez.jpg" height:"265px"; alt=""></a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="#">MR Parveez  <small>ACCOUNTS Contact Person</small></a></h4>
                                <p class="description"><i class="soap-icon-phone circle"></i>+91 9922046955  ,<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 07126649588</p>
                                <p class="description"><i class="soap-icon-message circle"></i><a href="mailto:accounts@uniglobeidealtours.in"></a>accounts@uniglobeidealtours.in </p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>

            <div class="large-block row image-box style2">
                <h3>OUR SERVICES</h3>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/flight/main1.png" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 5px 20px 5px 480px;">
                            <h4 style="margin: 0 0 8px;">FLIGHT BOOKING</h4>
                            <ul class="triangle">
                                <li>Automated Profiling of clients for error free bookings.</li>
                                <li>24 X 7 by Trip Support. , Corporate Booking Tools.</li>
                                <li>Account Management Reporting to reduce travel costs.</li>
                                <li>Cost Containment. , Lowest Fare Assured. , Easy Cancellation & Refund.</li>
                                <li>Attractive Discount & offers on Online bookings.</li>
                                <li>Dedicated Travel Export. , Safe & secure Gateway for online Bookings.</li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/shortcodes/gallery-popup/4.jpg" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 5px 20px 5px 480px;">
                            <h4 style="margin: 0 0 8px;">HOTEL BOOKING</h4>
                            <ul class="triangle">
                                <li>Worldwide Hotel Inventory          </li>
                                <li>Online Price Match                                             </li>
                                <li>More than 250,000+ Hotel Rooms                                 </li>
                                <li>45,000+ Sightseeing items and over 5000 Tours in 500 cities    </li>
                                <li>5,000+ Transfer Options in Over 900 Airport and City Locations </li>
                                <li>Instant Confirmation                                           </li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/tour/packages/3-col/6.jpg" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 5px 20px 5px 480px;">
                            <h4 style="margin: 0 0 8px;">HOLIDAYS BOOKING</h4>
                            <ul class="triangle">
                                <li>International & Domestic Holidays</li>
                                <li>Honeymoon Tours                                                  </li>
                                <li>Corporate Events / Conference                                    </li>
                                <li>Theme Based Tours                                                </li>
                                <li>Pilgrimage Tours / Group Tours                                   </li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/offers02.jpg" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 5px 20px 5px 480px;">
                            <h4 style="margin: 0 0 8px;">CRUISE BOOKING</h4>
                            <ul class="triangle">
                                <li>Asian Cruise Bookings (Dream Cruise / Royal Caribbean)                      </li>
                                <li>International Cruises (MSC, NCL, Princess, Celebrity, Carnival Cruise etc)  </li>
                                <li>Expedition Cruises (Hurtigruten, Alaska, Antarctic expedition etc)          </li>
                                <li>European River Cruises                                                      </li>
                                <li>Yacht Charter, Dinner Cruise, Sunset Cruise                                 </li>
                                <li>Indian Cruises (Jalesh, Angriya Cruise, Brahmaputra Cruises)                </li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/jungle-safari.png" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 5px 20px 5px 480px;">
                            <h4 style="margin: 0 0 8px;">JUNGLE SAFARI</h4>
                            <ul class="triangle">
                                <li>Jungle Safari Bookings. , Resort Bookings.        </li>
                                <li>Corporate Events.              </li>
                                <li>Team building Activities.      </li>
                                <li>Night Safaris* , Services of Naturalists .</li>
                                <li>Birthday Party in wilderness.  </li>
                                <li>Bush Dinners. and many more options. </li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="col-md-12">
                    <article class="box">
                        <figure style="width: 450px">
                            <a href="#" title="">
                                <img src="images/Currency.jpg" alt="" width="272" height="192" /></a>
                        </figure>
                        <div class="details" style="padding: 20px 20px 10px 480px;">
                            <h4 style="margin: 0 0 8px;">FOREX</h4>
                            <ul class="triangle">
                                <li>We Buy & Sell Foreign Currencies. </li>
                                <li>Travel Currency Debit Cards.      </li>
                                <li>TT Payments.                      </li>
                                <li>(RBI Approved Money Changer).     </li>
                            </ul>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
