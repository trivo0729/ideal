﻿using CUT.BL;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CUTUK
{
    /// <summary>
    /// Summary description for DefaultHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DefaultHandler : System.Web.Services.WebService
    {
        string json = "";

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        [WebMethod(EnableSession = true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            DataTable dtResult;
            DBHelper.DBReturnCode retCode = CUTUK.DataLayer.DefaultManager.B2CUserLogin(sUserName, sPassword);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Logout()
        {
            Session["LoginUser"] = null;
            Session.Abandon();
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }

        [WebMethod(EnableSession = true)]
        public string CheckSession()
        {
            if (Session["LoginCustomer"] != null)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"0\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DefaultManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DefaultManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"City\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string ConfirmationDetails(string ReservationID)
        {

            string jsonString = "";
            DataTable dtResult;
            //string ReserveId = ReservationID.ToString();
            //DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatement(sid, sFrom, sTo, out dtResult);
            DBHelper.DBReturnCode retcode = CUTUK.DataLayer.DefaultManager.ConfirmationDetails(ReservationID, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public bool SendInvoice(string sEmail, string ReservationId, string UID, string Night)
        {
            bool sJsonString = false;

            sJsonString = EmailManager.SendInvoice(sEmail, ReservationId, UID, Night);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }

        [WebMethod(EnableSession = true)]
        public bool SendFlightInvoice(string sEmail, string ReservationId)
        {
            bool sJsonString = false;

            sJsonString = EmailManager.SendFlightInvoice(sEmail, ReservationId);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }



        [WebMethod(EnableSession = true)]
        public bool SendVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp)
        {
            bool sJsonString = false;
            sJsonString = EmailManager.SendVoucher(sEmail, ReservationId, UID, Latitude, Longitude, Night, Supp);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }

        [WebMethod(EnableSession = true)]
        public bool SendContactMail(string name, string email, string subject, string message)
        {
            bool sJsonString = false;
            sJsonString = ContactMail.SendEmail(name, email, subject, message);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }

    }
}
