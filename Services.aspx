﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="CUTUK.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper" class="service-page">

        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Services</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">HOME</a></li>
                    <li class="active">Services</li>
                </ul>
            </div>
        </div>

        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="large-block">
                        <div class="travelo-box">
                            <div class="overflow-hidden">
                                <div class="services column-5-no-margin">
                                    <article class="icon-box style9">
                                        <i class="soap-icon-securevault"></i>
                                        <h5 class="box-title">Domestic & International Ticketing</h5>
                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-search"></i>
                                        <h5 class="box-title">Railway Reservations</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-hotel"></i>
                                        <h5 class="box-title">Hotel Reservation</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-trunk-1"></i>
                                        <h5 class="box-title">Holiday/Tour packages(FIT-GIT)</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-passenger"></i>
                                        <h5 class="box-title">LTC Tour Packages</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-adventure"></i>
                                        <h5 class="box-title">Excursions</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-card"></i>
                                        <h5 class="box-title">Visas</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-baggage-status"></i>
                                        <h5 class="box-title">Umrah & Ziyarat Packages</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-photogallery"></i>
                                        <h5 class="box-title">Photo Gallery</h5>

                                    </article>
                                    <article class="icon-box style9">
                                        <i class="soap-icon-car-2"></i>
                                        <h5 class="box-title">Cabs/Car Rental/Transport</h5>

                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-block">
                        <div class="row image-box style4">
                            <div class="col-sm-4">
                                <article class="box">
                                    <figure>
                                        <a class="hover-effect" href="#" title="">
                                            <img src="images/film-shooting.jpg" alt="" width="370" height="172" /></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Film Tourism(Shooting Managements)</h4>
                                        <br />

                                    </div>
                                </article>
                            </div>
                            <div class="col-sm-4">
                                <article class="box">
                                    <figure>
                                        <a class="hover-effect" href="#" title="">
                                            <img src="images/event-party2.jpg" alt="" width="370" height="172" /></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Events</h4>

                                    </div>
                                </article>
                            </div>
                            <div class="col-sm-4">
                                <article class="box">
                                    <figure>
                                        <a class="hover-effect" href="#" title="">
                                            <img src="images/contrate-meeting.jpg" alt="" width="370" height="172" /></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">Corporate Meeting</h4>

                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>

    </div>
</asp:Content>
