﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="flight.aspx.cs" Inherits="CUTUK.flight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/tooltip.js"></script>
    <script src="scripts/Flights.js?v=1.1"></script>
    <script src="scripts/Filters.js"></script>
    <script src="scripts/flightAuto.js"></script>
    <script src="scripts/flightSearch.js"></script>
    <link href="css/flight.css" rel="stylesheet" />
    <link href="css/font.css" rel="stylesheet" />
    <link href="css/multiple-select.css" rel="stylesheet" />
    <script src="css/jquery.multiple.select.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
    <style type="text/css">
        .popover {
            white-space: pre-wrap;
            max-width: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="content">
        <img alt="" src="images/Processing.gif" id="loders" style="z-index: 9999; position: fixed; margin-top: 5%; margin-left: 50%; width: 10%">
        <div class="container">
            <div id="main">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <div id="m_Supplier"></div>
                    </div>
                    <div class="col-md-2" style="float: right; display: none" id="btn_Modify">
                        <button type="button" style="background-color: #01b7f2;" class="full-width" id="btn_Showr" onclick="ShowR();">Modify Search</button>
                        <button type="button" style="background-color: #01b7f2; display: none" class="full-width" id="btn_hider" onclick="HideR();">Modify Search</button>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="search-box-wrapper style2" style="margin-top: 0px; display: none" id="SearchDive">
                            <div class="search-box">
                                <div class="search-tab-content">
                                    <div class="tab-pane fade active in" id="hotels-tab">
                                        <form>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h5 style="margin-top: 0px" class="a-white">Book Domestic &amp; International Flight Tickets</h5>
                                                </div>
                                                <div>
                                                    <div class="col-sm-1">
                                                        <input type="radio" name="JourneyType" id="radio01" value="O" title="One Way" onclick="OneWayRadioFunc()" checked="checked" />

                                                        <label for="radio01">
                                                            <span></span>
                                                            <span class="a-white">One Way</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="radio" name="JourneyType" id="radio02" value="R" title="Round Way" onclick="RoundWayRadioFunc()" />
                                                        <label for="radio02">
                                                            <span></span>
                                                            <span class="a-white">Round Way</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" name="JourneyType" id="radio03" value="M" title="Multiple Destinations" onclick="MultiWayRadioFunc()" />
                                                        <label for="radio03">
                                                            <span></span>
                                                            <span class="a-white">Multiple Destinations</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2" style="display: none">

                                                        <input type="radio" name="JourneyType" id="radio04" value="M" title="Multi Way" onclick="CalendarFareRadioFunc()" />
                                                        <label for="radio04">
                                                            <span></span>
                                                            <span style="color: black">Calendar Fare</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2" style="display: none">

                                                        <input type="radio" name="JourneyType" id="radioAdvance" value="M" title="Multi Way" onclick="OneWayRadioFunc()" />
                                                        <label for="radioAdvance">
                                                            <span></span>
                                                            <span style="color: black">Advance Search</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2" style="float: left">
                                                        <input type="checkbox" name="NonStop" id="chkNonStop" value="NonStop" title="Non Stop Flights" />
                                                        <label for="chkNonStop">
                                                            <span></span>
                                                            <span class="a-white">Non Stop Flights</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="display: none">
                                                <div class="col-sm-4">
                                                </div>
                                                <div>
                                                    <div class="col-sm-2" style="display: none">
                                                        <input type="radio" name="RoundWay" id="rdb_Normal" value="O" style="display: none" class="RoundWay" title="Normal Return" checked="" style="display: inline-block;" />
                                                        <label for="rdb_Normal" class="RoundWay" style="display: none">
                                                            <span></span>
                                                            <span style="color: black">Normal Return</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2" style="display: none">
                                                        <input type="radio" name="RoundWay" id="rdb_SP" value="R" style="display: none" class="RoundWay" title="Special Return" style="display: inline-block;" />
                                                        <label for="rdb_SP" class="RoundWay" style="display: none">
                                                            <span></span>
                                                            <span style="color: black">LCC Special Returns</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2" style="display: none">
                                                        <input type="radio" name="RoundWay" id="rdb_GDS" value="M" style="display: none" class="RoundWay" title="Special GDS Return" style="display: inline-block;" />
                                                        <label for="rdb_GDS" class="RoundWay" style="display: none">
                                                            <span></span>
                                                            <span style="color: black">GDS Special Returns</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="GeneralSearch">
                                                <div class="row">
                                                    <div class="col-md-8" id="fromToOnwardDateDiv">
                                                        <div class="form-group row"></div>
                                                    </div>

                                                    <div class="col-md-3" style="display: none" id="returnDateDivId">
                                                        <div class="form-group row">
                                                            <span class="text-left a-white">Return Date</span>
                                                            <div class="datepicker-wrap">
                                                                <input id="datepicker_Return" type="text" name="date_from" class="form-control" placeholder="return On" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div id="addDestinationDivId" class="row white" style="float: right; padding-right: 234px; display: none; margin-bottom: 20px; margin-top: -60px">
                                                        <i class="soap-icon-plus a-white" onclick="addDestination()" title="Add Destination" style="font-size: 40px; cursor: pointer"></i>
                                                    </div>
                                                    <div id="removeLastDestnDivId" class="row white" style="float: right; padding-right: 234px; display: none; margin-bottom: 20px; margin-top: -60px">
                                                        <i class="soap-icon-minus a-white" onclick="removeLastDestn()" title="Remove Destination" style="font-size: 40px; cursor: pointer"></i>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6" style="display: none">
                                                        <div class="form-group row">
                                                            <div class="col-md-6">
                                                                <span class="text-left" style="color: #838383">Airlines</span>
                                                                <select id="sel_Airlines" class="form-control" multiple="multiple">
                                                                    <option value="SG">Spice Jet</option>
                                                                    <option value="6E">Indigo</option>
                                                                    <option value="G8">Go Air</option>
                                                                    <option value="G9">Air Arabia</option>
                                                                    <option value="FZ">Fly Dubai</option>
                                                                    <option value="IX">Air India Express</option>
                                                                    <option value="AK">Air Asia</option>
                                                                    <option value="LB">Air Costa</option>
                                                                    <option value="UK">Air Vistara</option>
                                                                    <option value="UK">Air Vistara</option>
                                                                    <option value="AI">Air India</option>
                                                                    <option value="9W">Jet Airways</option>
                                                                    <option value="S2">JetLite</option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group row">
                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Class</span>
                                                                <select class="form-control" id="sel_Class">
                                                                    <option selected="selected" value="1">All</option>
                                                                    <option value="2">Economy</option>
                                                                    <option value="3">Premium Economy</option>
                                                                    <option value="4">Business</option>
                                                                    <option value="5">Premium Business</option>
                                                                    <option value="6">First</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Adults</span>
                                                                <select class="form-control" id="sel_Adults">
                                                                    <option selected="selected" value="1">01</option>
                                                                    <option value="2">02</option>
                                                                    <option value="3">03</option>
                                                                    <option value="4">04</option>
                                                                    <option value="">05</option>
                                                                    <option value="1">06</option>
                                                                    <option value="2">07</option>
                                                                    <option value="3">08</option>
                                                                    <option value="4">09</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Kids</span>
                                                                <select class="form-control" id="sel_Child">
                                                                    <option selected="selected" value="0">00</option>
                                                                    <option value="1">01</option>
                                                                    <option value="2">02</option>
                                                                    <option value="3">03</option>
                                                                    <option value="4">04</option>
                                                                    <option value="">05</option>
                                                                    <option value="1">06</option>
                                                                    <option value="2">07</option>
                                                                    <option value="3">08</option>
                                                                    <option value="4">09</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-left a-white">Infants</span>
                                                                <select class="form-control" id="sel_Infant">
                                                                    <option selected="selected" value="0">00</option>
                                                                    <option value="1">01</option>
                                                                    <option value="2">02</option>
                                                                    <option value="3">03</option>
                                                                    <option value="4">04</option>
                                                                    <option value="">05</option>
                                                                    <option value="1">06</option>
                                                                    <option value="2">07</option>
                                                                    <option value="3">08</option>
                                                                    <option value="4">09</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-xs-3">
                                                            </div>
                                                            <div class="col-xs-6 pull-right">
                                                                <input type="button" class="button btn-medium sky-blue1" value="Search" onclick="SearchFlight()" />
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="CalendarSearch" style="display: none">
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="text-left" style="color: #838383">From</span>
                                                        <br />
                                                        <input type="text" id="CalenderFrom" class="form-control" />
                                                        <input type="hidden" id="hdCalenderFromCode" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <span class="text-left" style="color: #838383">To</span>
                                                        <br />
                                                        <input type="text" id="CalenderTo" class="form-control" />
                                                        <input type="hidden" id="hdCalenderToCode" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <span class="text-left" style="color: #838383">Month & Year</span>
                                                        <div class="datepicker-wrap">
                                                            <input id="dt_CalendarMonthYear" type="text" name="date_from" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="text-left" style="color: #838383">Preferred Carrier</span>
                                                        <select class="form-control" id="PreferredCarrier">
                                                            <option value="0">select</option>
                                                            <option value="SG">Spice Jet</option>
                                                            <option value="6E">Indigo</option>
                                                            <option value="G8">Go Air</option>
                                                            <option value="G9">Air Arabia</option>
                                                            <option value="FZ">Fly Dubai</option>
                                                            <option value="IX">Air India Express</option>
                                                            <option value="AK">Air Asia</option>
                                                            <option value="LB">Air Costa</option>
                                                            <option value="GDS">GDS</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <span class="text-left" style="color: #838383">Class</span>
                                                        <select class="form-control" id="sel_CalendarClass">
                                                            <option value="1">All</option>
                                                            <option value="2">Economy</option>
                                                            <option value="3">Premium Economy</option>
                                                            <option value="4">Business</option>
                                                            <option value="5">Premium Business</option>
                                                            <option value="6">First</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br />
                                                <input type="button" class="button btn-medium sky-blue1" id="btn_CalendarSearch" style="float: right" value="Search" onclick="CalendarSearchFlight()" />
                                                <br />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div_Airline"></div>
                    </div>
                    <div class="all-bottomdata" id="Div_domestic" style="display: none">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="AlertModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="AlertMessage"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ConformModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <%-- <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>--%>
                        <div>
                            <p class="" id="ConformMessage">
                            </p>
                            <table align="right" style="margin-left: -50%;">
                                <tbody>
                                    <tr>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" id="Cancel" value="Cancel" onclick="Cancel()" class="button btn-small sky-blue1" style="float: none;">
                                        </td>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Ok" id="btnOk" class="button btn-small orange" style="float: none;">
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <br />

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
