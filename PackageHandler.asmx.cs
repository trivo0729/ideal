﻿using CUT.DataLayer;
using CutAdmin.BL;
using CUTUK.BL;
using CUTUK.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CUTUK
{
    /// <summary>
    /// Summary description for PackageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PackageHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHandlerDataContext DB = new DBHandlerDataContext();
        string json = "";
        [WebMethod(EnableSession = true)]
        public string GetAllPackages(string Type)
        {
            bool iDomestic = true;
            if (Type != "Domestic")
                iDomestic = false;
            try
            {
                var List = (from Package in DB.tbl_Packages
                            join PackageImg in DB.tbl_PackageImages on Package.nID equals PackageImg.nPackageID
                            where Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"]) && Package.Status == true
                            && Package.IsDomestic == iDomestic
                            select new
                            {
                                Package.nID,
                                Package.sPackageName,
                                Package.sPackageDestination,
                                Package.sPackageCategory,
                                Package.sPackageThemes,
                                Package.sPackageDescription,
                                Package.nDuration,
                                Package.sInventory,
                                Package.dValidityFrom,
                                Package.dValidityTo,
                                PackageImg.ImageArray,
                            }).Distinct().ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }



        [WebMethod(EnableSession = true)]
        public string GetPackagesByCity(string City)
        {

            try
            {
                var List = (from Package in DB.tbl_Packages
                            join PackageImg in DB.tbl_PackageImages on Package.nID equals PackageImg.nPackageID
                            where Package.sPackageDestination.Contains(City)
                            && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"]) && Package.Status == true
                            select new
                            {
                                Package.nID,
                                Package.sPackageName,
                                Package.sPackageDestination,
                                Package.sPackageCategory,
                                Package.sPackageThemes,
                                Package.sPackageDescription,
                                Package.nDuration,
                                Package.sInventory,
                                Package.dValidityFrom,
                                Package.dValidityTo,
                                PackageImg.ImageArray,
                            }).Distinct().ToList();
                if (List.Any())
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                else
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPackageCity(string name)
        {
            string jsonString = "";
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            List<string> arrCity = new List<string>();
            var List = (from Package in DB.tbl_Packages
                        where Package.sPackageDestination.Contains(name) && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"]) && Package.Status == true
                        select new
                        {
                            Package.sPackageDestination,
                        }).Distinct().ToList();
            foreach (var objcity in List)
            {
                var City = objcity.sPackageDestination.Split('|');
                foreach (var item in City)
                {
                    string city = item.TrimStart().TrimEnd();
                    if (city != "" && city.ToLower().StartsWith(name.ToLower()))
                    {
                        arrCity.Add(city);
                    }

                }
            }
            if (List.Any())
            {
                List<CUT.Models.AutoComplete> list_autocomplete = new List<CUT.Models.AutoComplete>();
                list_autocomplete = arrCity.AsEnumerable()
                    .Select(data => new CUT.Models.AutoComplete
                    {
                        id = data,
                        value = data,
                    }).Distinct().ToList();
                jsonString = objSerlizer.Serialize(list_autocomplete);
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "Not Available" });
            }
            return jsonString;
        }

        //public static DBHelper.DBReturnCode Get(string name, out DataTable dtResult)
        //{
        //    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["Id"]);
        //    SqlParameter[] sqlParams = new SqlParameter[2];
        //    sqlParams[0] = new SqlParameter("@Destination", name);
        //    sqlParams[1] = new SqlParameter("@ParentID", ParentID);
        //    DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("PkgCity_GetPackageDestination", out dtResult, sqlParams);
        //    return retCode;
        //}

        [WebMethod(EnableSession = true)]
        public string GetPrice(Int64 Id)
        {
            try
            {
                var List = (from Price in DB.tbl_PackagePricings where Price.nPackageID == Id select Price).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPackageDetail(Int64 Id)
        {
            try
            {

                var Pack = (from Package in DB.tbl_Packages
                            where Package.nID == Id && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])
                            select Package).ToList();

                var PackImg = (from Images in DB.tbl_PackageImages
                               where Images.nPackageID == Id
                               select Images).ToList();

                var Pric = (from Price in DB.tbl_PackagePricings
                            where Price.nPackageID == Id
                            select Price).ToList();

                var Itn = (from Itinary in DB.tbl_Itineraries
                           where Itinary.nPackageID == Id
                           select Itinary).ToList();

                var ItnHotels = (from Hotels in DB.tbl_ItineraryHotels
                                 where Hotels.nPackageID == Id
                                 select Hotels).ToList();

                var HotelImgs = (from Imgs in DB.tbl_HotelImages
                                 where Imgs.nPackageID == Id
                                 select Imgs).ToList().Distinct();

                HttpContext.Current.Session["Pack_Session"] = Pack;
                HttpContext.Current.Session["Pric_Session"] = Pric;

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack, PackImg = PackImg, Pric = Pric, Itn = Itn, ItnHotels = ItnHotels, HotelImgs = HotelImgs });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetPackage(Int64 Id)
        {
            try
            {

                var Pack = (from Package in DB.tbl_Packages
                            where Package.nID == Id && Package.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])
                            select Package).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetPackagePrice(Int64 Id, string CategoryName)
        {
            try
            {

                var Pack = (from PackagePrice in DB.tbl_PackagePricings
                            where PackagePrice.nPackageID == Id && PackagePrice.sCategoryName == CategoryName
                            select PackagePrice).ToList();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Pack });
            }
            catch
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(true)]
        public string BookPackage(string nID, string noOfDays, decimal AdultPrice, decimal ChildBedsPrice, decimal ChildWBed, string CategoryID, string Name, string MobileNo, string Email, Int64 noAdults, Int64 noChild, string noInfant, string ChildAges, string TarvelDate, string AddOnReq, string PackageName, string StartDate, string TillDate, string CategoryType)
        {
            string jsonString = "";
            try
            {
                GlobalDefault objGlobalDefaultCustomer = (GlobalDefault)HttpContext.Current.Session["LoginCustomer"];
                string CustomerId = objGlobalDefaultCustomer.B2C_Id;
                tbl_PackageReservation Add = new tbl_PackageReservation();
                Add.PackageID = Convert.ToInt64(nID);
                Add.CategoryId = Convert.ToInt64(CategoryID);
                Add.TravelDate = TarvelDate;
                Add.StartFrom = StartDate;
                Add.EndDate = TillDate;
                Add.noDays = noOfDays;
                Add.Adults = noAdults;
                Add.Childs = noChild;
                Add.Infant = 0;
                Add.AdultPrice = AdultPrice;
                Add.ChildBedsPrice = ChildBedsPrice;
                Add.ChildWBed = ChildWBed;
                Add.LeadingPax = Name;
                Add.email = Email;
                Add.PhoneNumber = MobileNo;
                //Add.Nationality = Nationality;
                Add.Type = CategoryType;
                Add.AddOn = AddOnReq;
                Add.ChildAges = ChildAges;
                Add.B2CId = CustomerId;
                DB.tbl_PackageReservations.InsertOnSubmit(Add);
                DB.SubmitChanges();

                PackageMail.BookPackage(nID, CategoryID, Name, MobileNo, Email, noAdults, noChild, noInfant, ChildAges, TarvelDate, AddOnReq, PackageName, StartDate, TillDate, CategoryType);
                PackageMail.AdminMail(nID, CategoryID, Name, MobileNo, Email, noAdults, noChild, noInfant, ChildAges, TarvelDate, AddOnReq, PackageName, StartDate, TillDate, CategoryType);
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;


        }

        [WebMethod(EnableSession = true)]
        public string GetStaticPackage()
        {
            try
            {
                var Package = DB.tbl_StaticPackages.Where(x => x.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["Id"])).ToList();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Packages = Package });
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SendPackagesDetails(string ID, string Email, string Name, string Remark)
        {
            string Doc = ""; Int64 nID = Convert.ToInt64(ID);
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            Int64 AdminSuperID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminSuperID"]);
            Int64 AdminID = Convert.ToInt64(ConfigurationManager.AppSettings["Id"]);
            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
                #region List_PackageDetail
                DataTable dtResult = new DataTable();
                retCode = CutAdmin.DataLayer.PackageManager.SinglePackageDetails(nID, out dtResult);
                var List_PackageDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         sPackageName = data.Field<string>("sPackageName"),
                         sPackageDestination = data.Field<string>("sPackageDestination"),
                         sPackageCategory = data.Field<string>("sPackageCategory"),
                         sPackageThemes = data.Field<string>("sPackageThemes"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sPackageDescription = data.Field<string>("sPackageDescription"),
                         dValidityFrom = data.Field<string>("dValidityFrom"),
                         dValidityTo = data.Field<string>("dValidityTo"),
                         dTax = data.Field<Decimal>("dTax"),
                         nCancelDays = data.Field<Int64>("nCancelDays"),
                         dCancelCharge = data.Field<Decimal>("dCancelCharge"),
                         sTermsCondition = data.Field<String>("sTermsCondition"),
                         sInventory = data.Field<String>("sInventory"),
                         IsDomestic = data.Field<bool>("IsDomestic"),
                         Transfer = data.Field<bool>("Transfer")
                     }).ToList();

                #endregion

                #region List_pricingDetail
                retCode = CutAdmin.DataLayer.PackageManager.GetPricingDetail(nID, out dtResult);

                var List_pricingDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         sCategoryName = data.Field<string>("sCategoryName"),
                         dSingleAdult = data.Field<Decimal>("dSingleAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dSingleAdult").ToString(),
                         dCouple = data.Field<Decimal>("dCouple").ToString() == "0.00" ? "" : data.Field<Decimal>("dCouple").ToString(),
                         dExtraAdult = data.Field<Decimal>("dExtraAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dExtraAdult").ToString(),
                         dInfantKid = data.Field<Decimal>("dInfantKid").ToString() == "0.00" ? "" : data.Field<Decimal>("dInfantKid").ToString(),
                         dKidWBed = data.Field<Decimal>("dKidWBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWBed").ToString(),
                         dKidWOBed = data.Field<Decimal>("dKidWOBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWOBed").ToString(),
                         sStaticInclusions = data.Field<string>("sStaticInclusions").Trim(','),
                         sDynamicInclusion = data.Field<string>("sDynamicInclusion").Trim(','),
                         sStaticExclusion = data.Field<string>("sStaticExclusion").Trim(','),
                         sDynamicExclusion = data.Field<string>("sDynamicExclusion").Trim(',')
                     }).ToList();
                Int64 PricingDetailnCount = Convert.ToInt64(dtResult.Rows.Count);
                #endregion

                #region List_ItineraryDetail
                retCode = CutAdmin.DataLayer.PackageManager.GetItineraryDetail(nID, out dtResult);

                var List_ItineraryDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("nCategoryName"),
                         sItinerary_1 = data.Field<string>("sItinerary_1"),
                         sItinerary_2 = data.Field<string>("sItinerary_2"),
                         sItinerary_3 = data.Field<string>("sItinerary_3"),
                         sItinerary_4 = data.Field<string>("sItinerary_4"),
                         sItinerary_5 = data.Field<string>("sItinerary_5"),
                         sItinerary_6 = data.Field<string>("sItinerary_6"),
                         sItinerary_7 = data.Field<string>("sItinerary_7"),
                         sItinerary_8 = data.Field<string>("sItinerary_8"),
                         sItinerary_9 = data.Field<string>("sItinerary_9"),
                         sItinerary_10 = data.Field<string>("sItinerary_10"),
                         sItinerary_11 = data.Field<string>("sItinerary_11"),
                         sItinerary_12 = data.Field<string>("sItinerary_12"),
                         sItinerary_13 = data.Field<string>("sItinerary_13"),
                         sItinerary_14 = data.Field<string>("sItinerary_14"),
                         sItinerary_15 = data.Field<string>("sItinerary_15")
                     }).ToList();
                Int64 ItineraryDetailnCount = dtResult.Rows.Count;
                #endregion

                #region List_ItineraryHotelDetail
                retCode = CutAdmin.DataLayer.PackageManager.GetItineraryHotelDetail(nID, out dtResult);

                var List_ItineraryHotelDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("sCategoryName"),
                         sHotelName_1 = data.Field<string>("sHotelName_1"),
                         sHotelName_2 = data.Field<string>("sHotelName_2"),
                         sHotelName_3 = data.Field<string>("sHotelName_3"),
                         sHotelName_4 = data.Field<string>("sHotelName_4"),
                         sHotelName_5 = data.Field<string>("sHotelName_5"),
                         sHotelName_6 = data.Field<string>("sHotelName_6"),
                         sHotelName_7 = data.Field<string>("sHotelName_7"),
                         sHotelName_8 = data.Field<string>("sHotelName_8"),
                         sHotelName_9 = data.Field<string>("sHotelName_9"),
                         sHotelName_10 = data.Field<string>("sHotelName_10"),
                         sHotelName_11 = data.Field<string>("sHotelName_11"),
                         sHotelName_12 = data.Field<string>("sHotelName_12"),
                         sHotelName_13 = data.Field<string>("sHotelName_13"),
                         sHotelName_14 = data.Field<string>("sHotelName_14"),
                         sHotel_Code_1 = data.Field<string>("sHotel_Code_1"),
                         sHotel_Code_2 = data.Field<string>("sHotel_Code_2"),
                         sHotel_Code_3 = data.Field<string>("sHotel_Code_3"),
                         sHotel_Code_4 = data.Field<string>("sHotel_Code_4"),
                         sHotel_Code_5 = data.Field<string>("sHotel_Code_5"),
                         sHotel_Code_6 = data.Field<string>("sHotel_Code_6"),
                         sHotel_Code_7 = data.Field<string>("sHotel_Code_7"),
                         sHotel_Code_8 = data.Field<string>("sHotel_Code_8"),
                         sHotel_Code_9 = data.Field<string>("sHotel_Code_9"),
                         sHotel_Code_10 = data.Field<string>("sHotel_Code_10"),
                         sHotel_Code_11 = data.Field<string>("sHotel_Code_11"),
                         sHotel_Code_12 = data.Field<string>("sHotel_Code_12"),
                         sHotel_Code_13 = data.Field<string>("sHotel_Code_13"),
                         sHotel_Code_14 = data.Field<string>("sHotel_Code_14"),
                         sHotelDescrption_1 = data.Field<string>("sHotelDescrption_1"),
                         sHotelDescrption_2 = data.Field<string>("sHotelDescrption_2"),
                         sHotelDescrption_3 = data.Field<string>("sHotelDescrption_3"),
                         sHotelDescrption_4 = data.Field<string>("sHotelDescrption_4"),
                         sHotelDescrption_5 = data.Field<string>("sHotelDescrption_5"),
                         sHotelDescrption_6 = data.Field<string>("sHotelDescrption_6"),
                         sHotelDescrption_7 = data.Field<string>("sHotelDescrption_7"),
                         sHotelDescrption_8 = data.Field<string>("sHotelDescrption_8"),
                         sHotelDescrption_9 = data.Field<string>("sHotelDescrption_9"),
                         sHotelDescrption_10 = data.Field<string>("sHotelDescrption_10"),
                         sHotelDescrption_11 = data.Field<string>("sHotelDescrption_11"),
                         sHotelDescrption_12 = data.Field<string>("sHotelDescrption_12"),
                         sHotelDescrption_13 = data.Field<string>("sHotelDescrption_13"),
                         sHotelDescrption_14 = data.Field<string>("sHotelDescrption_14"),
                         sHotelImage1 = data.Field<string>("sHotelImage1"),
                         sHotelImage2 = data.Field<string>("sHotelImage2"),
                         sHotelImage3 = data.Field<string>("sHotelImage3"),
                         sHotelImage4 = data.Field<string>("sHotelImage4"),
                         sHotelImage5 = data.Field<string>("sHotelImage5"),
                         sHotelImage6 = data.Field<string>("sHotelImage6"),
                         sHotelImage7 = data.Field<string>("sHotelImage7"),
                         sHotelImage8 = data.Field<string>("sHotelImage8"),
                         sHotelImage9 = data.Field<string>("sHotelImage9"),
                         sHotelImage10 = data.Field<string>("sHotelImage10"),
                         sHotelImage11 = data.Field<string>("sHotelImage11"),
                         sHotelImage12 = data.Field<string>("sHotelImage12"),
                         sHotelImage13 = data.Field<string>("sHotelImage13"),
                         sHotelImage14 = data.Field<string>("sHotelImage14")
                     }).ToList();
                Int64 ItineraryHotelDetailnCount = dtResult.Rows.Count;
                #endregion

                #region List_ImageDetail
                retCode = CutAdmin.DataLayer.PackageManager.GetProductImages(nID, out dtResult);

                var List_ImageDetail = dtResult.AsEnumerable()
                    .Select(data => new
                    {
                        nID = data.Field<Int64>("nID"),
                        nPackageID = data.Field<Int64>("nPackageID"),
                        ImageArray = data.Field<string>("ImageArray"),
                    }).ToList();
                #endregion

                #region List_TransferDetail

                var List_TransferDetail = (from obj in DB.tbl_PackageCabs where obj.PackageId == nID select obj).ToList();
                #endregion

                #region List_PackageActivityDetail
                var List_PackageActivityDetail = (from obj in DB.tbl_PackageActivities
                                                  join Det in DB.tbl_SightseeingMasters on obj.ActivityId equals Det.Activity_Id
                                                  where obj.PackageId == nID && Det.ParentID == AdminID
                                                  select new
                                                  {
                                                      obj.ActivityId,
                                                      Det.Act_Name,
                                                  }).ToList();
                Int64 PackageActivityDetailnCount = List_PackageActivityDetail.Count();
                #endregion


                using (var dbb = new DBHandlerDataContext())
                {
                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["Id"]);
                    Int64 ContactID = 0;
                    ContactID = dbb.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().ContactID;
                    #region SupplierDetails
                    var dtSupplierDetails = (from obj in dbb.tbl_AdminLogins
                                             join objc in dbb.tbl_Contacts on obj.ContactID equals objc.ContactID
                                             from objh in dbb.tbl_HCities
                                             where obj.ContactID == ContactID && objc.Code == objh.Code
                                             select new
                                             {
                                                 obj.AgencyName,
                                                 obj.Agentuniquecode,
                                                 objc.Address,
                                                 objc.email,
                                                 objc.Mobile,
                                                 objc.phone,
                                                 objc.PinCode,
                                                 objc.Fax,
                                                 objc.sCountry,
                                                 objc.Website,
                                                 objc.StateID,
                                                 objh.Countryname,
                                                 objh.Description
                                             }).FirstOrDefault();
                    #endregion

                    //using (var db = new Trivo_AmsHelper())
                    //{
                    //Doc = GetTemplate(AdminID, "Details");
                    string html = "";
                    string PriceDetailhtml = "";

                    foreach (var item in List_pricingDetail)
                    {
                        PriceDetailhtml += "<tr>";
                        PriceDetailhtml += "    <td style='width:102px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.sCategoryName + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dSingleAdult + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:114px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dCouple + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dExtraAdult + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:78px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dInfantKid + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:126px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dKidWOBed + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                        PriceDetailhtml += "        <strong>" + item.dKidWBed + "</strong>";
                        PriceDetailhtml += "    </td>";
                        PriceDetailhtml += "</tr>";
                    }

                    for (int i = 0; i < List_PackageDetail[0].nDuration; i++)
                    {
                        TemplateGenrator.GetTemplatePath(AdminSuperID, "Package_Days");
                        dynamic arrItineraryHotelDetail; string HotelImage = "";
                        string HotelDescription = ""; string HotelName = "";
                        if (List_ItineraryHotelDetail.Count != 0)
                        {
                            arrItineraryHotelDetail = List_ItineraryHotelDetail[0];

                            HotelName = "sHotelName_" + (i + 1);
                            HotelName = arrItineraryHotelDetail.GetType().GetProperty(HotelName).GetValue(arrItineraryHotelDetail, null);

                            HotelDescription = "sHotelDescrption_" + (i + 1);
                            HotelDescription = arrItineraryHotelDetail.GetType().GetProperty(HotelDescription).GetValue(arrItineraryHotelDetail, null);

                            HotelImage = "sHotelImage" + (i + 1);
                            HotelImage = arrItineraryHotelDetail.GetType().GetProperty(HotelImage).GetValue(arrItineraryHotelDetail, null);
                        }

                        dynamic arrItineraryDetail; string ItenaryDetail = "";
                        string Url = "";
                        if (List_ItineraryDetail.Count != 0)
                        {
                            arrItineraryDetail = List_ItineraryDetail[0];
                            ItenaryDetail = "sItinerary_" + (i + 1);
                            ItenaryDetail = arrItineraryDetail.GetType().GetProperty(ItenaryDetail).GetValue(arrItineraryDetail, null);
                            if (ItenaryDetail != "")
                            {
                                ItenaryDetail = ItenaryDetail.Split('<')[1];
                                ItenaryDetail = "<" + ItenaryDetail;
                            }

                            Url = Convert.ToString(ConfigurationManager.AppSettings["AdminUrl"]) + "/HotelImagesFolder/" + List_ItineraryHotelDetail[0].nPackageID + "/" + List_ItineraryHotelDetail[0].sCategoryName + "/" + HotelImage.Replace("-", "_");
                        }

                        //  string Url = CutAdmin.DataLayer.AccountManager.GetAdminDomain() + "/HotelImagesFolder/" + "148" + "/Standard/1_0.jpg";

                        string Image = "<img width='30%'   alt='' src='" + Url + "'  />";

                        TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"Title","Days -"+(i+1) },
                         {"HotelImage", Image },
                         {"HotelName", HotelName},
                         {"HotelDescription",  HotelDescription}  ,
                         {"ItenaryDetail",ItenaryDetail},
                         };
                        html += TemplateGenrator.GetTemplate;
                    }

                    string UrlPack = Convert.ToString(ConfigurationManager.AppSettings["AdminUrl"]) + "/ImagesFolder/" + List_ItineraryDetail[0].nPackageID + "/" + List_ImageDetail[0].ImageArray.Split('^')[0].Replace("-", "_");
                    string ImagePackage = "<img width='50%'   alt='' src='" + UrlPack + "'  />";

                    string urll = Convert.ToString(ConfigurationManager.AppSettings["AdminUrl"]);

                    string URLLogo = urll + "/Agencylogo/" + dtSupplierDetails.Agentuniquecode + ".jpg";
                    string ImageLogo = "<img width='50%'   alt='' src='" + URLLogo + "'  />";

                    TemplateGenrator.GetTemplatePath(AdminSuperID, "Package_Details");
                    TemplateGenrator.arrParms = new Dictionary<string, string> {
                      {"Title", List_PackageDetail[0].sPackageName},
                      {"PackFrom", List_PackageDetail[0].dValidityFrom},
                      {"PackTo", List_PackageDetail[0].dValidityTo},
                      {"Name", Name},
                       {"TermsandConditions", List_PackageDetail[0].sTermsCondition},
                      {"Email", Email},
                      {"Remark", Remark},
                      {"Logo", ImageLogo},
                      {"PackageImage", ImagePackage},
                      {"DaysDetails", html},
                      {"PriceDetail",PriceDetailhtml},
                      {"CompanyName", dtSupplierDetails.AgencyName},
                      {"CompanyAddress",dtSupplierDetails.Address},
                      {"CompanyMail", dtSupplierDetails.email},
                      {"Description", List_PackageDetail[0].sPackageDescription}  ,
                      {"Copyright", dtSupplierDetails.AgencyName},
                    };
                    string sMail = TemplateGenrator.GetTemplate;
                    CUTUK.DataLayer.EmailManager.GenrateAttachment(sMail, List_PackageDetail[0].sPackageName.Replace(" ", ""), "PackageDetail");
                    string DocPath = urll + "/PackageDetailPdf/" + List_PackageDetail[0].sPackageName.Replace(" ", "") + "_PackageDetail.pdf";

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                    List<string> attachmentList = new List<string>();
                    attachmentList.Add(DocPath);
                    string mailbody = "Please see below attachment for details.";
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(Email, "");
                    bool reponse = true;
                    reponse = CutAdmin.DataLayer.MailManager.SendMail(accessKey, Email1List, "Package Detail", mailbody, from, attachmentList);
                    if (reponse)
                        return objSerializer.Serialize(new { Session = 1, retCode = 1 });
                    else
                        return objSerializer.Serialize(new { Session = 1, retCode = 0 });
                    //}
                }
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }
    }
}
